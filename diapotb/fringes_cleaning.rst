===========================
Performing fringes cleaning
===========================

Two scripts are available to perform fringes cleaning :
   - compute_gradient_fringes.py : This script computes a paired value (gradient_range, gradient_azimut) used to perform fringes cleaning
   - clean_fringes.py : This script performs fringes cleaning

Setting environment
-------------------

Getting pydiapotb project
~~~~~~~~~~~~~~~~~~~~~~~~~

PYDIAPOTB is a git project and the following command will provide all files :

.. code-block::

    git clone https://gitlab.orfeo-toolbox.org/remote_modules/pydiapotb.git


Getting python environment
~~~~~~~~~~~~~~~~~~~~~~~~~~

All python scripts need following libraries :

- argparse
- json
- warnings
- gdal
- scipy
- numpy

A python containing all these packages is already available and can be activated using command :

.. code-block::

    module load otb

Activated python environment is : /softs/rh7/Python/3.7.2/bin/python

compute_gradient_fringes.py
---------------------------

compute_gradient_fringes.py is a python version of c code : diapason/libradar/diapason.dir/gradient_franges.dir/gradient_franges.c

The python version uses format geotiff for input image instead of .pha format for c code.

Usage of the function
~~~~~~~~~~~~~~~~~~~~~

usage: compute_gradient_fringes.py [-h] --input_image INPUT_IMAGE [--gradient_file [GRADIENT_FILE]] [--horizontal_distance [HORIZONTAL_DISTANCE]] [--vertical_distance [VERTICAL_DISTANCE]] [--horizontal_distance_increment [HORIZONTAL_DISTANCE_INCREMENT]] [--vertical_distance_increment [VERTICAL_DISTANCE_INCREMENT]] [--horizontal_distance_auxiliaire [HORIZONTAL_DISTANCE_AUXILIAIRE]] [--vertical_distance_auxiliaire [VERTICAL_DISTANCE_AUXILIAIRE]] [--horizontal_border_size [HORIZONTAL_BORDER_SIZE]] [--vertical_border_size [VERTICAL_BORDER_SIZE]] [--horizontal_lower_bound [HORIZONTAL_LOWER_BOUND]] [--horizontal_upper_bound [HORIZONTAL_UPPER_BOUND]] [--vertical_lower_bound [VERTICAL_LOWER_BOUND]] [--vertical_upper_bound [VERTICAL_UPPER_BOUND]] [--ratio_hist_1 [RATIO_HIST_1]] [--ratio_hist_2 [RATIO_HIST_2]]

positional arguments:
  -h, --help            show this help message and exit
  --input_image INPUT_IMAGE
                        interferometry image containing one band of phase or the 3 bands (amplitude, phase and coherence)
  --gradient_file GRADIENT_FILE
                        path of file containing both gradient values || is gradient_franges.txt by default
  --horizontal_distance HORIZONTAL_DISTANCE
                        Horizontal distance for gradient computation on line || is 10 by default
  --vertical_distance VERTICAL_DISTANCE
                        Vertical distance for gradient computation on column || is 50 by default
  --horizontal_distance_increment HORIZONTAL_DISTANCE_INCREMENT
                        Horizontal distance increment || is 20 by default
  --vertical_distance_increment VERTICAL_DISTANCE_INCREMENT
                        Vertical distance increment || is 20 by default
  --horizontal_distance_auxiliaire HORIZONTAL_DISTANCE_AUXILIAIRE
                        Horizontal distance increment if histogram maximum can not be found || is 100 by default
  --vertical_distance_auxiliaire VERTICAL_DISTANCE_AUXILIAIRE
                        Vertical distance increment if histogram maximum can not be found || is 100 by default
  --horizontal_border_size HORIZONTAL_BORDER_SIZE
                        Size of horizontal border || is 50 by default
  --vertical_border_size VERTICAL_BORDER_SIZE
                        Size of vertical border || is 100 by default
  --horizontal_lower_bound HORIZONTAL_LOWER_BOUND
                        If the result is not in horizontal bound, distance is incremented to compute gradient once again || is 128 by default
  --horizontal_upper_bound HORIZONTAL_UPPER_BOUND
                        If the result is not in horizontal bound, distance is incremented to compute gradient once again || is 128 by default
  --vertical_lower_bound VERTICAL_LOWER_BOUND
                        If the result is not in vertical bound, distance is incremented to compute gradient once again || is 50 by default
  --vertical_upper_bound VERTICAL_UPPER_BOUND
                        If the result is not in vertical bound, distance is incremented to compute gradient once again || is 200 by default
  --ratio_hist_1 RATIO_HIST_1
                        Horizontal ratio used to determine if histogram is too flat || is 1.1 by default
  --ratio_hist_2 RATIO_HIST_2
                        Vertical ratio used to determine if histogram is too flat || is 1.07 by default

Examples
~~~~~~~~
This script needs at least the argument input_image.

.. code-block:: python

    python pydiapotb/diapotb/compute_gradient_fringes.py --input_image pha_14369_14703_ml33.tif

The file gradient_franges.txt is created containing :

    {"horizontal gradient": 0.00029761904761904765, "vertical gradient": -0.006875}

This script can be called using optional arguments in the following manner :

.. code-block:: python

    python pydiapotb/diapotb/compute_gradient_fringes.py --input_image pha_14369_14703_ml33.tif --gradient_file output_gradient_file_test_h20_v60.txt --horizontal_distance 20 --vertical_distance 60

The file output_gradient_file_test_h20_v60.txt is created containing :

    {"horizontal gradient": 0.00037109375, "vertical gradient": 0.0033420138888888887}

clean_fringes.py
----------------

clean_fringe.py is a python version of c code : /work/scratch/degoulr/diapason/libradar/diapason.dir/propre_lineaire.dir/propre_lineaire.c

Usage of the function
~~~~~~~~~~~~~~~~~~~~~

usage: clean_fringe.py [-h] [--input_image INPUT_IMAGE]  [--output_image OUTPUT_IMAGE] ([--gradient_range GRADIENT_RANGE] [--gradient_azimut GRADIENT_AZIMUT] | [--gradient_file GRADIENT_FILE])

positional arguments:
  -h, --help            show this help message and exit
  --input_image INPUT_IMAGE
                        image containing phase to be cleaned. Image is either one band containing phase or 3 bands containing phase on band n°2
  --output_image OUTPUT_IMAGE
                        output image which have been cleaned
  --gradient_range GRADIENT_RANGE
                        Horizontal distance for gradient computation on line
  --gradient_azimut GRADIENT_AZIMUT
                        Vertical distance for gradient computation on column
  --gradient_file GRADIENT_FILE
                        Input gradient file

Example
~~~~~~~

This script needs at least the argument input_image and either a file gradient_file containing a paired value for the gradient or both values (horizontal_distance,vertical_distance)

Here an example for the input gradient_file

.. code-block:: python

    python pydiapotb/diapotb/clean_fringes.py --input_image pha_14369_14703_ml33.tif --output_image pha_14369_14703_ml33_filtered.tif --gradient_file gradient_franges.txt

Here an example for the inputs horizontal_distance & vertical_distance

.. code-block:: python

    python pydiapotb/diapotb/clean_fringes.py --input_image pha_14369_14703_ml33.tif --output_image pha_14369_14703_ml33_filtered.tif --gradient_range 0.000298 --gradient_azimut 0.053000