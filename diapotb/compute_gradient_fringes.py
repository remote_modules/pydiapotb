import argparse
import json
import warnings

from osgeo import gdal
from scipy.ndimage import uniform_filter1d
import numpy as np


def detect_zero_edges(image: np.ndarray):
    """This function identifies all zeros around the edges of the images and return their position
    This functions returns 4 np.ndarray containing the positions of non-zeros elements :
        1 - first non-zero element by lines from the left
        2 - last non-zero element by lines from the right
        3 - first non-zero element by columns from the top
        4 - last non-zero element by columns from the bottom

    Note : Position -1 is returned is there no element different from zero on a line or on a column

    For example :
        _________
        |00010010|
        |00110000|
        |11111100|
        |00001100|
        |01111100|
        |________|

    returns the 4 arrays:
        - [3,2,0,4,1] for the position of non zeros by lines on the left
        - [6,3,5,5,5] for the position of non zeros by line on the right
        - [2,2,1,0,2,2,1,nan] for the position of non zeros by columns from the top
        - [2,0,0,0,0,0,4,nan] for the position of non zeros by columns from the bottom

    """

    nl, nc = image.shape
    padding = np.zeros((1, nl))

    # padded image is computed to add 0 at each line on the left and on the right
    # the purpose here is to be able to detect lines which have only non zero values

    padded_image = np.concatenate((padding.T, image), axis=1)
    padded_image = np.concatenate((padded_image, padding.T), axis=1)
    yy, xx = np.meshgrid(np.arange(nc + 1), np.arange(nl))

    yy = np.array(yy, dtype=int)
    xx = np.array(xx, dtype=int)

    non_zeros = np.array(padded_image != 0, dtype=int)
    changing_from_0_to_1 = np.diff(non_zeros)
    array_non_zeros_lines_left = np.nanmin(
        np.where(changing_from_0_to_1 == 1, yy, np.nan), axis=1
    )
    array_non_zeros_lines_right = np.nanmax(
        np.where(changing_from_0_to_1 == -1, yy, np.nan), axis=1
    )

    return (array_non_zeros_lines_left, array_non_zeros_lines_right - 1)


def compute_mask(size: tuple, edges: tuple, gap: int, border=None):
    if border is None:
        border = 0
    result = np.zeros(size, dtype=bool)
    line = 0
    for start, end in zip(*edges):
        if np.isnan(start) and np.isnan(end):
            pass
        elif np.isnan(start) and not np.isnan(end):
            result[line, border : int(end) - border - gap] = True
        elif not np.isnan(start) and np.isnan(end):
            result[line, int(start) + border : -border - gap] = True
        else:
            result[line, int(start) + border : int(end) - border - gap] = True
        line = line + 1

    return result


def compute_difference_by_line(data: np.ndarray, gap: int):
    return np.array(np.roll(data, -gap, axis=1) - data, dtype="B")


def compute_difference_by_column(data: np.ndarray, gap: int):
    return np.array(np.roll(data, -gap, axis=0) - data, dtype="B")


def compute_gradient_cleaning_rates(
    image: np.ndarray,
    gap1: int,
    gap2: int,
    delta_gap_1: int,
    delta_gap_2: int,
    delta_prime_gap_1: int,
    delta_prime_gap_2: int,
    border1: int,
    border2: int,
    ind1_top_border: int,
    ind1_bottom_border: int,
    ind2_top_border: int,
    ind2_bottom_border: int,
    ratio_hist1: float,
    ratio_hist2: float,
):
    pas_optimal1 = False
    pas_optimal2 = False
    iteration1 = 0
    iteration2 = 0
    pas_optimal1 = False
    pas_optimal2 = False
    positive_gradient1 = True
    positive_gradient2 = True

    histogram_too_flat1 = False
    histogram_too_flat2 = False

    max_gap1 = 200
    max_gap2 = 400

    one, two = detect_zero_edges(image)
    three, four = detect_zero_edges(image.T)

    while np.logical_not(pas_optimal1):
        iteration1 = iteration1 + 1
        mask1 = compute_mask(image.shape, (one, two), border=border1, gap=gap1)
        diff1 = compute_difference_by_line(image, gap=gap1)

        hist1, bins1 = np.histogram(
            diff1[mask1], range=(0, diff1[mask1].max()), bins=diff1[mask1].max() + 1
        )

        # Elimination of value zero
        hist1[0] = (hist1[1] + hist1[255]) / 2

        hist1 = uniform_filter1d(hist1, size=7)

        # Elimination of value zero
        hist1[0] = (hist1[1] + hist1[255]) / 2

        indmax1 = np.argmax(hist1)
        min1 = np.min(hist1)

        if iteration1 == 1:
            if hist1[indmax1] <= ratio_hist1 * min1:
                warnings.warn("Histogram too flat")
                taux1 = 0
                pas_optimal1 = True
                histogram_too_flat1 = False
            else:
                if indmax1 > 2 and indmax1 <= 127:
                    positive_gradient1 = True
                elif indmax1 < 3 or indmax1 >= 254:
                    gap1 = gap1 + delta_prime_gap_1
                    continue1 = False
                else:
                    positive_gradient1 = False

        if not histogram_too_flat1:
            if positive_gradient1:
                if indmax1 > ind1_top_border:
                    pas_optimal1 = True
                    taux1 = indmax1 / (256 * gap1)
                elif gap1 >= max_gap1:
                    pas_optimal1 = True
                    taux1 = indmax1 / (256 * gap1)
                else:
                    gap1 = gap1 + delta_gap_1
            else:
                if indmax1 < ind1_bottom_border:
                    pas_optimal1 = True
                    taux1 = -(256 - indmax1) / (256 * gap1)
                elif gap1 >= max_gap1:
                    pas_optimal1 = True
                    taux1 = -(256 - indmax1) / (256 * gap1)
                else:
                    gap1 = gap1 + delta_gap_1

    while np.logical_not(pas_optimal2):
        iteration2 = iteration2 + 1
        mask2 = compute_mask(image.T.shape, (three, four), border=100, gap=gap2)
        mask2 = mask2.T

        diff2 = compute_difference_by_column(image, gap=gap2)

        t2 = diff2[mask2]

        diff2[np.logical_not(mask2)] = 0

        hist2, bins2 = np.histogram(t2, range=(0, t2.max()), bins=t2.max() + 1)

        # Elimination of value zero
        hist2[0] = (hist2[1] + hist2[255]) / 2

        hist2 = uniform_filter1d(hist2, size=7)

        indmax2 = np.argmax(hist2)
        min2 = np.min(hist2)

        if iteration2 == 1:
            if hist2[indmax2] <= ratio_hist2 * min2:
                warnings.warn("Histogram too flat")
                taux2 = 0
                pas_optimal2 = True
                histogram_too_flat2 = True
            else:
                if indmax2 > 2 and indmax2 <= 127:
                    positive_gradient2 = True
                    taux2 = indmax2 / (256.0 * gap2)
                    gap2 = gap2 + delta_prime_gap_2
                else:
                    positive_gradient2 = False

        if not histogram_too_flat2:
            if positive_gradient2:
                if indmax2 > ind2_top_border:
                    pas_optimal2 = True
                    taux2 = indmax2 / (256 * gap2)
                elif gap2 >= max_gap2:
                    pas_optimal2 = True
                    taux2 = indmax2 / (256 * gap2)
                else:
                    gap2 = gap2 + delta_gap_2
            else:
                if indmax2 < ind2_bottom_border:
                    pas_optimal2 = True
                    taux2 = -(256 - indmax2) / (256 * gap2)
                elif gap2 >= max_gap2:
                    pas_optimal2 = True
                    taux2 = -(256 - indmax2) / (256 * gap2)
                else:
                    gap2 = gap2 + delta_gap_2
    return (taux1, taux2)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    usage = """usage: compute_gradient_fringes.py [-h] --input_image INPUT_IMAGE [--gradient_file [GRADIENT_FILE]] [--horizontal_distance [HORIZONTAL_DISTANCE]] [--vertical_distance [VERTICAL_DISTANCE]]
    [--horizontal_distance_increment [HORIZONTAL_DISTANCE_INCREMENT]] [--vertical_distance_increment [VERTICAL_DISTANCE_INCREMENT]]
    [--horizontal_distance_auxiliaire [HORIZONTAL_DISTANCE_AUXILIAIRE]] [--vertical_distance_auxiliaire [VERTICAL_DISTANCE_AUXILIAIRE]]
    [--horizontal_border_size [HORIZONTAL_BORDER_SIZE]] [--vertical_border_size [VERTICAL_BORDER_SIZE]] [--horizontal_lower_bound [HORIZONTAL_LOWER_BOUND]]
    [--horizontal_upper_bound [HORIZONTAL_UPPER_BOUND]] [--vertical_lower_bound [VERTICAL_LOWER_BOUND]] [--vertical_upper_bound [VERTICAL_UPPER_BOUND]]
    [--ratio_hist_1 [RATIO_HIST_1]] [--ratio_hist_2 [RATIO_HIST_2]]"""

    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument(
        "--input_image",
        help="interferometry image containing one band of phase or the 3 bands (amplitude, phase and coherence)",
    )

    parser.add_argument(
        "--gradient_file",
        nargs="?",
        default="gradient_franges.txt",
        help="path of file containing both gradient values || is gradient_franges.txt by default",
    )

    parser.add_argument(
        "--horizontal_distance",
        nargs="?",
        default=10,
        type=int,
        help="Horizontal distance for gradient computation on line || is 10 by default",
    )

    parser.add_argument(
        "--vertical_distance",
        nargs="?",
        default=50,
        type=int,
        help="Vertical distance for gradient computation on column || is 50 by default",
    )

    parser.add_argument(
        "--horizontal_distance_increment",
        nargs="?",
        default=20,
        type=int,
        help="Horizontal distance increment || is 20 by default",
    )

    parser.add_argument(
        "--vertical_distance_increment",
        nargs="?",
        default=20,
        type=int,
        help="Vertical distance increment || is 20 by default",
    )

    parser.add_argument(
        "--horizontal_distance_auxiliaire",
        nargs="?",
        default=100,
        type=int,
        help="Horizontal distance increment if histogram maximum can not be found || is 100 by default",
    )

    parser.add_argument(
        "--vertical_distance_auxiliaire",
        nargs="?",
        default=100,
        type=int,
        help="Vertical distance increment if histogram maximum can not be found || is 100 by default",
    )

    parser.add_argument(
        "--horizontal_border_size",
        nargs="?",
        default=50,
        type=int,
        help="Size of horizontal border || is 50 by default",
    )

    parser.add_argument(
        "--vertical_border_size",
        nargs="?",
        default=100,
        type=int,
        help="Size of vertical border || is 100 by default",
    )

    parser.add_argument(
        "--horizontal_lower_bound",
        nargs="?",
        default=128,
        type=int,
        help="If the result is not in horizontal bound, distance is incremented to compute gradient once again || is 128 by default",
    )

    parser.add_argument(
        "--horizontal_upper_bound",
        nargs="?",
        default=128,
        type=int,
        help="If the result is not in horizontal bound, distance is incremented to compute gradient once again || is 128 by default",
    )

    parser.add_argument(
        "--vertical_lower_bound",
        nargs="?",
        default=50,
        type=int,
        help="If the result is not in vertical bound, distance is incremented to compute gradient once again || is 50 by default",
    )

    parser.add_argument(
        "--vertical_upper_bound",
        nargs="?",
        default=200,
        type=int,
        help="If the result is not in vertical bound, distance is incremented to compute gradient once again || is 200 by default",
    )

    parser.add_argument(
        "--ratio_hist_1",
        nargs="?",
        default=1.1,
        type=float,
        help="Horizontal ratio used to determine if histogram is too flat || is 1.1 by default",
    )

    parser.add_argument(
        "--ratio_hist_2",
        nargs="?",
        default=1.07,
        type=float,
        help="Vertical ratio used to determine if histogram is too flat || is 1.07 by default",
    )

    args = parser.parse_args()

    raster = gdal.Open(args.input_image)

    nb_frames = raster.RasterCount

    if nb_frames == 1:
        data = raster.GetRasterBand(1).ReadAsArray()
    elif nb_frames == 3:
        data = raster.GetRasterBand(2).ReadAsArray()
    else:
        raise ValueError(
            "Image must have either 1 band (phase) or 3 bands (amplitude,phase,coherence)"
        )

    taux1, taux2 = compute_gradient_cleaning_rates(
        data,
        args.horizontal_distance,
        args.vertical_distance,
        args.horizontal_distance_increment,
        args.vertical_distance_increment,
        args.horizontal_distance_auxiliaire,
        args.vertical_distance_auxiliaire,
        args.horizontal_border_size,
        args.vertical_border_size,
        args.horizontal_lower_bound,
        args.horizontal_upper_bound,
        args.vertical_lower_bound,
        args.vertical_upper_bound,
        args.ratio_hist_1,
        args.ratio_hist_2,
    )

    with open(args.gradient_file, "w") as output_file:
        result = {}
        result["horizontal gradient"] = taux1
        result["vertical gradient"] = taux2

        json_object = json.dumps(result)
        output_file.write(json_object)
