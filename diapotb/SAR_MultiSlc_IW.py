#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

import os
import argparse
import tempfile
import logging
import shutil
import re
import glob

from diapotb.lib.core.ApplicationWrapper import OTBApplicationWrapper
from diapotb.ConfigFile import ConfigFile
from diapotb.lib.core.DiapOTBEnums import ChainNames, ChainModes, ScriptNames, Sensor
from diapotb.lib.DInSAR import (
    DInSarParamS1IW,
    DInSarOutputKeysOthers,
    DInSarOutputKeysS1IW,
)
from diapotb.lib.PreProcessing import PreProcessingParamS1IW, PreProcessingOutputKeys
from diapotb.lib.PostProcessing import PostProcessingParamS1IW, PostProcessingOutputKeys
from diapotb.lib.Ground import GroundParamS1IW

from diapotb.lib.DiapOTBProcessingFactory import DiapOTBProcessingFactory

import diapotb.lib.core.Utils as utils


def prepare_output(output_dir):
    """Prepare the global output directory to store the current processing (each couple)"""
    if not os.path.exists(output_dir):
        print(
            "The output directory " + output_dir + " does not exist and will be created"
        )
        os.makedirs(output_dir)
    else:
        print(
            "The output directory "
            + output_dir
            + " exists. Some files can be overwritten"
        )


def prepare_burst_dir(output_dir, burst_list, esd_nbiter):
    """Prepare burst directory to store the current processing (in reference and secondary directories)"""
    # Maybe to change
    for burst_id in burst_list:
        burst_dir = os.path.join(output_dir, "burst" + str(burst_id))

        if not os.path.exists(burst_dir):
            os.makedirs(burst_dir)

            if esd_nbiter > 0:
                os.makedirs(os.path.join(burst_dir, "esd"))


def rename_file(output_dir, filename_old, filename_new, only_meta=False):
    """Rename some files"""

    if only_meta:
        available_ext = [".geom"]
    else:
        available_ext = [".tif", ".geom"]

    for ext in available_ext:
        if os.path.exists(os.path.join(output_dir, filename_old + ext)):
            os.rename(
                os.path.join(output_dir, filename_old + ext),
                os.path.join(output_dir, filename_new + ext),
            )


def clean_tmp_dir(tmp_dir):
    """Clean tmp directory"""
    # Check if directory exist
    if not os.path.exists(tmp_dir):
        return 1

    # Check if tmp_dir is has a tmp prefix
    if os.path.commonprefix([tmp_dir, tempfile.gettempdir()]) == os.path.commonprefix(
        [tempfile.gettempdir()]
    ):
        shutil.rmtree(tmp_dir)

    return 0


def clean_output_dir(output_dir, all_files, files_to_keep):
    """Clean output directory to keep only file_to_keep"""
    # Define possible extensions
    output_ext = [".tif*", ".geom", ".txt"]

    # Keep only specified files
    for file_out in all_files:
        if file_out not in files_to_keep:
            file_abs = os.path.abspath(file_out)

            if os.path.exists(file_abs):
                # Select file to remove following :
                # the absolute path and output extensions in DiapOTB
                for ext in output_ext:
                    pattern_file = file_abs.split(".")[0] + ext

                    for f in glob.glob(pattern_file):
                        if os.path.exists(f):
                            os.remove(f)

    # Scan output directory and remove (if empty), burst/esd directories
    for subdir in glob.glob(os.path.join(output_dir, "burst*")):
        if os.path.exists(os.path.join(subdir, "esd")) and not any(
            os.scandir(os.path.join(subdir, "esd"))
        ):
            shutil.rmtree(os.path.join(subdir, "esd"))
        if not any(os.scandir(subdir)):
            shutil.rmtree(subdir)


if __name__ == "__main__":

    ### First part : get argument and read the configuration file ###
    # Check arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "configfile", help="input conguration file for the application DiapOTB"
    )
    args = parser.parse_args()

    # Check and load configuration file
    config_handler = ConfigFile(args.configfile, str(ScriptNames.MULTI_SLC_S1IW))
    config_handler.load_configfile()

    # Prepare output and logger
    output_dir = config_handler.get_output_path()

    prepare_output(output_dir)

    utils.init_logger()
    utils.init_filelog(output_dir)

    # Retrieve parameters from configuration file
    param_dict = config_handler.create_param_dict_from_config_file(
        DInSarParamS1IW, str(ChainNames.DINSAR)
    )

    param_dict_pre = config_handler.create_param_dict_from_config_file(
        PreProcessingParamS1IW, str(ChainNames.PRE_PROCESSING)
    )

    # Append param dict with doppler_file
    param_dict_pre[str(PreProcessingParamS1IW.DOPFILE)] = (
        config_handler.get_doppler_file()
    )

    param_dict_post = config_handler.create_param_dict_from_config_file(
        PostProcessingParamS1IW, str(ChainNames.POST_PROCESSING)
    )

    param_dict_ground = config_handler.create_param_dict_from_config_file(
        GroundParamS1IW, str(ChainNames.GROUND)
    )

    ### Part 2 : some check and get all secondaries
    # Get and check main inputs : reference name + secondary path + date + dem paht and eof_path (if present)
    reference_image = config_handler.get_master_image()

    srtm_shapefile = config_handler.get_srtm_sfile()
    hgts_path = config_handler.get_srtm_path()
    geoid_path = config_handler.get_geoid()
    input_path = config_handler.get_input_path()
    start_date = config_handler.get_start_date()
    end_date = config_handler.get_end_date()

    clean_tag = config_handler.get_clean()

    # Get and configure RAM parameter to launch each OTB applications
    ram_param = config_handler.get_ram()
    # Configure our OTBWrapper with the defined RAM
    OTBApplicationWrapper.configure_ram(ram_param)

    all_files_ref = []
    files_ref_to_keep = []

    reference_path, pol, reference_ext, reference_date, dict_kwl_reference = (
        utils.get_image_data(
            reference_image, input_path, str(ScriptNames.MULTI_SLC_S1IW)
        )
    )

    reference_dir = os.path.dirname(reference_path)

    eof_path = config_handler.get_eof()

    exclude = "-9999"
    # Get all secondary images by selection (with dates), look at utils functions
    # + Handle the case, we have no secondary image
    tiff_list, throw_warning = utils.get_all_tiff(
        pol=pol, iw=reference_image.split("-")[1], search_dir=input_path
    )
    tiff_date_list = utils.get_tiff_with_dates(
        start_date, end_date, exclude, tiff_list, reference_ext
    )
    tiff_date_list = utils.avoid_duplicates(tiff_date_list)

    if len(tiff_date_list) - 1 <= 0:
        utils.log(
            logging.CRITICAL,
            "ERROR : None secondary images found, please check your input path and your selection (dates, exclude ...)"
            + "\n",
        )
        quit()

    if throw_warning:
        # Indicate reference and all secondary images
        utils.print_on_std("Reference and Secondary images : ")
        utils.print_on_std(tiff_date_list)
    # Check on reference
    utils.check_image_format(reference_path)

    # Get satellite
    satellite = utils.get_sensor_from_md(dict_kwl_reference)

    # Prepare global and reference (for all burst)
    # ====== Create global folder with starting and ending dates + reference date
    output_glob = "{}/output_{}_to_{}_m_{}".format(
        output_dir, start_date, end_date, reference_date
    )
    output_ref_dir = output_glob + "/{}_master_directory".format(reference_date)
    prepare_output(output_ref_dir)

    # Adapt geom files with eof files, only reference, first
    # change output_dir to output_ref
    reference_name = utils.apply_eof_path_on_orbit(
        satellite, eof_path, reference_path, dict_kwl_reference, output_ref_dir
    )
    files_ref_to_keep.append(reference_name)
    # Create DEM (with utils function and dem_path)
    # ====== Check if images exist
    utils.check_if_exist(srtm_shapefile)
    utils.check_if_exist(hgts_path)
    utils.check_if_exist(reference_path)

    dem = utils.build_virutal_raster(
        reference_path, srtm_shapefile, hgts_path, output_glob
    )

    # Init Processing Factory with S1_IW mode
    # Do reference processsing (only once) for all burst : aka PreProcessing + Groud (with ref_dir as output_dir)
    # Create our factory to build all processing following the current mode
    chain_factory = DiapOTBProcessingFactory(mode=ChainModes.S1_IW)

    utils.print_on_std("\n Beginning of DiapOTB processing (S1 IW mode) \n")

    utils.print_on_std("\n Processing on reference image : " + reference_image + "\n")

    #### Part 3 : processing on reference image
    utils.print_on_std("\n Pre_Processing on reference image \n")

    burst_index_list = config_handler.get_burst_list()
    if not utils.check_burst_index_with_number(burst_index_list, dict_kwl_reference):
        utils.print_on_std(
            "Burst index are not consistent. \
        Please check your burst_index parameter (following the number of burst)"
        )
        quit()

    param_dict_pre["burst_ids"] = burst_index_list
    param_dict_pre["burst_to_extract"] = burst_index_list
    # If tmp not in output dir then
    tmp_in_ouput_dir = config_handler.get_tmp_status()

    ref_dir = output_ref_dir
    if not tmp_in_ouput_dir:
        ref_dir = tempfile.mkdtemp()
    prepare_burst_dir(
        ref_dir, param_dict_pre["burst_ids"], param_dict[str(DInSarParamS1IW.ESD_ITER)]
    )

    pre_procesing_chain_reference = chain_factory.create_processing(
        str(ChainNames.PRE_PROCESSING),
        image=reference_name,
        image_dir=reference_dir,
        param=param_dict_pre,
        output_dir=ref_dir,
    )
    pre_procesing_chain_reference.execute()

    # All files created in the preprocessing reference chain
    all_files_ref.extend(pre_procesing_chain_reference.get_all_files_list())

    utils.print_on_std("\n Ground projection on reference image \n")

    param_dict_ground["burst_ids"] = burst_index_list

    ground_chain_reference = chain_factory.create_processing(
        str(ChainNames.GROUND),
        image=reference_name,
        image_dir=reference_dir,
        param=param_dict_ground,
        output_dir=ref_dir,
    )

    ground_chain_reference.append_inputs(pre_procesing_chain_reference.get_outputs())

    ground_chain_reference.execute(dem=dem)

    # Additionnal processing for reference : concatenate deramp burst + ml
    # These files are not put to all_files on purpose (to keep it at the end)
    reference_deramp_whole = (
        utils.get_slcml_namming_from_productname(reference_image, mode=str(Sensor.S1IW))
        + ".tif"
    )
    reference_deramp_list = pre_procesing_chain_reference.get_outputs()[
        str(PreProcessingOutputKeys.DERAMP)
    ]

    app_concatenate_ref = OTBApplicationWrapper("SARConcatenateBursts")
    app_concatenate_ref.set_input_images(
        insar=os.path.join(reference_dir, reference_name), il=reference_deramp_list
    )
    app_concatenate_ref.set_parameters(burstindex=param_dict_pre["burst_ids"][0])
    app_concatenate_ref.set_output_images(
        out=os.path.join(output_ref_dir, reference_deramp_whole)
    )
    app_concatenate_ref.execute_app(in_memory=False)

    reference_ml_whole = (
        reference_deramp_whole.split(".tif")[0]
        + "_ml"
        + str(param_dict_pre[str(PreProcessingParamS1IW.MLAZI)])
        + str(param_dict_pre[str(PreProcessingParamS1IW.MLRAN)])
        + ".tif"
    )

    app_ml_ref = OTBApplicationWrapper("SARMultiLook")
    app_ml_ref.set_parameters(
        mlran=param_dict_pre[str(PreProcessingParamS1IW.MLRAN)],
        mlazi=param_dict_pre[str(PreProcessingParamS1IW.MLAZI)],
        mlgain=param_dict_pre[str(PreProcessingParamS1IW.MLGAIN)],
    )
    app_ml_ref.set_input_images(
        incomplex=os.path.join(output_ref_dir, reference_deramp_whole)
    )
    app_ml_ref.set_output_images(out=os.path.join(output_ref_dir, reference_ml_whole))
    app_ml_ref.execute_app(in_memory=False)

    # All files created in the ground reference chain
    all_files_ref.extend(ground_chain_reference.get_all_files_list())

    #### Part 4 : Loop on secondaries image
    for secondary_file in (i for i in tiff_date_list if i != reference_image):

        # Prepare secondary_output
        secondary_cur_date = utils.get_date(secondary_file, reference_ext)
        output_secondary_cur_dir = output_glob + "/{}_m_{}_s".format(
            reference_date, secondary_cur_date
        )
        prepare_output(output_secondary_cur_dir)

        secondary_path, _, _, _, dict_kwl_secondary = utils.get_image_data(
            secondary_file, input_path, str(ScriptNames.MULTI_SLC_S1IW)
        )
        secondary_dir = os.path.dirname(secondary_path)

        # Set tmp dir
        tmp_secondary_cur_dir = None
        if not tmp_in_ouput_dir:
            tmp_secondary_cur_dir = tempfile.mkdtemp()

        # Init empty list
        files_secondary_to_keep = []
        all_files = []

        # Check on secondary
        utils.check_image_format(secondary_path)

        # Burst validy
        # Get burst list from configuration file (for reference image)
        # reference and secondary images may rarely have different burst match
        # for instance,  burst 0 in reference image can match with burst 2 in secondary instead of id 0
        # That's why a burst to process can be different than burst ids
        burst_list = config_handler.get_burst_list()
        valid_burst_reference, valid_burst_secondary = utils.select_burst(
            dict_kwl_reference, dict_kwl_secondary, min(burst_list), max(burst_list)
        )

        if not utils.check_burst_index_with_number(
            burst_index_list, dict_kwl_secondary
        ):
            utils.print_on_std(
                "Burst index are not consistent for secondary image : " + secondary_file
            )
            # Jump to the next iteration
            continue

        # Add burst id and burst to process to all param
        # "burst_ids" is always equal to reference burst
        # "burst_to_process" is equal to reference burst except for pre-processing chain (burst extraction) for secondary
        param_dict_pre_secondary = param_dict_pre.copy()
        param_dict_pre_secondary["burst_ids"] = valid_burst_reference
        param_dict_pre_secondary["burst_to_extract"] = valid_burst_secondary
        param_dict_ground["burst_ids"] = valid_burst_reference
        param_dict["burst_ids"] = valid_burst_reference

        # Prepare output (sec) directoriy for each burst
        if tmp_in_ouput_dir:
            prepare_burst_dir(
                output_secondary_cur_dir,
                valid_burst_reference,
                param_dict[str(DInSarParamS1IW.ESD_ITER)],
            )
        else:
            prepare_burst_dir(
                tmp_secondary_cur_dir,
                valid_burst_reference,
                param_dict[str(DInSarParamS1IW.ESD_ITER)],
            )

        # eof path and geom for sec
        secondary_name = utils.apply_eof_path_on_orbit(
            satellite,
            eof_path,
            secondary_path,
            dict_kwl_secondary,
            output_secondary_cur_dir,
        )

        ### Processing ###
        utils.print_on_std(
            "\n Processing on secondary image : " + secondary_file + "\n"
        )

        ### Main chain : Prepro/Ground sec + DInSar + PostProcessing
        utils.print_on_std("\n Pre_Processing on secondary image \n")

        # Robustify the loop : if one of secondary file failed, continue and log the error
        # For each execute function : exception_tolerance decorator is used with a return code as output.
        # If return code = 1 => an exception was raised => continue and process the next secondary image

        secondary_cur_dir = output_secondary_cur_dir
        if not tmp_in_ouput_dir:
            secondary_cur_dir = tmp_secondary_cur_dir
        pre_procesing_chain_secondary = chain_factory.create_processing(
            str(ChainNames.PRE_PROCESSING),
            image=secondary_name,
            image_dir=secondary_dir,
            param=param_dict_pre_secondary,
            output_dir=secondary_cur_dir,
        )

        pre_processing_execute = utils.exception_tolerance(
            pre_procesing_chain_secondary.execute
        )
        code_pre_proc = pre_processing_execute()
        if code_pre_proc == 1:
            utils.print_on_std(
                "\n Exception in Pre_Processing on secondary image: {} \n".format(
                    secondary_file
                )
            )
            continue

        utils.print_on_std("\n Ground projection on secondary image \n")

        # # Change cartesian estimation to False
        param_dict_ground[str(GroundParamS1IW.CARTESIAN_ESTIMATION)] = False
        ground_chain_secondary = chain_factory.create_processing(
            str(ChainNames.GROUND),
            image=secondary_name,
            image_dir=secondary_dir,
            param=param_dict_ground,
            output_dir=secondary_cur_dir,
        )

        ground_chain_secondary.append_inputs(
            pre_procesing_chain_secondary.get_outputs()
        )

        ground_chain_execute = utils.exception_tolerance(ground_chain_secondary.execute)
        code_ground = ground_chain_execute(dem=dem)
        if code_ground == 1:
            utils.print_on_std(
                "\n Exception in Ground on secondary image : {} \n".format(
                    secondary_file
                )
            )
            continue

        utils.print_on_std("\n DINSAR Processing \n")
        dinsar_chain = chain_factory.create_processing(
            str(ChainNames.DINSAR),
            secondary_image=secondary_name,
            secondary_dir=secondary_dir,
            reference_image=reference_name,
            reference_dir=reference_dir,
            param=param_dict,
            output_dir=output_secondary_cur_dir,
        )

        dinsar_chain.append_inputs_reference(
            pre_procesing_chain_reference.get_outputs()
        )
        dinsar_chain.append_inputs_secondary(
            pre_procesing_chain_secondary.get_outputs()
        )
        # Set tmp path, if required
        if not tmp_in_ouput_dir:
            dinsar_chain.set_tmp_path(tmp_secondary_cur_dir)
        dinsar_chain.append_inputs_reference(ground_chain_reference.get_outputs())
        dinsar_chain.append_inputs_secondary(ground_chain_secondary.get_outputs())

        dinsar_chain_execute = utils.exception_tolerance(dinsar_chain.execute)
        code_dinsar = dinsar_chain_execute(dem=dem)
        if code_dinsar == 1:
            utils.print_on_std(
                "\n Exception in DInSar on secondary image : {} \n".format(
                    secondary_file
                )
            )
            continue

        # Additionnal processing for reference : concatenate reramp burst + ml
        # These files are not put to all_files on purpose (to keep it at the end)
        secondary_core_reramp_whole = (
            utils.get_slcml_namming_from_productname(
                secondary_file, mode=str(Sensor.S1IW)
            )
            + ".tif"
        )
        secondary_core_reramp_list = dinsar_chain.get_outputs()[
            str(DInSarOutputKeysS1IW.COREGISTRATED_SECONDARY_RERAMP)
        ]

        app_concatenate_sec = OTBApplicationWrapper("SARConcatenateBursts")
        app_concatenate_sec.set_input_images(
            insar=os.path.join(reference_dir, reference_name),
            il=secondary_core_reramp_list,
        )
        app_concatenate_sec.set_parameters(
            burstindex=param_dict[str(DInSarParamS1IW.BURSTIDS)][0]
        )
        app_concatenate_sec.set_output_images(
            out=os.path.join(output_secondary_cur_dir, secondary_core_reramp_whole)
        )
        app_concatenate_sec.execute_app(in_memory=False)

        secondary_ml_whole = (
            secondary_core_reramp_whole.split(".tif")[0]
            + "_ml"
            + str(param_dict_pre[str(PreProcessingParamS1IW.MLAZI)])
            + str(param_dict_pre[str(PreProcessingParamS1IW.MLRAN)])
            + ".tif"
        )

        app_ml_sec = OTBApplicationWrapper("SARMultiLook")
        app_ml_sec.set_parameters(
            mlran=param_dict_pre[str(PreProcessingParamS1IW.MLRAN)],
            mlazi=param_dict_pre[str(PreProcessingParamS1IW.MLAZI)],
            mlgain=param_dict_pre[str(PreProcessingParamS1IW.MLGAIN)],
        )
        app_ml_sec.set_input_images(
            incomplex=os.path.join(
                output_secondary_cur_dir, secondary_core_reramp_whole
            )
        )
        app_ml_sec.set_output_images(
            out=os.path.join(output_secondary_cur_dir, secondary_ml_whole)
        )
        app_ml_sec.execute_app(in_memory=False)

        utils.print_on_std("\n Post_Processing \n")

        # # TODO : Add Ortho processing (in PostProcessing or external chain ???)
        post_processing_chain = chain_factory.create_processing(
            str(ChainNames.POST_PROCESSING),
            secondary_image=secondary_name,
            secondary_dir=secondary_dir,
            reference_image=reference_name,
            reference_dir=reference_dir,
            param=param_dict_post,
            output_dir=output_secondary_cur_dir,
        )

        post_processing_chain.append_inputs_reference(
            ground_chain_reference.get_outputs()
        )
        post_processing_chain.append_inputs(dinsar_chain.get_outputs())

        post_processing_chain.geoid_path = geoid_path

        post_processing_execute = utils.exception_tolerance(
            post_processing_chain.execute
        )
        code_post_processing = post_processing_execute(dem_path=dem)
        if code_post_processing == 1:
            utils.print_on_std(
                "\n Exception in PostProcessing on secondary image : {} \n".format(
                    secondary_file
                )
            )
            continue

        # Cleaning
        # Retrieve all generated files and specify which one, we want to keep
        interf_key = str(DInSarOutputKeysOthers.INTERFERO)
        filtered_key = str(PostProcessingOutputKeys.FILT_INTERFERO)
        ortho_key = str(PostProcessingOutputKeys.ORTHO_INTERFERO)

        # All files created by chains
        all_files.extend(pre_procesing_chain_secondary.get_all_files_list())
        all_files.extend(ground_chain_secondary.get_all_files_list())
        all_files.extend(dinsar_chain.get_all_files_list())
        all_files.extend(post_processing_chain.get_all_files_list())
        # Keep only interferogram, filtered_interferogram and ortho
        if post_processing_chain.get_outputs()[filtered_key]:
            files_secondary_to_keep.append(
                post_processing_chain.get_outputs()[filtered_key]
            )
        files_secondary_to_keep.append(dinsar_chain.get_outputs()[interf_key])
        if post_processing_chain.get_outputs()[ortho_key]:
            files_secondary_to_keep.append(
                post_processing_chain.get_outputs()[ortho_key]
            )

        all_files.append(
            os.path.join(output_secondary_cur_dir, config_handler.get_doppler_file())
        )
        # remove tmp files for secondary image
        if clean_tag:
            if not tmp_in_ouput_dir:
                clean_tmp_dir(tmp_secondary_cur_dir)

            clean_output_dir(
                output_secondary_cur_dir, all_files, files_secondary_to_keep
            )

        # Rename output interferograms
        interfero_base = utils.get_interfnamming_from_productname(
            reference_image, secondary_file, str(Sensor.S1IW)
        )
        interfo_filt = interfero_base + "_Filtred-Interferogram"
        interfo = interfero_base + "_Interferogram"
        interfo_ortho = interfero_base + "_Ortho-Interferogram"

        if utils.str2bool(
            param_dict_post[str(PostProcessingParamS1IW.ACTIVATE_FILTERING)]
        ):
            rename_file(
                output_secondary_cur_dir,
                os.path.basename(
                    post_processing_chain.get_outputs()[filtered_key]
                ).split(".")[0],
                interfo_filt,
            )

        # Extract only the useful bands (1 to 3) for interferogram and ortho
        utils.extract_band123(
            dinsar_chain.get_outputs()[interf_key],
            os.path.join(output_secondary_cur_dir, interfo + ".tif"),
        )
        rename_file(
            output_secondary_cur_dir,
            os.path.basename(dinsar_chain.get_outputs()[interf_key]).split(".")[0],
            interfo,
            only_meta=True,
        )

        if os.path.exists(dinsar_chain.get_outputs()[interf_key]):
            os.remove(dinsar_chain.get_outputs()[interf_key])

        if utils.str2bool(param_dict_post[str(PostProcessingParamS1IW.ACTIVATE_ORTHO)]):
            utils.extract_band123(
                post_processing_chain.get_outputs()[ortho_key],
                os.path.join(output_secondary_cur_dir, interfo_ortho + ".tif"),
            )

            rename_file(
                output_secondary_cur_dir,
                os.path.basename(post_processing_chain.get_outputs()[ortho_key]).split(
                    "."
                )[0],
                interfo_ortho,
                only_meta=True,
            )

            if os.path.exists(post_processing_chain.get_outputs()[ortho_key]):
                os.remove(post_processing_chain.get_outputs()[ortho_key])

    # After the loop : remove tmp files for reference
    all_files_ref.append(os.path.join(ref_dir, config_handler.get_doppler_file()))

    if clean_tag:
        if not tmp_in_ouput_dir:
            clean_tmp_dir(ref_dir)

        clean_output_dir(output_ref_dir, all_files_ref, files_ref_to_keep)

    utils.print_on_std("\n End of SAR_MultiSlc processing (S1 IW mode) \n")
