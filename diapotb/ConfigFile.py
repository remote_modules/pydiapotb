#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================
"""
    ConfigFile
    ==========

    Handle, check the input configuration file

"""
# pylint: disable=too-few-public-methods
# Specific enums

import json
import os

from diapotb.lib.core.DiapOTBEnums import ExtendedEnum, ScriptNames, ChainNames
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException
from diapotb.lib.core.Utils import (
    logger,
    check_if_exist,
    check_if_dir,
    str2bool,
    check_roi_format,
)
from jsonschema import validate


class MainTag(ExtendedEnum):
    """Specify the main tag for json file"""

    GLOBAL = "Global"
    PREPROCESSING = "Pre_Processing"
    DINSAR = "DIn_SAR"
    POSTPROCESSING = "Post_Processing"


class MiddleTag(ExtendedEnum):
    """Specify the main tag for json file"""

    INPUT = "in"
    OUTPUT = "out"
    PARAMETER = "parameter"
    SENSOR = "sensor"


class ConfigParamPattern(ExtendedEnum):
    """Pattern found in json file for parameters"""

    RAM = "optram"
    ROI = "roi"


class ConfigParamPatternMultiSlc(ExtendedEnum):
    """Pattern found in json file for parameters for SARMultiSlc chains

    Enums' names are not random here, they must be fit with processing name to find a match
    Enums' values depend on configuration file itself (json schema)
    """

    MLRAN = "ML_ran"
    MLAZI = "ML_azi"
    MLGAIN = "ML_gain"
    DOPFILE = "doppler_file"
    GRID_STEP_RAN = "GridStep_range"
    GRID_STEP_AZI = "GridStep_azimut"
    GRID_THRESHOLD = "Grid_Threshold"
    GRID_GAP = "Grid_Gap"
    INTERF_GAIN = "Interferogram_gain"
    ML_INTERFRAN = "Interferogram_mlran"
    ML_INTERFAZI = "Interferogram_mlazi"
    ACTIVATE_ORTHO = "Activate_Ortho"
    SPACING_XY = "Spacingxy"
    ACTIVATE_FILTERING = "Activate_Filtering"
    ML_FILT_RAN = "Filtered_Interferogram_mlran"
    ML_FILT_AZI = "Filtered_Interferogram_mlazi"
    ACTIVATE_ITS_PROCESSING = "Activate_Its_Processing"
    AMPLITUDE_MIN = "RGB_Interferogram_amplitude_min"
    AMPLITUDE_MAX = "RGB_Interferogram_amplitude_max"
    COHERENCE_MIN = "RGB_Interferogram_coherence_min"
    COHERENCE_MAX = "RGB_Interferogram_coherence_max"
    FILT_ALPHA = "Filtered_parameter_alpha"
    ESD_ITER = "ESD_iter"
    BURSTINDEX = "burst_index"
    ACTIVATE_FRINGES_CLEANING = "Activate_Fringes_Cleaning"
    HORIZONTAL_DISTANCE = "Horizontal_Distance"
    VERTICAL_DISTANCE = "Vertical_Distance"
    HORIZONTAL_DISTANCE_INCREMENT = "Horizontal_Distance_Increment"
    VERTICAL_DISTANCE_INCREMENT = "Vertical_Distance_Increment"
    HORIZONTAL_DISTANCE_AUXILIAIRE = "Horizontal_Distance_Auxiliaire"
    VERTICAL_DISTANCE_AUXILIAIRE = "Vertical_Distance_Auxiliaire"
    HORIZONTAL_BORDER_SIZE = "Horizontal_Border_Size"
    VERTICAL_BORDER_SIZE = "Vertical_Border_Size"
    HORIZONTAL_LOWER_BOUND = "Horizontal_Lower_Bound"
    HORIZONTAL_UPPER_BOUND = "Horizontal_Upper_Bound"
    VERTICAL_LOWER_BOUND = "Vertical_Lower_Bound"
    VERTICAL_UPPER_BOUND = "Vertical_Upper_Bound"
    RATIO_HIST_1 = "Ratio_Hist_1"
    RATIO_HIST_2 = "Ratio_Hist_2"


class ConfigParamPatternDiapOTB(ExtendedEnum):
    """Pattern found in json file for parameters for diapOTB chains

    Enums' names are not random here, they must be fit with processing name to find a match
    Enums' values depend on configuration file itself (json schema)
    """

    MLRAN = "ML_range"
    MLAZI = "ML_azimut"
    MLGAIN = "ML_gain"
    DOPFILE = "doppler_file"
    GRID_STEP_RAN = "GridStep_range"
    GRID_STEP_AZI = "GridStep_azimut"
    GRID_THRESHOLD = "Grid_Threshold"
    GRID_GAP = "Grid_Gap"
    INTERF_GAIN = "Interferogram_gain"
    ML_INTERFRAN = "Interferogram_mlran"
    ML_INTERFAZI = "Interferogram_mlazi"
    ACTIVATE_ORTHO = "Activate_Ortho"
    SPACING_XY = "Spacingxy"
    ACTIVATE_FILTERING = "Activate_Filtering"
    ML_FILT_RAN = "Filtered_Interferogram_mlran"
    ML_FILT_AZI = "Filtered_Interferogram_mlazi"
    ACTIVATE_ITS_PROCESSING = "Activate_Its_Processing"
    AMPLITUDE_MIN = "RGB_Interferogram_amplitude_min"
    AMPLITUDE_MAX = "RGB_Interferogram_amplitude_max"
    COHERENCE_MIN = "RGB_Interferogram_coherence_min"
    COHERENCE_MAX = "RGB_Interferogram_coherence_max"
    FILT_ALPHA = "Filtered_parameter_alpha"
    ESD_ITER = "ESD_iter"
    BURSTINDEX = "burst_index"
    ACTIVATE_FRINGES_CLEANING = "Activate_Fringes_Cleaning"
    HORIZONTAL_DISTANCE = "Horizontal_Distance"
    VERTICAL_DISTANCE = "Vertical_Distance"
    HORIZONTAL_DISTANCE_INCREMENT = "Horizontal_Distance_Increment"
    VERTICAL_DISTANCE_INCREMENT = "Vertical_Distance_Increment"
    HORIZONTAL_DISTANCE_AUXILIAIRE = "Horizontal_Distance_Auxiliaire"
    VERTICAL_DISTANCE_AUXILIAIRE = "Vertical_Distance_Auxiliaire"
    HORIZONTAL_BORDER_SIZE = "Horizontal_Border_Size"
    VERTICAL_BORDER_SIZE = "Vertical_Border_Size"
    HORIZONTAL_LOWER_BOUND = "Horizontal_Lower_Bound"
    HORIZONTAL_UPPER_BOUND = "Horizontal_Upper_Bound"
    VERTICAL_LOWER_BOUND = "Vertical_Lower_Bound"
    VERTICAL_UPPER_BOUND = "Vertical_Upper_Bound"
    RATIO_HIST_1 = "Ratio_Hist_1"
    RATIO_HIST_2 = "Ratio_Hist_2"


class ConfigParamAdapter:
    """Adapter to match Config file (json) to processing enums

    To find a match a comparison is made between enums' names
    """

    def __init__(self, script_name):
        if script_name in ScriptNames.list():

            self._param_enum = ConfigParamPatternDiapOTB

            if script_name in [
                str(ScriptNames.MULTI_SLC_S1SM),
                str(ScriptNames.MULTI_SLC_S1IW),
            ]:
                self._param_enum = ConfigParamPatternMultiSlc

    def match_config_index_with_processing(self, processing_key):
        """Match Processing keys with config file"""
        # Check and match with name
        config_key = None

        # Scan configuration key to find a match
        for config_key_id in self._param_enum:
            # Compares enums' names
            if processing_key.name == config_key_id.name:
                config_key = config_key_id

        return config_key


class ConfigInputPattern(ExtendedEnum):
    """Pattern found in json file for global inputs"""

    MASTER = "Master_Image_Path"
    MASTER_IMAGE = "Master_Image"
    SLAVE = "Slave_Image_Path"
    DEM = "DEM_Path"
    EOF = "EOF_Path"
    GEOID = "Geoid"
    START_DATE = "Start_Date"
    END_DATE = "End_Date"
    INPUT_PATH = "Input_Path"
    SRTM_PATH = "SRTM_Path"
    SRTM_SHAPEFILE = "SRTM_Shapefile"
    OUTPUTDIR = "output_dir"
    OUTPUT_PATH = "Output_Path"


# pylint: enable=too-few-public-methods


class ConfigFile:
    """Class to handle configuration file and prepare inputs/parameters for processing"""

    @classmethod
    def create_configfile_from_ex(cls, script_name):
        """Factory to build a confiHandler for json examples"""
        if script_name not in ScriptNames.list():
            raise DiapOTBProcessing(
                "Unkwown script to retrieve configuration file elements"
            )

        # Retrieve example following the current script :
        # diapotb.py or diapot_S1IW.py or SARMulti_SLC*S1IW.py
        ex_json = "ex_config_diapOTB_Cosmo.json"
        if script_name == str(ScriptNames.SIMPLE_S1IW):
            ex_json = "ex_config_diapOTB_S1IW.json"
        elif script_name == str(ScriptNames.MULTI_SLC_S1SM):
            ex_json = "ex_config_MultiSlc_CosmoS1SM.json"
        elif script_name == str(ScriptNames.MULTI_SLC_S1IW):
            ex_json = "ex_config_MultiSlc_IW.json"

        # Load example (relative path)
        current_dir = os.path.dirname(os.path.abspath(__file__))
        ex_path = os.path.join(current_dir, "./share/ex_config/")

        if os.path.exists(ex_path):
            ex_file = os.path.join(ex_path, ex_json)

            # Build a ConfigHandler from example
            config_handler = ConfigFile(ex_file, script_name)
        else:
            raise DiapOTBException("example file does not exist")

        return config_handler

    def __init__(self, config_file, script_name):
        self._config_file = config_file

        if script_name not in ScriptNames.list():
            raise DiapOTBProcessing(
                "Unkwown script to retrieve configuration file elements"
            )

        # Init a adapter to convert config file keys to processing keys (for enums)
        self._adapter = ConfigParamAdapter(script_name)

        self._script_name = script_name
        self._data_config = {}

    def get_data_config(self):
        """Getter on data_config (used in generateConfigFile)"""
        return self._data_config

    def override_data_config(self, **kwargs):
        """Override data_config following kwarg arguments"""
        for key, val in kwargs.items():
            if key in ConfigInputPattern.list():
                if key not in [
                    str(ConfigInputPattern.OUTPUT_PATH),
                    str(ConfigInputPattern.OUTPUTDIR),
                ]:
                    middle_key = str(MiddleTag.INPUT)
                else:
                    middle_key = str(MiddleTag.OUTPUT)

                self._data_config[str(MainTag.GLOBAL)][middle_key][key] = val

            else:
                print("Unkwown key to override : " + key)

    def _validate_with_schema(self, schema):
        """
        Compare input with JSON schame the configuration file
        """
        try:
            validate(self._data_config, schema)
        except Exception as valid_err:
            print("Invalid JSON: {}".format(valid_err))
            return False
        else:
            # Realise votre travail
            print("Valid JSON")
            return True

    def _validate_json(self):
        """Check json content following a schema"""
        # Retrieve the schame following the current script :
        # diapotb.py or diapot_S1IW.py or SARMulti_SLC*S1IW.py
        schema_json = "schema_S1SM.json"
        if self._script_name == str(ScriptNames.SIMPLE_S1IW):
            schema_json = "schema_S1IW.json"
        elif self._script_name == str(ScriptNames.MULTI_SLC_S1SM):
            schema_json = "schema_MultiSlc.json"
        elif self._script_name == str(ScriptNames.MULTI_SLC_S1IW):
            schema_json = "schema_MultiSlc_IW.json"

        # Load schema (relative path)
        current_dir = os.path.dirname(os.path.abspath(__file__))
        schema_path = os.path.join(current_dir, "./share/json_schemas/")

        if os.path.exists(schema_path):
            schema_file = os.path.join(schema_path, schema_json)

            try:
                with open(schema_file, "r") as sch:
                    data_schema = json.load(sch)
            except Exception as err:
                logger.critical(
                    "Impossible to read or load JSON configuration schema : {name}. Check its path and content, exception is {err}".format(
                        name=self._config_file, err=err
                    )
                )
                quit()

            # Check Json file
            json_is_valid = self._validate_with_schema(data_schema)

            if not json_is_valid:
                logger.critical(
                    "Error, the input config file does not fulfill requirements"
                )
                quit()

    def load_configfile(self):
        """
        Read and Load the configuration file (check this file according to a schmema)
        """
        # Read and Load the configuration file
        try:
            with open(self._config_file, "r") as f:
                self._data_config = json.load(f)

        except Exception as err:
            logger.critical(
                "Impossible to read or load JSON configuration file : {name}."
                "Check its path and content. For your information, exception is \n : {err}".format(
                    name=self._config_file, err=err
                )
            )
            quit()

        self._validate_json()

    def create_param_dict_from_config_file(self, param_enum, chain_name):
        """Build a parameter dictionary for a processing chain from configuratio file"""
        # Checks argument
        if not issubclass(param_enum, ExtendedEnum):
            logger.critical("Wrong input enum to build the parameter dictionary")

        if chain_name not in ChainNames.list():
            logger.critical("Wrong input enum to build the parameter dictionary")

        # empty dict
        param_dict = {}

        # Try to retrieve all parameters from the input configuration file
        for param in param_enum:
            # Find corresponding key in config file thanks to our adapter
            conf_key = self._adapter.match_config_index_with_processing(param)

            # If a key was found in config file
            if conf_key:
                # first try on the chainName part
                try:
                    param_dict[str(param)] = self._data_config[chain_name][
                        str(MiddleTag.PARAMETER)
                    ][str(conf_key)]
                except KeyError:
                    # Additionnal tries on others chains
                    for config_part in MainTag:
                        if str(config_part) != chain_name:
                            try:
                                param_dict[str(param)] = self._data_config[
                                    str(config_part)
                                ][str(MiddleTag.PARAMETER)][str(conf_key)]
                            except KeyError:
                                continue

        return param_dict

    # Getters
    def get_ram(self):
        """Getter on RAM parameter (to run each OTB applications)"""
        try:
            # Get defined RAM (by user)
            ram_param = self._data_config[str(MainTag.GLOBAL)][
                str(MiddleTag.PARAMETER)
            ][str(ConfigParamPattern.RAM)]

            # Add a protection to avoid a too high consumption
            if ram_param > 4000:
                ram_param = 4000
        except KeyError:
            # optram not present in json file => return None
            ram_param = None

        return ram_param

    def get_reference_image(self):
        """Getter on reference image"""
        reference_image = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)][
            "Master_Image_Path"
        ]
        if not check_if_exist(reference_image):
            raise DiapOTBException(
                "Reference image : %s does not exist" % reference_image
            )

        return reference_image

    def get_secondary_image(self):
        """Getter on secondary image"""
        secondary_image = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)][
            "Slave_Image_Path"
        ]
        if not check_if_exist(secondary_image):
            raise DiapOTBException(
                "Secondary image : %s does not exist" % secondary_image
            )

        return secondary_image

    def get_dem(self):
        """Getter on dem"""
        dem = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)]["DEM_Path"]
        if not check_if_exist(dem):
            raise DiapOTBException("dem : %s does not exist" % dem)

        return dem

    def get_eof(self):
        """Getter on EOF Path"""
        eof_path = ""
        try:
            eof_path = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)][
                "EOF_Path"
            ]

            if not check_if_dir(eof_path):
                raise DiapOTBException("EOF_PATH : %s does not exist" % eof_path)

        except KeyError:
            logger.info("EOF PATH does not exist for this configuration file")

        return eof_path

    def get_output_dir(self):
        """Getter on output directory"""
        return self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.OUTPUT)][
            "output_dir"
        ]

    def get_doppler_file(self):
        """Getter on doppler file name"""
        return self._data_config[str(MainTag.PREPROCESSING)][str(MiddleTag.OUTPUT)][
            "doppler_file"
        ]

    # getter for multislc
    def get_output_path(self):
        """Getter on output directory"""
        return self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.OUTPUT)][
            "Output_Path"
        ]

    def get_geoid(self):
        """Getter on geoid"""
        geoid = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)]["Geoid"]
        if not check_if_exist(geoid):
            raise DiapOTBException("geoid : %s does not exist" % geoid)

        return geoid

    def get_start_date(self):
        """Getter on start_date"""
        start_date = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)][
            "Start_Date"
        ]

        return int(start_date)

    def get_end_date(self):
        """Getter on end_date"""
        end_date = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)][
            "End_Date"
        ]

        return int(end_date)

    def get_tmp_status(self):
        """Getter on tmpdir_into_outputdir"""
        try:
            tmp_status = self._data_config[str(MainTag.GLOBAL)][
                str(MiddleTag.PARAMETER)
            ]["tmpdir_into_outputdir"]
        except KeyError:
            tmp_status = "true"

        return str2bool(tmp_status)

    def get_master_image(self):
        """Getter on master image basename"""
        master_image = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)][
            "Master_Image"
        ]

        return master_image

    def get_srtm_sfile(self):
        """Getter on srtm shape file"""
        srtm_sfile = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)][
            "SRTM_Shapefile"
        ]
        if not check_if_exist(srtm_sfile):
            raise DiapOTBException("srtm_sfile : %s does not exist" % srtm_sfile)

        return srtm_sfile

    def get_srtm_path(self):
        """Getter on srtm path"""
        srtm_path = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)][
            "SRTM_Path"
        ]
        if not check_if_exist(srtm_path):
            raise DiapOTBException("srtm_path : %s does not exist" % srtm_path)

        return srtm_path

    def get_input_path(self):
        """Getter on input path"""
        input_path = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.INPUT)][
            "Input_Path"
        ]
        if not check_if_exist(input_path):
            raise DiapOTBException("input_path : %s does not exist" % input_path)

        return input_path

    def get_burst_list(self):
        """Getter on burst list (from configuration file)"""
        # Default 0 to 8 (usally 9 bursts in S1IW image)
        burst_list = [int(i) for i in range(0, 9)]
        try:
            burst_index = self._data_config[str(MainTag.GLOBAL)][
                str(MiddleTag.PARAMETER)
            ]["burst_index"]
            burst_list = [int(i) for i in burst_index.split("-")]

        except KeyError:
            logger.warning("burst_index does not exist for this configuration file")

        # Check burst id
        # Nb burst max : 15 => index max = 14
        if min(burst_list) < 0 or max(burst_list) > 14:
            raise DiapOTBException("burst index are not consistent for S1IW processing")

        burst_list = range(min(burst_list), max(burst_list) + 1)

        return burst_list

    def get_clean(self):
        """Getter on clean tag"""
        try:
            clean = self._data_config[str(MainTag.GLOBAL)][str(MiddleTag.PARAMETER)][
                "clean"
            ]
        except KeyError:
            clean = "false"

        return str2bool(clean)

    def get_roi(self):
        """Getter on roi area for ortho"""
        try:
            # Get defined ROI
            roi = self._data_config[str(MainTag.DINSAR)][str(MiddleTag.PARAMETER)][
                str(ConfigParamPattern.ROI)
            ]

            # Check format
            is_ok = check_roi_format(roi)

            if not is_ok:
                roi = ""

        except KeyError:
            roi = ""

        return roi
