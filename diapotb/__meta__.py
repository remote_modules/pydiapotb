#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Holds project meta-information for setup.py
"""

__title__ = "DiapOTB"
__description__ = "."
__version__ = "1.1.0"
__author__ = "Gaëlle Usseglio"
__author_email__ = "gaelle.usseglio@cnes.fr"
__url__ = "https://gitlab.orfeo-toolbox.org/remote_modules/diapotb"
__license__ = "Apache 2.0"
__copyright__ = "2017-2021, CNES"
