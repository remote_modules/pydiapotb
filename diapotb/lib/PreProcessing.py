#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
Pre-processing chain
"""
import os

from diapotb.lib.core.DiapOTBProcessing import (
    DiapOTBProcessingSingleImage,
    ParamHandler,
    FilenamesHandler,
    ExecutorSingleImage,
)
from diapotb.lib.core.DiapOTBEnums import (
    ExtendedEnum,
    ChainModes,
    FilenamesEnum,
    extend_enum,
    ExtPosition,
    RequiredKeysForSingleImageProcessing,
    DefaultEnum,
)
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException

from diapotb.lib.core.ApplicationWrapper import OTBApplicationWrapper

from diapotb.lib.core import Utils as utils

from diapotb.lib.internal_processing.resampling import main_resampling


# pylint: disable=too-few-public-methods
# Specific enums
class PreProcessingParamOthers(ExtendedEnum):
    """Define each required parameters for PreProcessing S1SM_CSK chain"""

    MLAZI = "ML_azi"
    MLRAN = "ML_ran"
    MLGAIN = "ML_gain"
    DOPFILE = "doppler_file"


@extend_enum(PreProcessingParamOthers)
class PreProcessingParamS1IW(ExtendedEnum):
    """Define each required parameters for PreProcessing S1IW chain"""

    BURSTIDS = "burst_ids"
    BURSTEXTRACT = "burst_to_extract"


@extend_enum(PreProcessingParamOthers)
class PreProcessingParamTSX(ExtendedEnum):
    """Define each required parameters for PreProcessing TSX/PAZ/TDX chain"""

    ACTIVATE_DERAMP = "with_deramp"


class PreProcessingParamDefaultValueTSX(DefaultEnum):
    """Define some default values or redirect to other values
    The following paramaters are optional (other are mandatory)
    """

    ACTIVATE_DERAMP = (str(PreProcessingParamTSX.ACTIVATE_DERAMP), True)


class PreProcessingFilenamesS1IW(FilenamesEnum):
    """Define key for intermediate/output filenames for PreProcessing S1IW chain
    3 str to speficy, a key, the extension to add and the position of the extension in order to create the file
    """

    BURSTEXTRACT = ("burst_extract_files", "", str(ExtPosition.SUFIX))
    DERAMP = ("deramp_files", "_deramp", str(ExtPosition.SUFIX))
    ML = ("ml_files", "_ml", str(ExtPosition.SUFIX))


class PreProcessingFilenamesTSX(FilenamesEnum):
    """Define key for intermediate/output filenames for PreProcessing TSX chain
    3 str to speficy, a key, the extension to add and the position of the extension in order to create the file
    """

    RESAMPLED = ("resampled_deramp_files", "", str(ExtPosition.SUFIX))
    DERAMP = ("deramp_files", "_deramp", str(ExtPosition.SUFIX))
    ML = ("ml_files", "_ml", str(ExtPosition.SUFIX))


class PreProcessingFilenamesOthers(FilenamesEnum):
    """Define key for intermediate/output filenames for PreProcessing Others chain
    3 str to speficy, a key, the extension to add and the position of the extension in order to create the file
    """

    ML = ("ml_files", "_ml", str(ExtPosition.SUFIX))


class PreProcessingOutputKeys(ExtendedEnum):
    """Define output keys for PreProcessing chain"""

    DOP0 = "doppler_0_list"
    BURSTS = "burst_list"
    DERAMP = "deramped_burst_list"
    RESAMPLE = "resampled_list"
    ML = "ml_list"


# pylint: enable=too-few-public-methods


# PreProcessing class
class PreProcessing(DiapOTBProcessingSingleImage):
    """Use the module to launch Pre_Processing chain.

    main function : execute
    """

    def __init__(self, **kwargs):
        # Base constructor to init required elts such as image/dir, output_dir and parameters
        super().__init__(**kwargs)

        # Init the specific arguments for the PreProcessing chain
        self._name = "Pre Processing"
        self._applications = [
            "SARDoppler0",
            "SARMultiLook",
            "SARDeramp",
            "SARBurstExtraction",
        ]

        # Default mode : Others
        self._mode = str(ChainModes.OTHERS)
        if "mode" in kwargs and str(kwargs["mode"]) in ChainModes.list():
            self._mode = str(kwargs["mode"])

        # Init Handlers according to the mode
        param_enum = PreProcessingParamOthers
        file_enum = PreProcessingFilenamesOthers

        if self._mode == str(ChainModes.S1_IW):
            param_enum = PreProcessingParamS1IW
            file_enum = PreProcessingFilenamesS1IW

        elif self._mode == str(ChainModes.TSX):
            # param_enum same than OTHERS
            file_enum = PreProcessingFilenamesTSX

            param_enum = PreProcessingParamTSX
            default_param_enum = PreProcessingParamDefaultValueTSX

            self.param_handler = ParamHandler(
                param_enum, self._param, default_param_enum
            )

        if self._mode != str(ChainModes.TSX):
            self.param_handler = ParamHandler(param_enum, self._param)
        self.file_handler = FilenamesHandler(file_enum, self._image_base, self._mode)

        # Get from parameter dictionary each argument
        self.param_handler.check_param()

    def retrieve_output(self, key):
        """Retrieve a given output of the PreProcessing chain"""
        if (
            not isinstance(key, PreProcessingOutputKeys)
            and key not in PreProcessingOutputKeys.list()
        ):
            raise DiapOTBException(
                "The current key is not a available "
                "output key for PreProcessing chain"
            )

        return self._dict_outputs[str(key)]

    # Process functions
    def execute(self, **kwargs):
        """PreProcessing chain

        Execute pre-processing following current mode (from ChainModes) thanks to an Executor.
        Executor may be ExecutorPreProcessingOthers,ExecutorPreProcessingTSX or ExecutorPreProcessingS1IW.

        :param kwargs: parameters as kwargs (used to provide additionnal input to the selected executor)
        :type kwargs: dict-like
        """
        # Add for each mode, a dedicated executor
        if str(self._mode) in [str(ChainModes.OTHERS), str(ChainModes.TSX)]:
            self._input_enum = None
        else:
            self._input_enum = None

        self._executor_builder.add_mode(ChainModes.OTHERS, ExecutorPreProcessingOthers)

        self._executor_builder.add_mode(ChainModes.TSX, ExecutorPreProcessingTSX)

        self._executor_builder.add_mode(ChainModes.S1_IW, ExecutorPreProcessingS1IW)

        super().execute(**kwargs)


# Executors, one per mode
class ExecutorPreProcessingOthers(ExecutorSingleImage):
    """Execute processing for pre porcessing chain mode S1SM-CSK"""

    def execute(self):
        """PreProcessing chain for S1SM and CSK sensors

        Two applications are called here : SARDoppler0 and SARMultiLook on the single input image.

        These applications have the self._image as input and put outputs in self._output_dir
        """
        # retrieve input : image and output_dir
        output_dir = self._inputs[str(RequiredKeysForSingleImageProcessing.OUTPUT_DIR)]

        # retrive parameters
        ml_azimut = self.param_handler.get_param(str(PreProcessingParamOthers.MLAZI))
        ml_range = self.param_handler.get_param(str(PreProcessingParamOthers.MLRAN))
        ml_gain = self.param_handler.get_param(str(PreProcessingParamOthers.MLGAIN))
        dop_file = self.param_handler.get_param(str(PreProcessingParamOthers.DOPFILE))

        self.file_handler.create_intermediate_names(
            ml_azimut=ml_azimut, ml_range=ml_range
        )

        # SARDoppler0
        app_doppler0 = OTBApplicationWrapper("SARDoppler0")
        app_doppler0.set_input_images(insar=self.image_path())
        app_doppler0.set_output_images(outfile=os.path.join(output_dir, dop_file))
        app_doppler0.execute_app(in_memory=False)

        # SARMultiLook
        # pylint: disable=no-member
        ml_file = self.file_handler.get_filename(
            PreProcessingFilenamesOthers.ML.get_key()
        )
        # pylint: enable=no-member
        ml_output = os.path.join(output_dir, ml_file)
        app_multilook = OTBApplicationWrapper("SARMultiLook")
        app_multilook.set_parameters(mlran=ml_range, mlazi=ml_azimut, mlgain=ml_gain)
        app_multilook.set_input_images(incomplex=self.image_path())
        app_multilook.set_output_images(out=ml_output)
        app_multilook.execute_app(in_memory=False)

        # Assign the outputs
        self._outputs[str(PreProcessingOutputKeys.DOP0)] = (
            app_doppler0.get_output_float_parameter("doppler0")
        )
        self._outputs[str(PreProcessingOutputKeys.ML)] = ml_output

        # Extend tmp_list wiht ml files
        self._all_files_list.append(ml_output)


class ExecutorPreProcessingTSX(ExecutorSingleImage):
    """Execute processing for pre porcessing chain mode TSX"""

    def execute(self):
        """PreProcessing chain for TSX

        Three applications are called here : SARDoppler0, SARMultiLook and SARDeramp on the single input image.

        These applications have the self._image as input and put outputs in self._output_dir.

        A resampling (from internal_processing package) may also be applied on image
        """
        # retrieve input : image and output_dir
        output_dir = self._inputs[str(RequiredKeysForSingleImageProcessing.OUTPUT_DIR)]

        # retrive parameters (TSX param are the same than Other => same enum)
        ml_azimut = self.param_handler.get_param(str(PreProcessingParamOthers.MLAZI))
        ml_range = self.param_handler.get_param(str(PreProcessingParamOthers.MLRAN))
        ml_gain = self.param_handler.get_param(str(PreProcessingParamOthers.MLGAIN))
        dop_file = self.param_handler.get_param(str(PreProcessingParamOthers.DOPFILE))

        self.file_handler.create_intermediate_names(
            ml_azimut=ml_azimut, ml_range=ml_range
        )

        # SARDeramp
        # pylint: disable=no-member
        deramp_file = self.file_handler.get_filename(
            PreProcessingFilenamesTSX.DERAMP.get_key()
        )
        # pylint: enable=no-member

        # default deramp output set as input
        deramp_output_path = self.image_path()

        if self.param_handler.get_param(str(PreProcessingParamTSX.ACTIVATE_DERAMP)):
            deramp_output_path = os.path.join(output_dir, deramp_file)

            app_deramp = OTBApplicationWrapper("SARDeramp")
            app_deramp.set_input_images(in_=self.image_path())
            app_deramp.set_output_images(out=deramp_output_path)
            app_deramp.execute_app(in_memory=False)
        else:
            utils.logger.info("Deramping is disabled (mode : ST)")

        # SARDoppler0
        app_doppler0 = OTBApplicationWrapper("SARDoppler0")
        app_doppler0.set_input_images(insar=deramp_output_path)
        app_doppler0.set_output_images(outfile=os.path.join(output_dir, dop_file))
        app_doppler0.execute_app(in_memory=False)

        # Assign resample path to its input
        resample_output_path = deramp_output_path
        # Check if kwl are present (check if a resampling on secondary image is required following frequencies)
        # kwl must be present only for secondary. If reference inputs do not contain kwl keys => KeyError exception
        try:
            # pylint: disable=no-member
            resample_deramp_file = self.file_handler.get_filename(
                PreProcessingFilenamesTSX.RESAMPLED.get_key()
            )
            # pylint: enable=no-member
            resample_deramp_output_path = os.path.join(output_dir, resample_deramp_file)

            # Resampling may use tmp to improve performance
            tmp_dir = ""
            if self._tmp_path:
                tmp_dir = self._tmp_path

            resample_output_path = main_resampling(
                deramp_output_path,
                resample_deramp_output_path,
                self._inputs["kwl_ref"],
                self._inputs["kwl_sec"],
                app_doppler0.get_output_float_parameter("doppler0"),
                output_dir,
                tmp_dir,
            )

            # If a resampling was applied, only
            if resample_output_path != deramp_output_path:
                utils.logger.info(
                    "A resampling was applied on : %s", deramp_output_path
                )
                # ReCreate Doppler0
                del app_doppler0
                app_doppler0 = OTBApplicationWrapper("SARDoppler0")
                app_doppler0.set_input_images(insar=resample_output_path)
                app_doppler0.set_output_images(
                    outfile=os.path.join(output_dir, dop_file)
                )
                app_doppler0.execute_app(in_memory=False)
            else:
                utils.logger.info("No resampling on : %s", deramp_output_path)

        except KeyError:
            pass

        # SARMultiLook
        # pylint: disable=no-member
        ml_file = self.file_handler.get_filename(PreProcessingFilenamesTSX.ML.get_key())
        # pylint: enable=no-member
        ml_output = os.path.join(output_dir, ml_file)
        app_multilook = OTBApplicationWrapper("SARMultiLook")
        app_multilook.set_parameters(mlran=ml_range, mlazi=ml_azimut, mlgain=ml_gain)
        app_multilook.set_input_images(incomplex=resample_output_path)
        app_multilook.set_output_images(out=ml_output)
        app_multilook.execute_app(in_memory=False)

        # Assign the outputs
        self._outputs[str(PreProcessingOutputKeys.DOP0)] = (
            app_doppler0.get_output_float_parameter("doppler0")
        )
        self._outputs[str(PreProcessingOutputKeys.ML)] = ml_output
        self._outputs[str(PreProcessingOutputKeys.DERAMP)] = deramp_output_path
        self._outputs[str(PreProcessingOutputKeys.RESAMPLE)] = resample_output_path

        # Extend tmp_list wiht ml files
        self._all_files_list.append(ml_output)
        if deramp_output_path != self.image_path():
            self._all_files_list.append(deramp_output_path)

        if resample_output_path != deramp_output_path:
            self._all_files_list.append(resample_output_path)


class ExecutorPreProcessingS1IW(ExecutorSingleImage):
    """Execute processing for pre porcessing chain mode S1IW"""

    def execute_one_burst(
        self, image, image_dir, output_dir, burst_id_in, burst_id_out
    ):

        # retrive parameters
        ml_azimut = self.param_handler.get_param(str(PreProcessingParamOthers.MLAZI))
        ml_range = self.param_handler.get_param(str(PreProcessingParamOthers.MLRAN))
        ml_gain = self.param_handler.get_param(str(PreProcessingParamOthers.MLGAIN))
        dop_file = self.param_handler.get_param(str(PreProcessingParamOthers.DOPFILE))

        # Output directory for the current burst
        burst_dir = os.path.join(output_dir, "burst" + str(burst_id_out))

        # Intermediate names for the current burst
        self.file_handler.create_intermediate_names(
            ml_azimut=ml_azimut, ml_range=ml_range, burst_id=burst_id_out
        )

        # SARBurstExtraction
        # pylint: disable=no-member
        burst_file = self.file_handler.get_filename(
            PreProcessingFilenamesS1IW.BURSTEXTRACT.get_key()
        )
        # pylint: enable=no-member
        burst_output_path = os.path.join(burst_dir, burst_file)

        app_burst_extract = OTBApplicationWrapper("SARBurstExtraction")
        app_burst_extract.set_parameters(burstindex=burst_id_in, allpixels="true")
        app_burst_extract.set_input_images(in_=os.path.join(image_dir, image))
        app_burst_extract.set_output_images(out=burst_output_path)
        app_burst_extract.execute_app(in_memory=False)

        # SARDeramp on extracted burst
        # pylint: disable=no-member
        deramp_file = self.file_handler.get_filename(
            PreProcessingFilenamesS1IW.DERAMP.get_key()
        )
        # pylint: enable=no-member
        deramp_output_path = os.path.join(burst_dir, deramp_file)

        app_deramp = OTBApplicationWrapper("SARDeramp")
        app_deramp.set_input_images(in_=burst_output_path)
        app_deramp.set_output_images(out=deramp_output_path)
        app_deramp.execute_app(in_memory=False)

        # SARDoppler0 to extimate doppler 0 on deramped burst
        app_doppler0 = OTBApplicationWrapper("SARDoppler0")
        app_doppler0.set_input_images(insar=deramp_output_path)
        app_doppler0.set_output_images(outfile=os.path.join(output_dir, dop_file))
        app_doppler0.execute_app(in_memory=False)

        # SARMultiLook on deramped burst
        # pylint: disable=no-member
        ml_file = self.file_handler.get_filename(
            PreProcessingFilenamesS1IW.ML.get_key()
        )
        # pylint: enable=no-member
        ml_output_path = os.path.join(burst_dir, ml_file)
        app_multilook = OTBApplicationWrapper("SARMultiLook")
        app_multilook.set_parameters(mlran=ml_range, mlazi=ml_azimut, mlgain=ml_gain)
        app_multilook.set_input_images(incomplex=deramp_output_path)
        app_multilook.set_output_images(out=ml_output_path)
        app_multilook.execute_app(in_memory=False)

        # Return outputs as tuple
        return (
            app_doppler0.get_output_float_parameter("doppler0"),
            burst_output_path,
            deramp_output_path,
            ml_output_path,
        )

    def execute(self):
        """PreProcessing chain for S1IW sensor

        Four applications are called here : SARBurstExtraction, SARDeramp, SARDoppler0 and SARMultiLook
        These applications processed each burst provided in param dictionary with burst_ids as key

        NB : A particular case can be handle with a different burst_ids and burst_to_process.
        The case rarely occurs and deals with a gap between reference/secondary burst match.
        """
        # retrieve input : image and output_dir
        image_dir = self._inputs[str(RequiredKeysForSingleImageProcessing.DIR)]
        image_in = self._inputs[str(RequiredKeysForSingleImageProcessing.IMAGE)]
        output_dir = self._inputs[str(RequiredKeysForSingleImageProcessing.OUTPUT_DIR)]

        # Retrieve the specific parameters :
        # burst_ids : id of the burst
        # burst_to_process : burst to extract and then to process
        # (can be different for secondary image if burst ids betwwen reference and secondary image do not match)

        burst_ids_list = self.param_handler.get_param(
            str(PreProcessingParamS1IW.BURSTIDS)
        )
        burst_ids_to_extract_list = self.param_handler.get_param(
            str(PreProcessingParamS1IW.BURSTEXTRACT)
        )

        # Create an empty lists
        dop0_list = []
        bursts_list = []
        deramp_list = []
        ml_list = []

        # loop on burst
        for id_loop in range(0, len(burst_ids_list)):
            burst_id_in = burst_ids_to_extract_list[id_loop]
            burst_id_out = burst_ids_list[id_loop]

            # Process the current burst
            dop0, bursts, deramp, ml = self.execute_one_burst(
                image_in, image_dir, output_dir, burst_id_in, burst_id_out
            )
            dop0_list.append(dop0)
            bursts_list.append(bursts)
            deramp_list.append(deramp)
            ml_list.append(ml)

        # Assign outptus
        self._outputs[str(PreProcessingOutputKeys.DOP0)] = dop0_list
        self._outputs[str(PreProcessingOutputKeys.BURSTS)] = bursts_list
        self._outputs[str(PreProcessingOutputKeys.DERAMP)] = deramp_list
        self._outputs[str(PreProcessingOutputKeys.ML)] = ml_list
        # Extend tmp_list
        self._all_files_list.extend(bursts_list)
        self._all_files_list.extend(deramp_list)
        self._all_files_list.extend(ml_list)
