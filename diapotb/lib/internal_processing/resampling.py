#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

import os
from copy import deepcopy

from diapotb.lib.internal_processing.core.resampling_dimensions import (
    ResamplingDimension,
)
from diapotb.lib.internal_processing.core.processing_dimensions import Dimension
import diapotb.lib.internal_processing.core.utils as utils

from diapotb.lib.core.DiapOTBExceptions import DiapOTBException
from diapotb.lib.core.Utils import MetadataKeyInGeom

from diapotb.lib.core.Utils import logger


def retrieve_frequencies(kwl_ref, kwl_sec, dimension=Dimension.AZIMUT):
    """Retrieve f_in and f_out from geom file(s)"""
    # Select the metadata key to estimate frequency following dimension
    metadata_key = str(MetadataKeyInGeom.FREQ_SAMPLING)
    if dimension == Dimension.AZIMUT:
        metadata_key = str(MetadataKeyInGeom.LINE_INTERVAL)

    # Estimate f_in and f_out
    # Get f_in frequency from reference and f_out from input (secondary)
    # Define f_in/f_out
    if dimension == Dimension.AZIMUT:
        # Time to freq
        f_in = 1 / float(kwl_sec[metadata_key])
        f_out = 1 / float(kwl_ref[metadata_key])
    else:
        f_in = float(kwl_sec[metadata_key])
        f_out = float(kwl_ref[metadata_key])

    return f_in, f_out


def update_and_create_output_geom(
    geom_content_in, output_file, resampler, dimension=Dimension.AZIMUT
):
    """Update some values and write on disk the output geom file"""
    # Copy geom_content
    geom_content_out = deepcopy(geom_content_in)

    # Update values according to current processed dimension
    if dimension == Dimension.AZIMUT:
        # Update PRF, line_time_interval and nb_lines
        geom_content_out[str(MetadataKeyInGeom.NUMBER_LINE)] = resampler.nb_line_out
        # PRF may be very different than line_time_interval (TSX/PAZ/TDX products)
        # geom_content_out[str(MetadataKeyInGeom.PRF)] = resampler.f_out
        geom_content_out[str(MetadataKeyInGeom.LINE_INTERVAL)] = 1 / resampler.f_out
    else:
        # Update sampling_rate and nb_lines
        geom_content_out[str(MetadataKeyInGeom.NUMBER_COL)] = resampler.nb_col_out
        geom_content_out[str(MetadataKeyInGeom.FREQ_SAMPLING)] = resampler.f_out

    # Create path and write geom
    geom_file = os.path.join(
        os.path.dirname(output_file),
        os.path.basename(output_file).split(".")[0] + ".geom",
    )

    utils.write_geom(geom_content_out, geom_file)


def resampling_on_dimension(
    input_file, output_file, kwl_ref, kwl_sec, f0, dimension=Dimension.AZIMUT
):
    """Call resampling for the given Dimension"""
    # Retrieve geom file
    geom_file = os.path.join(
        os.path.dirname(input_file),
        os.path.basename(input_file).split(".")[0] + ".geom",
    )

    if not os.path.isfile(geom_file):
        # Check extension (if cos => native image without geom)
        # For ST mode, it is possible to resample input cos
        ext = os.path.basename(input_file).split(".")[1]
        if ext == "cos":
            # Build the .geom thanks to ReadImageInfo
            geom_content = utils.read_geom_from_raw_product(input_file)
        else:
            raise DiapOTBException("Input geom does not exist. Please check your path.")
    else:
        # Read the associated geom file
        geom_content = utils.read_geom(geom_file)

    # Get f_in and f_out from kwl
    f_in, f_out = retrieve_frequencies(kwl_ref, kwl_sec, dimension)

    # Define a block size for reading/resampling/writing
    # Could be 512, 1024 or 2048 folowwing gdal_cachemax
    block_size = utils.define_block_size_from_gdal_cache_max()

    # Instanciate a ResamplingDimension
    resampler = ResamplingDimension(dimension, block_size, input_file, output_file)

    # Assign f_in and f_out
    resampler.f_in = f_in
    resampler.f_out = f_out
    # f0 x prf : Do not get PRF directly for TSX (confusion in TSX metadata between RSF and PRF)
    # Use 1/line_time_interval, instead
    resampler.f0 = f0 * (1 / float(kwl_ref[str(MetadataKeyInGeom.LINE_INTERVAL)]))

    do_resampling = resampler.do_resampling(
        int(kwl_sec[str(MetadataKeyInGeom.NUMBER_LINE)]),
        int(kwl_sec[str(MetadataKeyInGeom.NUMBER_COL)]),
    )

    if do_resampling:
        logger.info("Resampling on %s for dimension %s", input_file, str(dimension))

        # Do the resampling
        resampler.read_process_write_all_blocks()

        # Create the ouptut geom file
        # Do not use kwl_sec directly because of date format.
        # The format from ReadImageInfo (kwl_sec) is different (remove the :) and triggers exceptions due to invalid SAR Sensor Model.
        update_and_create_output_geom(geom_content, output_file, resampler, dimension)

        del resampler
        return True
    else:
        logger.info(
            "No need to do a resampling on %s for dimension %s, input/output dimensions are equal",
            input_file,
            str(dimension),
        )
        del resampler
        return False


def main_resampling(
    input_file, output_file, kwl_ref, kwl_sec, dop0, output_dir, tmp_dir=""
):
    """Main function to apply resampling on two dimensions, if needed
    (if line_interval or Freq_sampling are different)

    :param input_file: path to input image
    :type input_file: str
    :param output_file: path to output image
    :type output_file: str
    :param kwl_ref: reference metadata as dictionary
    :type kwl_ref: dict
    :param kwl_sec: secondary metadata as dictionary
    :type kwl_sec: dict
    :param dop0: doppler 0 (for azimut dimension, 0 otherwise)
    :type kwl_sec: float
    :param output_dir: output directory
    :type output_dir: str
    :param tmp_dir: tmp directory (empty, by default)
    :type tmp_dir: str

    :return: path to output file (resampled file)
    :rtype: str
    """
    # Init output_file
    output_file_resampled = input_file
    output_tmp_dir = output_dir
    input_tmp_file = input_file
    # Check if tmp_dir has to be used to process (improve performance on some platforms)
    if tmp_dir and os.path.exists(tmp_dir):
        # Copy input_file in tmp_dir
        utils.prepare_tmp_input(input_file, tmp_dir)
        # Change output_dir and input_file to use tmp_dir
        output_tmp_dir = tmp_dir
        input_tmp_file = os.path.join(tmp_dir, os.path.basename(input_file))

    # Resampling on two dimensions:
    # First on Range and then on azimut with resampled image as input (if resampling was necessary on range)
    # dop0 is used on azimut to set f0 (central frequency). On range, f0 is 0
    # Create output file from input
    output_file_ran = os.path.join(
        output_tmp_dir,
        os.path.basename(input_tmp_file).split(".")[0] + "_resamp_range.tif",
    )

    # 0 to estimate f0
    was_resampled_on_range = resampling_on_dimension(
        input_tmp_file, output_file_ran, kwl_ref, kwl_sec, 0, Dimension.RANGE
    )

    if was_resampled_on_range:
        output_file_azi = os.path.join(
            output_tmp_dir,
            os.path.basename(output_file_ran).split(".")[0] + "_resamp_azimut.tif",
        )

        # dop0 to estimate f0
        was_resampled_on_azimut = resampling_on_dimension(
            output_file_ran, output_file_azi, kwl_ref, kwl_sec, dop0, Dimension.AZIMUT
        )
    else:
        output_file_azi = os.path.join(
            output_tmp_dir,
            os.path.basename(input_tmp_file).split(".")[0] + "_resamp_azimut.tif",
        )

        # dop0 to estimate f0
        was_resampled_on_azimut = resampling_on_dimension(
            input_tmp_file, output_file_azi, kwl_ref, kwl_sec, dop0, Dimension.AZIMUT
        )

    if was_resampled_on_azimut:
        output_file_resampled = output_file_azi
    else:
        if was_resampled_on_range:
            output_file_resampled = output_file_ran

    # Check if tmp_dir was used
    if tmp_dir and os.path.exists(tmp_dir):
        # If a resampling was applied => copy to output_dir and return the new path
        if os.path.basename(output_file_resampled) != os.path.basename(input_file):
            # Move output-file to output_dir
            utils.retrieve_tmp_output(output_file_resampled, output_dir)

            # Change directory on output_file_resampled
            output_file_resampled = os.path.join(
                output_dir, os.path.basename(output_file_resampled)
            )
        # else, return the input path
        else:
            output_file_resampled = input_file

    return output_file_resampled
