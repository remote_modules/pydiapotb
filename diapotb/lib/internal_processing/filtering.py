#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

import os
from copy import deepcopy

from osgeo import gdal

from diapotb.lib.internal_processing.core.filtering_dimensions import FilteringDimension
from diapotb.lib.internal_processing.core.processing_dimensions import Dimension
import diapotb.lib.internal_processing.core.utils as utils

from diapotb.lib.core.DiapOTBExceptions import DiapOTBException
from diapotb.lib.core.Utils import logger, get_image_md


def create_output_geom(geom_content_in, output_file):
    """Write on disk the output geom file (same as input)"""
    # Copy geom_content
    geom_content_out = deepcopy(geom_content_in)

    # Create path and write geom
    geom_file = os.path.join(
        os.path.dirname(output_file),
        os.path.basename(output_file).split(".")[0] + ".geom",
    )

    utils.write_geom(geom_content_out, geom_file)


def create_output_filtered(geom_content_in, output_file):
    """Write on disk the output file with new metadata"""
    # Use Gdal to put the new metadata into the input copy
    input_file_new_md_gdal = gdal.Open(output_file)
    input_file_new_md_gdal.SetMetadata(geom_content_in)
    input_file_new_md_gdal.FlushCache()
    del input_file_new_md_gdal

    # return input_file_new_md


def filtering_on_dimension(
    input_file, output_file, secondary, dimension=Dimension.AZIMUT
):
    """Call filtering for the given Dimension"""
    # Get the metadata
    geom_content = get_image_md(input_file)
    geom_content_sec = get_image_md(secondary)

    # Define a block size for reading/resampling/writing
    # Could be 512, 1024 or 2048 folowwing gdal_cachemax
    block_size = utils.define_block_size_from_gdal_cache_max()

    # Instanciate a FilteringDimension
    dim_filter = FilteringDimension(dimension, block_size, input_file, output_file)

    # Assign metadata dictionaries
    dim_filter.dict_metadata_reference = geom_content
    dim_filter.dict_metadata_secondary = geom_content_sec

    # Check if filtering is necessary following cutoff frequencies (from ref and sec metadata)
    do_filtering = dim_filter.do_filtering()

    if do_filtering:
        # Do the filtering
        dim_filter.read_process_write_all_blocks()

        # Create the ouptut geom file
        # create_output_geom(geom_content, output_file)
        create_output_filtered(geom_content, output_file)

        del dim_filter

        # Return output_path
        return output_file
    else:
        logger.info("No need to do a filtering on %s", input_file)
        del dim_filter
        # return input
        return input_file


def main_filtering(reference_path, secondary_path, output_dir, tmp_dir=""):
    """Main function to apply filtering on azimut dimension for reference only (deramp image)

    :param reference_path: path to reference image
    :type reference_path: str
    :param secondary_path: path to secondary image
    :type secondary_path: str
    :param output_dir: output directory
    :type output_dir: str
    :param tmp_dir: tmp directory (empty, by default)
    :type tmp_dir: str

    :return: path to output file (filtered file)
    :rtype: str
    """
    # Check if tmp_dir has to be used to process (improve performance on some platforms)
    output_tmp_dir = output_dir
    input_path = reference_path
    if tmp_dir and os.path.exists(tmp_dir):
        # Copy reference_path in tmp_dir
        utils.prepare_tmp_input(reference_path, tmp_dir)
        # Change output_dir and reference_path to use tmp_dir
        output_tmp_dir = tmp_dir
        input_path = os.path.join(tmp_dir, os.path.basename(reference_path))

    # Filtering on AZIMUT dimension, only:
    output_file_azi = os.path.join(
        output_tmp_dir,
        os.path.basename(input_path).split(".")[0] + "_filtering_azimut.tif",
    )

    output_file_azi = filtering_on_dimension(
        input_path, output_file_azi, secondary_path, Dimension.AZIMUT
    )

    # Check if tmp_dir was used
    if tmp_dir and os.path.exists(tmp_dir):
        # If a resampling was applied => copy to output_dir and return the new path
        if os.path.basename(output_file_azi) != os.path.basename(reference_path):
            # Move output-file to output_dir
            utils.retrieve_tmp_output(output_file_azi, output_dir)

            # Change directory on output_file_azi
            output_file_azi = os.path.join(
                output_dir, os.path.basename(output_file_azi)
            )
        # else, return the reference path
        else:
            output_file_azi = reference_path

    return output_file_azi
