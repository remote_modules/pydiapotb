#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
Resampling processing
"""

import math
import numpy as np

try:
    import gdal
except ImportError:
    import osgeo.gdal as gdal

from diapotb.lib.internal_processing.core.processing_dimensions import (
    ProcessDimension,
    Dimension,
)
import diapotb.lib.internal_processing.core.utils as utils

from diapotb.lib.core.Utils import logger


class ResamplingDimension(ProcessDimension):
    """
    Class to implement resampling on a whole dimension (range or azimut)

    Based on ProcessDimension : Override _pre_process, _process, _init_datasets functions
    """

    def __init__(self, dimension, max_block, in_image, out_image):
        # Call base constructor
        super().__init__(dimension, max_block, in_image, out_image)

        # Init frequencies to zero
        self._f0 = 0
        self._f_in = 0
        self._f_out = 0

        # Init output size
        self._nb_line_out = 0
        self._nb_col_out = 0
        self._mid = 0

    @property
    def f0(self):
        return self._f0

    @f0.setter
    def f0(self, value):
        self._f0 = value

    @property
    def f_in(self):
        return self._f_in

    @f_in.setter
    def f_in(self, value):
        self._f_in = value

    @property
    def f_out(self):
        return self._f_out

    @f_out.setter
    def f_out(self, value):
        self._f_out = value

    @property
    def nb_col_out(self):
        return self._nb_col_out

    @property
    def nb_line_out(self):
        return self._nb_line_out

    def _init_datasets(self):
        """
        Init input and output datasets

        :return: ds_in and ds_out
        :rtype: tuple
        """
        # Open input dataset
        ds_in = gdal.Open(self._in_image, gdal.GA_ReadOnly)
        self._nb_line = ds_in.RasterYSize
        self._nb_col = ds_in.RasterXSize
        nb_bands = ds_in.RasterCount

        self._store_ds.append(ds_in)

        # Get output size
        nb_line_out, nb_col_out = self._init_output_size(
            nb_line=self._nb_line, nb_col=self._nb_col
        )

        # Create output
        # First, create this large raster file for testing
        driver = gdal.GetDriverByName("GTiff")
        # Default encoding : float complex
        encoding = gdal.GDT_CFloat32
        if nb_bands == 2:
            encoding = gdal.GDT_Float32

        ds_out = driver.Create(
            self._out_image, nb_col_out, nb_line_out, nb_bands, encoding
        )

        self._store_ds.append(ds_out)

        # Readjust block size
        if self._dimension == Dimension.AZIMUT:
            self._block_processed_dim = self._nb_line
            if self._block_unprocessed_dim > self._nb_col:
                self._block_unprocessed_dim = self._nb_col
        else:
            self._block_processed_dim = self._nb_col
            if self._block_unprocessed_dim > self._nb_line:
                self._block_unprocessed_dim = self._nb_line

    def do_resampling(self, nb_line_in, nb_col_in):
        """
        Indicate if a resampling is necessary following input/output dimensions (if same dimension, no need to do the resampling)

        :param nb_line_in: number of line for input image
        :type nb_line_in: int
        :param nb_col_in: number of colunm for input image
        :type nb_col_in: int

        :return: Indicate if a resampling is necessary
        :rtype: bool
        """
        do_resample = True

        # Init output size
        nb_line_out, nb_col_out = self._init_output_size(
            nb_line=nb_line_in, nb_col=nb_col_in
        )

        # Compare dimension
        if (self._dimension == Dimension.AZIMUT and nb_line_in == nb_line_out) or (
            self._dimension == Dimension.RANGE and nb_col_in == nb_col_out
        ):
            do_resample = False

        return do_resample

    def _init_output_size(self, **kwargs):
        """
        Init output size following some parameters

        :param kwargs: parameters as kwargs (nb_line and nb_col)
        :type kwargs: dict-like

        :return: nb_line_output, nb_col_output
        :rtype: tuple
        """
        # Retrieve kwargs options
        nb_line_in = kwargs["nb_line"]
        nb_col_in = kwargs["nb_col"]

        # Check frequencies (must be different than 0)
        if self._f_in == 0 or self._f_out == 0:
            raise Exception(
                "input and output frequencies must be assigned before launching the resampling"
            )

        if self._dimension == Dimension.AZIMUT:
            nb_line_out = math.floor((nb_line_in * self._f_out / self._f_in) + 0.5)
            nb_col_out = nb_col_in
        else:
            nb_line_out = nb_line_in
            nb_col_out = math.floor((nb_col_in * self._f_out / self._f_in) + 0.5)

        self._nb_line_out = nb_line_out
        self._nb_col_out = nb_col_out

        return nb_line_out, nb_col_out

    def _process(self, zone, off_unprocessed):
        """Resample process

        :param zone: input array to process
        :type zone: array-like
        :param off_unprocessed: current offset on unprocessed dimension to read/write the required area
        :type off_unprocessed: int

        :return: output resampled array (ready to be written)
        :rtype: array-like
        """
        nb_bands = self._store_ds[0].RasterCount

        # Define block size according to the processed dimension
        if self._dimension == Dimension.AZIMUT:
            current_size_unprocessed = self._block_unprocessed_dim
            if (self._block_unprocessed_dim + off_unprocessed) > self._nb_col_out:
                current_size_unprocessed = self._nb_col_out - off_unprocessed

            zone_f_out = np.zeros(
                (self._nb_line_out, current_size_unprocessed, nb_bands), dtype=complex
            )

            # Init current frequency/index with offset
            diff = abs(self._nb_line - self._nb_line_out)

            unprocessed_dim = 1

        else:
            current_size_unprocessed = self._block_unprocessed_dim
            if (self._block_unprocessed_dim + off_unprocessed) > self._nb_line_out:
                current_size_unprocessed = self._nb_line_out - off_unprocessed

            zone_f_out = np.zeros(
                (current_size_unprocessed, self._nb_col_out, nb_bands), dtype=complex
            )

            # Init current frequency/index with offset
            diff = abs(self._nb_col - self._nb_col_out)

            unprocessed_dim = 0

        # FFT on processed dim for all unprocessed (slice(0, current_size_unprocessed)) indexes
        zone_f_in = utils.apply_fft_on_a_dimension_2d(
            zone, unprocessed_dim, slice(0, current_size_unprocessed), sens="forward"
        )

        # Adapt spectrum following current frequency (with index + offset) and middle frequency or each line
        # Two cases :
        # _ Remove some frequencies if f_in > f_out
        # _ Add some frequencies (with zeros thanks to initialization) if f_out > f_in
        # Assign zone_f_out with zone_in
        if self._f_in > self._f_out:
            # Slice arrays : [0, self._mid[ and [self._mid or (self._mid+diff), size[ for all unprocessed indexes
            # input values [self._mid, (self._mid+diff)[ not used
            # None for slice stop means at the end of the array
            utils.assign_output_fft_2d(
                zone_f_in,
                slice(0, self._mid),
                zone_f_out,
                slice(0, self._mid),
                unprocessed_dim,
                slice(0, current_size_unprocessed),
            )
            utils.assign_output_fft_2d(
                zone_f_in,
                slice(self._mid + diff, None),
                zone_f_out,
                slice(self._mid, None),
                unprocessed_dim,
                slice(0, current_size_unprocessed),
            )

        else:
            # Slice arrays : [0, self._mid[ and [self._mid+diff or self._mid), size[ for all unprocessed indexes
            # output values [self._mid, (self._mid+diff)[ unchanged => remained to zeros
            utils.assign_output_fft_2d(
                zone_f_in,
                slice(0, self._mid),
                zone_f_out,
                slice(0, self._mid),
                unprocessed_dim,
                slice(0, current_size_unprocessed),
            )
            utils.assign_output_fft_2d(
                zone_f_in,
                slice(self._mid, None),
                zone_f_out,
                slice(self._mid + diff, None),
                unprocessed_dim,
                slice(0, current_size_unprocessed),
            )

        ## FFT -1 on processed dim for all unprocessed (slice(0, current_size_unprocessed)) indexes
        zone_out = utils.apply_fft_on_a_dimension_2d(
            zone_f_out,
            unprocessed_dim,
            slice(0, current_size_unprocessed),
            sens="inverse",
        )

        utils.assign_output_fft_2d(
            zone_out,
            slice(0, None),
            zone_f_out,
            slice(0, None),
            unprocessed_dim,
            slice(0, current_size_unprocessed),
        )

        return zone_f_out

    def _pre_process(self):
        """pre_proccess for resampling"""
        ambiguity = round(self._f0 / self._f_in)

        # Assign mid frequency
        wrapped_doppler = (
            math.fmod(self._f0 - ambiguity * self._f_in, self._f_in) / self._f_in
        )
        logger.info("wrapped_doppler value is : %f", wrapped_doppler)

        if self._dimension == Dimension.AZIMUT:
            diff = abs(self._nb_line - self._nb_line_out)

            self._mid = (
                math.floor(self._nb_line / 2)
                + math.floor(wrapped_doppler * self._nb_line)
                - math.floor(diff / 2)
            )

            if self._nb_line > self._nb_line_out:
                msg = "Downsampling on azimut : {} lines".format(diff)
            else:
                msg = "Upsampling on azimut : {} lines".format(diff)

        else:
            diff = abs(self._nb_col - self._nb_col_out)

            self._mid = (
                math.floor(self._nb_col / 2)
                + math.floor(wrapped_doppler * self._nb_col)
                - math.floor(diff / 2)
            )

            if self._nb_col > self._nb_col_out:
                msg = "Downsampling on range : {} columns".format(diff)
            else:
                msg = "Upsampling on range : {} columns".format(diff)

        logger.info(msg)
        logger.info("Middle frequency for resampling is : %s", str(self._mid))
