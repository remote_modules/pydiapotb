#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
Base processing
"""

import os
from enum import Enum

try:
    import gdal
except ImportError:
    import osgeo.gdal as gdal

import diapotb.lib.internal_processing.core.utils as utils


class Dimension(Enum):
    RANGE = "range"
    AZIMUT = "azimut"


class ProcessDimension:
    """
    Base class to read/process/write a whole dimension (range or azimut)
    """

    def __init__(self, dimension, max_block, in_image, out_image):
        # Check dimension
        if not isinstance(dimension, Dimension):
            raise Exception("input argument for ProcessDimension must be a Dimension")

        # Init arguments
        self._dimension = dimension
        self._block_unprocessed_dim = max_block
        self._block_processed_dim = max_block

        # Check images
        if not os.path.isfile(in_image):
            raise Exception("Input image does not exist. Please check your path.")

        self._in_image = in_image
        self._out_image = out_image
        self._nb_line = 0
        self._nb_col = 0

        self._store_ds = []
        self._store_driver = []

    def _clean_ds(self):
        """
        Clean all datasets and driver
        """
        for ds in self._store_ds:
            del ds
            ds = None

        for driver in self._store_driver:
            driver = None

    def __del__(self):
        self._clean_ds()

    def _init_datasets(self):
        """
        Init input and output datasets

        :return: ds_in and ds_out
        :rtype: tuple
        """
        # Open input dataset
        ds_in = gdal.Open(self._in_image, gdal.GA_ReadOnly)
        self._nb_line = ds_in.RasterYSize
        self._nb_col = ds_in.RasterXSize
        nb_bands = ds_in.RasterCount

        self._store_ds.append(ds_in)

        # Get output size
        nb_line_out, nb_col_out = self._init_output_size(
            nb_line=self._nb_line, nb_col=self._nb_col
        )

        # Create output
        # First, create this large raster file for testing
        driver = gdal.GetDriverByName("GTiff")
        # Default encoding : float complex
        encoding = gdal.GDT_CFloat32
        if nb_bands == 2:
            encoding = gdal.GDT_Float32

        ds_out = driver.Create(
            self._out_image, nb_col_out, nb_line_out, nb_bands, encoding
        )

        self._store_ds.append(ds_out)

        # Readjust block size
        if self._dimension == Dimension.AZIMUT:
            self._block_processed_dim = self._nb_line
            if self._block_unprocessed_dim > self._nb_col:
                self._block_unprocessed_dim = self._nb_col
        else:
            self._block_processed_dim = self._nb_col
            if self._block_unprocessed_dim > self._nb_line:
                self._block_unprocessed_dim = self._nb_line

    def _init_output_size(self, **kwargs):
        """
        Init output size following some parameters
        By default, same than input

        :param kwargs: parameters as kwargs (nb_line and nb_col)
        :type kwargs: dict-like

        :return: nb_line_output, nb_col_output
        :rtype: tuple
        """
        return kwargs["nb_line"], kwargs["nb_col"]

    def read_process_write_all_blocks(self):
        """
        Read a block following a Dimension (nb_block * size_dim), process that block and write it into output image
        """
        # Init datasets
        self._init_datasets()

        # Preporcessing
        self._pre_process()

        # Define size block line/col
        if self._dimension == Dimension.AZIMUT:
            size_block_line = self._block_processed_dim
            size_block_col = self._block_unprocessed_dim
        else:
            size_block_line = self._block_unprocessed_dim
            size_block_col = self._block_processed_dim

        # Loop to read/process/write
        yoff_write = 0
        xoff_write = 0
        off_unprocessed = 0
        count = 0
        for _, yoff in enumerate(range(0, self._nb_line, size_block_line)):
            for _, xoff in enumerate(range(0, self._nb_col, size_block_col)):

                # Readapt current size to not read/write out of the raster
                current_block_line = size_block_line
                current_block_col = size_block_col
                if (size_block_line + yoff) > self._store_ds[0].RasterYSize:
                    current_block_line = self._store_ds[0].RasterYSize - yoff

                if (size_block_col + xoff) > self._store_ds[0].RasterXSize:
                    current_block_col = self._store_ds[0].RasterXSize - xoff

                # Read the current zone
                zone = utils.read_a_block(
                    self._store_ds[0], current_block_line, current_block_col, yoff, xoff
                )

                zone_out = self._process(zone, off_unprocessed)

                if count == 0:
                    count += 1
                    size_block_out_line, size_block_out_col, _ = zone_out.shape

                # Write the current zone
                utils.write_a_block(self._store_ds[1], zone_out, yoff_write, xoff_write)

                # Offset migth be different than input if size is different
                if self._dimension == Dimension.AZIMUT:
                    xoff_write += size_block_out_col
                    off_unprocessed = xoff_write
                else:
                    yoff_write += size_block_out_line
                    off_unprocessed = yoff_write

                del zone

        # Clean datasets
        self._clean_ds()

    def _process(self, zone, off_unprocessed):
        """Empty proccess

        Return zone, by default
        """
        return zone

    def _pre_process(self):
        """Empty pre_proccess"""
        pass
