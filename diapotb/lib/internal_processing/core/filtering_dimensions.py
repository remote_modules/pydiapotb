#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
Filtering processing
"""

import math
import numpy as np

try:
    import gdal
except ImportError:
    import osgeo.gdal as gdal

from diapotb.lib.internal_processing.core.processing_dimensions import (
    ProcessDimension,
    Dimension,
)
import diapotb.lib.internal_processing.core.utils as utils
from diapotb.lib.core.Utils import MetadataKeyInGeom
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException

from diapotb.lib.core.Utils import logger


class FilteringDimension(ProcessDimension):
    """
    Class to implement filtering on a whole dimension (range or azimut)

    Based on ProcessDimension : Override _pre_process, _process, _init_datasets functions
    """

    def __init__(self, dimension, max_block, in_image, out_image):
        # Call base constructor
        super().__init__(dimension, max_block, in_image, out_image)

        # Init frequencies to zero
        self._step_freq = 0
        self._f_cutoff_upper = 0
        self._f_cutoff_lower = 0

        # Init output size
        self._nb_line_out = 0
        self._nb_col_out = 0

        # Init intervals (to cut the spectrum)
        self._cutoff_intervals = []

        # init metadata info
        self._dict_metadata_reference = {}
        self._dict_metadata_secondary = {}

    @property
    def dict_metadata_reference(self):
        return self._dict_metadata_reference

    @dict_metadata_reference.setter
    def dict_metadata_reference(self, dict_ref):
        self._dict_metadata_reference = dict_ref

    @property
    def dict_metadata_secondary(self):
        return self._dict_metadata_secondary

    @dict_metadata_secondary.setter
    def dict_metadata_secondary(self, dict_ref):
        self._dict_metadata_secondary = dict_ref

    @property
    def nb_col_out(self):
        return self._nb_col_out

    @property
    def nb_line_out(self):
        return self._nb_line_out

    def _init_datasets(self):
        """
        Init input and output datasets

        :return: ds_in and ds_out
        :rtype: tuple
        """
        # Open input dataset
        ds_in = gdal.Open(self._in_image, gdal.GA_ReadOnly)
        self._nb_line = ds_in.RasterYSize
        self._nb_col = ds_in.RasterXSize
        nb_bands = ds_in.RasterCount

        self._store_ds.append(ds_in)

        # Get output size
        nb_line_out, nb_col_out = self._init_output_size(
            nb_line=self._nb_line, nb_col=self._nb_col
        )

        # Create output
        # First, create this large raster file for testing
        driver = gdal.GetDriverByName("GTiff")
        # Default encoding : float complex
        encoding = gdal.GDT_CFloat32
        if nb_bands == 2:
            encoding = gdal.GDT_Float32

        ds_out = driver.Create(
            self._out_image, nb_col_out, nb_line_out, nb_bands, encoding
        )

        self._store_ds.append(ds_out)

        # Readjust block size
        if self._dimension == Dimension.AZIMUT:
            self._block_processed_dim = self._nb_line
            if self._block_unprocessed_dim > self._nb_col:
                self._block_unprocessed_dim = self._nb_col
        else:
            self._block_processed_dim = self._nb_col
            if self._block_unprocessed_dim > self._nb_line:
                self._block_unprocessed_dim = self._nb_line

    def do_filtering(self):
        """Check if filtering is required (following conditions on cutoff frequencies)

        :return: Indicate if a filtering is required
        :rtype: bool
        """
        # Estimate cutoff frequencies
        self._assign_cutoff_frequencies()

        do_filtering = False

        # Conditions on frequencies
        if self._f_cutoff_upper > 0 and self._f_cutoff_lower < 0:
            do_filtering = True

        if self._f_cutoff_upper > 0 and self._f_cutoff_lower > 0:
            do_filtering = True

        if self._f_cutoff_upper < 0 and self._f_cutoff_lower < 0:
            do_filtering = True

        return do_filtering

    def _process(self, zone, off_unprocessed):
        """Filtering process

        :param zone: input array to process
        :type zone: array-like
        :param off_unprocessed: current offset on unprocessed dimension to read/write the required area
        :type off_unprocessed: int

        :return: output filtered array (ready to be written)
        :rtype: array-like
        """
        nb_bands = self._store_ds[0].RasterCount

        # Define block size according to the processed dimension
        if self._dimension == Dimension.AZIMUT:

            current_size_unprocessed = self._block_unprocessed_dim
            if (self._block_unprocessed_dim + off_unprocessed) > self._nb_col:
                current_size_unprocessed = self._nb_col - off_unprocessed

            zone_f_out = np.zeros(
                (self._nb_line, current_size_unprocessed, nb_bands), dtype=complex
            )

            unprocessed_dim = 1

        else:
            current_size_unprocessed = self._block_unprocessed_dim
            if (self._block_unprocessed_dim + off_unprocessed) > self._nb_line:
                current_size_unprocessed = self._nb_line - off_unprocessed

            zone_f_out = np.zeros(
                (current_size_unprocessed, self._nb_col, nb_bands), dtype=complex
            )

            unprocessed_dim = 0

        ### FFT on processed dim for all unprocessed (slice(0, current_size_unprocessed)) indexes
        zone_f_in = utils.apply_fft_on_a_dimension_2d(
            zone, unprocessed_dim, slice(0, current_size_unprocessed), sens="forward"
        )

        # Set to 0 the spectrum in some intervals
        for interval in self._cutoff_intervals:
            # for all unprocessed (slice(0, current_size_unprocessed)) indexes
            utils.assign_output_fft_to_zeros(
                zone_f_in,
                slice(interval[0], interval[1]),
                unprocessed_dim,
                slice(0, current_size_unprocessed),
            )

        ## FFT -1 on processed dim for all unprocessed (slice(0, current_size_unprocessed)) indexes
        zone_out = utils.apply_fft_on_a_dimension_2d(
            zone_f_in,
            unprocessed_dim,
            slice(0, current_size_unprocessed),
            sens="inverse",
        )

        # Adapt the output to the input (following bands : complex or two bands real/imag)
        utils.assign_output_fft_2d(
            zone_out,
            slice(0, None),
            zone_f_out,
            slice(0, None),
            unprocessed_dim,
            slice(0, current_size_unprocessed),
        )

        return zone_f_out

    def _estimate_fdc_avg(self, is_secondary=False):
        """Estimate the doppler centroid average from metadata"""
        # Select metadata dict : reference or secondary
        dict_metadata = self._dict_metadata_reference

        if is_secondary:
            dict_metadata = self._dict_metadata_secondary

        # nb pol for doppler centroid and fm rate
        nb_dop_rate = int(dict_metadata[str(MetadataKeyInGeom.NB_FM_RATE)])
        nb_dop_centroid = int(dict_metadata[str(MetadataKeyInGeom.NB_DOP_CENTROID)])

        # Estimate fdc_avg
        # Fm rate avg (first and last pol) on coef order 0, only
        # Ex. : SAR.AzimuthFmRates_9.AzimuthTime
        # Ex. : FM_RATE_BASE_str(i).FM_COEF
        fm_rate_0 = (
            str(MetadataKeyInGeom.FM_RATE_BASE)
            + str(0)
            + "."
            + str(MetadataKeyInGeom.FM_COEF)
        )
        fm_rate_max = (
            str(MetadataKeyInGeom.FM_RATE_BASE)
            + str(nb_dop_rate - 1)
            + "."
            + str(MetadataKeyInGeom.FM_COEF)
        )
        fm = (
            float(dict_metadata[fm_rate_0].split(" ")[0])
            + float(dict_metadata[fm_rate_max].split(" ")[0])
        ) / 2

        # fdc_1
        slant_range_at_first_pixel = float(
            dict_metadata[str(MetadataKeyInGeom.SLANT_RANGE_TIME_0)]
        )

        # Ex. : 'SAR.DopplerCentroid_0.DopCoef': '41.000279999999996505 18750.900000000001455 -189257200 '
        # Ex. : DOP_CENTROID_BASE_str(i).DOP_COEF
        dop_1_coef_0 = float(
            dict_metadata[
                str(MetadataKeyInGeom.DOP_CENTROID_BASE)
                + str(0)
                + "."
                + str(MetadataKeyInGeom.DOP_COEF)
            ].split(" ")[0]
        )

        dop_1_coef_1 = float(
            dict_metadata[
                str(MetadataKeyInGeom.DOP_CENTROID_BASE)
                + str(0)
                + "."
                + str(MetadataKeyInGeom.DOP_COEF)
            ].split(" ")[1]
        )

        dop_1_slant_range = float(
            dict_metadata[
                str(MetadataKeyInGeom.DOP_CENTROID_BASE)
                + str(0)
                + "."
                + str(MetadataKeyInGeom.SLANT_RANGE)
            ]
        )

        fdc_1 = dop_1_coef_0 + dop_1_coef_1 * (
            slant_range_at_first_pixel - dop_1_slant_range
        )

        # fdc_n
        dop_n_coef_0 = float(
            dict_metadata[
                str(MetadataKeyInGeom.DOP_CENTROID_BASE)
                + str(nb_dop_centroid - 1)
                + "."
                + str(MetadataKeyInGeom.DOP_COEF)
            ].split(" ")[0]
        )

        dop_n_coef_1 = float(
            dict_metadata[
                str(MetadataKeyInGeom.DOP_CENTROID_BASE)
                + str(nb_dop_centroid - 1)
                + "."
                + str(MetadataKeyInGeom.DOP_COEF)
            ].split(" ")[1]
        )

        dop_n_slant_range = float(
            dict_metadata[
                str(MetadataKeyInGeom.DOP_CENTROID_BASE)
                + str(nb_dop_centroid - 1)
                + "."
                + str(MetadataKeyInGeom.SLANT_RANGE)
            ]
        )

        fdc_n = dop_n_coef_0 + dop_n_coef_1 * (
            slant_range_at_first_pixel - dop_n_slant_range
        )

        # tdc_1
        dop_1_time = dict_metadata[
            str(MetadataKeyInGeom.DOP_CENTROID_BASE)
            + str(0)
            + "."
            + str(MetadataKeyInGeom.DOP_TIME)
        ]

        tdc_1 = utils.convert_utc_to_seconds(dop_1_time) - fdc_1 / fm

        # tdc_n
        dop_n_time = dict_metadata[
            str(MetadataKeyInGeom.DOP_CENTROID_BASE)
            + str(nb_dop_centroid - 1)
            + "."
            + str(MetadataKeyInGeom.DOP_TIME)
        ]

        tdc_n = utils.convert_utc_to_seconds(dop_n_time) - fdc_n / fm

        # fdc_deriv and fdc_avg
        fdc_deriv = (fdc_n - fdc_1) / (tdc_n - tdc_1)

        time_azi_0 = utils.convert_utc_to_seconds(
            dict_metadata[str(MetadataKeyInGeom.START_DATE)]
        )

        fdc_avg = fdc_deriv * time_azi_0 + fdc_1 - fdc_deriv * tdc_1

        return fdc_avg

    def _assign_cutoff_frequencies(self):
        """Assign cutoff freq"""
        # Check reference metadata
        if not self._dict_metadata_reference or not self._dict_metadata_secondary:
            raise DiapOTBException(
                "Set metadata before filtering or estimating cutoff frequencies"
            )

        # For reference
        fdc_avg_ref = self._estimate_fdc_avg()
        pbw_ref = float(
            self._dict_metadata_reference[str(MetadataKeyInGeom.AZI_BANDWIDTH)]
        )

        # For secondary
        fdc_avg_sec = self._estimate_fdc_avg(is_secondary=True)
        pbw_sec = float(
            self._dict_metadata_secondary[str(MetadataKeyInGeom.AZI_BANDWIDTH)]
        )

        # Select min/max
        self._f_cutoff_lower = max(
            fdc_avg_ref - 0.5 * pbw_ref, fdc_avg_sec - 0.5 * pbw_sec
        )
        self._f_cutoff_upper = min(
            fdc_avg_ref + 0.5 * pbw_ref, fdc_avg_sec + 0.5 * pbw_sec
        )

        # Normalize cutoff frequencies with PRF (must be in [-0.5; 0.5])
        prf = 1 / float(
            self._dict_metadata_reference[str(MetadataKeyInGeom.LINE_INTERVAL)]
        )

        self._f_cutoff_lower = (self._f_cutoff_lower % prf) / prf
        self._f_cutoff_upper = (self._f_cutoff_upper % prf) / prf

        if self._f_cutoff_upper < -0.5:
            self._f_cutoff_upper += 1.0
        if self._f_cutoff_upper > 0.5:
            self._f_cutoff_upper -= 1.0

        if self._f_cutoff_lower < -0.5:
            self._f_cutoff_lower += 1.0
        if self._f_cutoff_lower > 0.5:
            self._f_cutoff_lower -= 1.0

    def _define_cutoff_intervals(self, size_processed):
        """Define all intervals where the spectrum will be set to 0"""
        # Deifne mid and step freq
        mid_freq = math.ceil(size_processed * 0.5)
        if self._step_freq == 0:
            self._step_freq = 1 / size_processed

        # Define intervals following cutoff frequencies
        if self._f_cutoff_upper > 0 and self._f_cutoff_lower < 0:
            # Positive freq : [0, mid_freq[
            # Find which index is superior to f_cutoff_upper
            # freq_current = ind_cur*self._step_freq => ind_upper = self._f_cutoff_upper/self._step_freq
            ind_upper = int(self._f_cutoff_upper / self._step_freq)

            # Check if ind_upper is in [0, mid_freq] to apply the cutoff
            if ind_upper > 0 and ind_upper < mid_freq:
                self._cutoff_intervals.append([ind_upper, mid_freq])

            # Negative freq : [mid_freq, :]
            # Find which index is inferior to f_cutoff_lower (shift to -0.5 for negative frequence))
            # freq_current = -0.5 + ind_cur*self._step_freq =>
            # ind_lower = (0.5 + self._f_cutoff_lower)/self._step_freq
            # ind_lower = int((0.5 + self._f_cutoff_lower)/self._step_freq)
            ind_lower = mid_freq + int((0.5 + self._f_cutoff_lower) / self._step_freq)

            # Check if ind_upper is in [mid_freq, :] to apply the cutoff
            if ind_lower >= mid_freq and ind_lower < size_processed:
                self._cutoff_intervals.append([mid_freq, ind_lower])

        elif self._f_cutoff_upper > 0 and self._f_cutoff_lower > 0:
            # Positive freq : [0, mid_freq[ ONLY

            # Find which index is superior to f_cutoff_upper and inferior to f_cutoff_lower
            ind_upper = int(self._f_cutoff_upper / self._step_freq)
            ind_lower = int(self._f_cutoff_lower / self._step_freq)

            if self._f_cutoff_upper > self._f_cutoff_lower:
                # Check if ind_upper and ind_lower are in [0, mid_freq] to apply the cutoff
                # Adapt, if needed
                if ind_lower < 0:
                    ind_lower = 0
                if ind_upper > mid_freq:
                    ind_upper = mid_freq

                self._cutoff_intervals.append([0, ind_lower])
                self._cutoff_intervals.append([ind_upper, mid_freq])

            else:
                # Check if ind_upper and ind_lower are in [0, mid_freq] to apply the cutoff (keep zone_f_out to zeros)
                # Adapt, if needed
                if ind_upper < 0:
                    ind_upper = 0
                if ind_lower > mid_freq:
                    ind_lower = mid_freq

                self._cutoff_intervals.append([ind_upper, ind_lower])

        elif self._f_cutoff_upper < 0 and self._f_cutoff_lower < 0:
            # Negative freq : [mid_freq, :] ONLY

            # Find which index is superior to f_cutoff_upper and inferior to f_cutoff_lower
            # Shift to -0.5 for neg freq (the most negative freq after FFT is in middle of the array)
            # ind_upper = int((0.5 + self._f_cutoff_upper)/self._step_freq)
            # ind_lower = int((0.5 + self._f_cutoff_lower)/self._step_freq)
            ind_upper = mid_freq + int((0.5 + self._f_cutoff_upper) / self._step_freq)
            ind_lower = mid_freq + int((0.5 + self._f_cutoff_lower) / self._step_freq)

            if self._f_cutoff_upper > self._f_cutoff_lower:
                # Check if ind_upper and ind_lower are in [mid_freq, :] to apply the cutoff
                # Adapt, if needed
                if ind_upper > size_processed:
                    ind_upper = size_processed
                if ind_lower < mid_freq:
                    ind_lower = mid_freq

                self._cutoff_intervals.append([mid_freq, ind_lower])
                self._cutoff_intervals.append([ind_upper, size_processed])

            else:
                # Check if ind_upper and ind_lower are in [mid_freq, :] to apply the cutoff
                # Adapt, if needed
                if ind_lower > size_processed:
                    ind_lower = size_processed
                if ind_upper < mid_freq:
                    ind_upper = mid_freq

                self._cutoff_intervals.append([ind_upper, ind_lower])

    def _pre_process(self):
        """Filetring pre_proccess to assign cutoff frequencies and define cutoff intervals"""
        # Assign cutoff frequencies if not done yet
        if self._f_cutoff_lower == 0 and self._f_cutoff_upper == 0:
            self._assign_cutoff_frequencies()

        # Define only once, cutoff intervals
        if self._dimension == Dimension.AZIMUT:
            size_processed = self._nb_line
        else:
            size_processed = self._nb_col

        self._define_cutoff_intervals(size_processed)

        logger.info("Cutoff frequency (lower) is : %f", self._f_cutoff_lower)
        logger.info("Cutoff frequency (upper) is : %f", self._f_cutoff_upper)
