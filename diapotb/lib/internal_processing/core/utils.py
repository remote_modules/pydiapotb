#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#          Nadine Pourthié (CNES)
#          Emmanuelle Sarrazin (CNES)
# =========================================================================#

"""
Pool of function used in internal_processing package
"""

import re
import datetime
import shutil
import os
import numpy as np

try:
    import gdal
except ImportError:
    import osgeo.gdal as gdal

from diapotb.lib.core.DiapOTBExceptions import DiapOTBException
from diapotb.lib.core.Utils import write_image_md, get_image_raw_md


def read_a_block(ds_in, nb_line, nb_col, offset_line, offset_col):
    """
    Read GTiff by block (a nb colunm/line at offset col/line)

    :param ds_in: input dataset
    :type ds_in: GDALDataset
    :param nb_line: number of line to read
    :type nb_line: int
    :param nb_col: number of colunm to read
    :type nb_col: int
    :param offset_line: first line to read
    :type offset_line: int
    :param offset_col: first of colunm to read
    :type offset_col: int

    :return: in-memory array (3D : row/col/band)
    :rtype: array-like
    """
    # Get number of bands to init the output array
    nb_bands = ds_in.RasterCount

    zone = np.zeros((nb_line, nb_col, nb_bands), dtype=complex)

    # Read only the required block for each band
    for i in range(nb_bands):
        zone[:, :, i] = np.array(
            ds_in.GetRasterBand(i + 1).ReadAsArray(
                xoff=offset_col, yoff=offset_line, win_xsize=nb_col, win_ysize=nb_line
            )
        )

    return zone


def write_a_block(ds_out, array_in, offset_line, offset_col):
    """
    Write GTiff by block (a nb colunm/line at offset col/line)

    :param ds_out: output dataset
    :type ds_out: GDALDataset
    :param arrray_in: in-memory array (3D : row/col/band)
    :type arrray_in: array-like
    :param offset_line: first line to write
    :type offset_line: int
    :param offset_col: first of colunm to write
    :type offset_col: int
    """
    # Get nb_bands from input array
    try:
        _, _, nb_bands = array_in.shape
    except ValueError:
        nb_bands = 1

    # Write each band (the first band is 1)
    for n_band in range(1, nb_bands + 1):
        band = ds_out.GetRasterBand(n_band)
        band.WriteArray(array_in[:, :, n_band - 1], xoff=offset_col, yoff=offset_line)
        band.FlushCache()


def define_block_size_from_gdal_cache_max():
    """
    Return a block size value from gdal cachemax to adapt processing to RAM
    block size has 2048 as max value and 512 as min value

    :return: block size for reading/resampling/writing
    :rtype: int
    """
    # First, get gdal_cachemax value (can be setted by user or remained as defaut value (5% of RAM)
    gdal_cache_max = gdal.GetCacheMax()

    # Convert in MB
    gdal_cache_max_in_mb = gdal_cache_max / (1024 * 1024)

    # Define following our value range [512, 1024, 2048], the block size
    # > 0.8 Gb => 16Gb RAM at least, by default
    if gdal_cache_max_in_mb > 800:
        block_size = 2048
    # > 0.4 Gb => 8Gb RAM at least, by default
    elif 400 <= gdal_cache_max_in_mb < 800:
        block_size = 1024
    else:
        block_size = 512

    return block_size


def prepare_tmp_input(input_file, tmp_dir):
    """Copy input_file to tmp_dir

    :param input_file: input file to copy on tmp_dir
    :type input_file: str
    :param tmp_dir: tmp directory
    :type tmp_dir: str
    """
    if not os.path.exists(input_file) or not os.path.exists(tmp_dir):
        raise DiapOTBException(
            "Impossible to prepare tmp_dir in internal_processing \
        (directory or file does not exist)"
        )

    ext = os.path.basename(input_file).split(".")[1]

    # Copy input_file and geom file
    # retrieve geom_file
    geom_file = os.path.join(
        os.path.dirname(input_file),
        os.path.basename(input_file).split(".")[0] + ".geom",
    )

    shutil.copy2(input_file, tmp_dir)
    if os.path.exists(geom_file):
        shutil.copy2(geom_file, tmp_dir)
    else:
        # Check extension (if cos => native image without geom)
        if ext == "cos":
            # Build the .geom thanks to ReadImageInfo
            write_image_md(
                input_file, os.path.join(tmp_dir, os.path.basename(geom_file))
            )


def retrieve_tmp_output(output_file, output_dir):
    """Move output_file to output_dir

    :param output_file: output file to move
    :type output_file: str
    :param output_dir: output directory
    :type output_dir: str
    """
    if not os.path.exists(output_file) or not os.path.exists(output_dir):
        raise DiapOTBException(
            "Impossible to prepare tmp_dir in internal_processing \
        (directory or file does not exist)"
        )

    # retrieve geom_file
    geom_file = os.path.join(
        os.path.dirname(output_file),
        os.path.basename(output_file).split(".")[0] + ".geom",
    )

    shutil.copy2(output_file, output_dir)
    shutil.copy2(geom_file, output_dir)


def my_eval(s):
    """
    Transform a string to a float or int

    :param s: input string
    :type s: str

    :return: output value
    :rtype: int/float
    """
    from ast import literal_eval

    try:
        return literal_eval(s)
    except:
        return s


def read_geom(filename):
    """
    Read a geom filename

    :param filename: input geom file
    :type filename: str

    :return: in-memory geom as a dictionary
    :rtype: dict
    """
    md = {}

    with open(filename) as f:
        for line in f.readlines():
            name, var = line.partition(":")[::2]
            md[name.strip()] = my_eval(var.strip())

    return md


def read_geom_from_raw_product(image):
    """
    Get a geom dict from native product

    :param filename: input SAR image (from native product => with available metadata)
    :type filename: str

    :return: in-memory geom as a dictionary
    :rtype: dict
    """
    keyword = get_image_raw_md(image)

    # Convert to dict
    # If maxsplit is given n split function :
    # at most maxsplit splits are done (thus, the list will have at most maxsplit+1 elements)
    dict_kwl = {
        i.split(":")[0].strip(): my_eval(
            (re.sub(r"[\n\t\s]*", "", "".join(i.split(":", 1)[1:]))).strip()
        )
        for i in keyword
    }

    return dict_kwl


def write_geom(content, out_filename):
    """
    Write a geom filename

    :param content: in-memory geom
    :type content: dict
    :param out_filename: output geom file
    :type out_filename: str
    """
    with open(out_filename, "w") as f:
        for key, value in content.items():
            f.write("%s:\t\t%s\n" % (key, value))


def apply_fft_on_a_dimension(array_in, dim, index_dim, sens="forward"):
    """
    Apply fft or fft-1 on a dimension of array_in

    :param array_in: input array (2D or 3D, if real and img bands)
    :type array_in: array-like
    :param dim: unprocessed dimension, must be 0 or 1 (0 = col, 1 = row)
    :type dim: int
    :param index_dim: index for unprocessed_dim (for processed dim, indexes : the whole dimension)
    :type index_dim: slice object
    :param sens: FFT forward or backward
    :type sens: str

    :return: output of FFT or FFT-1 (1D)
    :rtype: array-like
    """
    # Check dimensions : 0 = col/x and 1 = line/y
    if not dim in [0, 1]:
        raise DiapOTBException("dim argument must be 0 or 1")

    # All the lines/colunms in complex
    if dim == 0:
        if len(array_in.shape) > 2 and array_in.shape[2] == 2:
            dim_samples = array_in[index_dim, :, 0] + 1j * array_in[index_dim, :, 1]
        else:
            dim_samples = array_in[index_dim, :]
    else:
        if len(array_in.shape) > 2 and array_in.shape[2] == 2:
            dim_samples = array_in[:, index_dim, 0] + 1j * array_in[:, index_dim, 1]
        else:
            dim_samples = array_in[:, index_dim]

    # Apply fft or fft-1
    if sens == "inverse":
        array_fft = np.fft.ifft(dim_samples)
    else:
        array_fft = np.fft.fft(dim_samples)

    return array_fft


def apply_fft_on_a_dimension_2d(array_in, unprocessed_dim, index_dim, sens="forward"):
    """
    Apply fft or fft-1 on a dimension of array_in with axis parameter to choose which dimension

    :param array_in: input array (2D or 3D, if real and img bands)
    :type array_in: array-like
    :param dim: unprocessed dimension, must be 0 or 1 (0 = col, 1 = row)
    :type dim: int
    :param index_dim: index for unprocessed_dim (for processed dim, indexes : the whole dimension)
    :type index_dim: slice object
    :param sens: FFT forward or backward
    :type sens: str

    :return: output of FFT or FFT-1 (2D)
    :rtype: array-like
    """
    # Check dimensions : 0 = col/x and 1 = line/y
    if not unprocessed_dim in [0, 1]:
        raise DiapOTBException("dim argument must be 0 or 1")

    # All the lines/colunms in complex
    if unprocessed_dim == 0:
        if len(array_in.shape) > 2 and array_in.shape[2] == 2:
            dim_samples = array_in[index_dim, :, 0] + 1j * array_in[index_dim, :, 1]
        else:
            dim_samples = array_in[index_dim, :]
    else:
        if len(array_in.shape) > 2 and array_in.shape[2] == 2:
            dim_samples = array_in[:, index_dim, 0] + 1j * array_in[:, index_dim, 1]
        else:
            dim_samples = array_in[:, index_dim]

    # Apply fft or fft-1 on given dimension (select dimension to use axis parameter and apply fft on that dimension)
    # processed_dim = 0 => fft on column
    # processed_dim = 1 => fft on line
    if unprocessed_dim == 0:
        processed_dim = 1
    else:
        processed_dim = 0

    if sens == "inverse":
        array_fft = np.fft.ifft(dim_samples, axis=processed_dim)
    else:
        array_fft = np.fft.fft(dim_samples, axis=processed_dim)

    return array_fft


def assign_output_fft(
    array_in, in_indexes, array_out, out_indexes, dim, id_unprocessed_dim
):
    """Assign input array in output (following in/out dimensions and slicing)

    :param array_in: input array (1D)
    :type array_in: array-like
    :param in_indexes: input indexes for processed_dim (to copy)
    :type in_indexes: slice object
    :param array_out: output array (2D or 3D, if real and img bands)
    :type array_out: array-like
    :param out_indexes: output indexes for processed_dim (to paste)
    :type out_indexes: slice object
    :param dim: unprocessed dimension, must be 0 or 1 (0 = col, 1 = row)
    :type dim: int
    :param id_unprocessed_dim: index for unprocessed_dim
    :type id_unprocessed_dim: int
    """
    # Check instances
    if not isinstance(in_indexes, slice) or not isinstance(out_indexes, slice):
        raise DiapOTBException("Input indexes must be slice objet")

    # Check dimensions : 0 = col/x and 1 = line/y
    if not dim in [0, 1]:
        raise DiapOTBException("dim argument must be 0 or 1")

    if len(array_out.shape) > 2 and array_out.shape[2] == 2:
        if dim == 0:
            # processing x/column
            array_out[id_unprocessed_dim, out_indexes, 0] = array_in[in_indexes].real
            array_out[id_unprocessed_dim, out_indexes, 1] = array_in[in_indexes].imag
        else:
            # processing y/row
            array_out[out_indexes, id_unprocessed_dim, 0] = array_in[in_indexes].real
            array_out[out_indexes, id_unprocessed_dim, 1] = array_in[in_indexes].imag
    else:
        if dim == 0:
            # processing x/column
            array_out[id_unprocessed_dim, out_indexes] = array_in[in_indexes]
        else:
            # processing y/row
            array_out[out_indexes, id_unprocessed_dim] = array_in[in_indexes]


def assign_output_fft_2d(
    array_in, in_indexes, array_out, out_indexes, dim, id_unprocessed_dim
):
    """Assign input array in output (following in/out dimensions and slicing)

    :param array_in: input array (2D)
    :type array_in: array-like
    :param in_indexes: input indexes for processed_dim (to copy)
    :type in_indexes: slice object
    :param array_out: output array (2D or 3D, if real and img bands)
    :type array_out: array-like
    :param out_indexes: output indexes for processed_dim (to paste)
    :type out_indexes: slice object
    :param dim: unprocessed dimension, must be 0 or 1 (0 = col, 1 = row)
    :type dim: int
    :param id_unprocessed_dim: index for unprocessed_dim
    :type id_unprocessed_dim: int
    """
    # Check instances
    if not isinstance(in_indexes, slice) or not isinstance(out_indexes, slice):
        raise DiapOTBException("Input indexes must be slice objet")

    # Check dimensions : 0 = col/x and 1 = line/y
    if not dim in [0, 1]:
        raise DiapOTBException("dim argument must be 0 or 1")

    if len(array_out.shape) > 2 and array_out.shape[2] == 2:
        if dim == 0:
            # processing x/column
            array_out[id_unprocessed_dim, out_indexes, 0] = array_in[
                id_unprocessed_dim, in_indexes
            ].real
            array_out[id_unprocessed_dim, out_indexes, 1] = array_in[
                id_unprocessed_dim, in_indexes
            ].imag
        else:
            # processing y/row
            array_out[out_indexes, id_unprocessed_dim, 0] = array_in[
                in_indexes, id_unprocessed_dim
            ].real
            array_out[out_indexes, id_unprocessed_dim, 1] = array_in[
                in_indexes, id_unprocessed_dim
            ].imag
    else:
        if dim == 0:
            # processing x/column
            array_out[id_unprocessed_dim, out_indexes] = array_in[
                id_unprocessed_dim, in_indexes
            ]
        else:
            # processing y/row
            array_out[out_indexes, id_unprocessed_dim] = array_in[
                in_indexes, id_unprocessed_dim
            ]


def assign_output_fft_to_zeros(array_out, out_indexes, dim, id_unprocessed_dim):
    """Assign input array in output (following in/out dimensions and slicing)

    :param array_out: output array (2D or 3D, if real and img bands)
    :type array_out: array-like
    :param out_indexes: output indexes for processed_dim (to paste)
    :type out_indexes: slice object
    :param dim: unprocessed dimension, must be 0 or 1 (0 = col, 1 = row)
    :type dim: int
    :param id_unprocessed_dim: index for unprocessed_dim
    :type id_unprocessed_dim: int
    """
    # Check instances
    if not isinstance(out_indexes, slice):
        raise DiapOTBException("Input indexes must be slice objet")

    # Check dimensions : 0 = col/x and 1 = line/y
    if not dim in [0, 1]:
        raise DiapOTBException("dim argument must be 0 or 1")

    if len(array_out.shape) > 2 and array_out.shape[2] == 2:
        if dim == 0:
            # processing x/column
            array_out[id_unprocessed_dim, out_indexes, 0] = 0
            array_out[id_unprocessed_dim, out_indexes, 1] = 0
        else:
            # processing y/row
            array_out[out_indexes, id_unprocessed_dim, 0] = 0
            array_out[out_indexes, id_unprocessed_dim, 1] = 0
    else:
        if dim == 0:
            # processing x/column
            array_out[id_unprocessed_dim, out_indexes] = 0
        else:
            # processing y/row
            array_out[out_indexes, id_unprocessed_dim] = 0


def convert_utc_to_seconds(str_utc_date):
    """Convert UTC date to seconds

    :param str_utc_date: input UTC date (pattern : yyyy-mm-ddThh:mm:ss.ms)
    :type str_utc_date: str

    :return: seconds date
    :rtype: float
    """
    # Define splitters (-, T, : and .)
    pattern = "[-T:.]"

    # Split input UTC time
    list_s = re.split(pattern, str_utc_date)

    # Remove the Z caracter at the end of UTC time
    if "Z" in list_s[-1]:
        list_s[-1] = list_s[-1][:-1]

    # Convert to int values
    list_i = [int(x) for x in list_s]

    # return timestamp
    return datetime.datetime(*list_i).timestamp()
