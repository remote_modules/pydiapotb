#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
Ground chain
"""

import os

from diapotb.lib.PreProcessing import PreProcessingOutputKeys

from diapotb.lib.core.DiapOTBProcessing import (
    DiapOTBProcessingSingleImage,
    ParamHandler,
    FilenamesHandler,
    ExecutorSingleImage,
)
from diapotb.lib.core.DiapOTBEnums import (
    ExtendedEnum,
    ChainModes,
    ExtPosition,
    FilenamesEnum,
    extend_enum,
    RequiredKeysForSingleImageProcessing,
    DefaultEnum,
)
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException

from diapotb.lib.core.ApplicationWrapper import OTBApplicationWrapper


# pylint: disable=too-few-public-methods
# Specific enums
class GroundParamOthers(ExtendedEnum):
    """Define each required parameters for Ground chain"""

    XYZ = "withxyz"
    NO_DATA = "nodata"
    CARTESIAN_ESTIMATION = "withcart"


@extend_enum(GroundParamOthers)
class GroundParamS1IW(ExtendedEnum):
    """Define each required parameters for Ground chain
    (Others keys + the following ones)
    """

    BURSTIDS = "burst_ids"


class GroundDefaultValue(DefaultEnum):
    """Define some default values or redirect to other values
    The following paramaters are optional (other are mandatory)
    """

    XYZ = (str(GroundParamOthers.XYZ), True)
    NO_DATA = (str(GroundParamOthers.NO_DATA), -32768)
    CARTESIAN_ESTIMATION = (str(GroundParamOthers.CARTESIAN_ESTIMATION), True)


class GroundFilenames(FilenamesEnum):
    """Define key for intermediate/output filenames for Ground chain
    3 str to speficy, a key, the extension to add and the position of the extension in order to create the file
    """

    FILES_AFTER_DEMPROJ = ("dem_proj_files", "demProj_", str(ExtPosition.PREFIX))
    FILES_AFTER_CARTESAIN_ESTIMATION = (
        "cart_files",
        "cartMean_",
        str(ExtPosition.PREFIX),
    )


class GroundInputKeysOthers(ExtendedEnum):
    """Define intput keys for Ground chain"""

    DEM = "dem"


class GroundInputKeysTSX(ExtendedEnum):
    """Define intput keys for Ground chain"""

    DEM = "dem"
    RESAMPLE_DERAMP = str(PreProcessingOutputKeys.RESAMPLE)


class GroundInputKeysS1IW(ExtendedEnum):
    """Define intput keys for Ground chain"""

    DEM = "dem"
    DERAMP = "deramped_burst_list"


class GroundOutputKeys(ExtendedEnum):
    """Define output keys for PreProcessing chain"""

    DEMPROJ = "dem_proj_list"
    CARTESIAN_ESTIMATION = "cart_list"


# pylint: enable=too-few-public-methods


# Ground class
class Ground(DiapOTBProcessingSingleImage):
    """Use the module to launch Ground chain.

    main function : execute
    """

    def __init__(self, **kwargs):
        # Base constructor to init required elts such as image/dir, output_dir and parameters
        super().__init__(**kwargs)

        # Init the specific arguments for the PreProcessing chain
        self._name = "Ground"
        self._applications = ["SARDEMProjection", "SARCartesianMeanEstimation"]

        # Default mode : OTHERS
        self._mode = str(ChainModes.OTHERS)
        if "mode" in kwargs and str(kwargs["mode"]) in ChainModes.list():
            self._mode = str(kwargs["mode"])

        # Init Handlers according to the mode
        param_enum = GroundParamOthers
        file_enum = GroundFilenames
        default_param_enum = GroundDefaultValue
        self._inputs_list = GroundInputKeysOthers.list()

        if self._mode == str(ChainModes.S1_IW):
            param_enum = GroundParamS1IW
            self._inputs_list = GroundInputKeysS1IW.list()
        elif self._mode == str(ChainModes.TSX):
            # Same param enum than others
            self._inputs_list = GroundInputKeysTSX.list()

        self.param_handler = ParamHandler(param_enum, self._param, default_param_enum)

        self.file_handler = FilenamesHandler(file_enum, self._image_base, self._mode)

        # Get from parameter dictionary each argument
        self.param_handler.check_param()

    def retrieve_output(self, key):
        """Retrieve a given output of the Ground chain"""
        if not isinstance(key, GroundOutputKeys) and key not in GroundOutputKeys.list():
            raise DiapOTBException(
                "The current key is not a available "
                "output key for PreProcessing chain"
            )

        return self._dict_outputs[str(key)]

    # Process functions
    def execute(self, **kwargs):
        """Ground chain

        Execute Ground processing following current mode (from ChainModes) thanks to an Executor.
        Executor may be ExecutorGroundOthers, ExecutorGroundTSX or ExecutorGroundS1IW.

        :param kwargs: parameters as kwargs (used to provide additionnal input to the selected executor)
        :type kwargs: dict-like
        """
        # Add for each mode, a dedicated executor
        if str(self._mode) == str(ChainModes.OTHERS):
            self._input_enum = GroundInputKeysOthers
        elif str(self._mode) == str(ChainModes.TSX):
            self._input_enum = GroundInputKeysTSX
        else:
            self._input_enum = GroundInputKeysS1IW

        self._executor_builder.add_mode(ChainModes.OTHERS, ExecutorGroundOthers)

        self._executor_builder.add_mode(ChainModes.TSX, ExecutorGroundTSX)

        self._executor_builder.add_mode(ChainModes.S1_IW, ExecutorGroundS1IW)

        super().execute(**kwargs)


# Executors, one per mode
class ExecutorGroundOthers(ExecutorSingleImage):
    """Execute processing for ground chain mode S1SM-CSK"""

    def execute(self):
        """Ground chain for S1SM and CSK sensors

        Two applications are called here : SARDEMProjection and SARCartesianMeanEstimation on the single input image
        These applications have the self._image as input and put outputs in self._output_dir
        """

        # retrieve input : image and output_dir
        output_dir = self._inputs[str(RequiredKeysForSingleImageProcessing.OUTPUT_DIR)]

        # Retrieve parameters
        withxyz = self.param_handler.get_param(str(GroundParamOthers.XYZ))
        no_data = self.param_handler.get_param(str(GroundParamOthers.NO_DATA))
        withcart = self.param_handler.get_param(
            str(GroundParamOthers.CARTESIAN_ESTIMATION)
        )

        self.file_handler.create_intermediate_names()

        # SARDEMProjection
        # pylint: disable=no-member
        dem_proj_path = os.path.join(
            output_dir,
            self.file_handler.get_filename(
                GroundFilenames.FILES_AFTER_DEMPROJ.get_key()
            ),
        )
        # pylint: enable=no-member
        # adapt_image_format to handle h5 dataset (CSK sensor)
        app_dem_proj = OTBApplicationWrapper("SARDEMProjection")
        app_dem_proj.set_input_images(
            insar=self.image_path(), indem=self._inputs[str(GroundInputKeysOthers.DEM)]
        )
        app_dem_proj.set_parameters(withxyz=withxyz, nodata=no_data)
        app_dem_proj.set_output_images(out=dem_proj_path)
        app_dem_proj.execute_app(in_memory=False)

        dir_dem_c = app_dem_proj.get_output_int_parameter("directiontoscandemc")
        dir_dem_l = app_dem_proj.get_output_int_parameter("directiontoscandeml")

        self._outputs[str(GroundOutputKeys.DEMPROJ)] = dem_proj_path
        # Extend tmp_list
        self._all_files_list.append(dem_proj_path)

        # SARCartesianMeanEstimation
        # adapt_image_format to handle h5 dataset (CSK sensor)
        if withcart:
            # pylint: disable=no-member
            cart_mean_path = os.path.join(
                output_dir,
                self.file_handler.get_filename(
                    GroundFilenames.FILES_AFTER_CARTESAIN_ESTIMATION.get_key()
                ),
            )
            # pylint: enable=no-member
            app_cart_mean = OTBApplicationWrapper("SARCartesianMeanEstimation")
            app_cart_mean.set_input_images(
                insar=self.image_path(),
                indem=self._inputs[str(GroundInputKeysOthers.DEM)],
                indemproj=dem_proj_path,
            )
            app_cart_mean.set_parameters(
                indirectiondemc=dir_dem_c, indirectiondeml=dir_dem_l, mlran=1, mlazi=1
            )
            app_cart_mean.set_output_images(out=cart_mean_path)
            app_cart_mean.execute_app(in_memory=False)

            self._outputs[str(GroundOutputKeys.CARTESIAN_ESTIMATION)] = cart_mean_path
            # Extend tmp_list
            self._all_files_list.append(cart_mean_path)


class ExecutorGroundTSX(ExecutorSingleImage):
    """Execute processing for ground chain mode TSX"""

    def execute(self):
        """Ground chain for TSX/PAZ sensors

        Two applications are called here : SARDEMProjection and SARCartesianMeanEstimation on the single input image.

        These applications have the self._image as input and put outputs in self._output_dir
        """
        # TODO : Gather this executor with ExecutorGroundOthers
        # Only, one difference : application input is deramp_resample_in not image_path

        # retrieve input : image and output_dir
        output_dir = self._inputs[str(RequiredKeysForSingleImageProcessing.OUTPUT_DIR)]
        deramp_resample_in = self._inputs[str(GroundInputKeysTSX.RESAMPLE_DERAMP)]

        # Retrieve parameters
        withxyz = self.param_handler.get_param(str(GroundParamOthers.XYZ))
        no_data = self.param_handler.get_param(str(GroundParamOthers.NO_DATA))
        withcart = self.param_handler.get_param(
            str(GroundParamOthers.CARTESIAN_ESTIMATION)
        )

        self.file_handler.create_intermediate_names()

        # SARDEMProjection
        # pylint: disable=no-member
        dem_proj_path = os.path.join(
            output_dir,
            self.file_handler.get_filename(
                GroundFilenames.FILES_AFTER_DEMPROJ.get_key()
            ),
        )
        # pylint: enable=no-member
        # adapt_image_format to handle h5 dataset (CSK sensor)
        app_dem_proj = OTBApplicationWrapper("SARDEMProjection")
        app_dem_proj.set_input_images(
            insar=deramp_resample_in, indem=self._inputs[str(GroundInputKeysTSX.DEM)]
        )
        app_dem_proj.set_parameters(withxyz=withxyz, nodata=no_data)
        app_dem_proj.set_output_images(out=dem_proj_path)
        app_dem_proj.execute_app(in_memory=False)

        dir_dem_c = app_dem_proj.get_output_int_parameter("directiontoscandemc")
        dir_dem_l = app_dem_proj.get_output_int_parameter("directiontoscandeml")

        self._outputs[str(GroundOutputKeys.DEMPROJ)] = dem_proj_path
        # Extend tmp_list
        self._all_files_list.append(dem_proj_path)

        # SARCartesianMeanEstimation
        # adapt_image_format to handle h5 dataset (CSK sensor)
        if withcart:
            # pylint: disable=no-member
            cart_mean_path = os.path.join(
                output_dir,
                self.file_handler.get_filename(
                    GroundFilenames.FILES_AFTER_CARTESAIN_ESTIMATION.get_key()
                ),
            )
            # pylint: enable=no-member
            app_cart_mean = OTBApplicationWrapper("SARCartesianMeanEstimation")
            app_cart_mean.set_input_images(
                insar=deramp_resample_in,
                indem=self._inputs[str(GroundInputKeysTSX.DEM)],
                indemproj=dem_proj_path,
            )
            app_cart_mean.set_parameters(
                indirectiondemc=dir_dem_c, indirectiondeml=dir_dem_l, mlran=1, mlazi=1
            )
            app_cart_mean.set_output_images(out=cart_mean_path)
            app_cart_mean.execute_app(in_memory=False)

            self._outputs[str(GroundOutputKeys.CARTESIAN_ESTIMATION)] = cart_mean_path
            # Extend tmp_list
            self._all_files_list.append(cart_mean_path)


class ExecutorGroundS1IW(ExecutorSingleImage):
    """Execute processing for ground chain mode S1SM-CSK"""

    def execute_one_burst(self, deramp_in, output_dir, burst_id):
        """Execute ground chain for each burst"""

        # Retrieve parameters
        withxyz = self.param_handler.get_param(str(GroundParamS1IW.XYZ))
        no_data = self.param_handler.get_param(str(GroundParamS1IW.NO_DATA))
        withcart = self.param_handler.get_param(
            str(GroundParamS1IW.CARTESIAN_ESTIMATION)
        )

        self.file_handler.create_intermediate_names(burst_id=burst_id)

        # Output directory for the current burst
        burst_dir = os.path.join(output_dir, "burst" + str(burst_id))

        # Input image for applications : deramped burst
        in_image = deramp_in

        # SARDEMProjection
        # pylint: disable=no-member
        dem_proj_path = os.path.join(
            burst_dir,
            self.file_handler.get_filename(
                GroundFilenames.FILES_AFTER_DEMPROJ.get_key()
            ),
        )
        # pylint: enable=no-member
        app_dem_proj = OTBApplicationWrapper("SARDEMProjection")
        app_dem_proj.set_input_images(
            insar=in_image, indem=self._inputs[str(GroundInputKeysS1IW.DEM)]
        )
        app_dem_proj.set_parameters(withxyz=withxyz, nodata=no_data)
        app_dem_proj.set_output_images(out=dem_proj_path)
        app_dem_proj.execute_app(in_memory=False)

        dir_dem_c = app_dem_proj.get_output_int_parameter("directiontoscandemc")
        dir_dem_l = app_dem_proj.get_output_int_parameter("directiontoscandeml")

        # SARCartesianMeanEstimation
        cart_mean_path = ""
        if withcart:
            # pylint: disable=no-member
            cart_mean_path = os.path.join(
                burst_dir,
                self.file_handler.get_filename(
                    GroundFilenames.FILES_AFTER_CARTESAIN_ESTIMATION.get_key()
                ),
            )
            # pylint: enable=no-member
            app_cart_mean = OTBApplicationWrapper("SARCartesianMeanEstimation")
            app_cart_mean.set_input_images(
                insar=in_image,
                indem=self._inputs[str(GroundInputKeysS1IW.DEM)],
                indemproj=dem_proj_path,
            )
            app_cart_mean.set_parameters(
                indirectiondemc=dir_dem_c, indirectiondeml=dir_dem_l, mlran=1, mlazi=1
            )
            app_cart_mean.set_output_images(out=cart_mean_path)
            app_cart_mean.execute_app(in_memory=False)

        # Return outputs
        return dem_proj_path, cart_mean_path

    def execute(self):
        """PreProcessing chain for S1SM and CSK sensors

        Two applications are called here : SARDEMProjection and SARCartesianMeanEstimation on the single input image.

        These applications have the self._image as input and put outputs in self._output_dir
        """

        # retrieve input : output_dir
        output_dir = self._inputs[str(RequiredKeysForSingleImageProcessing.OUTPUT_DIR)]

        # Retrieve the specific parameters :
        # burst_ids : id of the burst
        # burst_to_process : burst to extract and then to process
        # (can be different for secondary image if burst ids betwwen reference and secondary image do not match)

        burst_ids_list = self.param_handler.get_param(str(GroundParamS1IW.BURSTIDS))

        # Empty lists
        dem_proj_list = []
        cart_mean_list = []

        # loop on burst
        for id_loop, burst_id in enumerate(burst_ids_list):

            # retrieve the current deramp file for the given index
            deramp_in = self._inputs[str(GroundInputKeysS1IW.DERAMP)][id_loop]

            # Process the current burst
            dem_proj_path, cart_mean_path = self.execute_one_burst(
                deramp_in, output_dir, burst_id
            )

            dem_proj_list.append(dem_proj_path)
            if cart_mean_path != "":
                cart_mean_list.append(cart_mean_path)

        self._outputs[str(GroundOutputKeys.DEMPROJ)] = dem_proj_list
        self._outputs[str(GroundOutputKeys.CARTESIAN_ESTIMATION)] = cart_mean_list
        # Extend tmp_list
        self._all_files_list.extend(dem_proj_list)
        self._all_files_list.extend(cart_mean_list)
