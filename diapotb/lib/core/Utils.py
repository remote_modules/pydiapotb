#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
    func_utils module
    ==================

    Pool of functions for logger, checks, image operations ...

"""


# Imports
import datetime
from enum import Enum
import logging
import os
import py_compile
import re
import subprocess
import sys
import time

from diapotb.__meta__ import __version__
from diapotb.lib.core.DiapOTBEnums import Satellite
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException
import h5py
import otbApplication as otb
import xml.etree.ElementTree as ET

from .DiapOTBEnums import Sensor, ScriptNames


try:
    import gdal
    import osr
    import ogr
except ImportError:
    import osgeo.gdal as gdal
    import osgeo.osr as osr
    import osgeo.ogr as ogr


class MetadataKeyInGeom(Enum):
    """Gather all metadata keys used in python scripts"""

    LINE_INTERVAL = "SAR.AzimuthTimeInterval"
    PRF = "PRF"
    FREQ_SAMPLING = "SAR.RangeSamplingRate"
    NUMBER_LINE = "NumberOfLines"
    NUMBER_COL = "NumberOfColumns"
    BURST_NUMBER = "SAR.BurstRecords.number"
    ORBIT_NUMBER = "OrbitNumber"
    SENSOR = "SensorID"
    SLANT_RANGE_TIME_0 = "SAR.NearRangeTime"
    START_DATE = "AcquisitionStartTime"
    LAST_DATE = "AcquisitionStopTime"
    INSTRUMENT = "Instrument"
    NB_FM_RATE = "SAR.AzimuthFmRates.number"
    FM_RATE_BASE = "SAR.AzimuthFmRates_"
    FM_COEF = "AzimuthFmRatePolynomial"
    FM_TIME = "AzimuthTime"
    NB_DOP_CENTROID = "SAR.DopplerCentroid.number"
    DOP_CENTROID_BASE = "SAR.DopplerCentroid_"
    DOP_COEF = "DopCoef"
    DOP_TIME = "AzimuthTime"
    SLANT_RANGE = "t0"
    ACQUISITION_MODE = "Mode"
    AZI_BANDWIDTH = "SAR.AzimuthBandwidth"

    def __str__(self):
        """Return key as str"""
        return self.value


# TODO : Gather and re-deifne some functions
# TODO : Find an uniform format for our logger


# Streamer to our log file
class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """

    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level

    def write(self, buf):
        """Write into log file"""
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(self):
        """Flush into log file"""
        for handler in self.logger.handlers:
            handler.flush()


# Global variables for logger and std_out
logger = logging.getLogger(__name__)
LOG_FORMATTER = logging.Formatter("%(filename)s :: %(levelname)s :: %(message)s")
STEARMER = StreamToLogger(logger, logging.INFO)
STDOUT_SAVE = STEARMER

### Functions for logger ###


def init_logger():
    """
    Init logger with a stream handler.
    """
    logger.setLevel(logging.INFO)

    # Create console handler with a high log level (warning level)
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.WARNING)

    # Add Handlers
    logger.addHandler(stream_handler)


def init_filelog(output_dir):
    """
    Init logger with a file handler (info.log).
    the standard ouput is redirected into this file handler. The std was saved into STDOUT_SAVE.
    """
    # File handler for the logger
    # Create file handler which logs even info messages (used as stdout redirection)
    file_handler = logging.FileHandler(os.path.join(output_dir, "info.log"), "a")
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(LOG_FORMATTER)

    # Add Handlers
    logger.addHandler(file_handler)

    # Redirect stdout and stderr to logger
    stdout_save_write = (
        sys.stdout.write
    )  # Save stdout.write to print some info into the console
    stdout_save_flush = (
        sys.stdout.flush
    )  # Save stdout.flush to print some info into the console
    sys.stdout.write = STEARMER.write  # Replace stdout.write by our StreamToLogger
    sys.stdout.flush = STEARMER.flush  # Replace stdout.flush by our StreamToLogger
    # STDOUT_SAVE = STEARMER # Different object
    STDOUT_SAVE.write = stdout_save_write  # Restore stdout.write into STDOUT_SAVE
    STDOUT_SAVE.flush = stdout_save_flush  # Restore stdout.write into STDOUT_SAVE

    # Print current versions
    log_otb_diapotb_versions()


def log(level, msg):
    """
    Transfer the msg to our logger with the required level.
    """
    logger.log(level, msg)


def print_on_std(msg):
    """
    Transfer the msg to our stdout => real print on standard output.
    """
    # Print on STDOUT_SAVE aka the original/true stdout
    print(msg, file=STDOUT_SAVE)


def otb_version():
    """Get the current OTB version (through ReadImageInfo)
    Call ReadImageInfo -version only once (result is cached)
    """
    if "version" not in otb_version.__dict__:
        try:
            r = subprocess.run(
                ["otbcli_OrthoRectification"],
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
            )
            version = r.stdout.decode("utf-8").strip("\n")
            version = re.search("version [0-9].?[0-9.]*", version)[0]

            otb_version.version = version.split(" ")[1]
        except Exception as exp:
            raise RuntimeError(
                "Cannot retrieve current OTB version : {exp}".format(exp=exp)
            )

    return otb_version.version


def log_otb_diapotb_versions():
    """Log OTB and DiapOTB versions"""
    msg = (
        "Current versions are : {diapotb_version} for DiapOTB (python scripts) and "
        "{otb_version} for OTB"
    )
    msg = msg.format(diapotb_version=__version__, otb_version=otb_version())

    logger.log(logging.INFO, msg)


def exception_tolerance(func):
    """Decorator to allow exceptions in some cases (ie : in SARMultiSlc*)"""

    def wrapper(*args, **kwargs):
        """Wrapper to allow exceptions"""
        # Return OK : 0
        res = 0

        # Execute func with try/except
        try:
            func(*args, **kwargs)
        except Exception as exp:
            # log and print the exception
            msg = "An exception was raised in {name} : {exp}".format(
                name=func.__name__, exp=exp
            )
            logger.warning(msg)
            print_on_std(msg)
            # res to KO
            res = 1

        # Return 0 or 1
        return res

    # Return the wrapper function
    return wrapper


def check_if_tmp_for_TSX():
    """Check if processing in tmp directory is required and necessary"""
    is_tmp = False

    # Check variable environment
    tmp_tsx = os.getenv("DIAPOTB_USE_TMP_TSX")

    if tmp_tsx:
        is_tmp = True
        logger.info(
            "tmp dir will be used to store input/output for resampling and filtering (increase performance)"
        )

    return is_tmp


### Functions for image format ###


def check_image_format(image):
    """Return the SLC dataset from h5 format"""
    image_name = os.path.basename(image)
    ext = image_name.split(".")[-1]

    # Check extension
    available_ext = ["tif", "tiff", "h5", "cos"]
    if ext not in available_ext:
        raise DiapOTBException(
            "Image extension is not available in DiapOTB. Available extension are : "
            + str(available_ext)
        )


def adapt_image_format(image):
    """Check format : only tiff, h5 and cosar are available
    Return the SLC dataset from h5 format
    """
    output_image = image
    image_name = os.path.basename(image)
    ext = image_name.split(".")[-1]

    # Adapt image if h5 format to retrieve the dedicated dataset
    if ext == "h5":
        image_h5 = h5py.File(image, "r")
        ldataset = list(image_h5.keys())

        if len(ldataset) != 1 and ldataset[0] != "S01":
            raise DiapOTBException(
                "Error, H5 input files does not contain the expected dataset"
            )

        slc_dataset = dict(image_h5["S01"])

        if "SBI" not in slc_dataset:
            raise DiapOTBException(
                "Error, H5 input files does not contain the expected dataset"
            )

        # Change the name of image to read directly the //S01/SBI
        output_image = "HDF5:" + output_image + "://S01/SBI"

    return output_image


### Functions for metadata ###


def get_image_md(image):
    """
    Retrieve metadata from an image thanks to ReadImageInfo
    """
    # TODO : Temporary solution for to get metadata with otb v8 by using ExtractROI
    # TODO : Change when the Python API is complete
    # adapt image, if needed (for h5)
    image = adapt_image_format(image)
    image_name = os.path.splitext(os.path.basename(image))[0]
    image_tmp = os.path.join("/tmp", "tmp_" + image_name + ".tiff")

    # Create a small image with ExtractROI to get the metadata set by otb with GDAL
    app_extract_roi = otb.Registry.CreateApplication("ExtractROI")
    app_extract_roi.SetParameterString("in", image)
    app_extract_roi.SetParameterString("out", image_tmp)
    app_extract_roi.SetParameterInt("sizex", 5)
    app_extract_roi.SetParameterInt("sizey", 5)
    app_extract_roi.ExecuteAndWriteOutput()

    # Get the metadata with GDAL
    metadata = gdal.Open(image_tmp)
    dict_md = metadata.GetMetadata()

    # Cleanup
    if os.path.exists(image_tmp):
        os.remove(image_tmp)

    return dict_md


def get_image_raw_md(image):
    """
    Retrieve metadata from an image thanks to ReadImageInfo (raw metadata)
    """
    # adapt image, if needed (for h5)
    image = adapt_image_format(image)
    image_name = os.path.splitext(os.path.basename(image))[0]
    image_tmp = os.path.join("/tmp", "tmp_raw_" + image_name + ".tiff")

    # Create a small image with ExtractROI to get the metadata set by otb with GDAL
    app_extract_roi = otb.Registry.CreateApplication("ExtractROI")
    app_extract_roi.SetParameterString("in", image)
    app_extract_roi.SetParameterString("out", image_tmp)
    app_extract_roi.SetParameterInt("sizex", 5)
    app_extract_roi.SetParameterInt("sizey", 5)
    app_extract_roi.ExecuteAndWriteOutput()

    # Get the metadata with GDAL
    metadata = gdal.Open(image_tmp)
    dict_md = metadata.GetMetadata()

    # Cleanup
    if os.path.exists(image_tmp):
        os.remove(image_tmp)

    return dict_md


def write_image_md(image, out_path):
    # TODO Remove this. No use in otb v8
    # Looks like it's used only in case of tmp with cos files
    """
    Retrieve keyword list from an image thanks to ReadImageInfo
    """
    # adapt image, if needed (for h5)
    image = adapt_image_format(image)

    logger.info("outpath is : " + out_path)

    # Retrieve some information about our input images with ReadImageInfo application
    app_read_image = otb.Registry.CreateApplication("ReadImageInfo")
    app_read_image.SetParameterString("in", image)
    app_read_image.SetParameterString("outkwl", out_path)
    app_read_image.ExecuteAndWriteOutput()
    del app_read_image


def get_sensor_from_md(dict_md):
    """Get sensor Id and check if sensor is available for DiapOTB"""
    if not isinstance(dict_md, dict):
        logger.warning("input dict_md is not a dict-like")
        return 1

    try:
        sensor = dict_md[str(MetadataKeyInGeom.SENSOR)]
    except KeyError:
        raise DiapOTBException(
            "Unknown sensor. Only CSK, S1 or TSX/PAZ/TDX are available"
        )

    if sensor not in Satellite.list():
        raise DiapOTBException(
            "Unknown sensor. Only CSK, S1 or TSX/PAZ/TDX are available"
        )

    return sensor


def convert_satelitte_to_sensor(satellite, script_name):
    """Convert Satellite instance to Sensor"""
    if not isinstance(satellite, Satellite) and satellite not in Satellite.list():
        logger.warning("input is not a Satellite instance.")
        return None

    if (
        not isinstance(script_name, ScriptNames)
        and script_name not in ScriptNames.list()
    ):
        logger.warning("input is not a ScriptNames instance.")
        return None

    # Define the sensor following Satellite (and possibly script_name)
    if str(satellite) in [str(Satellite.S1A), str(Satellite.S1B)]:
        if str(script_name) in [
            str(ScriptNames.SIMPLE_S1SM),
            str(ScriptNames.MULTI_SLC_S1SM),
        ]:
            sensor = str(Sensor.S1SM)
        else:
            sensor = str(Sensor.S1IW)
    elif satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:
        sensor = str(Sensor.TSX)
    else:
        sensor = str(Sensor.CSK)

    return sensor


def get_orbit_number_from_md(dict_md):
    """Get orbit number from metadata"""
    if not isinstance(dict_md, dict):
        logger.warning("input dict_md is not a dict-like")
        return 1

    try:
        orbit_num = dict_md[str(MetadataKeyInGeom.ORBIT_NUMBER)]
    except KeyError:
        raise DiapOTBException(
            "Unknown sensor. Only CSK, S1 or TSX/PAZ/TDX are available"
        )

    return orbit_num


def get_dem_information(dem):
    """
    Retrieve DEM information thanks to ReadImageInfo
    """
    # Get information about DEM (spacing, size ..)
    ReadDEMInfo = otb.Registry.CreateApplication("ReadImageInfo")
    ReadDEMInfo.SetParameterString("in", dem)
    ReadDEMInfo.SetParameterString("imagemetadata", "true")
    ReadDEMInfo.Execute()

    dict_dem_information = {}
    dict_dem_information["spacingXDEM"] = ReadDEMInfo.GetParameterFloat("spacingx")
    dict_dem_information["estimatedGroundSpacingXDEM"] = ReadDEMInfo.GetParameterFloat(
        "estimatedgroundspacingx"
    )
    dict_dem_information["spacingYDEM"] = ReadDEMInfo.GetParameterFloat("spacingy")
    dict_dem_information["estimatedGroundSpacingYDEM"] = ReadDEMInfo.GetParameterFloat(
        "estimatedgroundspacingy"
    )

    return dict_dem_information


def get_image_size(dict_md):
    """Retrieve image size from metadata"""
    nb_lines = dict_md[str(MetadataKeyInGeom.NUMBER_LINE)]
    nb_col = dict_md[str(MetadataKeyInGeom.NUMBER_COL)]

    return nb_lines, nb_col


# Functions for image/file selection


def get_image_data(
    image_basename, input_path, script_name=str(ScriptNames.MULTI_SLC_S1SM)
):
    """Retrieve the selected image following the input sensor and additionan information"""
    # Get name and ext
    image_ext = image_basename.split(".")[-1:]
    image_ext = image_ext[0]

    # Retrieve image from a directory
    # For TSX image_basename is a relative path
    image_path = get_img_from_dir(image_basename, input_path)

    # Get sensor thanks to ReadImageInfo
    dict_md = get_image_md(image_path)
    sat = get_sensor_from_md(dict_md)
    if sat == str(Satellite.CSK):
        sensor = str(Sensor.CSK)
    elif sat in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:
        sensor = str(Sensor.TSX)
    else:
        if script_name == str(ScriptNames.MULTI_SLC_S1SM) or script_name == str(
            ScriptNames.SIMPLE_S1SM
        ):
            sensor = str(Sensor.S1SM)
        else:
            sensor = str(Sensor.S1IW)

    # Do some checks (if exists + pattern)
    if not image_path:
        raise DiapOTBException(
            image_basename
            + " not found into given input path "
            + input_path
            + ". Please check your input path"
        )

    correct = check_image_pattern(image_basename, mode=sensor)

    if not correct:
        logger.info(
            "%s does not respect naming conventions for %s", image_basename, sensor
        )

    if sensor == str(Sensor.CSK):
        pol, image_ext, image_date = get_image_info_cosmo(image_basename)
    elif sensor == str(Sensor.TSX):
        pol, image_ext, image_date = get_image_info_tsx(image_basename, dict_md)
    else:
        pol, image_ext, image_date = get_image_info_s1(image_basename)

    return image_path, pol, image_ext, image_date, dict_md


def get_image_info_tsx(image, dict_md):
    """Retrieve image information : TSX/PAZ/TDX"""
    image_basename = os.path.basename(image)

    # Get name and ext
    image_ext = image_basename.split(".")[-1:]
    image_ext = image_ext[0]

    # Get pol from name
    pol = image_basename.split("_")[1]

    # Get date from kwl
    image_date = dict_md[str(MetadataKeyInGeom.START_DATE)]
    # date format from kwl : YYYY-MM-DDThhmmss.mmmmmZ
    image_date = "".join(image_date.split("T")[0].split("-"))

    # Return a tuple (with image + some information)
    return pol, image_ext, image_date


def get_image_info_cosmo(image_basename):
    """Retrieve image information : Cosmo"""
    # Get name and ext
    image_ext = image_basename.split(".")[-1:]
    image_ext = image_ext[0]

    # Retrieve info
    image_date = image_basename.split("_")[8][:8]
    pol = image_basename.split("_")[5]

    # Return a tuple (with image + some information)
    return pol, image_ext, image_date


def get_image_info_s1(image_basename):
    """Get image information for S1"""
    # Get name and ext
    image_ext = image_basename.split(".")[-1:]
    image_ext = image_ext[0]

    # Retrieve info
    image_date = image_basename.split("-")[4].split("t")[0]
    pol = image_basename.split("-")[3]

    # Return a tuple (with image + some information)
    return pol, image_ext, image_date


def get_img_from_safe(arg, search_dir="."):
    """
    Retrive selected image from a SAFE directory
    """
    img = []
    for root, _, files in os.walk(search_dir):
        for i in (i for i in files if i == arg):
            img.append(os.path.join(root, i))
            img = str("".join(img))
            return img


def get_img_from_dir(arg, search_dir="."):
    """
    Retrive selected image from a directory (for Cosmo sensor)
    """
    # if arg is a relative path (TSX case)
    if os.path.dirname(arg):
        img_path = os.path.realpath(os.path.join(search_dir, arg))
        if not os.path.exists(img_path):
            raise DiapOTBException(
                arg
                + " not found into given input path "
                + search_dir
                + ". Please check your input path"
            )
        else:
            return img_path
    # If arg is a basename
    else:
        img = []
        for root, _, files in os.walk(search_dir):
            for i in (i for i in files if i == arg):
                img.append(os.path.join(root, i))
                img = str("".join(img))
                return img


def check_image_pattern(img, mode=str(Sensor.S1SM)):
    """
    Check pattern for current image. Must be cohetrent according to the sensor and mode
    """
    correct_pattern = False

    if mode == str(Sensor.S1SM):
        # Mode S1 SM : mmm-bb-ttt-pp-yyyymmddthhmmss-yyyymmddthhmmss*
        # mmm : Mission identifier (s1a or s1b)
        # bb : Mode/Beam (s1-s6 for SM)
        # ttt : Product type (always slc here)
        # pp : Polarisations (2 letters : hh or vv or vh or hv)
        # yyyymmddthhmmss : Product start/stop date and times
        # (14 digits representing the date and time separated by the character "t")
        # * : Others representations such as orbits number or images number ...
        pattern = "".join(
            [
                "s1.",
                "-",
                "\\w{1}",
                "\\d",
                "-slc-",
                "\\w{2}",
                "-",
                "\\d{8}",
                "t",
                "\\d{6}",
                "-",
                "\\d{8}",
                "t",
                "\\d{6}",
            ]
        )

        if re.match(pattern, img):
            correct_pattern = True

    elif mode == str(Sensor.CSK):
        # Mode Cosmo : CSKS<i>_*_<YYYYMMDDhhmmss>_<YYYYMMDDhhmmss>
        # i : Identifier of the satellite (1, 2, 3 or 4)
        # YYYYMMDDhhmmss : Product start/stop date and times
        # * : Others representations such as identifier for orbit direction or look side
        pattern = "".join(["CSKS", "\\d"])

        if re.match(pattern, img):
            pattern_dates = "".join(["\\d{14}", "_", "\\d{14}"])
            dates = re.findall(pattern_dates, img)

            if len(dates) == 1:
                correct_pattern = True

    elif mode == str(Sensor.TSX):
        # Mode TSX/PAZ/TDX
        # img is a relative path => basename
        img_basename = os.path.basename(img)
        pattern = "IMAGE_\\w{2}_\\w{3}.*\\d{3}.cos"

        if re.match(pattern, img_basename):
            correct_pattern = True

    elif mode == str(Sensor.S1IW):
        # Mode S1 IW : mmm-bb-ttt-pp-yyyymmddthhmmss-yyyymmddthhmmss*
        # mmm : Mission identifier (s1A or s1B)
        # bb : Mode/Beam (iw1-iw3 for IW)
        # ttt : Product type (always slc here)
        # pp : Polarisations (2 letters : hh or vv or vh or hv)
        # yyyymmddthhmmss : Product start/stop date and times
        # (14 digits representing the date and time separated by the character "t")
        # * : Others representations such as orbits number or images number ...
        pattern = "".join(
            [
                "s1.",
                "-",
                "\\w{2}",
                "\\d",
                "-slc-",
                "\\w{2}",
                "-",
                "\\d{8}",
                "t",
                "\\d{6}",
                "-",
                "\\d{8}",
                "t",
                "\\d{6}",
            ]
        )

        if re.match(pattern, img):
            correct_pattern = True

    else:
        logger.exception("Unknown sensor in check_image_pattern")

    return correct_pattern


def get_slcml_namming_from_productname(product_name, mode=str(Sensor.S1SM), dict_md={}):
    """
    Get correct names (with conventions) for SLC and ML outputs (without extension)
    """

    # At the end, the name for slc and ml images must be :
    # fullsensor_subwath_product_pol_UTCfirstdate with
    # fullsensor : S1A/B for S1 or CSK for Cosmo
    # subwath : for instance, s4 for S1SM and Cosmo or iw1 for S1 IW
    # product : SCS_U/B for Cosmo or SLC for S1
    # UTCdate : date with YYYYMMDDthhmmss format
    slc_ml_name = ""

    if mode == str(Sensor.CSK):
        # Mode Cosmo : CSKS<i>_SCS_U/B_Sk_*_pol_<YYYYMMDDhhmmss>_<YYYYMMDDhhmmss>
        # i : Identifier of the satellite (1, 2, 3 or 4)
        # Sk : Mode/Beam (s1-s6)
        # pol : Polarisations (HH or VV or VH or HV)
        # YYYYMMDDhhmmss : Product start/stop date and times
        # * : Others representations such as identifier for orbit direction or look side
        productname_list = product_name.split("_")

        slc_ml_name = (
            productname_list[0][:3]
            + "_"
            + productname_list[3]
            + "_"
            + productname_list[1]
            + productname_list[2]
            + "_"
            + productname_list[5]
            + "_"
            + productname_list[8][:8]
            + "t"
            + productname_list[8][8:]
        )

    elif mode in [str(Sensor.S1SM), str(Sensor.S1IW)]:
        # Mode S1 IW : mmm-bb-ttt-pp-yyyymmddthhmmss-yyyymmddthhmmss*
        # mmm : Mission identifier (s1A or s1B)
        # bb : Mode/Beam (iw1-iw3 for IW or s1-s6)
        # ttt : Product type (always slc here)
        # pp : Polarisations (2 letters : hh or vv or vh or hv)
        # yyyymmddthhmmss : Product start/stop date and times
        # (14 digits representing the date and time separated by the character "t")
        # * : Others representations such as orbits number or images number ...
        productname_list = product_name.split("-")

        slc_ml_name = (
            productname_list[0]
            + "_"
            + productname_list[1]
            + "_"
            + productname_list[2]
            + "_"
            + productname_list[3]
            + "_"
            + productname_list[4]
        )

    elif mode == str(Sensor.TSX):
        # Mode TSX
        # Get pol from name
        image_basename = os.path.basename(product_name)
        pol = image_basename.split("_")[1]

        if not dict_md:
            raise DiapOTBException(
                "keyword list is required to get some information for TSX/PAZ/TDX products"
            )

        image_date = dict_md[str(MetadataKeyInGeom.START_DATE)]
        # date format from kwl : YYYY-MM-DDThhmmss.mmmmmZ
        image_date = "".join(image_date.split("T")[0].split("-"))

        slc_ml_name = (
            dict_md[str(MetadataKeyInGeom.SENSOR)].split("-")[0]
            + "_"
            + pol
            + "_"
            + dict_md[str(MetadataKeyInGeom.ORBIT_NUMBER)]
            + "_"
            + image_date
        )

    else:
        logger.exception("Unknown sensor in check_image_pattern")

    # Return output name without extension or ml factors
    return slc_ml_name.lower()


def get_interfnamming_from_productname(
    product_master_name,
    product_slave_name,
    mode=str(Sensor.S1SM),
    ref_kwl={},
    sec_kwl={},
):
    """
    Get correct names (with conventions) for interferogram outputs (without extension)
    """
    # At the end, the name for interf images must be :
    # sensor_M_UTCfirstdatemaster_S_* UTCfirstdateslave with
    # sensor : S1 for S1 or CSK for Cosmo
    # UTCdate : date with YYYYMMDDthhmss format
    interf_name = ""

    if mode == str(Sensor.CSK):
        # Mode Cosmo : CSKS<i>_SCS_U/B_Sk_*_pol_<YYYYMMDDhhmmss>_<YYYYMMDDhhmmss>
        # i : Identifier of the satellite (1, 2, 3 or 4)
        # Sk : Mode/Beam (s1-s6)
        # pol : Polarisations (HH or VV or VH or HV)
        # YYYYMMDDhhmmss : Product start/stop date and times
        # * : Others representations such as identifier for orbit direction or look side
        product_m_list = product_master_name.split("_")
        product_s_list = product_slave_name.split("_")

        interf_name = (
            product_m_list[0][:3]
            + "_M_"
            + product_m_list[8][:8]
            + "t"
            + product_m_list[8][8:]
            + "_S_"
            + product_s_list[8][:8]
            + "t"
            + product_s_list[8][8:]
        )

    elif mode in [str(Sensor.S1SM), str(Sensor.S1IW)]:
        # Mode S1 IW : mmm-bb-ttt-pp-yyyymmddthhmmss-yyyymmddthhmmss*
        # mmm : Mission identifier (s1A or s1B)
        # bb : Mode/Beam (iw1-iw3 or s1-s6 for IW)
        # ttt : Product type (always slc here)
        # pp : Polarisations (2 letters : hh or vv or vh or hv)
        # yyyymmddthhmmss : Product start/stop date and times
        # (14 digits representing the date and time separated by the character "t")
        # * : Others representations such as orbits number or images number ...
        product_m_list = product_master_name.split("-")
        product_s_list = product_slave_name.split("-")

        interf_name = (
            product_m_list[0][:2].upper()
            + "_M_"
            + product_m_list[4]
            + "_S_"
            + product_s_list[4]
        )

    elif mode == str(Sensor.TSX):
        # Mode TSX
        if not ref_kwl or sec_kwl:
            if not ref_kwl or not sec_kwl:
                raise DiapOTBException(
                    "keyword lists are required to get some information for TSX/PAZ/TDX products"
                )

        ref_date = ref_kwl[str(MetadataKeyInGeom.START_DATE)]
        sec_date = sec_kwl[str(MetadataKeyInGeom.START_DATE)]
        # date format from kwl : YYYY-MM-DDThhmmss.mmmmmZ
        ref_date = "".join(ref_date.split("T")[0].split("-"))
        sec_date = "".join(sec_date.split("T")[0].split("-"))

        interf_name = (
            ref_kwl[str(MetadataKeyInGeom.SENSOR)].split("-")[0]
            + "_M_"
            + ref_date
            + "_S_"
            + sec_date
        )

    else:
        logger.exception("Unknown sensor in check_image_pattern")

    # Return output name without extension or ml factors
    return interf_name


# EOF function for fine orbits
def metadata_correction_with_fine_orbits(image, eof_path_file, output_dir):
    """Create a new geom file with fine orbit thanks to SARMetadataCorrection application"""
    # Name of new geom
    image_base = os.path.splitext(os.path.basename(image))[0]
    out_geom_name = image_base + "_extended" + ".tiff"
    out_path = os.path.join(output_dir, out_geom_name)

    app_metadata_correction = otb.Registry.CreateApplication("SARMetadataCorrection")
    app_metadata_correction.SetParameterString("mode", "orbits")
    app_metadata_correction.SetParameterString("insar", image)
    app_metadata_correction.SetParameterString("infineorbits", eof_path_file)
    app_metadata_correction.SetParameterString("outmd", out_path)
    app_metadata_correction.ExecuteAndWriteOutput()

    return out_path


def is_st_mode(sensor, image_kwl):
    """Return a boll to indicate if current mode is ST (for TSX/PAZ/TDX only)"""
    if sensor in [str(Satellite.PAZ), str(Satellite.TSX), str(Satellite.TDX)]:
        if image_kwl[str(MetadataKeyInGeom.ACQUISITION_MODE)] == "ST":
            return True
        else:
            return False
    else:
        return False


def apply_eof_path_on_orbit(sensor, eof_path, image, image_kwl, output_dir):
    """Change orbit vector by the eof file (if sensor = S1 and if eof_paht is present)
    Return an extended filename to target the "fine" geom file or retunr image as it was
    (image base name, only)
    """

    new_image_name = os.path.basename(image)
    # Find eof files for each image if S1 only and eof_paht not empty
    # Then, create the "fine" geom (with precise orbits)
    # Eventually, assign an extended filename if EOF file correspond to the image
    if sensor in [str(Satellite.S1A), str(Satellite.S1B)]:
        if eof_path:

            # Get all eof files in eof_path
            list_of_eof = get_all_files_with_ext(eof_path, ".EOF")

            # Get image information (start/end date + instrment :S1A or S1B) from the
            # input keyword list
            start_date = image_kwl[str(MetadataKeyInGeom.START_DATE)]
            end_date = image_kwl[str(MetadataKeyInGeom.LAST_DATE)]
            # Get satellite number : S1A or S1B
            sat_number = image_kwl[str(MetadataKeyInGeom.INSTRUMENT)]

            # Get a eof file
            eof_file = select_eof_with_date(
                start_date, end_date, list_of_eof, sat_number
            )

            if eof_file:
                # Create the new geom file into dedicated repository
                extended_geom_path = os.path.join(output_dir, "extended_geom")
                if not os.path.exists(extended_geom_path):
                    os.makedirs(extended_geom_path)

                # Call SARMetadataCorrection
                out_geom_path = metadata_correction_with_fine_orbits(
                    image, os.path.join(eof_path, eof_file), extended_geom_path
                )

                # Assign new geom file with extended filename
                new_image_name = out_geom_path

    return new_image_name


# utils function to define geometry
def image_envelope(in_tif, out_shp):
    """
    This method returns a shapefile of an image
    """
    app = otb.Registry.CreateApplication("ImageEnvelope")
    app.SetParameterString("in", in_tif)
    app.SetParameterString("out", out_shp)
    app.ExecuteAndWriteOutput()
    return out_shp


def get_master_geometry(in_shp):
    """
    This method returns the geometry, of an input georeferenced
    shapefile
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    mstr_ds = driver.Open(in_shp, 0)
    mstr_layer = mstr_ds.GetLayer()
    for master in mstr_layer:
        master.GetGeometryRef().Clone()
        return master.GetGeometryRef().Clone()


def check_srtm_coverage(in_shp_geo, srtm):
    """
    This method checks and returns the SRTM tiles intersected
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    srtm_ds = driver.Open(srtm, 0)
    srtm_layer = srtm_ds.GetLayer()
    needed_srtm_tiles = {}
    srtm_tiles = []
    for srtm_tile in srtm_layer:
        srtm_footprint = srtm_tile.GetGeometryRef()
        intersection = in_shp_geo.Intersection(srtm_footprint)
        if intersection.GetArea() > 0:
            # coverage = intersection.GetArea()/area
            srtm_tiles.append(srtm_tile.GetField("FILE"))  # ,coverage))
    needed_srtm_tiles = srtm_tiles
    return needed_srtm_tiles


def add_wgs_projection(in_tiff):
    """
    Add a projection reference (WPS 84) to input tiff
    """
    # Open input tiff
    ds = gdal.Open(in_tiff, gdal.GA_Update)

    # First : Adapt Projetion for WGS84
    wkt = ds.GetProjection()

    # if no projection defined => projection by default (ESPG 4326)
    if not wkt:
        sr = osr.SpatialReference()
        sr.ImportFromEPSG(4326)  # ESPG : 4326 = WGS84
        wkt = sr.ExportToWkt()

    # Set Projection and set to None to apply chgts
    ds.SetProjection(wkt)
    ds = None


def get_all_tiff(pol, iw="", ext="", search_dir="."):
    """
    Get all tiff from an input directory (check on pattern)
    """
    tiff_list = []
    throw_warning = False

    # Mode S1 IW
    if iw != "":
        for _, _, files in os.walk(search_dir):
            for i in (i for i in files):

                # Selection with extension (.tiff)
                if i.endswith(".tiff"):

                    # Check pattern
                    correct = check_image_pattern(i, str(Sensor.S1IW))

                    if correct:
                        # Selection with polarisation and subwath
                        if pol == i.split("-")[3]:
                            if iw == i.split("-")[1]:
                                tiff_list.append(i)
                    else:
                        throw_warning = True

    else:
        # Mode Cosmo
        if ext == "h5":
            for _, _, files in os.walk(search_dir):
                for i in (i for i in files):

                    # Selection with extension (.h5)
                    if i.endswith(".h5"):

                        # Check pattern
                        correct = check_image_pattern(i, str(Sensor.CSK))

                        if correct:
                            # Selection with polarisation
                            if pol == i.split("_")[5]:
                                tiff_list.append(i)
                        else:
                            throw_warning = True

        # Mode TSX
        elif ext == "cos":
            for root, _, files in os.walk(search_dir):
                for i in (i for i in files):

                    # Selection with extension (.h5)
                    if i.endswith(".cos"):

                        # Check pattern
                        correct = check_image_pattern(i, str(Sensor.TSX))

                        if correct:
                            # Selection with polarisation
                            if pol == i.split("_")[1]:
                                # TSX products have similar names (IMAGE_*)
                                # => Append a relative path not a basename for TSX
                                tiff_list.append(os.path.join(root, i))
                        else:
                            throw_warning = True

        # Mode S1 SM
        else:
            for _, _, files in os.walk(search_dir):
                for i in (i for i in files):

                    # Selection with extension (.tiff)
                    if i.endswith(".tiff"):

                        # Check pattern
                        correct = check_image_pattern(i, str(Sensor.S1SM))

                        if correct:
                            # Selection with polarisation
                            if pol == i.split("-")[3]:
                                tiff_list.append(i)
                        else:
                            throw_warning = True

    return tiff_list, throw_warning


def get_all_files_with_ext(search_dir, ext):
    """
    Get all into a search directory with a given extension
    """
    list_files = []
    for _, _, files in os.walk(search_dir):
        for i in (i for i in files):
            if i.endswith(ext):
                list_files.append(i)

    return list_files


def get_date(in_tif, ext="", dict_md={}):
    """
    Get all date from an input tiff
    """
    # Cosmo mode
    if ext == "h5":
        in_tif_date = in_tif.split("_")[8][:8]
        return in_tif_date

    # TSX mode
    elif ext == "cos":
        if not dict_md:
            dict_md = get_image_md(os.path.realpath(in_tif))

        in_tif_date = dict_md[str(MetadataKeyInGeom.START_DATE)]
        # date format from kwl : YYYY-MM-DDThhmmss.mmmmmZ
        in_tif_date = "".join(in_tif_date.split("T")[0].split("-"))

        return in_tif_date

    # S1 mode
    else:
        in_tif_date = in_tif.split("-")[4].split("t")[0]
        return in_tif_date


def get_tiff_with_dates(start, end, exclude, tiff_list, ext=""):
    """
    Get all tiff from an input list and between start and end date
    """
    date_list = []
    exclude = [exclude]

    for i in tiff_list:
        try:
            date = get_date(i, ext)
            if int(start) <= int(date) and int(end) >= int(date):
                if date not in exclude:
                    date_list.append(i)
        # If keyError in date selection, just pass (possible for TSX if metadata are not available)
        except KeyError:
            pass

    return date_list


def select_eof_with_date(start, end, eof_list, sat_number="S1A"):
    """
    Select into the input list, the file that correspond to dates
    """
    time_start = time.mktime(time.strptime(start, "%Y-%m-%dT%H:%M:%S.%fZ"))

    time_end = time.mktime(time.strptime(end, "%Y-%m-%dT%H:%M:%S.%fZ"))

    for i_eof in eof_list:
        # Get sat number for current EOF file
        sat_nb = i_eof.split("_")[0]

        if sat_nb == sat_number:

            # Without extension
            i_eof = i_eof.split(".EOF")[0]

            start_eof_date = i_eof.split("_")[-2]
            start_eof_date = start_eof_date.split("V")[1]
            end_eof_date = i_eof.split("_")[-1]

            # Save date format
            time_start_eof_date = time.mktime(
                time.strptime(start_eof_date, "%Y%m%dT%H%M%S")
            )

            time_end_eof_date = time.mktime(
                time.strptime(end_eof_date, "%Y%m%dT%H%M%S")
            )

            # Compare dates and return eof file if eof file contains the current image dates
            if time_start >= time_start_eof_date and time_end <= time_end_eof_date:
                return i_eof + ".EOF"

    # if none of files contains wanted dates : return None
    return None


def get_relative_orbit(manifest):
    """
    Get from manifest file, the orbit number
    """
    root = ET.parse(manifest)
    return int(
        root.find(
            "metadataSection/metadataObject/metadataWrap/xmlData/{http://www.esa.int/safe/sentinel-1.0}orbitReference/{http://www.esa.int/safe/sentinel-1.0}relativeOrbitNumber"
        ).text
    )


def build_virutal_raster(master_image, srtm_shapefile, hgts_path, output_dir="."):
    """
    Build a vrt file corresponding to a dem from hgt (SRTM) files.
    The hgt file are contained into a global path : hgts_path
    """
    # create a vector envelope of the raster
    master_envelope = os.path.join(output_dir, "master_envelope.shp")
    master_envelope = image_envelope(master_image, master_envelope)

    # Get master geometry
    master_footprint = get_master_geometry(master_envelope)

    # Create a virtual raster that will be used as DEM
    hgts = check_srtm_coverage(master_footprint, srtm_shapefile)
    hgts_tuple = []
    for hgt in hgts:
        hgt_path = os.path.join(hgts_path, hgt)
        if os.path.exists(hgt_path):
            hgts_tuple.append(os.path.join(hgts_path, hgt))
        else:
            raise DiapOTBException(
                "{} not found. Please check your path to hgts files (only .hgt extension are available)".format(
                    hgt
                )
            )

    logger.info("\n Creating virtual raster from intersected hgt files...\n")

    dem = os.path.join(output_dir, "dem_scene.vrt")
    gdal.BuildVRT(dem, hgts_tuple)

    return dem


def arg_dates_to_iso_dates(start_date, end_date):
    """
    Date conversion
    """
    if start_date is not None:
        iso_start = datetime.datetime.strptime(start_date, "%Y%m%d").date()
        iso_end = datetime.datetime.strptime(end_date, "%Y%m%d").date()
        logger.info(
            "\n\n Selected dates: \n From {}, to {} \n".format(iso_start, iso_end)
        )
    if not start_date:
        logger.critical("Start time is needeed.")
        quit()
    return iso_start, iso_end


### Image operations ###


def extract_roi(inp, out, roi):
    """
    Extracts ROI
    """
    ds = gdal.Open(inp)
    ds = gdal.Translate(out, ds, projWin=roi)
    ds = None  # close and save ds


def extract_band123(inp, out):
    """
    Extracts ROI from a vector image (extracts several bands, 3 in total)
    """
    ds = gdal.Open(inp, gdal.GA_ReadOnly)
    ds = gdal.Translate(
        out,
        ds,
        bandList=["1", "2", "3"],
        format="GTiff",
        outputType=gdal.GDT_Float32,
        creationOptions=["TILED=YES"],
    )
    ds = None  # close and save ds
    return out


### Functions for some checks ###
def select_burst(dict_master, dict_slave, first_burst, last_burst):
    """
    Selects from master bursts, the corresponding slave ones.
    This selection uses a metadata parameter (azimuth_anx_time) to do so.
    """
    key1_burst = "SAR.BurstRecords_"
    nb_burst_slave = int(dict_slave["SAR.BurstRecords.number"])

    # Initialize the output lists (empty lists)
    valid_burst_master = []
    valid_burst_slave = []

    # Loop on Master bursts
    for id_b in range(first_burst, last_burst + 1):
        key_burst_master = key1_burst + str(id_b) + ".AzimuthAnxTime"

        # Get the anx time for the current burst (into Master Image)
        anx_master = float(dict_master[key_burst_master])

        # Loop on slave bursts to find the closest anx time
        min_diff = 200
        id_b_save = id_b
        for id_b_slave in range(0, nb_burst_slave):
            key_burst_slave = key1_burst + str(id_b_slave) + ".AzimuthAnxTime"

            # Get anx time for slave burst
            anx_slave = float(dict_slave[key_burst_slave])

            # Comparaison between master and slave
            diff = abs(anx_master - anx_slave)

            if min_diff > diff:
                min_diff = diff
                id_b_save = id_b_slave

        # Check if difference between the anx time is valid (must be inferior to 1)
        if min_diff < 1.0:
            # Fill lists with master Burst_id and the selected slave burst_id
            valid_burst_master.append(id_b)
            valid_burst_slave.append(id_b_save)

    return valid_burst_master, valid_burst_slave


def check_roi_format(roi):
    """
    Check the roi format, must be 'ulx uly lrx lry' (ex: -roi 2.44115 48.96126 2.44176 48.95927)
    """
    is_ok = True

    regx_exp = re.compile("^([+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)\\s){4}$")
    if not roi.endswith(" "):
        roi = roi + " "

    res = regx_exp.match(roi)
    # If res = None => does not match with the correct format => warning
    if not res:
        logger.warning(
            "Wrong format for roi paramater, must be 'ulx uly lrx lry' (ex: -roi 2.44115 48.96126 2.44176 48.95927)."
        )
        is_ok = False

    return is_ok


def str2bool(v):
    """
    Conversion between a string to a boolean (several ways to say yes into json configuration file).
    """
    return v.lower() in ("yes", "true", "t", "1")


def avoid_duplicates(inlist):
    """
    Remove duplicates into an input list. Also remove any _extended duplicates.
    """
    outlist = list(dict.fromkeys(inlist))
    extended_string = "_extended"
    outlist = [image for image in outlist if extended_string not in image]
    return outlist


def check_if_exist(file_or_path_or_img):
    """
    Check if a file, path or image exists. If not quit the program.
    """
    is_exist = True
    if not os.path.exists(file_or_path_or_img):
        is_exist = False
    return is_exist


def check_if_dir(in_dir):
    """
    Check if the input path exists and is a directory
    """
    is_dir = True
    if not os.path.isdir(in_dir):
        is_dir = False
    return is_dir


def check_burst_index(burst_index):
    """
    Check if the burst_index as string input is correctly sent
    """
    burst_index_ok = bool(re.match(r"^[\-0-9]+$", burst_index))
    first_burst = 0
    last_burst = 0
    if burst_index_ok:
        burst_list = burst_index.split("-")
        burst_list = [int(i) for i in burst_list]
        if len(burst_list) != 2:
            burst_index_ok = False
        else:
            first_burst = min(burst_list)
            last_burst = max(burst_list)
    if not burst_index_ok:
        logger.error("Wrong Burst Index Format (for instance 0-5)")
        quit()
    return first_burst, last_burst, burst_list


def check_burst_index_with_number(burst_index, dict_md):
    """Check if burst_index is consistent with the number of burst for reference/secondary"""
    burst_ok = True

    # Get number of burst
    nb_burst = dict_md[str(MetadataKeyInGeom.BURST_NUMBER)]

    if int(nb_burst) < min(burst_index) or int(nb_burst) < max(burst_index):
        burst_ok = False

    return burst_ok
