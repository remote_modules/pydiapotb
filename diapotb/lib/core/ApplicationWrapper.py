#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
Application wrapper to call OTB/DiapOTB applications
"""

from numpy import double
import otbApplication as otb
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException
from diapotb.lib.core.Utils import logger


class OTBApplicationWrapper:
    """Wrapper to call OTB/DiapOTB applications"""

    # Static argument to define a default ram parameter
    RAM = 2000

    def __init__(self, app_name):
        # Create the required OTB application
        self._app = otb.Registry.CreateApplication(app_name)
        if self._app is None:
            raise DiapOTBException(
                "Application Name : {} does not match with any OTB/DiapOTB application. {} ",
                str(otb.Registry.GetAvailableApplications()),
                "Please choose an existing application.",
            )

        # Initialize parameters, inputs and outputs
        self._parameters = {}
        self._inputs = {}
        self._outputs = {}

        self._available_params = self._app.GetParametersKeys()
        self._use_ram_parameter = True

    def __del__(self):
        """Remove the current application"""
        self._app = None

    @classmethod
    def configure_ram(cls, ram_value):
        """Configure the static argument RAM"""
        if ram_value:
            cls.RAM = int(ram_value)

    def disable_ram_parameter(self):
        """Disable the ram parameter (if this parameter is not available ofr a specific application, for instance)
        I.e : OrthoRectification does not have ram as parameter
        """
        self._use_ram_parameter = False

    def set_parameters(self, **kwargs):
        """Set parameters (only integer/float/bool parameters)

        :param kwargs: parameters as kwargs (following the current application)
        :type kwargs: dict-like

        :raises DiapOTBException: If unexpected instance : not int, float or bool
        :raises DiapOTBException: If one of kwargs is not an available parameters for the application
        """
        # For each parameters
        for param_name, param_value in kwargs.items():
            # Check if the current parameters is available for the current application
            if param_name not in self._available_params:
                raise DiapOTBException(
                    "{} is not available for {}", param_name, self._app.GetName()
                )

            # Call the associated functions to set the current parameter
            try:
                if isinstance(param_value, int):
                    self._app.SetParameterInt(param_name, param_value)
                elif isinstance(param_name, double):
                    self._app.SetParameterDouble(param_name, param_value)
                elif isinstance(param_value, float):
                    self._app.SetParameterFloat(param_name, param_value)
                elif isinstance(param_value, bool):
                    self._app.SetParameterBool(param_name, param_value)
                else:
                    self._app.SetParameterString(param_name, param_value)
            except:
                raise DiapOTBException("Unexpected type for {}", param_name)

    def set_input_images(self, **kwargs):
        """Set inputs (only string or image)

        :param kwargs: input images as kwargs (following the current application)
        :type kwargs: dict-like

        :raises DiapOTBException: If unexpected instance : not str, list or OTB Input image
        :raises DiapOTBException: If one of kwargs is not an available parameters for the application
        """
        # For each inputs
        for input_name, input_value in kwargs.items():
            # Check if the current input is available for the current application
            # (specific case for in_ to avoid the python keyword in)
            if input_name not in self._available_params and input_name != "in_":
                raise DiapOTBException(
                    "{} is not available for {}", input_name, self._app.GetName()
                )

            # Call the associated functions to set the current parameter, only two choices :
            # files => str
            # image => input_image
            try:
                if input_name == "in_":
                    input_name = "in"
                if isinstance(input_value, str):
                    self._app.SetParameterString(input_name, input_value)
                elif isinstance(input_value, list):
                    self._app.SetParameterStringList(input_name, input_value)
                else:
                    self._app.SetParameterInputImage(input_name, input_value)
            except:
                raise DiapOTBException("Unexpected type for {}", input_name)

    def set_output_images(self, **kwargs):
        """Set outputs (only string = files)

        :param kwargs: output images as kwargs (following the current application)
        :type kwargs: dict-like

        :raises DiapOTBException: If unexpected instance : not str
        :raises DiapOTBException: If one of kwargs is not an available parameters for the application
        """
        # For each inputs
        for output_name, output_value in kwargs.items():
            # Check if the current input is available for the current application
            if output_name not in self._available_params:
                raise DiapOTBException(
                    "{} is not available for {}", output_name, self._app.GetName()
                )

            # Call the associated functions to set the current parameter, only one choice :
            # files => str
            try:
                if isinstance(output_value, str):
                    self._app.SetParameterString(output_name, output_value)
                else:
                    raise DiapOTBException("Unexpected type for {}", output_name)
            except:
                raise DiapOTBException("Unexpected type for {}", output_name)

    def get_output_image(self, out_name):
        """Getter on output image

        :param out_name: key to retrieve output image
        :type out_name: str

        :raises DiapOTBException: if unavailable key or NoneType application
        """
        out_img = None
        try:
            out_img = self._app.GetParameterOutputImage(out_name)
        except:
            raise DiapOTBException(
                "A problem occured getting the required output image"
            )

        return out_img

    def get_output_float_parameter(self, out_name):
        """Getter on an output parameter

        :param out_name: key to retrieve output float
        :type out_name: str

        :raises DiapOTBException: if unavailable key or NoneType application
        """
        out_param = 0.0
        try:
            out_param = self._app.GetParameterFloat(out_name)
        except:
            raise DiapOTBException(
                "A problem occured getting the required output parameter"
            )

        return out_param

    def get_output_int_parameter(self, out_name):
        """Getter on an output parameter

        :param out_name: key to retrieve output float
        :type out_name: str

        :raises DiapOTBException: if unavailable key or NoneType application
        """
        out_param = 0.0
        try:
            out_param = self._app.GetParameterInt(out_name)
        except:
            raise DiapOTBException(
                "A problem occured getting the required output parameter"
            )

        return out_param

    def execute_app(self, in_memory=False):
        """Execute OTB/DiapOTB application (in-memory or not)

        :param in_memory: bool to specify if application will call Execute or ExecuteAndWriteOutput function
        :type in_memory: bool

        :raises DiapOTBException: if some mandatory arguments to launch the application are missing
        """
        if not self._app.IsApplicationReady():
            raise DiapOTBException(
                "Some mandatory arguments are missing for {}. Only the following arguments were provided : {} ",
                self._app.GetName(),
                str(self._app.GetParameters()),
            )
        if "ram" not in self._app.GetParameters() and self._use_ram_parameter:
            self._app.SetParameterString("ram", str(self.RAM))

        logger.info("{} execution .....".format(self._app.GetName()))

        if in_memory:
            self._app.Execute()
        else:
            self._app.ExecuteAndWriteOutput()
