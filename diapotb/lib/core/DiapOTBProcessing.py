#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
Base classes to define and create a processing
"""


from abc import ABCMeta, abstractmethod
import inspect
import os

from diapotb.lib.core.DiapOTBEnums import (
    RequiredKeysForSingleImageProcessing,
    RequiredKeysForDualImagesProcessing,
    ChainModes,
    FilenamesEnum,
    ExtPosition,
    DefaultEnum,
)
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException
from diapotb.lib.core.Utils import adapt_image_format


class DiapOTBProcessing(metaclass=ABCMeta):
    """Abstract/Base class to define standard processing"""

    def __init__(self, **kwargs):
        # Init the specific arguments
        self._name = "Base Processing"

        # Default mode
        self._mode = ChainModes.OTHERS

        # Empty arguments
        self._applications = []
        self._dict_outputs = {}
        self._dict_inputs = {}
        self._all_files = []

        # Handlers and Executor as composition (bridge)
        # To check parameters and execute appplications following the mode and chain
        self.param_handler = None
        self.file_handler = None

        self._inputs_list = []

        self._executor_builder = ExecutorBuilder()
        self._input_enum = None

        self._tmp_path = None

    def append_inputs(self, dict_to_add):
        """Append the key/value from dict_to_add into dict_inputs.
        Add only those needed (speficied in input_list)

        :param dict_to_add: inputs to add to the current processing
        :type dict_to_add: dict
        """
        for in_key, in_elt in dict_to_add.items():
            if in_key in self._inputs_list:
                self._dict_inputs[in_key] = in_elt

    @abstractmethod
    def retrieve_output(self, key):
        """Retrieve a specific output with a key

        :param key: key to specify which output is required
        :type key: str
        """

    def set_tmp_path(self, tmp_path):
        """Set a tmp path

        :param tmp_path: path to a tmp directory
        :type tmp_path: str
        """
        self._tmp_path = tmp_path

    def get_tmp_path(self):
        """Set a tmp path

        :return: path to the tmp directory
        :rtype: str
        """
        return self._tmp_path

    def set_all_files_list(self, tmp_input):
        """Set the list of temporary files created

        :param tmp_input: paths to generated files
        :type tmp_input: list or str
        """
        self._all_files.extend(tmp_input)

    def get_outputs(self):
        """Retrieve the output dictionary (with all outputs)

        :return: all available outputs
        :rtype: dict
        """
        return self._dict_outputs

    def get_all_files_list(self):
        """Retrieve all the temporary files created

        :return: paths to generated files
        :rtype: list
        """
        return self._all_files

    @abstractmethod
    def execute(self, **kwargs):
        """Execute the current processing according a given mode (sensor)

        :param kwargs: parameters as kwargs (following the current processing). These parameters will be added to inputs
        :type kwargs: dict-like
        """

    def info_chain(self):
        """Print all information about the current processing chain"""


class DiapOTBProcessingSingleImage(DiapOTBProcessing, metaclass=ABCMeta):
    """Abstract/Base class to define standard processing.

    This kind of processing takes a single image as input to perform a dedicated processing on it.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Check then assign global arguments : output_dir, reference_image ...
        self._check_required_elts(**kwargs)

        self._image = kwargs[str(RequiredKeysForSingleImageProcessing.IMAGE)]
        self._image_dir = kwargs[str(RequiredKeysForSingleImageProcessing.DIR)]
        self._output_dir = kwargs[str(RequiredKeysForSingleImageProcessing.OUTPUT_DIR)]
        self._param = kwargs[str(RequiredKeysForSingleImageProcessing.PARAMETERS)]

        # Get the image base
        self._image_base = os.path.splitext(os.path.basename(self._image))[0]

        # In case case we are not using extended Metadata, then we use the base image
        if not os.path.exists(self._image):
            self._image = os.path.join(self._image_dir, self._image)

        # First of all, check if image and directories exist
        if (
            not os.path.exists(self._image_dir)
            or not os.path.exists(self._image)
            or not os.path.exists(self._output_dir)
        ):
            raise DiapOTBException(
                "Input image or one of directories do not exist for "
                + self._name
                + ". Please, check the given paths"
            )

    def add_orbit_to_filenames(self, orbit_num):
        """Add orbit number to image_base to build filenames with it. Use for TSX/PAZ/TDX inputs.

        :param orbit_num: orbit number
        :type orbit_num: str
        """
        self._image_base += "_" + str(orbit_num)
        self.file_handler.override_image_base(self._image_base)

    def _check_required_elts(self, **kwargs):
        """Check if all required elts were provided"""
        # Required elements
        check_required_elts = all(
            elt in kwargs for elt in RequiredKeysForSingleImageProcessing.list()
        )

        if not check_required_elts:
            raise DiapOTBException(
                "Missing some required inputs to instantiate the current processing : "
                + self._name
                + "Only : "
                + kwargs.keys()
                + " were provided"
            )

    def execute(self, **kwargs):
        """Execute the current processing according a given mode (sensor)

        :param kwargs: parameters as kwargs (following the current processing). These parameters will be added to inputs
        :type kwargs: dict-like

        :raises DiapOTBException: if some required inputs are missing
        """
        # Gather self._dict_input with kwargs
        self._dict_inputs.update(kwargs)

        # Check arguments and init executor
        # Checks arguments
        if self._input_enum is not None:
            check_required_elts = all(
                elt in self._dict_inputs for elt in self._input_enum.list()
            )

            if not check_required_elts:
                raise DiapOTBException(
                    "Missing some required inputs to execute the current processing : "
                    + self._name
                    + ". Only : "
                    + str(self._dict_inputs)
                    + " were provided"
                )

        # Build the executor
        self._executor_builder.add_element("param", self.param_handler).add_element(
            "filename", self.file_handler
        )
        executor = self._executor_builder.build(self._mode)

        # Add inputs
        executor.inputs = self._dict_inputs
        executor.set_tmp_path(self._tmp_path)
        executor.inputs[str(RequiredKeysForSingleImageProcessing.IMAGE)] = self._image
        executor.inputs[str(RequiredKeysForSingleImageProcessing.DIR)] = self._image_dir
        executor.inputs[str(RequiredKeysForSingleImageProcessing.OUTPUT_DIR)] = (
            self._output_dir
        )

        # Execute the processing
        executor.execute()

        # Get outputs
        self._dict_outputs = executor.outputs
        # Get all tmp files
        self.set_all_files_list(executor.all_files_list)


class DiapOTBProcessingDualImages(DiapOTBProcessing, metaclass=ABCMeta):
    """Abstract/Base class to define standard processing with two images (ie : DIn-SAR)

    This kind of processing takes a couple of images as inputs to perform a dedicated processing on them.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Check then assign global arguments : output_dir, reference_image ...
        self._check_required_elts(**kwargs)

        self._reference_image = kwargs[
            str(RequiredKeysForDualImagesProcessing.REFERENCE_IMAGE)
        ]
        self._secondary_image = kwargs[
            str(RequiredKeysForDualImagesProcessing.SECONDARY_IMAGE)
        ]
        self._reference_dir = kwargs[
            str(RequiredKeysForDualImagesProcessing.REFERENCE_DIR)
        ]
        self._secondary_dir = kwargs[
            str(RequiredKeysForDualImagesProcessing.SECONDARY_DIR)
        ]
        self._output_dir = kwargs[str(RequiredKeysForDualImagesProcessing.OUTPUT_DIR)]
        self._param = kwargs[str(RequiredKeysForDualImagesProcessing.PARAMETERS)]

        # Get image base for reference and secondary (split ? to handle extended filename)
        self._reference_base = os.path.splitext(
            os.path.basename(self._reference_image.split("?")[0])
        )[0]
        self._secondary_base = os.path.splitext(
            os.path.basename(self._secondary_image.split("?")[0])
        )[0]

        # Two file handler : for reference and secondary images
        self.file_handler_reference = None
        self.file_handler_secondary = None

    def _check_required_elts(self, **kwargs):
        """Check if all required elts were provided"""
        # Required elements
        check_required_elts = all(
            elt in kwargs for elt in RequiredKeysForDualImagesProcessing.list()
        )

        if not check_required_elts:
            raise DiapOTBException(
                "Missing some required inputs to instantiate the current processing : "
                + self._name
                + "Only : "
                + kwargs.keys()
                + " were provided"
            )

        return check_required_elts

    def add_orbit_to_filenames(self, orbit_num_ref, orbit_num_sec):
        """Add orbit numbers to reference_base and secondary_base  to build filenames with them. Use for TSX/PAZ/TDX inputs.

        :param orbit_num_ref: orbit number for reference
        :type orbit_num_ref: str
        :param orbit_num_sec: orbit number for secondary
        :type orbit_num_sec: str
        """
        # Change base names
        self._reference_base += "_" + str(orbit_num_ref)
        self._secondary_base += "_" + str(orbit_num_sec)

        # Overrride inot FileHandlers
        self.file_handler_reference.override_image_base(self._reference_base)
        self.file_handler_secondary.override_image_base(self._secondary_base)

    def append_inputs_reference(self, dict_to_add):
        """Append the key/value from dict_to_add into dict_inputs for reference image.
        Add only those needed (speficied in input_list)

        :param dict_to_add: inputs to add to the current processing
        :type dict_to_add: dict
        """
        for in_key, in_elt in dict_to_add.items():
            # Change in_key to tag it as reference
            in_key += "_reference"
            if in_key in self._inputs_list:
                self._dict_inputs[in_key] = in_elt

    def append_inputs_secondary(self, dict_to_add):
        """Append the key/value from dict_to_add into dict_inputs for secondary image
        Add only those needed (speficied in input_list)

        :param dict_to_add: inputs to add to the current processing
        :type dict_to_add: dict
        """
        for in_key, in_elt in dict_to_add.items():
            # Change in_key to tag it as reference
            in_key += "_secondary"
            if in_key in self._inputs_list:
                self._dict_inputs[in_key] = in_elt

    def execute(self, **kwargs):
        """Execute the current processing according a given mode (sensor)

        :param kwargs: parameters as kwargs (following the current processing). These parameters will be added to inputs
        :type kwargs: dict-like

        :raises DiapOTBException: if some required inputs are missing
        """

        # Gather self._dict_input with kwargs
        self._dict_inputs.update(kwargs)

        # Check arguments and init executor
        # Checks arguments
        check_required_elts = all(
            elt in self._dict_inputs for elt in self._input_enum.list()
        )

        if not check_required_elts:
            raise DiapOTBException(
                "Missing some required inputs to execute the current processing : "
                + self._name
                + ". Only : "
                + str(self._dict_inputs)
                + " were provided"
            )

        # Build the executor
        self._executor_builder.add_element("param", self.param_handler).add_element(
            "filename_reference", self.file_handler_reference
        ).add_element("filename_secondary", self.file_handler_secondary)
        executor = self._executor_builder.build(self._mode)

        # Add inputs
        executor.inputs = self._dict_inputs
        executor.set_tmp_path(self._tmp_path)
        executor.inputs[str(RequiredKeysForDualImagesProcessing.REFERENCE_IMAGE)] = (
            self._reference_image
        )
        executor.inputs[str(RequiredKeysForDualImagesProcessing.REFERENCE_DIR)] = (
            self._reference_dir
        )
        executor.inputs[str(RequiredKeysForDualImagesProcessing.SECONDARY_IMAGE)] = (
            self._secondary_image
        )
        executor.inputs[str(RequiredKeysForDualImagesProcessing.SECONDARY_DIR)] = (
            self._secondary_dir
        )
        executor.inputs[str(RequiredKeysForDualImagesProcessing.OUTPUT_DIR)] = (
            self._output_dir
        )

        # Execute the processing
        executor.execute()

        # Get outputs
        self._dict_outputs = executor.outputs
        # Get all tmp files
        self.set_all_files_list(executor.all_files_list)


# Aditional classes to handle application execution, paramaters and filenames
class ParamHandler:
    """Base class to handle paramaters.

    Check if all required parameters were provided
    """

    def __init__(self, enum_class, param, enum_default=None):
        # Instanciate arguments
        self._enum_param = enum_class
        self._param = param

        self._enum_default = enum_default
        if enum_default is not None:
            if not issubclass(enum_default, DefaultEnum):
                raise DiapOTBException(
                    "Wrong parameters for ParamHandler, must be an Enum class"
                )

        # Check the arguments types : enum and dict
        if not inspect.isclass(self._enum_param):
            raise DiapOTBException(
                "Wrong parameters for ParamHandler, must be an Enum class"
            )

        if not isinstance(self._param, dict):
            raise DiapOTBException("Wrong parameters type, must be a dict")

    def check_param(self):
        """Check if all required paramaters are provided thanks to input enum as template

        :raises DiapOTBException: If some required parameters are missing
        """
        # Put some value default if some paramaters were not set
        if self._enum_default is not None:
            for elt in self._enum_default:
                # Check if elt is already in param, if not put the default value
                if str(elt) not in self._param:
                    if elt.bind_with_other_key(self._enum_param):
                        self._param[str(elt)] = self._param[
                            elt.get_default_value_or_key()
                        ]
                    else:
                        self._param[str(elt)] = elt.get_default_value_or_key()

        # Check parameters
        check_required_param = all(
            elt in self._param for elt in self._enum_param.list()
        )

        if not check_required_param:
            raise DiapOTBException(
                "Check param failed for : " ". Please, check the input parameters"
            )

        return check_required_param

    def get_param(self, key):
        """Getter on param

        :param key: input key
        :type key: str
        """
        param = False
        if key in self._param:
            param = self._param[key]
        else:
            raise DiapOTBException("param dictionary does not contain : " + key)

        return param


class FilenamesHandler:
    """Base class to handle filenames (intermediate/output files)"""

    def __init__(self, enum_class, image_base, mode):
        # Instanciate arguments
        self._enum_files = enum_class
        self._mode = ChainModes.OTHERS
        # Check mode
        if isinstance(mode, ChainModes) or str(mode) in ChainModes.list():
            self._mode = mode
        self._image = image_base

        # Empty filenames
        self._filenames = {}

        # Check the arguments types : enum
        if not issubclass(self._enum_files, FilenamesEnum):
            raise DiapOTBException(
                "Wrong parameters for FilenamesHandler, must be an Enum class"
            )

    def override_image_base(self, new_image_name):
        """Replace image_base by a new name

        :param new_image_name: new image name (override old name)
        :type new_image_name: str
        """
        self._image = new_image_name

    def create_intermediate_names(self, **kwargs):
        """Create all intermediate names used in the current processing from reference/secondary info

        :param kwargs: parameters as kwargs (several available keys : burst_id, ml_*)
        :type kwargs: dict-like
        """
        # TODO : Bind names with utils functions :
        # get_slcml_namming_from_productname and get_interfnamming_from_productname

        # Fix the image_base according to the mode
        # image keeps the same except for S1IW with _burst_burst_id
        image = self._image
        if str(self._mode) == str(ChainModes.S1_IW):
            burst_id = kwargs.get("burst_id", 1)

            image = self._image + "_burst" + str(burst_id)

        # Loop on Enums to create filenames
        for filename in self._enum_files:
            ext = filename.get_extension()
            # Specific case for ml files (get ml factors)
            if "ml" in filename.get_extension():
                ml_azi = kwargs.get("ml_azimut", 3)
                ml_ran = kwargs.get("ml_range", 3)

                ext += str(ml_azi) + str(ml_ran)

            # Get position to set the given extension (sufix/prefix or replace the current name)
            if filename.get_position() in [
                str(ExtPosition.WHOLE),
                str(ExtPosition.EXCLUSIF),
            ]:
                if str(self._mode) == str(
                    ChainModes.S1_IW
                ) and filename.get_position() in [str(ExtPosition.WHOLE)]:
                    ext += "_burst" + burst_id
                # Replace the name by the extension
                current_file = ext + ".tif"
            elif filename.get_position() == str(ExtPosition.PREFIX):
                current_file = ext + image + ".tif"
            else:
                current_file = image + ext + ".tif"

            # Assign dictionary
            self._filenames[str(filename.get_key())] = current_file

    def get_filename(self, key):
        """Getter on filename"""
        filename = ""
        if key in self._filenames:
            filename = self._filenames[key]
        else:
            raise DiapOTBException("filename dictionary does not contain : " + str(key))

        return filename

    def add_extension_to_filename(self, key, ext):
        """Add an extension to a given filename (key)

        :param key: given key to specify the current filename
        :type key: str
        :param ext: ext to add
        :type ext: str

        :raises DiapOTBException: If filename dictionary does not contain the current key
        """
        if key in self._filenames:
            # Split the current filename to avoid .tif extension and add the given ext
            self._filenames[key] = self._filenames[key].split(".tif")[0] + ext + ".tif"
        else:
            raise DiapOTBException("filename dictionary does not contain : " + str(key))

    @property
    def filenames(self):
        """Getter on all filenames"""
        return self._filenames


class Executor(metaclass=ABCMeta):
    """Base class to run applications and retrive outputs"""

    def __init__(self, param_handler, file_handler):
        # Composition of param_handler and file_handler
        self.param_handler = param_handler
        self.file_handler = file_handler

        # Empty inputs/outputs
        self._inputs = {}
        self._outputs = {}
        self._all_files_list = []
        self._tmp_path = None

        # Empty list of applications
        self._appplications = []

    @abstractmethod
    def check_inputs(self, **kwargs):
        pass

    @abstractmethod
    def execute(self):
        pass

    @property
    def outputs(self):
        return self._outputs

    def set_tmp_path(self, tmp_path):
        self._tmp_path = tmp_path

    @property
    def all_files_list(self):
        return self._all_files_list

    @property
    def inputs(self):
        return self._inputs

    @inputs.setter
    def inputs(self, input_dict):
        self._inputs = input_dict


class ExecutorSingleImage(Executor, metaclass=ABCMeta):
    """Base class to run applications and retrive outputs on a single image"""

    def image_path(self):
        """Gather and adapt format to get image path"""
        image_dir = self._inputs[str(RequiredKeysForSingleImageProcessing.DIR)]
        image_in = self._inputs[str(RequiredKeysForSingleImageProcessing.IMAGE)]

        # adapt_image_format to handle h5 dataset (CSK sensor)
        return adapt_image_format(os.path.join(image_dir, image_in))

    def check_inputs(self, **kwargs):
        """Check global inputs to execute a single image"""
        # Loop on required inputs (exclude parameters)
        check_required_elts = all(
            elt in kwargs or elt == str(RequiredKeysForSingleImageProcessing.PARAMETERS)
            for elt in RequiredKeysForSingleImageProcessing.list()
        )

        if not check_required_elts:
            raise DiapOTBException(
                "Missing some required inputs to instantiate the current executor"
            )


class ExecutorDualImages(Executor, metaclass=ABCMeta):
    """Base class to run applications and retrive outputs on a couple image"""

    def __init__(self, param_handler, file_handler_reference, file_handler_secondary):
        super().__init__(param_handler, None)
        self.file_handler_reference = file_handler_reference
        self.file_handler_secondary = file_handler_secondary

    def reference_path(self):
        """Gather and adapt format to get reference image path

        :return: path to reference image
        :rtype: str
        """
        reference = self._inputs[
            str(RequiredKeysForDualImagesProcessing.REFERENCE_IMAGE)
        ]
        reference_dir = self._inputs[
            str(RequiredKeysForDualImagesProcessing.REFERENCE_DIR)
        ]

        # adapt_image_format to handle h5 dataset (CSK sensor)
        return adapt_image_format(os.path.join(reference_dir, reference))

    def secondary_path(self):
        """Gather and adapt format to get secondary image path

        :return: path to secondary image
        :rtype: str
        """
        secondary = self._inputs[
            str(RequiredKeysForDualImagesProcessing.SECONDARY_IMAGE)
        ]
        secondary_dir = self._inputs[
            str(RequiredKeysForDualImagesProcessing.SECONDARY_DIR)
        ]

        # adapt_image_format to handle h5 dataset (CSK sensor)
        return adapt_image_format(os.path.join(secondary_dir, secondary))

    def check_inputs(self, **kwargs):
        """Check global inputs to execute two images

        :param kwargs: inputs as kwargs to check if required inputs are present
        :type kwargs: dict-like
        """
        # Loop on required inputs (exclude parameters)
        check_required_elts = all(
            elt in kwargs or elt == str(RequiredKeysForSingleImageProcessing.PARAMETERS)
            for elt in RequiredKeysForDualImagesProcessing.list()
        )

        if not check_required_elts:
            raise DiapOTBException(
                "Missing some required inputs to instantiate the current executor"
            )


class ExecutorBuilder:
    """Builder of Executors"""

    def __init__(self):
        self._elements = {}
        self._executor_per_mode = {}

    def add_mode(self, mode, executor):
        """Add a mode to build the following executor

        :param mode: selected mode among sensors from ChainModes (ie : ChainModes.OTHERS)
        :type mode: str
        :param executor: class to implement the current executor
        :type executor: class
        """
        # Check arguments
        if not isinstance(mode, ChainModes) and mode not in ChainModes.list():
            raise DiapOTBException("Given mode is unkwown")

        if not issubclass(executor, Executor):
            raise DiapOTBException("Given executor is not an Executor")

        # Add the executor class to our dictionary
        self._executor_per_mode[str(mode)] = executor

    def add_element(self, key, elt):
        """Add element to build the executor

        :param key: key to specify the elt to add
        :type key: str
        :param elt: elt to add (required to build an executor)
        """
        # Check elt type
        if not isinstance(elt, ParamHandler) and not isinstance(elt, FilenamesHandler):
            raise DiapOTBException(
                "element to build executor must \
            be param or filename handler"
            )

        # TODO, check key and persist available key names into an Enum
        self._elements[key] = elt

        return self

    def build(self, mode):
        """Return an instance of executor

        :param mode: selected mode among sensors from ChainModes (ie : ChainModes.OTHERS)
        :type mode: str

        :return: instance of executor
        :rtype: Executor
        """
        # Sort the elements according to the key
        try:
            param_handler = self._elements["param"]
            filename_handler_reference = None
            filename_handler_secondary = None
            if (
                "filename_reference" in self._elements
                and "filename_secondary" in self._elements
            ):
                filename_handler_reference = self._elements["filename_reference"]
                filename_handler_secondary = self._elements["filename_secondary"]
            else:
                filename_handler_reference = self._elements["filename"]
        except KeyError:
            raise DiapOTBException("Missing elements to build the executor")

        arguments = [param_handler, filename_handler_reference]
        if filename_handler_secondary is not None:
            arguments.append(filename_handler_secondary)

        # Build the instance following the mode
        return self._executor_per_mode[str(mode)](*arguments)
