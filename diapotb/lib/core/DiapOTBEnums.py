#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
Enums definitions for DiapOTB processings
"""

from enum import Enum


# pylint: disable=too-few-public-methods
def extend_enum(inherited_enum):
    """Decorator to extend enum"""

    def wrapper(added_enum):
        joined = {}
        for item in inherited_enum:
            joined[item.name] = item.value
        for item in added_enum:
            joined[item.name] = item.value
        return ExtendedEnum(added_enum.__name__, joined)

    return wrapper


class ExtendedEnum(Enum):
    """Base class for DiapOTB Enums (used mainly to print available values)"""

    @classmethod
    def list(cls):
        """Transform enum values to a list"""
        return list(map(lambda c: c.value, cls))

    @classmethod
    def list_lower(cls):
        """Transform enum values to a list"""
        return list(map(lambda c: c.value.lower(), cls))

    def __str__(self):
        """Define a str functions for basic enums (with single strings as values)"""
        return self.value


class DefaultEnum(Enum):
    """Base class to specify some default value for a few parameters"""

    @classmethod
    def list(cls):
        """Transform enum values to a list"""
        return list(map(lambda c: c.value[0], cls))

    def bind_with_other_key(self, enum):
        """Know if current Instance is bind with other key"""
        is_bind = False
        if self.value[1] in enum.list():
            is_bind = True

        return is_bind

    def get_default_value_or_key(self):
        """Get default value or the bind keys"""
        return self.value[1]

    def __str__(self):
        """Get key"""
        return self.value[0]


class RequiredKeysForSingleImageProcessing(ExtendedEnum):
    """Class to define the required elements to build a processing with a single image"""

    IMAGE = "image"
    DIR = "image_dir"
    PARAMETERS = "param"
    OUTPUT_DIR = "output_dir"


class RequiredKeysForDualImagesProcessing(ExtendedEnum):
    """Class to define the required elements to build a processing with a couple"""

    REFERENCE_IMAGE = "reference_image"
    SECONDARY_IMAGE = "secondary_image"
    REFERENCE_DIR = "reference_dir"
    SECONDARY_DIR = "secondary_dir"
    PARAMETERS = "param"
    OUTPUT_DIR = "output_dir"


class Sensor(ExtendedEnum):
    """Define all available sensors"""

    S1SM = "Sentinel-1 StriMap"
    S1IW = "Sentinel-1 IW"
    CSK = "Cosmo"
    TSX = "TSX/PAZ/TDX"


class Satellite(ExtendedEnum):
    """Define all available sensors/sat"""

    S1A = "SENTINEL-1A"
    S1B = "SENTINEL-1B"
    CSK = "CSK"
    TSX = "TSX-1"
    PAZ = "PAZ-1"
    TDX = "TDX-1"


class ScriptNames(ExtendedEnum):
    """Define the script names (represent the global processing by calling the main chains)"""

    SIMPLE_S1SM = "diapotb"
    SIMPLE_S1IW = "diapotb_S1IW"
    MULTI_SLC_S1SM = "SARMulti_SLC"
    MULTI_SLC_S1IW = "SARMulti_SLC_S1IW"


class ChainNames(ExtendedEnum):
    """Class to specify every chain names for DiapOTB"""

    PRE_PROCESSING = "Pre_Processing"
    GROUND = "Ground"
    DINSAR = "DIn_SAR"
    POST_PROCESSING = "Post_Processing"


class ChainModes(ExtendedEnum):
    """Class to specify every chain available mode in all chains.
    Mainly related to sensor, for instance S1SM or S1IW.
    """

    S1_IW = "S1IW"
    OTHERS = "Others"
    TSX = "TSX"


class ExtPosition(ExtendedEnum):
    """Define the available postions for extension in filename"""

    SUFIX = "sufix"
    PREFIX = "prefix"
    WHOLE = "whole"
    EXCLUSIF = "exclusif"


class FilenamesEnum(Enum):
    """
    Two str to speficy, a key and an extension to create the file
    The extension could be set at the beginning (prefix), at the end (sufix) or
    replace the current name (whole)
    """

    def __init__(self, key, ext, pos="sufix"):
        self.key = key
        self.ext = ext
        self.pos = pos

    def get_key(self):
        """Getter on key"""
        return self.value[0]

    def get_extension(self):
        """Getter on extension to add"""
        return self.value[1]

    def get_position(self):
        """Getter on position (where to set the given extension before/after the name or replace it)"""
        try:
            position = self.value[2]

            if position not in ExtPosition.list():
                position = "sufix"
        except:
            position = "sufix"
        return position


# pylint: enable=too-few-public-methods
