#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
DInSar chain
"""

import os

from diapotb.lib.core.DiapOTBProcessing import (
    DiapOTBProcessingDualImages,
    ParamHandler,
    FilenamesHandler,
    ExecutorDualImages,
)
from diapotb.lib.core.DiapOTBEnums import (
    ExtendedEnum,
    ChainModes,
    ExtPosition,
    FilenamesEnum,
    RequiredKeysForDualImagesProcessing,
    extend_enum,
    DefaultEnum,
)
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException

from diapotb.lib.PreProcessing import PreProcessingOutputKeys, PreProcessingParamS1IW
from diapotb.lib.Ground import GroundOutputKeys

from diapotb.lib.core.ApplicationWrapper import OTBApplicationWrapper

from diapotb.lib.internal_processing.filtering import main_filtering

from diapotb.lib.core import Utils as utils


# pylint: disable=too-few-public-methods
# Specific enums
class DInSarParamOthers(ExtendedEnum):
    """Define each required parameters for DINSAR chain"""

    MLAZI = str(PreProcessingParamS1IW.MLAZI)
    MLRAN = str(PreProcessingParamS1IW.MLRAN)
    MLAZI_GRID = "grid_ml_azi"
    MLRAN_GRID = "grid_ml_ran"
    GRID_STEP_RAN = "grid_step_ran"
    GRID_STEP_AZI = "grid_step_azi"
    GRID_THRESHOLD = "grid_treshold"
    GRID_GAP = "grid_gap"
    INTERF_GAIN = "interf_gain"
    ADVANTAGE = "advantage"
    ML_INTERFRAN = "interf_ran"
    ML_INTERFAZI = "interf_azi"


@extend_enum(DInSarParamOthers)
class DInSarParamS1IW(ExtendedEnum):
    """Define each required parameters for DINSAR chain
    (Others keys + the following ones)
    """

    ESD_ITER = "esd_iter"
    ESD_AUTOMODE = "esd_AutoMode"
    BURSTIDS = "burst_ids"
    WITH_INTERF = "with_interferogram"
    WITH_CONCATENATION = "with_concatenation"


@extend_enum(DInSarParamOthers)
class DInSarParamTSX(ExtendedEnum):
    """Define each required parameters for DInSar TSX/PAZ/TDX chain"""

    ACTIVATE_DERAMP = "with_deramp"


class DInSarParamDefaultValueOthers(DefaultEnum):
    """Define some default values or redirect to other values
    The following paramaters are optional (other are mandatory)
    """

    ML_INTERFRAN = (
        str(DInSarParamOthers.ML_INTERFRAN),
        str(PreProcessingParamS1IW.MLRAN),
    )
    ML_INTERFAZI = (
        str(DInSarParamOthers.ML_INTERFAZI),
        str(PreProcessingParamS1IW.MLAZI),
    )
    MLRAN_GRID = (str(DInSarParamOthers.MLRAN_GRID), str(PreProcessingParamS1IW.MLRAN))
    MLAZI_GRID = (str(DInSarParamOthers.MLAZI_GRID), str(PreProcessingParamS1IW.MLAZI))
    ADVANTAGE = (str(DInSarParamOthers.ADVANTAGE), "projection")


class DInSarParamDefaultValueTSX(DefaultEnum):
    """Define some default values or redirect to other values
    The following paramaters are optional (other are mandatory)
    """

    ML_INTERFRAN = (
        str(DInSarParamOthers.ML_INTERFRAN),
        str(PreProcessingParamS1IW.MLRAN),
    )
    ML_INTERFAZI = (
        str(DInSarParamOthers.ML_INTERFAZI),
        str(PreProcessingParamS1IW.MLAZI),
    )
    MLRAN_GRID = (str(DInSarParamOthers.MLRAN_GRID), str(PreProcessingParamS1IW.MLRAN))
    MLAZI_GRID = (str(DInSarParamOthers.MLAZI_GRID), str(PreProcessingParamS1IW.MLAZI))
    ADVANTAGE = (str(DInSarParamOthers.ADVANTAGE), "projection")
    ACTIVATE_DERAMP = (str(DInSarParamTSX.ACTIVATE_DERAMP), True)


class DInSarParamDefaultValueS1IW(DefaultEnum):
    """Define some default values or redirect to other values
    The following paramaters are optional (other are mandatory)
    """

    ML_INTERFRAN = (
        str(DInSarParamOthers.ML_INTERFRAN),
        str(PreProcessingParamS1IW.MLRAN),
    )
    ML_INTERFAZI = (
        str(DInSarParamOthers.ML_INTERFAZI),
        str(PreProcessingParamS1IW.MLAZI),
    )
    MLRAN_GRID = (str(DInSarParamOthers.MLRAN_GRID), str(PreProcessingParamS1IW.MLRAN))
    MLAZI_GRID = (str(DInSarParamOthers.MLAZI_GRID), str(PreProcessingParamS1IW.MLAZI))
    ADVANTAGE = (str(DInSarParamOthers.ADVANTAGE), "projection")
    ESD_AUTOMODE = (str(DInSarParamS1IW.ESD_AUTOMODE), True)
    WITH_INTERF = (str(DInSarParamS1IW.WITH_INTERF), True)
    WITH_CONCATENATION = (str(DInSarParamS1IW.WITH_CONCATENATION), True)


class DInSarFilenames(FilenamesEnum):
    """Define key for intermediate/output filenames for DInSAR chain
    3 str to speficy, a key, the extension to add and the position of the extension in order to create the file
    """

    FILES_AFTER_FINEGRID = ("grid_files", "fineDeformationGrid", str(ExtPosition.WHOLE))
    FILES_AFTER_COREGISTRATION = (
        "coRe_files",
        "_coregistrated",
        str(ExtPosition.SUFIX),
    )
    FILES_AFTER_DERAMP = (
        "coRe_deramp_files",
        "_coregistrated_reramp",
        str(ExtPosition.SUFIX),
    )
    FILES_AFTER_FILTERING = ("filter_files", "_filtered", str(ExtPosition.SUFIX))
    FILES_AFTER_FILTERING_DERAMP = (
        "deramp_filter_files",
        "_filtered_reramp",
        str(ExtPosition.SUFIX),
    )
    FILES_AFTER_INTERF = ("interf_files", "interferogram", str(ExtPosition.WHOLE))
    FILES_AFTER_CONCATENATE = (
        "interf_concatenate",
        "interferogram_swath",
        str(ExtPosition.EXCLUSIF),
    )


class DInSarInputKeysOthers(ExtendedEnum):
    """Define intput keys for DINSAR chain (Others keys + the following ones)"""

    DEM = "dem"
    ML_REFERENCE = str(PreProcessingOutputKeys.ML) + "_reference"
    DEMPROJ_REFERENCE = str(GroundOutputKeys.DEMPROJ) + "_reference"
    CARTESIAN_ESTIMATION_REFERENCE = (
        str(GroundOutputKeys.CARTESIAN_ESTIMATION) + "_reference"
    )
    DOP0_SECONDARY = str(PreProcessingOutputKeys.DOP0) + "_secondary"
    ML_SECONDARY = str(PreProcessingOutputKeys.ML) + "_secondary"
    DEMPROJ_SECONDARY = str(GroundOutputKeys.DEMPROJ) + "_secondary"


@extend_enum(DInSarInputKeysOthers)
class DInSarInputKeysTSX(ExtendedEnum):
    """Define intput keys for DINSAR chain (Others keys + the following ones)"""

    DERAMP_REFERENCE = str(PreProcessingOutputKeys.DERAMP) + "_reference"
    DERAMP_SECONDARY = str(PreProcessingOutputKeys.DERAMP) + "_secondary"
    DERAMP_RESAMPLE_SECONDARY = str(PreProcessingOutputKeys.RESAMPLE) + "_secondary"


@extend_enum(DInSarInputKeysOthers)
class DInSarInputKeysS1IW(ExtendedEnum):
    """Define intput keys for DINSAR chain (Others keys + the following ones)"""

    BURSTS_REFERENCE = str(PreProcessingOutputKeys.BURSTS) + "_reference"
    DERAMP_REFERENCE = str(PreProcessingOutputKeys.DERAMP) + "_reference"
    BURSTS_SECONDARY = str(PreProcessingOutputKeys.BURSTS) + "_secondary"
    DERAMP_SECONDARY = str(PreProcessingOutputKeys.DERAMP) + "_secondary"


class DInSarOutputKeysOthers(ExtendedEnum):
    """Define output keys for DINSAR chain"""

    GRIDS = "grids_list"
    INTERFERO = "interferogram"
    COREGISTRATED_SECONDARY = "coregistrated_secondary"


@extend_enum(DInSarOutputKeysOthers)
class DInSarOutputKeysS1IW(ExtendedEnum):
    """Define output keys for DInSAR chain (Others keys + the following ones)"""

    COREGISTRATED_SECONDARY_RERAMP = "coregistrated_secondary_reramp"
    REFERENCE_RERAMP = "reference_reramp"
    INTERFEROS = "interferograms_list"


# pylint: enable=too-few-public-methods


# DInSAR class
class DInSAR(DiapOTBProcessingDualImages):
    """Use the module to launch DINSAR chain.

    main function : execute
    """

    def __init__(self, **kwargs):
        # Base constructor to init required elts such as image/dir, output_dir and parameters
        super().__init__(**kwargs)

        # Init the specific arguments for the DINSAR chain
        self._name = "DINSAR"
        self._applications = [
            "SARFineDeformationGrid",
            "SARCoRegistration",
            "SARDeramp",
            "SARRobustInterferogram",
        ]

        # Default mode : Others
        self._mode = str(ChainModes.OTHERS)
        if "mode" in kwargs and str(kwargs["mode"]) in ChainModes.list():
            self._mode = str(kwargs["mode"])

        # Init Handlers according to the mode
        param_enum = DInSarParamOthers
        file_enum = DInSarFilenames
        default_param_enum = DInSarParamDefaultValueOthers
        self._inputs_list = DInSarInputKeysOthers.list()

        if self._mode == str(ChainModes.S1_IW):
            param_enum = DInSarParamS1IW
            default_param_enum = DInSarParamDefaultValueS1IW
            self._inputs_list = DInSarInputKeysS1IW.list()
        elif self._mode == str(ChainModes.TSX):
            # Same param enum than others
            self._inputs_list = DInSarInputKeysTSX.list()
            param_enum = DInSarParamTSX
            default_param_enum = DInSarParamDefaultValueTSX

        self.param_handler = ParamHandler(param_enum, self._param, default_param_enum)

        self.file_handler_reference = FilenamesHandler(
            file_enum, self._reference_base, self._mode
        )

        self.file_handler_secondary = FilenamesHandler(
            file_enum, self._secondary_base, self._mode
        )

        # Get from parameter dictionary each argument
        self.param_handler.check_param()

    def retrieve_output(self, key):
        """Retrieve a given output of the DInSAR chain"""
        if (
            not isinstance(key, DInSarOutputKeysS1IW)
            and key not in DInSarOutputKeysS1IW.list()
        ):
            raise DiapOTBException(
                "The current key is not a available "
                "output key for PreProcessing chain"
            )

        return self._dict_outputs[str(key)]

    # Process functions
    def execute(self, **kwargs):
        """DInSAR chain

        Execute DInSAR processing following current mode (from ChainModes) thanks to an Executor.
        Executor may be ExecutorDInSAROthers, ExecutorDInSARTSX or ExecutorDInSARS1IW.

        :param kwargs: parameters as kwargs (used to provide additionnal input to the selected executor)
        :type kwargs: dict-like
        """
        # Add for each mode, a dedicated executor
        if str(self._mode) == str(ChainModes.OTHERS):
            self._input_enum = DInSarInputKeysOthers
        elif str(self._mode) == str(ChainModes.TSX):
            self._input_enum = DInSarInputKeysTSX
        else:
            self._input_enum = DInSarInputKeysS1IW

        self._executor_builder.add_mode(ChainModes.OTHERS, ExecutorDInSAROthers)

        self._executor_builder.add_mode(ChainModes.TSX, ExecutorDInSARTSX)

        self._executor_builder.add_mode(ChainModes.S1_IW, ExecutorDInSARS1IW)

        super().execute(**kwargs)


# Executors, one per mode
class ExecutorDInSAROthers(ExecutorDualImages):
    """Execute processing for DINSAR chain mode S1SM-CSK"""

    def _deformation_grid(self, output_dir):
        """Execute SARFineDeformationGrid"""
        # Get required inputs to build the fine deformation grid
        ml_reference = self._inputs[str(DInSarInputKeysOthers.ML_REFERENCE)]
        ml_secondary = self._inputs[str(DInSarInputKeysOthers.ML_SECONDARY)]
        demproj_reference = self._inputs[str(DInSarInputKeysOthers.DEMPROJ_REFERENCE)]
        demproj_secondary = self._inputs[str(DInSarInputKeysOthers.DEMPROJ_SECONDARY)]

        # Get output path
        # pylint: disable=no-member
        fine_grid_path = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_FINEGRID.get_key()
            ),
        )
        # pylint: enable=no-member

        # Instanciate and execute SARFineDeformationGrid
        app_grid = OTBApplicationWrapper("SARFineDeformationGrid")
        app_grid.set_input_images(
            insarmaster=self.reference_path(),
            insarslave=self.secondary_path(),
            inmlmaster=ml_reference,
            inmlslave=ml_secondary,
            indemprojmaster=demproj_reference,
            indemprojslave=demproj_secondary,
            indem=self._inputs[str(DInSarInputKeysOthers.DEM)],
        )
        app_grid.set_parameters(
            mlran=self.param_handler.get_param(str(DInSarParamOthers.MLRAN_GRID)),
            mlazi=self.param_handler.get_param(str(DInSarParamOthers.MLAZI_GRID)),
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_AZI)
            ),
            threshold=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_THRESHOLD)
            ),
            gap=self.param_handler.get_param(str(DInSarParamOthers.GRID_GAP)),
            advantage=self.param_handler.get_param(str(DInSarParamOthers.ADVANTAGE)),
        )
        app_grid.set_output_images(out=fine_grid_path)
        app_grid.execute_app(in_memory=False)

        # Return the output grid
        return fine_grid_path

    def _coregistration(self, output_dir, grid):
        """CoRegistration"""
        # Get required inputs to launch coregistration
        # Inputs are list => retrieve only the current index (burst)
        doppler0_secondary = self._inputs[str(DInSarInputKeysOthers.DOP0_SECONDARY)]

        # Get output path
        # pylint: disable=no-member
        coregistred_image_path = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_COREGISTRATION.get_key()
            ),
        )
        # pylint: enable=no-member

        # Define some hard-coded parameters
        nb_ramps = 257
        tiles_size = 50
        margin = 7

        # Instanciate and execute SARCoRegistration
        app_core = OTBApplicationWrapper("SARCoRegistration")
        app_core.set_input_images(
            insarmaster=self.reference_path(),
            insarslave=self.secondary_path(),
            ingrid=grid,
        )
        app_core.set_parameters(
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_AZI)
            ),
            doppler0=doppler0_secondary,
            sizetiles=tiles_size,
            margin=margin,
            nbramps=nb_ramps,
        )
        app_core.set_output_images(out=coregistred_image_path)
        app_core.execute_app(in_memory=False)

        # Return the output image
        return coregistred_image_path

    def _interferogram(self, output_dir, grid, coregisted):
        """Build the interferogram"""
        # Get required inputs to launch coregistration
        # Inputs are list => retrieve only the current index (burst)
        cartmean_reference = self._inputs[
            str(DInSarInputKeysOthers.CARTESIAN_ESTIMATION_REFERENCE)
        ]

        # Get output path
        # pylint: disable=no-member
        interferogram_path = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_INTERF.get_key()
            ),
        )
        # pylint: enable=no-member

        # Define some hard-coded parameters
        marginran = 1
        marginazi = 1

        # Have different ml factors than processing
        # These factors still have to be a multiple of GRIDSteps
        mlran = self.param_handler.get_param(str(DInSarParamOthers.ML_INTERFRAN))
        mlazi = self.param_handler.get_param(str(DInSarParamOthers.ML_INTERFAZI))

        # Instanciate and execute SARRobustInterferogram
        app_interf = OTBApplicationWrapper("SARRobustInterferogram")
        app_interf.set_input_images(
            insarmaster=self.reference_path(),
            insarslave=self.secondary_path(),
            ingrid=grid,
            incoregistratedslave=coregisted,
            incartmeanmaster=cartmean_reference,
        )
        app_interf.set_parameters(
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_AZI)
            ),
            mlran=mlran,
            mlazi=mlazi,
            marginran=marginran,
            marginazi=marginazi,
            gain=self.param_handler.get_param(str(DInSarParamOthers.INTERF_GAIN)),
        )
        app_interf.set_output_images(out=interferogram_path)
        app_interf.execute_app(in_memory=False)

        # Return the output interferogram
        return interferogram_path

    def execute(self):
        """DInSar chain for S1SM and CSK sensors

        Several applications are called here : SARFineDeformationGrid, SARCoRegistration and SARRobustInterferogram.

        These applications have the self._reference_image and self._secondary_image as inputs and put outputs in self._output_dir
        """

        # retrieve input : output_dir
        output_dir = self._inputs[str(RequiredKeysForDualImagesProcessing.OUTPUT_DIR)]

        self.file_handler_secondary.create_intermediate_names()

        # SARFineDeformationGrid
        grid = self._deformation_grid(output_dir)

        # SARCoRegistration
        coregistred = self._coregistration(output_dir, grid)

        # SARRobustInterferogram
        interferogram = self._interferogram(output_dir, grid, coregistred)

        # Assign outputs
        self._outputs[str(DInSarOutputKeysOthers.GRIDS)] = grid
        self._outputs[str(DInSarOutputKeysOthers.INTERFERO)] = interferogram
        self._outputs[str(DInSarOutputKeysOthers.COREGISTRATED_SECONDARY)] = coregistred
        # Extend tmp_list
        self._all_files_list.append(grid)
        self._all_files_list.append(coregistred)
        self._all_files_list.append(interferogram)


class ExecutorDInSARTSX(ExecutorDualImages):
    """Execute processing for DINSAR chain mode TSX"""

    def _deformation_grid(self, output_dir):
        """Execute SARFineDeformationGrid"""
        # Get required inputs to build the fine deformation grid
        resample_deramp_reference = self._inputs[
            str(DInSarInputKeysTSX.DERAMP_REFERENCE)
        ]
        resample_deramp_secondary = self._inputs[
            str(DInSarInputKeysTSX.DERAMP_RESAMPLE_SECONDARY)
        ]
        ml_reference = self._inputs[str(DInSarInputKeysTSX.ML_REFERENCE)]
        ml_secondary = self._inputs[str(DInSarInputKeysTSX.ML_SECONDARY)]
        demproj_reference = self._inputs[str(DInSarInputKeysTSX.DEMPROJ_REFERENCE)]
        demproj_secondary = self._inputs[str(DInSarInputKeysTSX.DEMPROJ_SECONDARY)]

        # Get output path
        # pylint: disable=no-member
        fine_grid_path = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_FINEGRID.get_key()
            ),
        )
        # pylint: enable=no-member

        # Instanciate and execute SARFineDeformationGrid
        app_grid = OTBApplicationWrapper("SARFineDeformationGrid")
        app_grid.set_input_images(
            insarmaster=resample_deramp_reference,
            insarslave=resample_deramp_secondary,
            inmlmaster=ml_reference,
            inmlslave=ml_secondary,
            indemprojmaster=demproj_reference,
            indemprojslave=demproj_secondary,
            indem=self._inputs[str(DInSarInputKeysTSX.DEM)],
        )
        app_grid.set_parameters(
            mlran=self.param_handler.get_param(str(DInSarParamOthers.MLRAN_GRID)),
            mlazi=self.param_handler.get_param(str(DInSarParamOthers.MLAZI_GRID)),
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_AZI)
            ),
            threshold=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_THRESHOLD)
            ),
            gap=self.param_handler.get_param(str(DInSarParamOthers.GRID_GAP)),
            advantage=self.param_handler.get_param(str(DInSarParamOthers.ADVANTAGE)),
        )
        app_grid.set_output_images(out=fine_grid_path)
        app_grid.execute_app(in_memory=False)

        # Return the output grid
        return fine_grid_path

    def _coregistration(self, output_dir, grid):
        """CoRegistration"""
        # Get required inputs to launch coregistration
        resample_deramp_reference = self._inputs[
            str(DInSarInputKeysTSX.DERAMP_REFERENCE)
        ]
        resample_deramp_secondary = self._inputs[
            str(DInSarInputKeysTSX.DERAMP_RESAMPLE_SECONDARY)
        ]
        doppler0_secondary = self._inputs[str(DInSarInputKeysTSX.DOP0_SECONDARY)]

        # Get output path
        # pylint: disable=no-member
        coregistred_image_path = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_COREGISTRATION.get_key()
            ),
        )
        # pylint: enable=no-member

        # Define some hard-coded parameters
        nb_ramps = 257
        tiles_size = 50
        margin = 7

        # Instanciate and execute SARCoRegistration
        app_core = OTBApplicationWrapper("SARCoRegistration")
        app_core.set_input_images(
            insarmaster=resample_deramp_reference,
            insarslave=resample_deramp_secondary,
            ingrid=grid,
        )
        app_core.set_parameters(
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_AZI)
            ),
            doppler0=doppler0_secondary,
            sizetiles=tiles_size,
            margin=margin,
            nbramps=nb_ramps,
        )
        app_core.set_output_images(out=coregistred_image_path)
        app_core.execute_app(in_memory=False)

        # Return the output image
        return coregistred_image_path

    def _reramp_image(self, output_dir, image, grid=None):
        """Apply a reramping on input image following a defromation grid (if present)
        The input image could be the coregistred or filtered reference
        """
        # Apply deramp application to reramp the image
        reramp = "true"

        # Instanciate deramp application
        app_deramp = OTBApplicationWrapper("SARDeramp")

        # If grid : reramp the coregistrated secondary
        # Otherwise : filtered reference
        if grid:
            # Apply deramp application to reramp the image with a deformation grid
            shift = "true"

            # Get required inputs to launch reramping
            resample_deramp_secondary = self._inputs[
                str(DInSarInputKeysTSX.DERAMP_RESAMPLE_SECONDARY)
            ]

            # Get output path
            # pylint: disable=no-member
            image_deramp_path = os.path.join(
                output_dir,
                self.file_handler_secondary.get_filename(
                    DInSarFilenames.FILES_AFTER_DERAMP.get_key()
                ),
            )
            # pylint: enable=no-member

            # Define inputs/outpus/parameters for SARDeramp
            app_deramp.set_input_images(
                in_=image, ingrid=grid, inslave=resample_deramp_secondary
            )
            app_deramp.set_parameters(
                gridsteprange=self.param_handler.get_param(
                    str(DInSarParamOthers.GRID_STEP_RAN)
                ),
                gridstepazimut=self.param_handler.get_param(
                    str(DInSarParamOthers.GRID_STEP_AZI)
                ),
                reramp=reramp,
                shift=shift,
            )
            app_deramp.set_output_images(out=image_deramp_path)
        else:
            # Apply deramp application to reramp the image without a deformation grid
            shift = "false"

            # Get output path
            # pylint: disable=no-member
            image_deramp_path = os.path.join(
                output_dir,
                self.file_handler_secondary.get_filename(
                    DInSarFilenames.FILES_AFTER_FILTERING_DERAMP.get_key()
                ),
            )
            # pylint: enable=no-member

            # Execute SARDeramp
            app_deramp.set_input_images(in_=image)
            app_deramp.set_parameters(reramp=reramp, shift=shift)
            app_deramp.set_output_images(out=image_deramp_path)

        # Execute SARDeramp
        app_deramp.execute_app(in_memory=False)

        # Return the output
        return image_deramp_path

    def _interferogram(self, output_dir, grid, reramp_coregisted, reramp_filtred_ref):
        """Build the interferogram"""
        # Get required inputs to build interferogram
        cartmean_reference = self._inputs[
            str(DInSarInputKeysTSX.CARTESIAN_ESTIMATION_REFERENCE)
        ]
        # Keep deramp secondary (only metadata used, here => deramp eq to reramp)
        resample_deramp_secondary = self._inputs[
            str(DInSarInputKeysTSX.DERAMP_RESAMPLE_SECONDARY)
        ]

        # Get output path
        # pylint: disable=no-member
        interferogram_path = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_INTERF.get_key()
            ),
        )
        # pylint: enable=no-member

        # Define some hard-coded parameters
        marginran = 1
        marginazi = 1

        # Have different ml factors than processing
        # These factors still have to be a multiple of GRIDSteps
        mlran = self.param_handler.get_param(str(DInSarParamOthers.ML_INTERFRAN))
        mlazi = self.param_handler.get_param(str(DInSarParamOthers.ML_INTERFAZI))

        # Instanciate and execute SARRobustInterferogram
        app_interf = OTBApplicationWrapper("SARRobustInterferogram")
        app_interf.set_input_images(
            insarmaster=reramp_filtred_ref,
            insarslave=resample_deramp_secondary,
            ingrid=grid,
            incoregistratedslave=reramp_coregisted,
            incartmeanmaster=cartmean_reference,
        )
        app_interf.set_parameters(
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamOthers.GRID_STEP_AZI)
            ),
            mlran=mlran,
            mlazi=mlazi,
            marginran=marginran,
            marginazi=marginazi,
            gain=self.param_handler.get_param(str(DInSarParamOthers.INTERF_GAIN)),
        )
        app_interf.set_output_images(out=interferogram_path)
        app_interf.execute_app(in_memory=False)

        # Return the output interferogram
        return interferogram_path

    def execute(self):
        """DInSar chain for TSX/PAZ sensors

        Several applications are called here : SARFineDeformationGrid, SARCoRegistration SARDeramp and SARRobustInterferogram.

        These applications have the self._reference_image and self._secondary_image as inputs and put outputs in self._output_dir.

        A filtering (from internal_processing package) may also be applied on reference_image
        """

        # retrieve input : output_dir
        output_dir = self._inputs[str(RequiredKeysForDualImagesProcessing.OUTPUT_DIR)]

        self.file_handler_secondary.create_intermediate_names()

        # SARFineDeformationGrid
        grid = self._deformation_grid(output_dir)

        # SARCoRegistration
        coregistred = self._coregistration(output_dir, grid)

        # Filtering on reference
        deramp_reference = self._inputs[str(DInSarInputKeysTSX.DERAMP_REFERENCE)]
        deramp_secondary = self._inputs[str(DInSarInputKeysTSX.DERAMP_SECONDARY)]

        # Do a filtering on deramp reference with native secondary metadata
        # Filtering mayn use tmp to improve performance
        tmp_dir = ""
        if self._tmp_path:
            tmp_dir = self._tmp_path

        # main_filtering function can decide if filtering is necessary or not
        # if filtering is not applied, reference input file (aka deramp_reference) is returned
        filtered_reference = main_filtering(
            deramp_reference, deramp_secondary, output_dir, tmp_dir
        )

        # Reramp on filtered reference and coregisted, if required (not in ST mode)
        ref_reramp = filtered_reference
        coregistred_reramp = coregistred
        if self.param_handler.get_param(str(DInSarParamTSX.ACTIVATE_DERAMP)):
            ref_reramp = self._reramp_image(output_dir, filtered_reference)
            coregistred_reramp = self._reramp_image(output_dir, coregistred, grid)
        else:
            utils.logger.info("Reramping is disabled (mode : ST)")

        # SARRobustInterferogram
        interferogram = self._interferogram(
            output_dir, grid, coregistred_reramp, ref_reramp
        )

        # Assign outputs
        self._outputs[str(DInSarOutputKeysOthers.GRIDS)] = grid
        self._outputs[str(DInSarOutputKeysOthers.INTERFERO)] = interferogram
        self._outputs[str(DInSarOutputKeysOthers.COREGISTRATED_SECONDARY)] = (
            coregistred_reramp
        )
        # Extend tmp_list
        self._all_files_list.append(grid)
        self._all_files_list.append(coregistred)
        self._all_files_list.append(interferogram)

        if coregistred_reramp != coregistred:
            self._all_files_list.append(coregistred_reramp)

        if ref_reramp != filtered_reference:
            self._all_files_list.append(ref_reramp)

        if filtered_reference != deramp_reference:
            self._all_files_list.append(filtered_reference)


class ExecutorDInSARS1IW(ExecutorDualImages):
    """Execute processing for DINSAR chain mode S1IW"""

    def _deformation_grid(self, burst_dir, id_loop):
        """Execute SARFineDeformationGrid"""
        # Get required inputs to build the fine deformation grid
        # Inputs are list => retrieve only the current index (burst)
        deramp_reference = self._inputs[str(DInSarInputKeysS1IW.DERAMP_REFERENCE)][
            id_loop
        ]
        deramp_secondary = self._inputs[str(DInSarInputKeysS1IW.DERAMP_SECONDARY)][
            id_loop
        ]
        ml_reference = self._inputs[str(DInSarInputKeysS1IW.ML_REFERENCE)][id_loop]
        ml_secondary = self._inputs[str(DInSarInputKeysS1IW.ML_SECONDARY)][id_loop]
        demproj_reference = self._inputs[str(DInSarInputKeysS1IW.DEMPROJ_REFERENCE)][
            id_loop
        ]
        demproj_secondary = self._inputs[str(DInSarInputKeysS1IW.DEMPROJ_SECONDARY)][
            id_loop
        ]

        # Get output path
        # pylint: disable=no-member
        fine_grid_path = os.path.join(
            burst_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_FINEGRID.get_key()
            ),
        )
        # pylint: enable=no-member

        # Instanciate and execute SARFineDeformationGrid
        app_grid = OTBApplicationWrapper("SARFineDeformationGrid")
        app_grid.set_input_images(
            insarmaster=deramp_reference,
            insarslave=deramp_secondary,
            inmlmaster=ml_reference,
            inmlslave=ml_secondary,
            indemprojmaster=demproj_reference,
            indemprojslave=demproj_secondary,
            indem=self._inputs[str(DInSarInputKeysS1IW.DEM)],
        )
        app_grid.set_parameters(
            mlran=self.param_handler.get_param(str(DInSarParamS1IW.MLRAN_GRID)),
            mlazi=self.param_handler.get_param(str(DInSarParamS1IW.MLAZI_GRID)),
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_AZI)
            ),
            threshold=self.param_handler.get_param(str(DInSarParamS1IW.GRID_THRESHOLD)),
            gap=self.param_handler.get_param(str(DInSarParamS1IW.GRID_GAP)),
            advantage=self.param_handler.get_param(str(DInSarParamS1IW.ADVANTAGE)),
        )
        app_grid.set_output_images(out=fine_grid_path)
        app_grid.execute_app(in_memory=False)

        # Return the output grid
        return fine_grid_path

    def _coregistration(self, burst_dir, id_loop, grid):
        """CoRegistration"""
        # Get required inputs to launch coregistration
        # Inputs are list => retrieve only the current index (burst)
        deramp_reference = self._inputs[str(DInSarInputKeysS1IW.DERAMP_REFERENCE)][
            id_loop
        ]
        deramp_secondary = self._inputs[str(DInSarInputKeysS1IW.DERAMP_SECONDARY)][
            id_loop
        ]
        doppler0_secondary = self._inputs[str(DInSarInputKeysS1IW.DOP0_SECONDARY)][
            id_loop
        ]

        # Get output path
        # pylint: disable=no-member
        coregistred_image_path = os.path.join(
            burst_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_COREGISTRATION.get_key()
            ),
        )
        # pylint: enable=no-member

        # Define some hard-coded parameters
        nb_ramps = 256 * 2 * 10 + 1
        tiles_size = 50
        margin = 7

        # Instanciate and execute SARCoRegistration
        app_core = OTBApplicationWrapper("SARCoRegistration")
        app_core.set_input_images(
            insarmaster=deramp_reference, insarslave=deramp_secondary, ingrid=grid
        )
        app_core.set_parameters(
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_AZI)
            ),
            doppler0=doppler0_secondary,
            sizetiles=tiles_size,
            margin=margin,
            nbramps=nb_ramps,
        )
        app_core.set_output_images(out=coregistred_image_path)
        app_core.execute_app(in_memory=False)

        # Return the output image
        return coregistred_image_path

    def _reramp_coregistred(self, burst_dir, id_loop, grid, coregistred_image):
        """Apply a reramping on coregistred_image following a defromation grid"""
        # Get required inputs to launch coregistration
        # Inputs are list => retrieve only the current index (burst)
        deramp_secondary = self._inputs[str(DInSarInputKeysS1IW.DERAMP_SECONDARY)][
            id_loop
        ]

        # Get output path
        # pylint: disable=no-member
        coregistred_deramp_path = os.path.join(
            burst_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_DERAMP.get_key()
            ),
        )
        # pylint: enable=no-member

        # Apply deramp application to reramp the image with a deformation grid
        reramp = "true"
        shift = "true"

        # Instanciate and execute SARDeramp
        app_deramp = OTBApplicationWrapper("SARDeramp")
        app_deramp.set_input_images(
            in_=coregistred_image, ingrid=grid, inslave=deramp_secondary
        )
        app_deramp.set_parameters(
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_AZI)
            ),
            reramp=reramp,
            shift=shift,
        )
        app_deramp.set_output_images(out=coregistred_deramp_path)
        app_deramp.execute_app(in_memory=False)

        # Return the output
        return coregistred_deramp_path

    def _interferogram(self, burst_dir, id_loop, grid, reramp_coregisted):
        """Build the interferogram"""
        # Get required inputs to launch coregistration
        # Inputs are list => retrieve only the current index (burst)
        burst_reference = self._inputs[str(DInSarInputKeysS1IW.BURSTS_REFERENCE)][
            id_loop
        ]
        burst_secondary = self._inputs[str(DInSarInputKeysS1IW.BURSTS_SECONDARY)][
            id_loop
        ]
        cartmean_reference = self._inputs[
            str(DInSarInputKeysS1IW.CARTESIAN_ESTIMATION_REFERENCE)
        ][id_loop]

        # Get output path
        output_dir = burst_dir
        esd_nbiter = self.param_handler.get_param(str(DInSarParamS1IW.ESD_ITER))
        if esd_nbiter > 0:
            output_dir = os.path.join(burst_dir, "esd")

        # pylint: disable=no-member
        interferogram_path = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_INTERF.get_key()
            ),
        )
        # pylint: enable=no-member

        # Define some hard-coded parameters
        # ml factors have to be 1x1 here to call esd, then.
        # margin ran and azi (use to apply an average to the final value) set to 1
        mlran = 1
        mlazi = 1

        # Instanciate and execute SARRobustInterferogram
        app_interf = OTBApplicationWrapper("SARRobustInterferogram")
        app_interf.set_input_images(
            insarmaster=burst_reference,
            insarslave=burst_secondary,
            ingrid=grid,
            incoregistratedslave=reramp_coregisted,
            incartmeanmaster=cartmean_reference,
        )
        app_interf.set_parameters(
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_AZI)
            ),
            mlran=mlran,
            mlazi=mlazi,
            marginran=1,
            marginazi=1,
            gain=self.param_handler.get_param(str(DInSarParamS1IW.INTERF_GAIN)),
        )
        app_interf.set_output_images(out=interferogram_path)
        app_interf.execute_app(in_memory=False)

        # Return the output interferogram
        return interferogram_path

    def _concatenate(self, output_dir, interferogram_list, first_burst_index):
        """Concatenate all interferograms"""
        # Get required inputs
        reference_image = self._inputs[
            str(RequiredKeysForDualImagesProcessing.REFERENCE_IMAGE)
        ]
        reference_dir = self._inputs[
            str(RequiredKeysForDualImagesProcessing.REFERENCE_DIR)
        ]

        # Get output path
        # pylint: disable=no-member
        concatenate_path = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_CONCATENATE.get_key()
            ),
        )
        # pylint: enable=no-member

        # Instanciate and execute SARRobustInterferogram
        app_concatenate = OTBApplicationWrapper("SARConcatenateBursts")
        app_concatenate.set_input_images(
            insar=os.path.join(reference_dir, reference_image), il=interferogram_list
        )
        app_concatenate.set_parameters(burstindex=first_burst_index)
        app_concatenate.set_output_images(out=concatenate_path)
        app_concatenate.execute_app(in_memory=False)

        # Return the output interferogram
        return concatenate_path

    def execute_one_burst(self, burst_dir, id_loop):
        """Execute DInSAR chain for each burst"""
        # self.file_handler_secondary.create_intermediate_names(burst_id=burst_id)
        grid = self._deformation_grid(burst_dir, id_loop)

        coregistred_image = self._coregistration(burst_dir, id_loop, grid)

        reramp_coregistred = self._reramp_coregistred(
            burst_dir, id_loop, grid, coregistred_image
        )

        interferogram = self._interferogram(
            burst_dir, id_loop, grid, reramp_coregistred
        )

        return grid, coregistred_image, reramp_coregistred, interferogram

    def _esd_estimation(self, burst_dir, id_loop, burst_id, interferogram_list):
        """ESD processing"""
        reference_image = self._inputs[
            str(RequiredKeysForDualImagesProcessing.REFERENCE_IMAGE)
        ]
        reference_dir = self._inputs[
            str(RequiredKeysForDualImagesProcessing.REFERENCE_DIR)
        ]

        esd_path_dummy = os.path.join(burst_dir, "esdout.tif")

        app_esd = OTBApplicationWrapper("SARESD")
        app_esd.set_input_images(
            insar=os.path.join(reference_dir, reference_image),
            ininterfup=interferogram_list[id_loop],
            ininterflow=interferogram_list[id_loop + 1],
        )
        app_esd.set_parameters(burstindex=burst_id, threshold=0.3, mlazi=1)
        app_esd.set_output_images(out=esd_path_dummy)
        app_esd.execute_app(in_memory=True)

        return app_esd.get_output_float_parameter("azishift")

    def _esd_shift(
        self, burst_id, id_loop, last_burst, azimut_shift_esd_global, azimut_shift_esd
    ):
        """Adjust azimut shift according to the burst_id"""
        # Adjust azimut shift according to the burstId
        azi_shift = 0.0
        last_burst_1 = last_burst - 1
        if int(burst_id) == (last_burst_1):
            # Only accumulation between iterations
            azimut_shift_esd_global[id_loop] += azimut_shift_esd[id_loop]
            azi_shift = azimut_shift_esd_global[id_loop]
        elif int(burst_id) == last_burst:
            # Same as the last_burst -1
            azi_shift = azimut_shift_esd_global[id_loop - 1]
        else:
            # Accumulation of means between the current burstId and the next index
            azimut_shift_esd_global[id_loop] += (
                azimut_shift_esd[id_loop] + azimut_shift_esd[id_loop + 1]
            ) / 2
            azi_shift = azimut_shift_esd_global[id_loop]

        return azi_shift

    def _esd_correction(self, burst_dir, id_loop, grid_list, azimut_shift):
        """Apply the azimut shift on grids and re-estimate an interferogram"""
        # retrieve inputs
        deramp_reference = self._inputs[str(DInSarInputKeysS1IW.DERAMP_REFERENCE)][
            id_loop
        ]
        deramp_secondary = self._inputs[str(DInSarInputKeysS1IW.DERAMP_SECONDARY)][
            id_loop
        ]
        doppler0_secondary = self._inputs[str(DInSarInputKeysS1IW.DOP0_SECONDARY)][
            id_loop
        ]
        burst_reference = self._inputs[str(DInSarInputKeysS1IW.BURSTS_REFERENCE)][
            id_loop
        ]
        burst_secondary = self._inputs[str(DInSarInputKeysS1IW.BURSTS_SECONDARY)][
            id_loop
        ]
        cartmean_reference = self._inputs[
            str(DInSarInputKeysS1IW.CARTESIAN_ESTIMATION_REFERENCE)
        ][id_loop]

        # Define dummy names (in memory Pipeline)
        grid_path_dummy = os.path.join(burst_dir, "grid.tif")
        coregistred_path_dummy = os.path.join(burst_dir, "coregistrated.tif")
        deramp_path_dummy = os.path.join(burst_dir, "reramp.tif")

        # Get output path for interferogram
        output_dir = os.path.join(burst_dir, "esd")

        # pylint: disable=no-member
        interferogram_path = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                DInSarFilenames.FILES_AFTER_INTERF.get_key()
            ),
        )
        # pylint: enable=no-member

        self._all_files_list.append(interferogram_path)

        # Apply the offset on the deformation grid (an offset only in azimut)
        app_grid = OTBApplicationWrapper("SARGridOffset")
        app_grid.set_input_images(ingrid=grid_list[id_loop])
        app_grid.set_parameters(offsetran=0, offsetazi=azimut_shift)
        app_grid.set_output_images(out=grid_path_dummy)
        app_grid.execute_app(in_memory=True)

        # Define some hard-coded parameters
        nb_ramps = 256 * 2 * 10 + 1
        tiles_size = 50
        margin = 7
        reramp = "true"
        shift = "true"
        marginran = 1
        marginazi = 1
        # ml factors have to be 1x1 to call esd, then.
        mlran = 1
        mlazi = 1

        # Instanciate and execute SARCoRegistration
        app_core = OTBApplicationWrapper("SARCoRegistration")
        app_core.set_input_images(
            insarmaster=deramp_reference,
            insarslave=deramp_secondary,
            ingrid=app_grid.get_output_image("out"),
        )
        app_core.set_parameters(
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_AZI)
            ),
            doppler0=doppler0_secondary,
            sizetiles=tiles_size,
            margin=margin,
            nbramps=nb_ramps,
        )
        app_core.set_output_images(out=coregistred_path_dummy)
        app_core.execute_app(in_memory=True)

        # Instanciate and execute SARDeramp
        app_deramp = OTBApplicationWrapper("SARDeramp")
        app_deramp.set_input_images(
            in_=app_core.get_output_image("out"),
            ingrid=app_grid.get_output_image("out"),
            inslave=deramp_secondary,
        )
        app_deramp.set_parameters(
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_AZI)
            ),
            reramp=reramp,
            shift=shift,
        )
        app_deramp.set_output_images(out=deramp_path_dummy)
        app_deramp.execute_app(in_memory=True)

        # Instanciate and execute SARRobustInterferogram
        app_interf = OTBApplicationWrapper("SARRobustInterferogram")
        app_interf.set_input_images(
            insarmaster=burst_reference,
            insarslave=burst_secondary,
            ingrid=app_grid.get_output_image("out"),
            incoregistratedslave=app_deramp.get_output_image("out"),
            incartmeanmaster=cartmean_reference,
        )
        app_interf.set_parameters(
            gridsteprange=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(DInSarParamS1IW.GRID_STEP_AZI)
            ),
            mlran=mlran,
            mlazi=mlazi,
            marginran=marginran,
            marginazi=marginazi,
            gain=self.param_handler.get_param(str(DInSarParamS1IW.INTERF_GAIN)),
        )
        app_interf.set_output_images(out=interferogram_path)
        app_interf.execute_app(in_memory=False)

        # Return the output interferogram
        return interferogram_path

    def _esd_loop(
        self, esd_nbiter, burst_ids_list, output_dir, grid_list, interferogram_list
    ):
        """ESD processing"""
        # Check the number of burst
        nb_burst = len(grid_list)

        if nb_burst < 2:
            utils.logger.warning(
                "At least two burst are required to laucnh esd processing"
            )
            # return input
            return interferogram_list
        self._all_files_list.extend(interferogram_list)
        # Empty shift
        azimut_shift_esd = []
        azimut_shift_esd_global = [0.0] * (len(burst_ids_list))

        current_interferogram_list = interferogram_list

        # ESD Loop
        for iter_esd in range(1, esd_nbiter + 1):
            # Clear all azimut shifts
            azimut_shift_esd[:] = []

            # loop on burst
            last_burst = burst_ids_list[-1]

            # First, we need to estimate for each burst the current shitf to apply on azimut
            for id_loop, burst_id in enumerate(burst_ids_list):
                burst_dir = os.path.join(output_dir, "burst" + str(burst_id))

                if burst_id < last_burst:
                    azimut_shift_esd_current = self._esd_estimation(
                        burst_dir, id_loop, burst_id, current_interferogram_list
                    )

                    azimut_shift_esd.append(azimut_shift_esd_current)

            # ReInit interferogram_list (new ref) to update the current interferograms for esd loop
            current_interferogram_list = []
            # ESD Correction for each burst
            for id_loop, burst_id in enumerate(burst_ids_list):
                burst_dir = os.path.join(output_dir, "burst" + str(burst_id))

                # Then, the azi_shift is calculated for each burst following next burst
                azi_shift = self._esd_shift(
                    burst_id,
                    id_loop,
                    int(last_burst),
                    azimut_shift_esd_global,
                    azimut_shift_esd,
                )

                # Change interferogram names
                ext = "_iter" + str(iter_esd)
                self.file_handler_secondary.create_intermediate_names(
                    burst_id=str(burst_id)
                )
                # pylint: disable=no-member
                self.file_handler_secondary.add_extension_to_filename(
                    DInSarFilenames.FILES_AFTER_INTERF.get_key(), ext
                )

                # pylint: enable=no-member
                # Eventually, apply the correction and re-build an interferogram
                interferogram_path = self._esd_correction(
                    burst_dir, id_loop, grid_list, azi_shift
                )
                current_interferogram_list.append(interferogram_path)

        return current_interferogram_list

    def execute(self):
        """DInSar chain for S1IW

        Several applications are called here : SARFineDeformationGrid, SARCoRegistration SARDeramp, SARRobustInterferogram and SARESD.

        These applications have the self._reference_image and self._secondary_image as inputs and put outputs in self._output_dir.
        """

        # retrieve input : output_dir
        output_dir = self._inputs[str(RequiredKeysForDualImagesProcessing.OUTPUT_DIR)]
        # Retrieve the specific parameters :
        # burst_ids : id of the burst
        # burst_to_process : burst to extract and then to process
        # (can be different for secondary image if burst ids
        # betwwen reference and secondary image do not match)
        burst_ids_list = self.param_handler.get_param(str(DInSarParamS1IW.BURSTIDS))

        # Empty lists
        grid_list = []
        coregistred_list = []
        reramp_coregistred_list = []
        interferogram_list = []

        # loop on burst
        for id_loop, burst_id in enumerate(burst_ids_list):

            # Init filenames (mostly use filename_secondary)
            self.file_handler_secondary.create_intermediate_names(
                burst_id=str(burst_id)
            )
            self.file_handler_reference.create_intermediate_names(
                burst_id=str(burst_id)
            )

            ext = "_iter" + str(0)
            # pylint: disable=no-member
            self.file_handler_secondary.add_extension_to_filename(
                DInSarFilenames.FILES_AFTER_INTERF.get_key(), ext
            )
            # pylint: enable=no-member

            # Output directory for the current burst
            burst_dir = os.path.join(output_dir, "burst" + str(burst_id))
            if self._tmp_path is not None:
                burst_dir = os.path.join(self._tmp_path, "burst" + str(burst_id))

            # Process the current burst
            grid, coregistred, reramp_coregistred, interferogram = (
                self.execute_one_burst(burst_dir, id_loop)
            )

            grid_list.append(grid)
            coregistred_list.append(coregistred)
            reramp_coregistred_list.append(reramp_coregistred)
            interferogram_list.append(interferogram)

        # ESD processing
        interferogram_esd_list = interferogram_list
        esd_nbiter = self.param_handler.get_param(str(DInSarParamS1IW.ESD_ITER))
        if esd_nbiter > 0:
            if self._tmp_path is not None:
                interferogram_esd_list = self._esd_loop(
                    esd_nbiter,
                    burst_ids_list,
                    self._tmp_path,
                    grid_list,
                    interferogram_list,
                )
            else:
                interferogram_esd_list = self._esd_loop(
                    esd_nbiter,
                    burst_ids_list,
                    output_dir,
                    grid_list,
                    interferogram_list,
                )

        # Concatenate bursts
        interferogram = self._concatenate(
            output_dir, interferogram_esd_list, burst_ids_list[0]
        )

        # Assign outputs
        self._outputs[str(DInSarOutputKeysS1IW.GRIDS)] = grid_list
        self._outputs[str(DInSarOutputKeysS1IW.COREGISTRATED_SECONDARY_RERAMP)] = (
            reramp_coregistred_list
        )
        self._outputs[str(DInSarOutputKeysS1IW.INTERFEROS)] = interferogram_esd_list
        self._outputs[str(DInSarOutputKeysS1IW.INTERFERO)] = interferogram
        # Extend tmp_list
        self._all_files_list.extend(grid_list)
        self._all_files_list.extend(reramp_coregistred_list)
        self._all_files_list.extend(interferogram_esd_list)
        self._all_files_list.extend(coregistred_list)
        self._all_files_list.append(interferogram)
