#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

"""
Factory to create every kind of processing
"""

from diapotb.lib.DInSAR import DInSAR
from diapotb.lib.Ground import Ground
from diapotb.lib.PostProcessing import PostProcessing
from diapotb.lib.PreProcessing import PreProcessing
from diapotb.lib.core.DiapOTBEnums import ChainNames, ChainModes
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException


# Factory
class DiapOTBProcessingFactory:
    """Factory to create all processing"""

    # Static dictionary to match the chain_name with a class
    find_class = {
        str(ChainNames.PRE_PROCESSING): PreProcessing,
        str(ChainNames.GROUND): Ground,
        str(ChainNames.DINSAR): DInSAR,
        str(ChainNames.POST_PROCESSING): PostProcessing,
    }

    def __init__(self, mode=ChainModes.OTHERS):
        self._mode = mode

    def create_processing(self, chain_name, **kwargs):
        """Classmethod to create a given processing

        :param chain_name: name of current chain from ChainNames
        :type chain_name: str
        :param kwargs: parameters as kwargs (several available keys : burst_id, ml_*)
        :type kwargs: dict-like

        :raises DiapOTBException: If chain_name is not kwown as available chain

        :return: instance of DiapOTBProcessing
        :rtype: DiapOTBProcessing
        """
        # Check if chain_name matchs to a processing
        if chain_name not in ChainNames.list():
            raise DiapOTBException(
                "Unkwown chain : "
                + str(chain_name)
                + ". Only these following chains are available: "
                + str(ChainNames.list())
            )

        # Add mode
        kwargs["mode"] = self._mode
        instance = self.find_class[str(chain_name)](**kwargs)
        return instance
