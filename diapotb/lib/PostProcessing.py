#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================
"""
Post Processing chain
"""
# pylint: disable=too-few-public-methods
# Specific enums

import logging
import os

from numpy import double

from diapotb.computed_clean_fringes import computed_clean_fringes
from diapotb.interf_its import assemble_HSV_image
from diapotb.lib.DInSAR import (
    DInSarOutputKeysOthers,
    DInSarOutputKeysS1IW,
    DInSarParamS1IW,
)
from diapotb.lib.Ground import GroundOutputKeys
from diapotb.lib.core.ApplicationWrapper import OTBApplicationWrapper
from diapotb.lib.core.DiapOTBEnums import (
    ExtendedEnum,
    ChainModes,
    ExtPosition,
    FilenamesEnum,
    RequiredKeysForDualImagesProcessing,
    extend_enum,
    DefaultEnum,
)
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException
from diapotb.lib.core.DiapOTBProcessing import (
    DiapOTBProcessingDualImages,
    ParamHandler,
    FilenamesHandler,
    ExecutorDualImages,
)
from diapotb.lib.core.Utils import log, add_wgs_projection, str2bool
from diapotb.lib.core.Utils import print_on_std


class PostProcessingParamS1IW(ExtendedEnum):
    """Define each required parameters for PostProcessing chain"""

    ML_FILT_AZI = "ml_filt_azi"
    ML_FILT_RAN = "ml_filt_ran"
    ML_FILT_GAIN = "ml_filt_interf_gain"
    FILT_ALPHA = "ml_filt_alpha"
    INTERF_GAIN = str(DInSarParamS1IW.INTERF_GAIN)
    SPACING_XY = "Spacingxy"
    ACTIVATE_ORTHO = "Activate_Ortho"
    ACTIVATE_FILTERING = "Activate_Filtering"
    ACTIVATE_ITS_PROCESSING = "Activate_Its_Processing"
    AMPLITUDE_MIN = "RGB_Interferogram_amplitude_min"
    AMPLITUDE_MAX = "RGB_Interferogram_amplitude_max"
    COHERENCE_MIN = "RGB_Interferogram_coherence_min"
    COHERENCE_MAX = "RGB_Interferogram_coherence_max"
    ACTIVATE_FRINGES_CLEANING = "Activate_Fringes_Cleaning"
    HORIZONTAL_DISTANCE = "Horizontal_Distance"
    VERTICAL_DISTANCE = "Vertical_Distance"
    HORIZONTAL_DISTANCE_INCREMENT = "Horizontal_Distance_Increment"
    VERTICAL_DISTANCE_INCREMENT = "Vertical_Distance_Increment"
    HORIZONTAL_DISTANCE_AUXILIAIRE = "Horizontal_Distance_Auxiliaire"
    VERTICAL_DISTANCE_AUXILIAIRE = "Vertical_Distance_Auxiliaire"
    HORIZONTAL_BORDER_SIZE = "Horizontal_Border_Size"
    VERTICAL_BORDER_SIZE = "Vertical_Border_Size"
    HORIZONTAL_LOWER_BOUND = "Horizontal_Lower_Bound"
    HORIZONTAL_UPPER_BOUND = "Horizontal_Upper_Bound"
    VERTICAL_LOWER_BOUND = "Vertical_Lower_Bound"
    VERTICAL_UPPER_BOUND = "Vertical_Upper_Bound"
    RATIO_HIST_1 = "Ratio_Hist_1"
    RATIO_HIST_2 = "Ratio_Hist_2"


@extend_enum(PostProcessingParamS1IW)
class PostProcessingParamOthers(ExtendedEnum):
    """Define each required parameters for PostProcessing chain"""

    GRID_STEP_RAN = str(DInSarParamS1IW.GRID_STEP_RAN)
    GRID_STEP_AZI = str(DInSarParamS1IW.GRID_STEP_AZI)


class PostProcessingDefaultValue(DefaultEnum):
    """Define some default values or redirect to other values
    The following paramaters are optional (other are mandatory)
    """

    ML_FILT_AZI = (str(PostProcessingParamS1IW.ML_FILT_AZI), 3)
    ML_FILT_RAN = (str(PostProcessingParamS1IW.ML_FILT_RAN), 3)
    ML_FILT_GAIN = (
        str(PostProcessingParamS1IW.ML_FILT_GAIN),
        str(DInSarParamS1IW.INTERF_GAIN),
    )
    FILT_ALPHA = (str(PostProcessingParamS1IW.FILT_ALPHA), 0.7)
    SPACING_XY = (str(PostProcessingParamS1IW.SPACING_XY), 0.0001)
    ACTIVATE_ORTHO = (str(PostProcessingParamS1IW.ACTIVATE_ORTHO), "yes")
    ACTIVATE_FILTERING = (str(PostProcessingParamS1IW.ACTIVATE_FILTERING), "yes")
    ACTIVATE_ITS_PROCESSING = (
        str(PostProcessingParamS1IW.ACTIVATE_ITS_PROCESSING),
        "no",
    )
    AMPLITUDE_MIN = (str(PostProcessingParamS1IW.AMPLITUDE_MIN), 7)
    AMPLITUDE_MAX = (str(PostProcessingParamS1IW.AMPLITUDE_MAX), 100)
    COHERENCE_MIN = (str(PostProcessingParamS1IW.COHERENCE_MIN), 0)
    COHERENCE_MAX = (str(PostProcessingParamS1IW.COHERENCE_MAX), 1)
    ACTIVATE_FRINGES_CLEANING = (
        str(PostProcessingParamS1IW.ACTIVATE_FRINGES_CLEANING),
        "no",
    )
    HORIZONTAL_DISTANCE = (str(PostProcessingParamS1IW.HORIZONTAL_DISTANCE), 10)
    VERTICAL_DISTANCE = (str(PostProcessingParamS1IW.VERTICAL_DISTANCE), 50)
    HORIZONTAL_DISTANCE_INCREMENT = (
        str(PostProcessingParamS1IW.HORIZONTAL_DISTANCE_INCREMENT),
        20,
    )
    VERTICAL_DISTANCE_INCREMENT = (
        str(PostProcessingParamS1IW.VERTICAL_DISTANCE_INCREMENT),
        20,
    )
    HORIZONTAL_DISTANCE_AUXILIAIRE = (
        str(PostProcessingParamS1IW.HORIZONTAL_DISTANCE_AUXILIAIRE),
        100,
    )
    VERTICAL_DISTANCE_AUXILIAIRE = (
        str(PostProcessingParamS1IW.VERTICAL_DISTANCE_AUXILIAIRE),
        100,
    )
    HORIZONTAL_BORDER_SIZE = (str(PostProcessingParamS1IW.HORIZONTAL_BORDER_SIZE), 50)
    VERTICAL_BORDER_SIZE = (str(PostProcessingParamS1IW.VERTICAL_BORDER_SIZE), 100)
    HORIZONTAL_LOWER_BOUND = (str(PostProcessingParamS1IW.HORIZONTAL_LOWER_BOUND), 128)
    HORIZONTAL_UPPER_BOUND = (str(PostProcessingParamS1IW.HORIZONTAL_UPPER_BOUND), 128)
    VERTICAL_LOWER_BOUND = (str(PostProcessingParamS1IW.VERTICAL_LOWER_BOUND), 50)
    VERTICAL_UPPER_BOUND = (str(PostProcessingParamS1IW.VERTICAL_UPPER_BOUND), 200)
    RATIO_HIST_1 = (str(PostProcessingParamS1IW.RATIO_HIST_1), 1.1)
    RATIO_HIST_2 = (str(PostProcessingParamS1IW.RATIO_HIST_2), 1.07)


class PostProcessingFilenames(FilenamesEnum):
    """Define key for intermediate/output filenames for PostProcesssing chain
    3 str to speficy, a key, the extension to add and the position of the extension in order to create the file
    """

    FILES_AFTER_PHASE_FILTERING = (
        "phase_files",
        "filfPhaCoh",
        str(ExtPosition.EXCLUSIF),
    )
    FILES_AFTER_FILTERING = (
        "filtered_files",
        "filtered_interferogram",
        str(ExtPosition.EXCLUSIF),
    )
    FILES_AFTER_ORTHO = (
        "ortho_files",
        "interferogram_ortho",
        str(ExtPosition.EXCLUSIF),
    )
    FILES_AFTER_ITS_PROCESSING = (
        "its_files",
        "rgb_interferogram",
        str(ExtPosition.EXCLUSIF),
    )
    FILES_AFTER_ORTHO_AND_ITS_PROCESSING = (
        "its_ortho_files",
        "rgb_ortho_interferogram",
        str(ExtPosition.EXCLUSIF),
    )
    FILES_AFTER_FILTERING_AND_ITS_PROCESSING = (
        "its_filtered_files",
        "rgb_filtered_interferogram",
        str(ExtPosition.EXCLUSIF),
    )
    FILES_AFTER_CLEANING_FRINGES = (
        "cleaning_fringes_files",
        "cleaned_fringes_interferogram",
        str(ExtPosition.EXCLUSIF),
    )


class PostProcessingInputKeysOthers(ExtendedEnum):
    """Define intput keys for PostProcessing chain"""

    CARTESIAN_ESTIMATION_REFERENCE = (
        str(GroundOutputKeys.CARTESIAN_ESTIMATION) + "_reference"
    )
    COREGISTRATED_SECONDARY = str(DInSarOutputKeysOthers.COREGISTRATED_SECONDARY)
    GRIDS = str(DInSarOutputKeysOthers.GRIDS)
    INTERFERO = str(DInSarOutputKeysOthers.INTERFERO)
    DEM_PATH = "dem_path"


class PostProcessingInputKeysS1IW(ExtendedEnum):
    """Define intput keys for PostProcessing chain"""

    INTERFERO = str(DInSarOutputKeysS1IW.INTERFERO)
    DEM_PATH = "dem_path"


class PostProcessingOutputKeys(ExtendedEnum):
    """Define output keys for PostProcessing chain"""

    FILT_INTERFERO = "filtered_interferogram"
    PHASE_INTERFERO = "filfPhaCoh"
    ORTHO_INTERFERO = "interferogram_ortho"
    RGB_INTERFERO = "interferogram_rgb"
    CLEANED_FRINGES_INTERFERO = "interferogram_cleaned_fringes"


# pylint: enable=too-few-public-methods


class PostProcessing(DiapOTBProcessingDualImages):
    """Use the module to launch PostProcessing chain.

    main function : execute
    """

    def __init__(self, **kwargs):
        # Base constructor to init required elts such as image/dir, output_dir and
        # parameters
        super().__init__(**kwargs)

        # Init the specific arguments for the DINSAR chain
        self._name = "PostProcessing"
        self._applications = ["SAR"]

        # Default mode : Others
        self._mode = str(ChainModes.OTHERS)
        if "mode" in kwargs and str(kwargs["mode"]) in ChainModes.list():
            self._mode = str(kwargs["mode"])

        # Init Handlers according to the mode
        param_enum = PostProcessingParamOthers
        file_enum = PostProcessingFilenames
        default_param_enum = PostProcessingDefaultValue
        self._inputs_list = PostProcessingInputKeysOthers.list()

        if self._mode == str(ChainModes.S1_IW):
            self._inputs_list = PostProcessingInputKeysS1IW.list()
            param_enum = PostProcessingParamS1IW

        self.param_handler = ParamHandler(param_enum, self._param, default_param_enum)

        self.file_handler_reference = FilenamesHandler(
            file_enum, self._reference_base, self._mode
        )

        self.file_handler_secondary = FilenamesHandler(
            file_enum, self._secondary_base, self._mode
        )

        # Get from parameter dictionary each argument
        self.param_handler.check_param()

        # Init a geoid_path to None
        self._geoid_path = None

    @property
    def geoid_path(self):
        return self._geoid_path

    @geoid_path.setter
    def geoid_path(self, path):
        if not os.path.exists(path):
            log(logging.WARNING, "input geoid does not exist")
        else:
            self._geoid_path = path

    def retrieve_output(self, key):
        """Retrieve a given output of PostProcessing chain"""
        if (
            not isinstance(key, PostProcessingOutputKeys)
            and key not in PostProcessingOutputKeys.list()
        ):
            raise DiapOTBException(
                "The current key is not a available "
                "output key for PostProcessing chain"
            )

        return self._dict_outputs[str(key)]

    # Process functions
    def execute(self, **kwargs):
        """PostProcessing chain

        Execute post-processing following current mode (from ChainModes) thanks to an Executor.
        Executor may be ExecutorPostProcessingOthers and ExecutorPostProcessingS1IW.

        :param kwargs: parameters as kwargs (used to provide additionnal input to the selected executor)
        :type kwargs: dict-like
        """
        # Add for each mode, a dedicated executor
        # Here : TSX have the same executor than Others (S1SM/CSK)
        if str(self._mode) in [str(ChainModes.OTHERS), str(ChainModes.TSX)]:
            self._input_enum = PostProcessingInputKeysOthers
        else:
            self._input_enum = PostProcessingInputKeysS1IW

        self._executor_builder.add_mode(ChainModes.OTHERS, ExecutorPostProcessingOthers)

        self._executor_builder.add_mode(ChainModes.TSX, ExecutorPostProcessingOthers)

        self._executor_builder.add_mode(ChainModes.S1_IW, ExecutorPostProcessingS1IW)

        # Add geoid_path, if present
        if self._geoid_path:
            kwargs["geoid_path"] = self._geoid_path

        super().execute(**kwargs)


# Executors, one per mode
class ExecutorPostProcessingOthers(ExecutorDualImages):
    """Execute processing for PostProcessing chain mode S1SM-CSK and TSX"""

    def _filtering(self, output_dir):
        """Filtering with Goldstein method the input interferogram"""
        # Retrieve inputs
        cartmean_reference = self._inputs[
            str(PostProcessingInputKeysOthers.CARTESIAN_ESTIMATION_REFERENCE)
        ]
        grid = self._inputs[str(PostProcessingInputKeysOthers.GRIDS)]
        coregistred = self._inputs[
            str(PostProcessingInputKeysOthers.COREGISTRATED_SECONDARY)
        ]

        # Define dummy names (in memory Pipeline)
        topo_phase_dummy = os.path.join(output_dir, "topo.tif")
        compensated_complex_dummy = os.path.join(output_dir, "complex.tif")

        # SARTopographicPhase wiht ml factors set to 1
        app_topo = OTBApplicationWrapper("SARTopographicPhase")
        app_topo.set_input_images(
            insarslave=self.secondary_path(),
            ingrid=grid,
            incartmeanmaster=cartmean_reference,
            insarmaster=self.reference_path(),
        )
        app_topo.set_parameters(
            mlran=1,
            mlazi=1,
            gridsteprange=self.param_handler.get_param(
                str(PostProcessingParamOthers.GRID_STEP_RAN)
            ),
            gridstepazimut=self.param_handler.get_param(
                str(PostProcessingParamOthers.GRID_STEP_AZI)
            ),
        )
        app_topo.set_output_images(out=topo_phase_dummy)
        app_topo.execute_app(in_memory=True)

        # SARCompensatedComplex
        app_complex = OTBApplicationWrapper("SARCompensatedComplex")
        app_complex.set_input_images(
            insarmaster=self.reference_path(),
            insarslave=coregistred,
            topographicphase=app_topo.get_output_image("out"),
        )
        app_complex.set_output_images(out=compensated_complex_dummy)
        app_complex.execute_app(in_memory=True)

        # SARPhaseFiltering
        # pylint: disable=no-member
        out_phase = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                PostProcessingFilenames.FILES_AFTER_PHASE_FILTERING.get_key()
            ),
        )
        app_phase = OTBApplicationWrapper("SARPhaseFiltering")
        app_phase.set_input_images(incomplex=app_complex.get_output_image("out"))
        app_phase.set_parameters(
            mlran=self.param_handler.get_param(
                str(PostProcessingParamOthers.ML_FILT_RAN)
            ),
            mlazi=self.param_handler.get_param(
                str(PostProcessingParamOthers.ML_FILT_AZI)
            ),
            step=16,
            sizetiles=64,
            alpha=self.param_handler.get_param(
                str(PostProcessingParamOthers.FILT_ALPHA)
            ),
        )
        # pylint: enable=no-member
        app_phase.set_output_images(out=out_phase)
        app_phase.execute_app(in_memory=False)

        # SARAddBandInterferogram
        # pylint: disable=no-member
        out_filtered = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                PostProcessingFilenames.FILES_AFTER_FILTERING.get_key()
            ),
        )

        app_amp = OTBApplicationWrapper("SARAddBandInterferogram")
        app_amp.set_input_images(
            incomplexamp=app_complex.get_output_image("out"), ininterf=out_phase
        )
        app_amp.set_parameters(
            mlran=self.param_handler.get_param(
                str(PostProcessingParamOthers.ML_FILT_RAN)
            ),
            mlazi=self.param_handler.get_param(
                str(PostProcessingParamOthers.ML_FILT_AZI)
            ),
            gain=self.param_handler.get_param(
                str(PostProcessingParamOthers.ML_FILT_GAIN)
            ),
        )
        # pylint: enable=no-member
        app_amp.set_output_images(out=out_filtered)
        app_amp.execute_app(in_memory=False)

        # Extend tmp_list
        self._all_files_list.append(out_phase)
        self._all_files_list.append(out_filtered)

        return out_filtered

    def _orthorectification(self, output_dir):
        """Orthorectification for input interferogram"""
        # Retrieve interferogram and dem_path and spacingxy
        interferogram = self._inputs[str(PostProcessingInputKeysS1IW.INTERFERO)]
        dem_path = self._inputs[str(PostProcessingInputKeysS1IW.DEM_PATH)]
        spacing_xy = self.param_handler.get_param(
            str(PostProcessingParamS1IW.SPACING_XY)
        )

        # Instanciate dictionaries to avoid "Keyword can’t be an expression" with . or in
        dict_in = {"io.in": interferogram, "elev.dem": dem_path}

        if "geoid_path" in self._inputs:
            dict_in["elev.geoid"] = self._inputs["geoid_path"]

        dict_param = {
            "opt.ram": OTBApplicationWrapper.RAM,
            "map": "epsg",
            "outputs.spacingx": str(double(spacing_xy)),
            "outputs.spacingy": str(-abs(double(spacing_xy))),
            "opt.gridspacing": str(abs(double(spacing_xy) * 10)),
            "outputs.mode": "autosize",
        }

        out_key = PostProcessingFilenames.FILES_AFTER_ORTHO.get_key()
        dict_out = {
            "io.out": os.path.join(
                output_dir, self.file_handler_secondary.get_filename(out_key)
            )
        }

        app_ortho = OTBApplicationWrapper("OrthoRectification")
        app_ortho.set_input_images(**dict_in)
        app_ortho.set_parameters(**dict_param)
        app_ortho.set_output_images(**dict_out)
        # Disable RAM parameter : this parameter is not available for OrthoRectification
        app_ortho.disable_ram_parameter()
        app_ortho.execute_app(in_memory=False)

        # Add projection
        add_wgs_projection(dict_out["io.out"])

        # Extend tmp_list
        self._all_files_list.append(dict_out["io.out"])

        return dict_out["io.out"]

    def _its_processing(self, input_file, output_file):
        """ITS processing : creating an RGB image from Intensity, Saturation & Hue"""
        amplitude_min = self.param_handler.get_param(
            str(PostProcessingParamS1IW.AMPLITUDE_MIN)
        )
        amplitude_max = self.param_handler.get_param(
            str(PostProcessingParamS1IW.AMPLITUDE_MAX)
        )
        coherence_min = self.param_handler.get_param(
            str(PostProcessingParamS1IW.COHERENCE_MIN)
        )
        coherence_max = self.param_handler.get_param(
            str(PostProcessingParamS1IW.COHERENCE_MAX)
        )
        assemble_HSV_image(
            input_file,
            amplitude_min,
            amplitude_max,
            coherence_min,
            coherence_max,
            output_file,
        )

    def _cleaning_fringes(self, input_file, output_file):
        """Clean fringe : clean up fringes in an image's phase"""
        horizontal_distance = self.param_handler.get_param(
            str(PostProcessingParamS1IW.HORIZONTAL_DISTANCE)
        )
        vertical_distance = self.param_handler.get_param(
            str(PostProcessingParamS1IW.VERTICAL_DISTANCE)
        )
        horizontal_distance_increment = self.param_handler.get_param(
            str(PostProcessingParamS1IW.HORIZONTAL_DISTANCE_INCREMENT)
        )
        vertical_distance_increment = self.param_handler.get_param(
            str(PostProcessingParamS1IW.VERTICAL_DISTANCE_INCREMENT)
        )
        horizontal_distance_auxiliaire = self.param_handler.get_param(
            str(PostProcessingParamS1IW.HORIZONTAL_DISTANCE_AUXILIAIRE)
        )
        vertical_distance_auxiliaire = self.param_handler.get_param(
            str(PostProcessingParamS1IW.VERTICAL_DISTANCE_AUXILIAIRE)
        )
        horizontal_border_size = self.param_handler.get_param(
            str(PostProcessingParamS1IW.HORIZONTAL_BORDER_SIZE)
        )
        vertical_border_size = self.param_handler.get_param(
            str(PostProcessingParamS1IW.VERTICAL_BORDER_SIZE)
        )
        horizontal_lower_bound = self.param_handler.get_param(
            str(PostProcessingParamS1IW.HORIZONTAL_LOWER_BOUND)
        )
        horizontal_upper_bound = self.param_handler.get_param(
            str(PostProcessingParamS1IW.HORIZONTAL_UPPER_BOUND)
        )
        vertical_lower_bound = self.param_handler.get_param(
            str(PostProcessingParamS1IW.VERTICAL_LOWER_BOUND)
        )
        vertical_upper_bound = self.param_handler.get_param(
            str(PostProcessingParamS1IW.VERTICAL_UPPER_BOUND)
        )
        ratio_hist_1 = self.param_handler.get_param(
            str(PostProcessingParamS1IW.RATIO_HIST_1)
        )
        ratio_hist_2 = self.param_handler.get_param(
            str(PostProcessingParamS1IW.RATIO_HIST_2)
        )
        cleaning_args = {
            "horizontal_distance": self.param_handler.get_param(
                str(PostProcessingParamS1IW.HORIZONTAL_DISTANCE)
            ),
            "vertical_distance": self.param_handler.get_param(
                str(PostProcessingParamS1IW.VERTICAL_DISTANCE)
            ),
            "horizontal_distance_increment": self.param_handler.get_param(
                str(PostProcessingParamS1IW.HORIZONTAL_DISTANCE_INCREMENT)
            ),
            "vertical_distance_increment": self.param_handler.get_param(
                str(PostProcessingParamS1IW.VERTICAL_DISTANCE_INCREMENT)
            ),
            "horizontal_distance_auxiliaire": self.param_handler.get_param(
                str(PostProcessingParamS1IW.HORIZONTAL_DISTANCE_AUXILIAIRE)
            ),
            "vertical_distance_auxiliaire": self.param_handler.get_param(
                str(PostProcessingParamS1IW.VERTICAL_DISTANCE_AUXILIAIRE)
            ),
            "horizontal_border_size": self.param_handler.get_param(
                str(PostProcessingParamS1IW.HORIZONTAL_BORDER_SIZE)
            ),
            "vertical_border_size": self.param_handler.get_param(
                str(PostProcessingParamS1IW.VERTICAL_BORDER_SIZE)
            ),
            "horizontal_lower_bound": self.param_handler.get_param(
                str(PostProcessingParamS1IW.HORIZONTAL_LOWER_BOUND)
            ),
            "horizontal_upper_bound": self.param_handler.get_param(
                str(PostProcessingParamS1IW.HORIZONTAL_UPPER_BOUND)
            ),
            "vertical_lower_bound": self.param_handler.get_param(
                str(PostProcessingParamS1IW.VERTICAL_LOWER_BOUND)
            ),
            "vertical_upper_bound": self.param_handler.get_param(
                str(PostProcessingParamS1IW.VERTICAL_UPPER_BOUND)
            ),
            "ratio_hist_1": self.param_handler.get_param(
                str(PostProcessingParamS1IW.RATIO_HIST_1)
            ),
            "ratio_hist_2": self.param_handler.get_param(
                str(PostProcessingParamS1IW.RATIO_HIST_2)
            ),
        }
        computed_clean_fringes(
            input_file,
            output_file,
            horizontal_distance,
            vertical_distance,
            horizontal_distance_increment,
            vertical_distance_increment,
            horizontal_distance_auxiliaire,
            vertical_distance_auxiliaire,
            horizontal_border_size,
            vertical_border_size,
            horizontal_lower_bound,
            horizontal_upper_bound,
            vertical_lower_bound,
            vertical_upper_bound,
            ratio_hist_1,
            ratio_hist_2,
        )

    def execute(self):
        """PostProcessing chain for S1SM, CSK and TSX sensors

        Several applications are called here : SARTopographicPhase, SARCompensatedComplex,
        SARPhaseFiltering, SARAddBandInterferogram and OrthoRectification.

        These applications have the self._image as input and put outputs in self._output_dir
        """
        # retrieve input : output_dir
        output_dir = self._inputs[str(RequiredKeysForDualImagesProcessing.OUTPUT_DIR)]

        self.file_handler_secondary.create_intermediate_names()

        out_filtered = ""
        out_ortho = ""

        if str2bool(
            self.param_handler.get_param(
                str(PostProcessingParamS1IW.ACTIVATE_ITS_PROCESSING)
            )
        ):
            self._its_processing(
                self._inputs[str(PostProcessingInputKeysS1IW.INTERFERO)],
                os.path.join(
                    output_dir,
                    self.file_handler_secondary.get_filename(
                        PostProcessingFilenames.FILES_AFTER_ITS_PROCESSING.get_key()
                    ),
                ),
            )
        if str2bool(
            self.param_handler.get_param(
                str(PostProcessingParamS1IW.ACTIVATE_FILTERING)
            )
        ):
            out_filtered = self._filtering(output_dir)
            if str2bool(
                self.param_handler.get_param(
                    str(PostProcessingParamS1IW.ACTIVATE_ITS_PROCESSING)
                )
            ):
                self._its_processing(
                    out_filtered,
                    os.path.join(
                        output_dir,
                        self.file_handler_secondary.get_filename(
                            PostProcessingFilenames.FILES_AFTER_FILTERING_AND_ITS_PROCESSING.get_key()
                        ),
                    ),
                )
        if str2bool(
            self.param_handler.get_param(str(PostProcessingParamS1IW.ACTIVATE_ORTHO))
        ):
            out_ortho = self._orthorectification(output_dir)
            if str2bool(
                self.param_handler.get_param(
                    str(PostProcessingParamS1IW.ACTIVATE_ITS_PROCESSING)
                )
            ):
                self._its_processing(
                    out_ortho,
                    os.path.join(
                        output_dir,
                        self.file_handler_secondary.get_filename(
                            PostProcessingFilenames.FILES_AFTER_ORTHO_AND_ITS_PROCESSING.get_key()
                        ),
                    ),
                )

        if str2bool(
            self.param_handler.get_param(
                str(PostProcessingParamS1IW.ACTIVATE_FRINGES_CLEANING)
            )
        ):
            self._cleaning_fringes(
                self._inputs[str(PostProcessingInputKeysS1IW.INTERFERO)],
                os.path.join(
                    output_dir,
                    self.file_handler_secondary.get_filename(
                        PostProcessingFilenames.FILES_AFTER_CLEANING_FRINGES.get_key()
                    ),
                ),
            )

        # Assign outputs
        self._outputs[str(PostProcessingOutputKeys.FILT_INTERFERO)] = out_filtered
        self._outputs[str(PostProcessingOutputKeys.ORTHO_INTERFERO)] = out_ortho


class ExecutorPostProcessingS1IW(ExecutorPostProcessingOthers):
    """Execute processing for PostProcessing chain mode S1IW"""

    def _filtering(self, output_dir):
        """Filtering with Goldstein method the input interferogram"""
        # Retrieve interferogram
        interferogram = self._inputs[str(PostProcessingInputKeysS1IW.INTERFERO)]

        # SARPhaseFiltering
        # pylint: disable=no-member
        out_phase = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                PostProcessingFilenames.FILES_AFTER_PHASE_FILTERING.get_key()
            ),
        )
        # pylint: enable=no-member
        app_phase = OTBApplicationWrapper("SARPhaseFiltering")
        app_phase.set_input_images(ininterf=interferogram)
        app_phase.set_parameters(
            mlran=self.param_handler.get_param(
                str(PostProcessingParamS1IW.ML_FILT_RAN)
            ),
            mlazi=self.param_handler.get_param(
                str(PostProcessingParamS1IW.ML_FILT_AZI)
            ),
            step=16,
            sizetiles=64,
            alpha=self.param_handler.get_param(str(PostProcessingParamS1IW.FILT_ALPHA)),
        )
        app_phase.set_output_images(out=out_phase)
        app_phase.execute_app(in_memory=False)

        # SARAddBandInterferogram
        # pylint: disable=no-member
        out_filtered = os.path.join(
            output_dir,
            self.file_handler_secondary.get_filename(
                PostProcessingFilenames.FILES_AFTER_FILTERING.get_key()
            ),
        )
        # pylint: enable=no-member
        app_amp = OTBApplicationWrapper("SARAddBandInterferogram")
        app_amp.set_input_images(ininterfamp=interferogram, ininterf=out_phase)
        # gain is put here to 1 (already applied during interferogram step)
        app_amp.set_parameters(
            mlran=self.param_handler.get_param(
                str(PostProcessingParamS1IW.ML_FILT_RAN)
            ),
            mlazi=self.param_handler.get_param(
                str(PostProcessingParamS1IW.ML_FILT_AZI)
            ),
            gain=1,
        )
        app_amp.set_output_images(out=out_filtered)
        app_amp.execute_app(in_memory=False)

        # Extend tmp_list
        self._all_files_list.append(out_phase)
        self._all_files_list.append(out_filtered)

        return out_phase, out_filtered

    def execute(self):
        """PostProcessing chain for S1IW sensors

        Several applications are called here : SARPhaseFiltering, SARAddBandInterferogram and OrthoRectification.
        """
        # retrieve input : output_dir
        output_dir = self._inputs[str(RequiredKeysForDualImagesProcessing.OUTPUT_DIR)]

        self.file_handler_secondary.create_intermediate_names()

        out_filtered = ""
        out_phase = ""
        out_ortho = ""

        if str2bool(
            self.param_handler.get_param(
                str(PostProcessingParamS1IW.ACTIVATE_ITS_PROCESSING)
            )
        ):
            self._its_processing(
                self._inputs[str(PostProcessingInputKeysS1IW.INTERFERO)],
                os.path.join(
                    output_dir,
                    self.file_handler_secondary.get_filename(
                        PostProcessingFilenames.FILES_AFTER_ITS_PROCESSING.get_key()
                    ),
                ),
            )
        if str2bool(
            self.param_handler.get_param(
                str(PostProcessingParamS1IW.ACTIVATE_FILTERING)
            )
        ):
            _, out_filtered = self._filtering(output_dir)
            if str2bool(
                self.param_handler.get_param(
                    str(PostProcessingParamS1IW.ACTIVATE_ITS_PROCESSING)
                )
            ):
                self._its_processing(
                    out_filtered,
                    os.path.join(
                        output_dir,
                        self.file_handler_secondary.get_filename(
                            PostProcessingFilenames.FILES_AFTER_FILTERING_AND_ITS_PROCESSING.get_key()
                        ),
                    ),
                )
        if str2bool(
            self.param_handler.get_param(str(PostProcessingParamS1IW.ACTIVATE_ORTHO))
        ):
            out_ortho = self._orthorectification(output_dir)
            if str2bool(
                self.param_handler.get_param(
                    str(PostProcessingParamS1IW.ACTIVATE_ITS_PROCESSING)
                )
            ):
                self._its_processing(
                    out_ortho,
                    os.path.join(
                        output_dir,
                        self.file_handler_secondary.get_filename(
                            PostProcessingFilenames.FILES_AFTER_ORTHO_AND_ITS_PROCESSING.get_key()
                        ),
                    ),
                )

        if str2bool(
            self.param_handler.get_param(
                str(PostProcessingParamS1IW.ACTIVATE_FRINGES_CLEANING)
            )
        ):
            self._cleaning_fringes(
                self._inputs[str(PostProcessingInputKeysS1IW.INTERFERO)],
                os.path.join(
                    output_dir,
                    self.file_handler_secondary.get_filename(
                        PostProcessingFilenames.FILES_AFTER_CLEANING_FRINGES.get_key()
                    ),
                ),
            )

        # Assign outputs
        self._outputs[str(PostProcessingOutputKeys.FILT_INTERFERO)] = out_filtered
        self._outputs[str(PostProcessingOutputKeys.ORTHO_INTERFERO)] = out_ortho
        self._outputs[str(PostProcessingOutputKeys.PHASE_INTERFERO)] = out_phase
