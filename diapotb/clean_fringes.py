import argparse
import json

from osgeo import gdal

import numpy as np


def apply_gradient_cleaning_rates(image: np.ndarray, rate1: float, rate2: float):
    nl, nc = image.shape
    yy, xx = np.meshgrid(np.arange(nc), np.arange(nl))
    zeros = image == 0

    image_normalized = image / np.max(image)

    nb_franges = yy * rate1 + xx * rate2
    result = np.array(256 * (image_normalized - nb_franges), dtype=np.uint8)
    result[zeros] = 0
    return np.array(2 * np.pi * result / 256, dtype=float)


def clean_fringes(
    input_image, output_image, gradient_file, gradient_range, gradient_azimut
):
    """ """
    reading_input_file = check_reading_input_file(
        gradient_azimut, gradient_file, gradient_range
    )

    nb_frames = input_image.RasterCount
    if nb_frames == 1:
        data = input_image.GetRasterBand(1).ReadAsArray()
    else:
        data = input_image.GetRasterBand(2).ReadAsArray()
    print(data)
    if reading_input_file:
        with open(gradient_file) as input_gradient_file:
            gradient = json.load(input_gradient_file)
            result = apply_gradient_cleaning_rates(
                data, gradient["horizontal gradient"], gradient["vertical gradient"]
            )
    else:
        result = apply_gradient_cleaning_rates(data, gradient_range, gradient_azimut)
    print(result)
    write_results(nb_frames, output_image, input_image, result)


def write_results(nb_frames, output_image, input_image, result):
    """Write the resulting image with GDAL"""
    driver = gdal.GetDriverByName("GTiff")
    rows, cols = result.shape
    if nb_frames == 1:
        dst_ds = driver.Create(output_image, cols, rows, nb_frames, gdal.GDT_Float32)
        dst_ds.GetRasterBand(1).WriteArray(result)
    # else:
    #     for i in range(1, nb_frames+1):
    #         print(i)
    #         dst_ds.GetRasterBand(i).WriteRaster(raster.GetRasterBand(i))
    #     dst_ds.GetRasterBand(2).WriteArray(result)
    else:
        dst_ds = driver.CreateCopy(output_image, input_image)
        dst_ds.GetRasterBand(2).WriteArray(result)

    dst_ds.FlushCache()
    dst_ds = None


def check_reading_input_file(gradient_azimut, gradient_file, gradient_range):
    """Check if we are reading gradient range from an input file"""
    reading_input_file = False
    if gradient_file is None:
        if gradient_range is None or gradient_azimut is None:
            parser.error(
                "Either gradient_file or both (gradient_range,gradient_file) is required"
            )
        else:
            reading_input_file = False
    else:
        reading_input_file = True
        if gradient_range or gradient_azimut:
            parser.error(
                "If gradient file is set fields (gradient_range,gradient_file) must be empty"
            )
    return reading_input_file


if __name__ == "__main__":

    usage = "clean_fringe.py [-h] --input_image INPUT_IMAGE --output_image OUTPUT_IMAGE (--gradient_range GRADIENT_RANGE --gradient_azimut GRADIENT_AZIMUT | --gradient_file GRADIENT_FILE)"
    parser = argparse.ArgumentParser(usage=usage)

    parser.add_argument(
        "--input_image",
        help="image containing phase to be cleaned. Image is either one band containing phase or 3 bands containing phase on band n°2",
    )

    parser.add_argument("--output_image", help="output image which have been cleaned")

    parser.add_argument(
        "--gradient_range",
        type=float,
        help="Horizontal distance for gradient computation on line",
    )

    parser.add_argument(
        "--gradient_azimut",
        type=float,
        help="Vertical distance for gradient computation on column",
    )

    parser.add_argument("--gradient_file", help="Input gradient file")

    args = parser.parse_args()

    raster = gdal.Open(args.input_image)

    clean_fringes(
        raster,
        args.output_image,
        args.gradient_file,
        args.gradient_range,
        args.gradient_azimut,
    )

    raster = None
