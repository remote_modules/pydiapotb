#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Romain Degoul & Valentin Genin (Thales)
#
# =========================================================================
from osgeo import gdal

from diapotb.compute_gradient_fringes import compute_gradient_cleaning_rates
from diapotb.clean_fringes import clean_fringes
from diapotb.lib.core.Utils import print_on_std


class HorizontalArguments:
    def __init__(
        self,
        horizontal_distance,
        horizontal_distance_increment,
        horizontal_distance_auxiliaire,
        horizontal_border_size,
        horizontal_lower_bound,
        horizontal_upper_bound,
    ):
        self.horizontal_distance = horizontal_distance
        self.horizontal_distance_increment = horizontal_distance_increment
        self.horizontal_distance_auxiliaire = horizontal_distance_auxiliaire
        self.horizontal_border_size = horizontal_border_size
        self.horizontal_lower_bound = horizontal_lower_bound
        self.horizontal_upper_bound = horizontal_upper_bound


class VerticalArguments:
    def __init__(
        self,
        vertical_distance,
        vertical_distance_increment,
        vertical_distance_auxiliaire,
        vertical_border_size,
        vertical_lower_bound,
        vertical_upper_bound,
    ):
        self.vertical_distance = vertical_distance
        self.vertical_distance_increment = vertical_distance_increment
        self.vertical_distance_auxiliaire = vertical_distance_auxiliaire
        self.vertical_border_size = vertical_border_size
        self.vertical_lower_bound = vertical_lower_bound
        self.vertical_upper_bound = vertical_upper_bound


def computed_clean_fringes(
    image: str,
    image_output: str,
    gap1: int,
    gap2: int,
    delta_gap_1: int,
    delta_gap_2: int,
    delta_prime_gap_1: int,
    delta_prime_gap_2: int,
    border1: int,
    border2: int,
    ind1_top_border: int,
    ind1_bottom_border: int,
    ind2_top_border: int,
    ind2_bottom_border: int,
    ratio_hist1: float,
    ratio_hist2: float,
):
    input_file = gdal.Open(image)
    nb_frames = input_file.RasterCount
    if nb_frames == 1:
        data = input_file.GetRasterBand(1).ReadAsArray()
    else:
        data = input_file.GetRasterBand(2).ReadAsArray()

    taux1, taux2 = compute_gradient_cleaning_rates(
        data,
        gap1,
        gap2,
        delta_gap_1,
        delta_gap_2,
        delta_prime_gap_1,
        delta_prime_gap_2,
        border1,
        border2,
        ind1_top_border,
        ind1_bottom_border,
        ind2_top_border,
        ind2_bottom_border,
        ratio_hist1,
        ratio_hist2,
    )
    clean_fringes(input_file, image_output, None, taux1, taux2)
    input_file = None
