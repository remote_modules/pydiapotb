#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

import os
import argparse
import glob
import logging
import tempfile
import shutil

try:
    import gdal
except ImportError:
    import osgeo.gdal as gdal

from diapotb.lib.core.ApplicationWrapper import OTBApplicationWrapper
from diapotb.ConfigFile import ConfigFile
from diapotb.lib.core.DiapOTBEnums import (
    ChainNames,
    ChainModes,
    ScriptNames,
    Satellite,
    Sensor,
)
from diapotb.lib.DInSAR import DInSarParamOthers, DInSarOutputKeysOthers, DInSarParamTSX
from diapotb.lib.PreProcessing import (
    PreProcessingParamOthers,
    PreProcessingOutputKeys,
    PreProcessingParamTSX,
)
from diapotb.lib.PostProcessing import (
    PostProcessingParamOthers,
    PostProcessingOutputKeys,
)
from diapotb.lib.Ground import GroundParamOthers

from diapotb.lib.DiapOTBProcessingFactory import DiapOTBProcessingFactory

import diapotb.lib.core.Utils as utils


def prepare_output(output_dir):
    """Prepare output directory to store the current processing"""
    if not os.path.exists(output_dir):
        print("The output directory does not exist and will be created")
        os.makedirs(output_dir)
    else:
        print("The output directory exists. Some files can be overwritten")


def rename_file(output_dir, filename_old, filename_new, only_meta=False):
    """Rename some files"""
    if only_meta:
        available_ext = [".geom", ".tif.aux.xml"]
    else:
        available_ext = [".tif", ".geom", ".tif.aux.xml"]

    for ext in available_ext:
        if os.path.exists(os.path.join(output_dir, filename_old + ext)):
            os.rename(
                os.path.join(output_dir, filename_old + ext),
                os.path.join(output_dir, filename_new + ext),
            )


def clean_output_dir(output_dir, all_files, files_to_keep):
    """Clean output directory to keep only file_to_keep"""
    # Define possible extensions
    output_ext = [".tif*", ".geom", ".txt"]

    # Keep only specified files
    for file_out in all_files:
        if file_out not in files_to_keep:
            file_abs = os.path.abspath(file_out)

            if os.path.exists(file_abs):
                # Select file to remove following :
                # the absolute path and output extensions in DiapOTB
                for ext in output_ext:
                    pattern_file = file_abs.split(".")[0] + ext

                    for f in glob.glob(pattern_file):
                        if os.path.exists(f):
                            os.remove(f)


if __name__ == "__main__":

    # Check arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "configfile", help="input conguration file for the application DiapOTB"
    )
    args = parser.parse_args()

    # Check and load configuration file
    config_handler = ConfigFile(args.configfile, str(ScriptNames.MULTI_SLC_S1SM))
    config_handler.load_configfile()

    # Prepare output and logger
    output_dir = config_handler.get_output_path()

    prepare_output(output_dir)

    utils.init_logger()
    utils.init_filelog(output_dir)

    # Retrieve parameters from configuration file
    param_dict = config_handler.create_param_dict_from_config_file(
        DInSarParamOthers, str(ChainNames.DINSAR)
    )

    param_dict_pre = config_handler.create_param_dict_from_config_file(
        PreProcessingParamOthers, str(ChainNames.PRE_PROCESSING)
    )

    # Append param dict with doppler_file
    param_dict_pre[str(PreProcessingParamOthers.DOPFILE)] = (
        config_handler.get_doppler_file()
    )

    param_dict_post = config_handler.create_param_dict_from_config_file(
        PostProcessingParamOthers, str(ChainNames.POST_PROCESSING)
    )

    param_dict_ground = config_handler.create_param_dict_from_config_file(
        GroundParamOthers, str(ChainNames.GROUND)
    )

    # Get and check main inputs : reference name + secondary path + date + dem paht and eof_path (if present)
    reference_image = config_handler.get_master_image()
    srtm_shapefile = config_handler.get_srtm_sfile()
    hgts_path = config_handler.get_srtm_path()
    geoid_path = config_handler.get_geoid()
    input_path = config_handler.get_input_path()
    start_date = config_handler.get_start_date()
    end_date = config_handler.get_end_date()
    eof_path = config_handler.get_eof()
    clean_tag = config_handler.get_clean()

    reference_path, pol, reference_ext, reference_date, reference_kwl = (
        utils.get_image_data(
            reference_image, input_path, str(ScriptNames.MULTI_SLC_S1SM)
        )
    )
    reference_dir = os.path.dirname(reference_path)

    # Check image size and required ml (to prevent a too high memory consumption)
    nb_lines, nb_col = utils.get_image_size(reference_kwl)
    if int(nb_col) > 15000 or int(nb_lines) > 15000:

        intermediate_mlran = int(nb_col) / 15000 + 1
        intermediate_mlazi = int(nb_lines) / 15000 + 1

        # Compare ml factor with intermediate
        if (intermediate_mlazi * intermediate_mlran) > (
            param_dict_pre[str(PreProcessingParamOthers.MLRAN)]
            * param_dict_pre[str(PreProcessingParamOthers.MLAZI)]
        ):
            utils.log(
                logging.CRITICAL,
                "ML Factors are not appropriate (too low) for these estimations. Please use other factors such as : {iran} x {iazi} \n".format(
                    iran=intermediate_mlran, iazi=intermediate_mlazi
                ),
            )

            quit()

    # Get all secondary images by selection (with dates), look at utils functions
    # + Handle the case, we have no secondary image
    exclude = "-9999"
    tiff_list, throw_warning = utils.get_all_tiff(
        pol=pol, ext=reference_ext, search_dir=input_path
    )
    tiff_date_list = utils.get_tiff_with_dates(
        start_date, end_date, exclude, tiff_list, reference_ext
    )
    tiff_date_list = utils.avoid_duplicates(tiff_date_list)

    if len(tiff_date_list) - 1 <= 0:
        utils.log(
            logging.CRITICAL,
            "ERROR : None secondary images found, please check your input path and your selection (dates, exclude ...)"
            + "\n",
        )
        quit()

    if throw_warning:
        # Indicate reference and all secondary images
        utils.print_on_std("Reference and Secondary images : ")
        utils.print_on_std(tiff_date_list)

    # Check on reference
    utils.check_image_format(reference_path)

    # Get and configure RAM parameter to launch each OTB applications
    ram_param = config_handler.get_ram()
    # Configure our OTBWrapper with the defined RAM
    OTBApplicationWrapper.configure_ram(ram_param)

    # Get satellite and sensor
    satellite = utils.get_sensor_from_md(reference_kwl)
    sensor = utils.convert_satelitte_to_sensor(satellite, ScriptNames.MULTI_SLC_S1SM)

    # Init empty list to persist generated files
    all_files_ref = []
    files_ref_to_keep = []

    # Prepare global and reference (for all burst)
    # ====== Create global folder with starting and ending dates + master date
    output_glob = "{}/output_{}_to_{}_m_{}".format(
        output_dir, start_date, end_date, reference_date
    )
    output_ref_dir = output_glob + "/{}_master_directory".format(reference_date)
    prepare_output(output_ref_dir)

    # Adapt geom files with eof files (if satellite is S1 and eof_paht not empty), only reference, first
    # change output_dir to output_ref
    reference_name = utils.apply_eof_path_on_orbit(
        satellite, eof_path, reference_path, reference_kwl, output_ref_dir
    )
    files_ref_to_keep.append(reference_name)
    # Create DEM (with utils function and dem_path)
    # ====== Check if images exist
    utils.check_if_exist(srtm_shapefile)
    utils.check_if_exist(hgts_path)
    utils.check_if_exist(reference_path)

    dem = utils.build_virutal_raster(
        reference_path, srtm_shapefile, hgts_path, output_glob
    )

    # Define projection strategy according to the current satellite
    if (
        satellite == str(Satellite.CSK)
        and str(DInSarParamOthers.ADVANTAGE) not in param_dict
    ):
        # Correlation if CSK
        param_dict[str(DInSarParamOthers.ADVANTAGE)] = "correlation"

    # Create our factory to build all processing following the current mode
    mode = ChainModes.OTHERS
    # Mode TSX if TSX, PAZ or TDX sensor
    if satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:
        mode = ChainModes.TSX

        # Handle ST mode by disabling deramping
        # ST mode is different than other modes (SL or Strimap) :
        # Deramping flattens the spectrum and adds some artefacts (blacks strips) on final interferogram
        is_sterring = utils.is_st_mode(satellite, reference_kwl)
        if is_sterring:
            utils.print_on_std("\n Current mode : ST. Deramping disabled \n")
            utils.log(logging.INFO, "Current mode : ST. Deramping disabled")
            utils.print_on_std(
                "\n ST mode requires a high resolution for the input DEM. 30 meters may not be enough. \n"
            )
            utils.log(
                logging.WARNING,
                "ST mode requires a high resolution for the input DEM. 30 meters may not be enough.",
            )
            param_dict_pre[str(PreProcessingParamTSX.ACTIVATE_DERAMP)] = False
            param_dict[str(DInSarParamTSX.ACTIVATE_DERAMP)] = False

    chain_factory = DiapOTBProcessingFactory(mode=mode)

    utils.print_on_std("\n Beginning of DiapOTB processing (S1 SM mode) \n")

    utils.print_on_std("\n Processing on reference image : " + reference_image + "\n")

    # First processing for reference : extract the native image
    reference_slc = (
        utils.get_slcml_namming_from_productname(reference_image, sensor, reference_kwl)
        + ".tif"
    )

    if reference_ext in ["tiff", "tif", "cos"]:
        # === Here we convert master single band CInt16 to dual band FLOAT32
        # === To do so, OTB_ExtractRoi is much faster than OTB_DynamicConvert

        app_extract = OTBApplicationWrapper("ExtractROI")
        app_extract.set_input_images(in_=reference_path)
        app_extract.set_output_images(out=os.path.join(output_ref_dir, reference_slc))
        app_extract.execute_app(in_memory=False)

    else:
        # Extract reference (to save as tif image)
        ds = gdal.Open(utils.adapt_image_format(reference_path), gdal.GA_ReadOnly)
        ds = gdal.Translate(
            os.path.join(output_ref_dir, reference_slc),
            ds,
            format="GTiff",
            outputType=gdal.GDT_Float32,
            creationOptions=["TILED=YES"],
        )
        ds = None  # close and save ds

    # Processing on reference image
    utils.print_on_std("\n Pre_Processing on reference image \n")

    pre_processing_chain_reference = chain_factory.create_processing(
        str(ChainNames.PRE_PROCESSING),
        image=reference_name,
        image_dir=reference_dir,
        param=param_dict_pre,
        output_dir=output_ref_dir,
    )

    pre_processing_chain_reference.execute()

    # All files created in the preprocessing reference chain
    all_files_ref.extend(pre_processing_chain_reference.get_all_files_list())
    all_files_ref.append(
        os.path.join(output_ref_dir, config_handler.get_doppler_file())
    )
    files_ref_to_keep.append(
        pre_processing_chain_reference.get_outputs()[str(PreProcessingOutputKeys.ML)]
    )

    utils.print_on_std("\n Ground projection on reference image \n")

    ground_chain_reference = chain_factory.create_processing(
        str(ChainNames.GROUND),
        image=reference_name,
        image_dir=reference_dir,
        param=param_dict_ground,
        output_dir=output_ref_dir,
    )

    ground_chain_reference.append_inputs(pre_processing_chain_reference.get_outputs())

    ground_chain_reference.execute(dem=dem)

    # All files created in the ground reference chain
    all_files_ref.extend(ground_chain_reference.get_all_files_list())

    ### Loop on secondaries image
    for secondary_file in (
        i for i in tiff_date_list if (i != reference_image and i != reference_path)
    ):

        secondary_path, _, _, _, dict_kwl_secondary = utils.get_image_data(
            secondary_file, input_path, str(ScriptNames.MULTI_SLC_S1SM)
        )

        # Prepare secondary_output
        secondary_cur_date = utils.get_date(
            secondary_file, reference_ext, dict_kwl_secondary
        )
        output_secondary_cur_dir = output_glob + "/{}_m_{}_s".format(
            reference_date, secondary_cur_date
        )
        prepare_output(output_secondary_cur_dir)

        secondary_dir = os.path.dirname(secondary_path)

        # Init empty list to persist generated files and make a selection
        files_secondary_to_keep = []
        all_files = []

        # Check on secondary
        utils.check_image_format(secondary_path)

        # eof path and geom for sec
        secondary_name = utils.apply_eof_path_on_orbit(
            satellite,
            eof_path,
            secondary_path,
            dict_kwl_secondary,
            output_secondary_cur_dir,
        )

        utils.print_on_std(
            "\n Processing on secondary image : " + secondary_file + "\n"
        )
        utils.print_on_std("\n Pre_Processing on secondary image \n")

        # Robustify the loop : if one of secondary file failed, continue and log the error
        # For each execute function : exception_tolerance decorator is used with a return code as output.
        # If return code = 1 => an exception was raised => continue and process the next secondary image

        pre_processing_chain_secondary = chain_factory.create_processing(
            str(ChainNames.PRE_PROCESSING),
            image=secondary_name,
            image_dir=secondary_dir,
            param=param_dict_pre,
            output_dir=output_secondary_cur_dir,
        )

        pre_processing_execute = utils.exception_tolerance(
            pre_processing_chain_secondary.execute
        )

        if satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:

            # TMP_DIR might be used to improve processing (internal_processing like resampling)
            with_tmp_dir = utils.check_if_tmp_for_TSX()

            if with_tmp_dir:
                tmp_dir = tempfile.mkdtemp()
                pre_processing_chain_secondary.set_tmp_path(tmp_dir)

            # For TSX/PAZ/TDX a resampling might be required to abjust Line_interval (on azimut) or/and freq_sampling (on range)
            # To apply this resampling, metadata (from reference and secondary) are necessary
            # Only metadata are used and reference image remains as it is
            code_pre_proc = pre_processing_execute(
                kwl_ref=reference_kwl, kwl_sec=dict_kwl_secondary
            )
        else:
            code_pre_proc = pre_processing_execute()

        if code_pre_proc == 1:
            utils.print_on_std(
                "\n Exception in Pre_Processing on secondary image: {} \n".format(
                    secondary_file
                )
            )
            continue

        utils.print_on_std("\n Ground projection on secondary image \n")

        # Change cartesian estimation to False
        param_dict_ground[str(GroundParamOthers.CARTESIAN_ESTIMATION)] = False
        ground_chain_secondary = chain_factory.create_processing(
            str(ChainNames.GROUND),
            image=secondary_name,
            image_dir=secondary_dir,
            param=param_dict_ground,
            output_dir=output_secondary_cur_dir,
        )

        ground_chain_secondary.append_inputs(
            pre_processing_chain_secondary.get_outputs()
        )

        ground_chain_execute = utils.exception_tolerance(ground_chain_secondary.execute)
        code_ground = ground_chain_execute(dem=dem)
        if code_ground == 1:
            utils.print_on_std(
                "\n Exception in Ground on secondary image : {} \n".format(
                    secondary_file
                )
            )
            continue

        utils.print_on_std("\n DINSAR Processing \n")

        dinsar_chain = chain_factory.create_processing(
            str(ChainNames.DINSAR),
            secondary_image=secondary_name,
            secondary_dir=secondary_dir,
            reference_image=reference_name,
            reference_dir=reference_dir,
            param=param_dict,
            output_dir=output_secondary_cur_dir,
        )

        dinsar_chain.append_inputs_reference(
            pre_processing_chain_reference.get_outputs()
        )
        dinsar_chain.append_inputs_secondary(
            pre_processing_chain_secondary.get_outputs()
        )
        dinsar_chain.append_inputs_reference(ground_chain_reference.get_outputs())
        dinsar_chain.append_inputs_secondary(ground_chain_secondary.get_outputs())

        if satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:

            # TMP_DIR might be used to improve processing (internal_processing like filtering)
            with_tmp_dir = utils.check_if_tmp_for_TSX()

            if with_tmp_dir:
                # reuse pre-processing path
                dinsar_chain.set_tmp_path(pre_processing_chain_secondary.get_tmp_path())

        dinsar_chain_execute = utils.exception_tolerance(dinsar_chain.execute)
        code_dinsar = dinsar_chain_execute(dem=dem)
        if code_dinsar == 1:
            utils.print_on_std(
                "\n Exception in DInSar on secondary image : {} \n".format(
                    secondary_file
                )
            )
            continue

        # Remove tmp_path if present
        if dinsar_chain.get_tmp_path() and os.path.commonprefix(
            [dinsar_chain.get_tmp_path(), tempfile.gettempdir()]
        ) == os.path.commonprefix([tempfile.gettempdir()]):
            if os.path.exists(dinsar_chain.get_tmp_path()):
                shutil.rmtree(dinsar_chain.get_tmp_path())

        # Additional processing : multilook on coregistrated
        coregistred_key = str(DInSarOutputKeysOthers.COREGISTRATED_SECONDARY)

        core_ml = (
            utils.get_slcml_namming_from_productname(
                secondary_file, mode=sensor, dict_md=dict_kwl_secondary
            )
            + "_ml"
            + str(param_dict_pre[str(PreProcessingParamOthers.MLAZI)])
            + str(param_dict_pre[str(PreProcessingParamOthers.MLRAN)])
            + ".tif"
        )
        app_ml_sec = OTBApplicationWrapper("SARMultiLook")
        app_ml_sec.set_parameters(
            mlran=param_dict_pre[str(PreProcessingParamOthers.MLRAN)],
            mlazi=param_dict_pre[str(PreProcessingParamOthers.MLAZI)],
            mlgain=param_dict_pre[str(PreProcessingParamOthers.MLGAIN)],
        )
        app_ml_sec.set_input_images(
            incomplex=dinsar_chain.get_outputs()[coregistred_key]
        )
        app_ml_sec.set_output_images(
            out=os.path.join(output_secondary_cur_dir, core_ml)
        )
        app_ml_sec.execute_app(in_memory=False)

        utils.print_on_std("\n Post_Processing \n")

        # TODO : Add Ortho processing (in PostProcessing or external chain ???)
        post_processing_chain = chain_factory.create_processing(
            str(ChainNames.POST_PROCESSING),
            secondary_image=secondary_name,
            secondary_dir=secondary_dir,
            reference_image=reference_name,
            reference_dir=reference_dir,
            param=param_dict_post,
            output_dir=output_secondary_cur_dir,
        )

        post_processing_chain.append_inputs_reference(
            ground_chain_reference.get_outputs()
        )
        post_processing_chain.append_inputs(dinsar_chain.get_outputs())

        post_processing_chain.geoid_path = geoid_path

        post_processing_execute = utils.exception_tolerance(
            post_processing_chain.execute
        )
        code_post_processing = post_processing_execute(dem_path=dem)
        if code_post_processing == 1:
            utils.print_on_std(
                "\n Exception in PostProcessing on secondary image : {} \n".format(
                    secondary_file
                )
            )
            continue

        # Retrieve all generated files and specify which one, we want to keep
        interf_key = str(DInSarOutputKeysOthers.INTERFERO)
        filtered_key = str(PostProcessingOutputKeys.FILT_INTERFERO)
        ortho_key = str(PostProcessingOutputKeys.ORTHO_INTERFERO)

        # All files created by chains
        all_files.extend(pre_processing_chain_secondary.get_all_files_list())
        all_files.extend(ground_chain_secondary.get_all_files_list())
        all_files.extend(dinsar_chain.get_all_files_list())
        all_files.extend(post_processing_chain.get_all_files_list())
        # Keep only coregistrated, interferogram and filtered_interferogram
        if post_processing_chain.get_outputs()[filtered_key]:
            files_secondary_to_keep.append(
                post_processing_chain.get_outputs()[filtered_key]
            )
        files_secondary_to_keep.append(dinsar_chain.get_outputs()[interf_key])
        files_secondary_to_keep.append(dinsar_chain.get_outputs()[coregistred_key])
        if post_processing_chain.get_outputs()[ortho_key]:
            files_secondary_to_keep.append(
                post_processing_chain.get_outputs()[ortho_key]
            )

        all_files.append(
            os.path.join(output_secondary_cur_dir, config_handler.get_doppler_file())
        )

        # Clean output_secondary
        if clean_tag:
            clean_output_dir(
                output_secondary_cur_dir, all_files, files_secondary_to_keep
            )

        # Rename output interferograms
        core_base = utils.get_slcml_namming_from_productname(
            secondary_file, sensor, dict_kwl_secondary
        )
        interfero_base = utils.get_interfnamming_from_productname(
            reference_image, secondary_file, sensor, reference_kwl, dict_kwl_secondary
        )
        interfo_filt = interfero_base + "_Filtred-Interferogram"
        interfo = interfero_base + "_Interferogram"
        interfo_ortho = interfero_base + "_Ortho-Interferogram"

        if utils.str2bool(
            param_dict_post[str(PostProcessingParamOthers.ACTIVATE_FILTERING)]
        ):
            rename_file(
                output_secondary_cur_dir,
                os.path.basename(
                    post_processing_chain.get_outputs()[filtered_key]
                ).split(".")[0],
                interfo_filt,
            )

        rename_file(
            output_secondary_cur_dir,
            os.path.basename(dinsar_chain.get_outputs()[coregistred_key]).split(".")[0],
            core_base,
        )
        # Extract only the useful bands (1 to 3)
        utils.extract_band123(
            dinsar_chain.get_outputs()[interf_key],
            os.path.join(output_secondary_cur_dir, interfo + ".tif"),
        )
        rename_file(
            output_secondary_cur_dir,
            os.path.basename(dinsar_chain.get_outputs()[interf_key]).split(".")[0],
            interfo,
            only_meta=True,
        )

        if os.path.exists(dinsar_chain.get_outputs()[interf_key]):
            os.remove(dinsar_chain.get_outputs()[interf_key])

        if utils.str2bool(
            param_dict_post[str(PostProcessingParamOthers.ACTIVATE_ORTHO)]
        ):
            utils.extract_band123(
                post_processing_chain.get_outputs()[ortho_key],
                os.path.join(output_secondary_cur_dir, interfo_ortho + ".tif"),
            )
            rename_file(
                output_secondary_cur_dir,
                os.path.basename(post_processing_chain.get_outputs()[ortho_key]).split(
                    "."
                )[0],
                interfo_ortho,
                only_meta=True,
            )

            if os.path.exists(post_processing_chain.get_outputs()[ortho_key]):
                os.remove(post_processing_chain.get_outputs()[ortho_key])

    # Clean reference (after secondary loop)
    if clean_tag:
        clean_output_dir(output_ref_dir, all_files_ref, files_ref_to_keep)

    # Rename ml reference
    ml_base = (
        utils.get_slcml_namming_from_productname(reference_name, sensor, reference_kwl)
        + "_ml"
        + str(param_dict_pre[str(PreProcessingParamOthers.MLAZI)])
        + str(param_dict_pre[str(PreProcessingParamOthers.MLRAN)])
    )

    rename_file(
        output_ref_dir,
        os.path.basename(
            pre_processing_chain_reference.get_outputs()[
                str(PreProcessingOutputKeys.ML)
            ]
        ).split(".")[0],
        ml_base,
    )

    utils.print_on_std("\n End of DiapOTB processing (S1 SM or Cosmo mode) \n")
