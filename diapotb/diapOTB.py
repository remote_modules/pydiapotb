#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

import argparse
import logging
import os
import shutil
import tempfile

from diapotb.ConfigFile import ConfigFile
from diapotb.lib.DInSAR import DInSarParamOthers, DInSarParamTSX
from diapotb.lib.DiapOTBProcessingFactory import DiapOTBProcessingFactory
from diapotb.lib.Ground import GroundParamOthers
from diapotb.lib.PostProcessing import PostProcessingParamOthers
from diapotb.lib.PreProcessing import PreProcessingParamOthers, PreProcessingParamTSX
from diapotb.lib.core.ApplicationWrapper import OTBApplicationWrapper
from diapotb.lib.core.DiapOTBEnums import ChainNames, ChainModes, ScriptNames, Satellite
import diapotb.lib.core.Utils as utils


def prepare_output(output_dir):
    """Prepare output directory to store the current processing"""
    if not os.path.exists(output_dir):
        print("The output directory does not exist and will be created")
        os.makedirs(output_dir)
    else:
        print("The output directory exists. Some files can be overwritten")


if __name__ == "__main__":

    # Check arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "configfile", help="input conguration file for the application DiapOTB"
    )
    args = parser.parse_args()

    # Check and load configuration file
    config_handler = ConfigFile(args.configfile, str(ScriptNames.SIMPLE_S1SM))
    config_handler.load_configfile()

    # Prepare output and logger
    output_dir = config_handler.get_output_dir()

    prepare_output(output_dir)

    utils.init_logger()
    utils.init_filelog(output_dir)

    # Retrieve parameters from configuration file
    param_dict = config_handler.create_param_dict_from_config_file(
        DInSarParamOthers, str(ChainNames.DINSAR)
    )

    param_dict_pre = config_handler.create_param_dict_from_config_file(
        PreProcessingParamOthers, str(ChainNames.PRE_PROCESSING)
    )

    # Append param dict with doppler_file
    param_dict_pre[str(PreProcessingParamOthers.DOPFILE)] = (
        config_handler.get_doppler_file()
    )

    param_dict_post = config_handler.create_param_dict_from_config_file(
        PostProcessingParamOthers, str(ChainNames.POST_PROCESSING)
    )

    param_dict_ground = config_handler.create_param_dict_from_config_file(
        GroundParamOthers, str(ChainNames.GROUND)
    )

    # Get and check main inputs : reference/secondary + dem and eof_path (if present)
    reference_path = config_handler.get_reference_image()
    secondary_path = config_handler.get_secondary_image()
    reference_dir = os.path.dirname(reference_path)
    secondary_dir = os.path.dirname(secondary_path)
    reference_name = os.path.basename(reference_path)
    secondary_name = os.path.basename(secondary_path)

    dem = config_handler.get_dem()
    eof_path = config_handler.get_eof()

    utils.check_image_format(reference_path)
    utils.check_image_format(secondary_path)

    # Get and configure RAM parameter to launch each OTB applications
    ram_param = config_handler.get_ram()
    # Configure our OTBWrapper with the defined RAM
    OTBApplicationWrapper.configure_ram(ram_param)

    # Check satellite
    dict_kwl_reference = utils.get_image_md(reference_path)
    dict_kwl_secondary = utils.get_image_md(secondary_path)

    satellite = utils.get_sensor_from_md(dict_kwl_reference)

    # Adapt geom files with eof files (if satellite is S1 and eof_path not empty)
    reference_name = utils.apply_eof_path_on_orbit(
        satellite, eof_path, reference_path, dict_kwl_reference, output_dir
    )
    secondary_name = utils.apply_eof_path_on_orbit(
        satellite, eof_path, secondary_path, dict_kwl_secondary, output_dir
    )

    # Adapt below processing following dem resolution and satellite
    # Get information about DEM (spacing, size ..)
    dict_dem_info = utils.get_dem_information(dem)
    if (
        dict_dem_info["estimatedGroundSpacingXDEM"] > 40.0
        or dict_dem_info["estimatedGroundSpacingYDEM"] > 40.0
    ):
        # Correlation if resolution > 40 m
        param_dict[str(DInSarParamOthers.ADVANTAGE)] = "correlation"
        utils.print_on_std(
            "Resolution of the input DEM is inferior to 40 meters : "
            "A correlation will be used to correct all deformation grids"
        )

    if (
        satellite == str(Satellite.CSK)
        and str(DInSarParamOthers.ADVANTAGE) not in param_dict
    ):
        # Correlation if CSK
        param_dict[str(DInSarParamOthers.ADVANTAGE)] = "correlation"

    # Get orbit numbers (only if TSX or PAZ or TDX) to have different names between reference and secondary images
    # Note : By default, for TSX, PAZ or TDX, reference and secondary base names are the same =>
    # file name is not enough to create different intermediate files => add orbit number
    if satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:
        orbit_num_ref = utils.get_orbit_number_from_md(dict_kwl_reference)
        orbit_num_sec = utils.get_orbit_number_from_md(dict_kwl_secondary)

    ### Processing ###
    # TODO : Gather processing part with diapOTB (S1 IW)
    utils.print_on_std("\n Beginning of DiapOTB processing (S1 SM or Cosmo mode) \n")

    # Create our factory to build all processing following the current mode
    mode = ChainModes.OTHERS
    # Mode TSX if TSX, PAZ or TDX sensor
    if satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:
        mode = ChainModes.TSX

        # Handle ST mode by disabling deramping
        # ST mode is different than other modes (SL or Strimap) :
        # Deramping flattens the spectrum and adds some artefacts (blacks strips)
        # on final interferogram
        is_sterring = utils.is_st_mode(satellite, dict_kwl_reference)
        if is_sterring:
            utils.print_on_std("\n Current mode : ST. Deramping disabled \n")
            utils.log(logging.INFO, "Current mode : ST. Deramping disabled")
            param_dict_pre[str(PreProcessingParamTSX.ACTIVATE_DERAMP)] = False
            param_dict[str(DInSarParamTSX.ACTIVATE_DERAMP)] = False

    chain_factory = DiapOTBProcessingFactory(mode=mode)

    utils.print_on_std("\n Pre_Processing on reference image \n")

    pre_processing_chain_reference = chain_factory.create_processing(
        str(ChainNames.PRE_PROCESSING),
        image=reference_name,
        image_dir=reference_dir,
        param=param_dict_pre,
        output_dir=output_dir,
    )
    if satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:
        pre_processing_chain_reference.add_orbit_to_filenames(orbit_num_ref)

    pre_processing_chain_reference.execute()

    utils.print_on_std("\n Pre_Processing on secondary image \n")

    pre_processing_chain_secondary = chain_factory.create_processing(
        str(ChainNames.PRE_PROCESSING),
        image=secondary_name,
        image_dir=secondary_dir,
        param=param_dict_pre,
        output_dir=output_dir,
    )

    if satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:
        pre_processing_chain_secondary.add_orbit_to_filenames(orbit_num_sec)

        # TMP_DIR might be used to improve processing (internal_processing like resampling)
        with_tmp_dir = utils.check_if_tmp_for_TSX()

        if with_tmp_dir:
            tmp_dir = tempfile.mkdtemp()
            pre_processing_chain_secondary.set_tmp_path(tmp_dir)

        # For TSX/PAZ/TDX a resampling might be required to abjust Line_interval (on azimut) or/and freq_sampling (on range)
        # To apply this resampling, metadata (from reference and secondary) are necessary
        # Only metadata are used and reference image remains as it is
        pre_processing_chain_secondary.execute(
            kwl_ref=dict_kwl_reference, kwl_sec=dict_kwl_secondary
        )
    else:
        pre_processing_chain_secondary.execute()

    utils.print_on_std("\n Ground projection on reference image \n")

    ground_chain_reference = chain_factory.create_processing(
        str(ChainNames.GROUND),
        image=reference_name,
        image_dir=reference_dir,
        param=param_dict_ground,
        output_dir=output_dir,
    )

    ground_chain_reference.append_inputs(pre_processing_chain_reference.get_outputs())

    if satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:
        ground_chain_reference.add_orbit_to_filenames(orbit_num_ref)

    ground_chain_reference.execute(dem=dem)

    utils.print_on_std("\n Ground projection on secondary image \n")

    # Change cartesian estimation to False
    param_dict_ground[str(GroundParamOthers.CARTESIAN_ESTIMATION)] = False
    ground_chain_secondary = chain_factory.create_processing(
        str(ChainNames.GROUND),
        image=secondary_name,
        image_dir=secondary_dir,
        param=param_dict_ground,
        output_dir=output_dir,
    )

    ground_chain_secondary.append_inputs(pre_processing_chain_secondary.get_outputs())

    if satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:
        ground_chain_secondary.add_orbit_to_filenames(orbit_num_sec)

    ground_chain_secondary.execute(dem=dem)

    utils.print_on_std("\n DINSAR Processing \n")

    dinsar_chain = chain_factory.create_processing(
        str(ChainNames.DINSAR),
        secondary_image=secondary_name,
        secondary_dir=secondary_dir,
        reference_image=reference_name,
        reference_dir=reference_dir,
        param=param_dict,
        output_dir=output_dir,
    )

    dinsar_chain.append_inputs_reference(pre_processing_chain_reference.get_outputs())
    dinsar_chain.append_inputs_secondary(pre_processing_chain_secondary.get_outputs())
    dinsar_chain.append_inputs_reference(ground_chain_reference.get_outputs())
    dinsar_chain.append_inputs_secondary(ground_chain_secondary.get_outputs())

    if satellite in [str(Satellite.TSX), str(Satellite.PAZ), str(Satellite.TDX)]:
        dinsar_chain.add_orbit_to_filenames(orbit_num_ref, orbit_num_sec)

        # TMP_DIR might be used to improve processing (internal_processing like filtering)
        with_tmp_dir = utils.check_if_tmp_for_TSX()

        if with_tmp_dir:
            # reuse pre-processing path
            dinsar_chain.set_tmp_path(pre_processing_chain_secondary.get_tmp_path())

    dinsar_chain.execute(dem=dem)

    # Remove tmp_path if present
    if dinsar_chain.get_tmp_path() and os.path.commonprefix(
        [dinsar_chain.get_tmp_path(), tempfile.gettempdir()]
    ) == os.path.commonprefix([tempfile.gettempdir()]):
        if os.path.exists(dinsar_chain.get_tmp_path()):
            shutil.rmtree(dinsar_chain.get_tmp_path())

    utils.print_on_std("\n Post_Processing \n")

    post_processing_chain = chain_factory.create_processing(
        str(ChainNames.POST_PROCESSING),
        secondary_image=secondary_name,
        secondary_dir=secondary_dir,
        reference_image=reference_name,
        reference_dir=reference_dir,
        param=param_dict_post,
        output_dir=output_dir,
    )

    post_processing_chain.append_inputs_reference(ground_chain_reference.get_outputs())
    post_processing_chain.append_inputs(dinsar_chain.get_outputs())

    post_processing_chain.execute(dem_path=os.path.dirname(dem))

    utils.print_on_std("\n End of DiapOTB processing (S1 SM or Cosmo mode) \n")
