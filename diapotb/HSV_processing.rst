============================
Performing HSV processing
============================

One script is available to perform HSV processing :
   - interf_its.py : This script changes the dynamic of all bands for an interferogram image.

Setting environment
-------------------

Getting pydiapotb project
~~~~~~~~~~~~~~~~~~~~~~~~~~

PYDIAPOTB is a git project and the following command will provide all files :

.. code-block::

    git clone https://gitlab.orfeo-toolbox.org/remote_modules/pydiapotb.git


Getting python environment
~~~~~~~~~~~~~~~~~~~~~~~~~~

All python scripts need following libraries :

- argparse
- pathlib
- PIL
- matplotlib
- gdal
- numpy

A python containing all these packages is already available and can be activated using command :

.. code-block::

    module load otb

Activated python environment is : /softs/rh7/Python/3.7.2/bin/python

interf_its.py
-------------

interf_its.py is a python version of idl code : /work/ALT/sitr/oc_radar_adm/visu/interf_its.pro

The python version uses format geotiff for input image instead of .pha format for idl code.

Usage of the function
~~~~~~~~~~~~~~~~~~~~~

usage: interf_its.py [-h] [--min_amplitude [MIN_AMPLITUDE]] [--max_amplitude [MAX_AMPLITUDE]] [--min_coherence [MIN_COHERENCE]] [--max_coherence [MAX_COHERENCE]] interferometry_image image_out

positional arguments:
  interferometry_image  interferometry image containing in the first 3 bands amplitude, phase and coherence
  image_out             output path

optional arguments:
  -h, --help            show this help message and exit
  --min_amplitude MIN_AMPLITUDE
                        Minimum value for amplitude || is by default 7
  --max_amplitude MAX_AMPLITUDE
                        Maximum value for amplitude || is by default 100
  --min_coherence MIN_COHERENCE
                        Minimum value for coherence ||is by default 0
  --max_coherence MAX_COHERENCE
                        Maximum value for coherence ||is by default 1

Examples
~~~~~~~~
This script can be called in the following manner :

.. code-block:: python

    python interf_its.py interferogram.tif interferogram_new_dynamic.tif --max_amplitude 8  --min_amplitude 0

The image interferogram_new_dynamic.tif is created