#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2017-2022 (c) CNES. All rights reserved.
#
#   This file is part of DiapOTB project
#       https://gitlab.orfeo-toolbox.org/remote_modules/diapotb
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Gaelle Usseglio (Thales)
#
# =========================================================================

import argparse
import os

from diapotb.ConfigFile import ConfigFile
from diapotb.lib.DInSAR import DInSarParamS1IW
from diapotb.lib.DiapOTBProcessingFactory import DiapOTBProcessingFactory
from diapotb.lib.Ground import GroundParamS1IW
from diapotb.lib.PostProcessing import PostProcessingParamS1IW
from diapotb.lib.PreProcessing import PreProcessingParamS1IW
from diapotb.lib.core.ApplicationWrapper import OTBApplicationWrapper
from diapotb.lib.core.DiapOTBEnums import ChainNames, ChainModes, ScriptNames
import diapotb.lib.core.Utils as utils


def prepare_output(output_dir):
    """Prepare output directory to store the current processing"""
    if not os.path.exists(output_dir):
        print("The output directory does not exist and will be created")
        os.makedirs(output_dir)
    else:
        print("The output directory exists. Some files can be overwritten")


def prepare_burst_dir(output_dir, burst_list, esd_nbiter):
    """Prepare burst directory to store the current processing"""
    for burst_id in burst_list:
        burst_dir = os.path.join(output_dir, "burst" + str(burst_id))

        if not os.path.exists(burst_dir):
            os.makedirs(burst_dir)

            if esd_nbiter > 0:
                os.makedirs(os.path.join(burst_dir, "esd"))


if __name__ == "__main__":

    # Check arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "configfile", help="input conguration file for the application DiapOTB"
    )
    args = parser.parse_args()

    # Check and load configuration file
    config_handler = ConfigFile(args.configfile, str(ScriptNames.SIMPLE_S1IW))
    config_handler.load_configfile()

    # Prepare output and logger
    output_dir = config_handler.get_output_dir()

    prepare_output(output_dir)

    utils.init_logger()
    utils.init_filelog(output_dir)

    # Retrieve parameters from configuration file
    param_dict = config_handler.create_param_dict_from_config_file(
        DInSarParamS1IW, str(ChainNames.DINSAR)
    )

    param_dict_pre = config_handler.create_param_dict_from_config_file(
        PreProcessingParamS1IW, str(ChainNames.PRE_PROCESSING)
    )

    # Append param dict with doppler_file
    param_dict_pre[str(PreProcessingParamS1IW.DOPFILE)] = (
        config_handler.get_doppler_file()
    )

    param_dict_post = config_handler.create_param_dict_from_config_file(
        PostProcessingParamS1IW, str(ChainNames.POST_PROCESSING)
    )

    param_dict_ground = config_handler.create_param_dict_from_config_file(
        GroundParamS1IW, str(ChainNames.GROUND)
    )

    # Get and check main inputs : reference/secondary + dem and eof_path (if present)
    reference_path = config_handler.get_reference_image()
    secondary_path = config_handler.get_secondary_image()
    reference_dir = os.path.dirname(reference_path)
    secondary_dir = os.path.dirname(secondary_path)
    reference_name = os.path.basename(reference_path)
    secondary_name = os.path.basename(secondary_path)

    dem = config_handler.get_dem()
    eof_path = config_handler.get_eof()

    utils.check_image_format(reference_path)
    utils.check_image_format(secondary_path)

    # Get and configure RAM parameter to launch each OTB applications
    ram_param = config_handler.get_ram()
    # Configure our OTBWrapper with the defined RAM
    OTBApplicationWrapper.configure_ram(ram_param)

    # Check satellite
    dict_kwl_reference = utils.get_image_md(reference_path)
    dict_kwl_secondary = utils.get_image_md(secondary_path)

    satellite = utils.get_sensor_from_md(dict_kwl_reference)

    # Adapt geom files with eof files (if satellite is S1 and eof_paht not empty)
    reference_name = utils.apply_eof_path_on_orbit(
        satellite, eof_path, reference_path, dict_kwl_reference, output_dir
    )
    secondary_name = utils.apply_eof_path_on_orbit(
        satellite, eof_path, secondary_path, dict_kwl_secondary, output_dir
    )

    # Adapt below processing following dem resolution and satellite
    # Get information about DEM (spacing, size ..)
    dict_dem_info = utils.get_dem_information(dem)
    if (
        dict_dem_info["estimatedGroundSpacingXDEM"] > 40.0
        or dict_dem_info["estimatedGroundSpacingYDEM"] > 40.0
    ):
        # Correlation if resolution > 40 m
        param_dict[str(DInSarParamS1IW.ADVANTAGE)] = "correlation"
        utils.print_on_std(
            "Resolution of the input DEM is inferior to 40 meters : "
            "A correlation will be used to correct all deformation grids"
        )

    # Get burst list from configuration file (for reference image)
    # reference and secondary images may rarely have different burst match
    # for instance,  burst 0 in reference image can match with burst 2 in secondary instead of id 0
    # That's why a burst to process can be different than burst ids
    burst_list = config_handler.get_burst_list()

    if not utils.check_burst_index_with_number(
        burst_list, dict_kwl_reference
    ) or not utils.check_burst_index_with_number(burst_list, dict_kwl_secondary):
        utils.print_on_std(
            "Burst index are not consistent for images. \
            Please check your burst_index parameter (following the number of burst)"
        )
        quit()

    valid_burst_reference, valid_burst_secondary = utils.select_burst(
        dict_kwl_reference, dict_kwl_secondary, min(burst_list), max(burst_list)
    )

    # Add burst id and burst to process to all param
    # "burst_ids" is always equal to reference burst
    # "burst_to_process" is equal to reference burst except for pre-processing chain
    # (burst extraction) for secondary
    param_dict_pre["burst_ids"] = valid_burst_reference
    param_dict_pre["burst_to_extract"] = valid_burst_reference
    param_dict_pre_secondary = param_dict_pre.copy()
    param_dict_pre_secondary["burst_ids"] = valid_burst_reference
    param_dict_pre_secondary["burst_to_extract"] = valid_burst_secondary
    param_dict_ground["burst_ids"] = valid_burst_reference
    param_dict["burst_ids"] = valid_burst_reference

    # Prepare output directory for each burst
    prepare_burst_dir(
        output_dir, valid_burst_reference, param_dict[str(DInSarParamS1IW.ESD_ITER)]
    )

    # TODO : Gather processing part with diapOTB (S1SM and CSK sensor)
    ### Processing ###
    utils.print_on_std("\n Beginning of DiapOTB processing (S1 IW mode) \n")

    # Create our factory to build all processing following the current mode
    chain_factory = DiapOTBProcessingFactory(mode=ChainModes.S1_IW)

    utils.print_on_std("\n Pre_Processing on reference image \n")

    pre_procesing_chain_reference = chain_factory.create_processing(
        str(ChainNames.PRE_PROCESSING),
        image=reference_name,
        image_dir=reference_dir,
        param=param_dict_pre,
        output_dir=output_dir,
    )

    pre_procesing_chain_reference.execute()

    utils.print_on_std("\n Pre_Processing on secondary image \n")

    pre_procesing_chain_secondary = chain_factory.create_processing(
        str(ChainNames.PRE_PROCESSING),
        image=secondary_name,
        image_dir=secondary_dir,
        param=param_dict_pre_secondary,
        output_dir=output_dir,
    )

    pre_procesing_chain_secondary.execute()

    utils.print_on_std("\n Ground projection on reference image \n")

    ground_chain_reference = chain_factory.create_processing(
        str(ChainNames.GROUND),
        image=reference_name,
        image_dir=reference_dir,
        param=param_dict_ground,
        output_dir=output_dir,
    )

    ground_chain_reference.append_inputs(pre_procesing_chain_reference.get_outputs())

    ground_chain_reference.execute(dem=dem)

    utils.print_on_std("\n Ground projection on secondary image \n")

    # Change cartesian estimation to False
    param_dict_ground[str(GroundParamS1IW.CARTESIAN_ESTIMATION)] = False
    ground_chain_secondary = chain_factory.create_processing(
        str(ChainNames.GROUND),
        image=secondary_name,
        image_dir=secondary_dir,
        param=param_dict_ground,
        output_dir=output_dir,
    )

    ground_chain_secondary.append_inputs(pre_procesing_chain_secondary.get_outputs())

    ground_chain_secondary.execute(dem=dem)

    utils.print_on_std("\n DINSAR Processing \n")

    dinsar_chain = chain_factory.create_processing(
        str(ChainNames.DINSAR),
        secondary_image=secondary_name,
        secondary_dir=secondary_dir,
        reference_image=reference_name,
        reference_dir=reference_dir,
        param=param_dict,
        output_dir=output_dir,
    )

    dinsar_chain.append_inputs_reference(pre_procesing_chain_reference.get_outputs())
    dinsar_chain.append_inputs_secondary(pre_procesing_chain_secondary.get_outputs())
    dinsar_chain.append_inputs_reference(ground_chain_reference.get_outputs())
    dinsar_chain.append_inputs_secondary(ground_chain_secondary.get_outputs())

    dinsar_chain.execute(dem=dem)

    utils.print_on_std("\n Post_Processing \n")

    # TODO : Add Ortho processing (in PostProcessing or external chain ???)
    post_processing_chain = chain_factory.create_processing(
        str(ChainNames.POST_PROCESSING),
        secondary_image=secondary_name,
        secondary_dir=secondary_dir,
        reference_image=reference_name,
        reference_dir=reference_dir,
        param=param_dict_post,
        output_dir=output_dir,
    )

    post_processing_chain.append_inputs_reference(ground_chain_reference.get_outputs())
    post_processing_chain.append_inputs(dinsar_chain.get_outputs())

    post_processing_chain.execute(dem_path=os.path.dirname(dem))

    utils.print_on_std("\n End of DiapOTB processing (S1 IW mode) \n")
