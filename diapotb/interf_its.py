import argparse
from pathlib import Path

from PIL import Image
from matplotlib.colors import hsv_to_rgb
from osgeo import gdal

import numpy as np


class ITSException(Exception):
    """
    classdocs
    """

    def __init__(self, reason: str = None) -> None:
        super().__init__()
        self.reason = reason

    def __str__(self) -> str:
        if self.reason is None:
            return ""
        return f"{self.reason}"


class NumberOfBandsError(ITSException):
    """Exception raised when there is not only one band available"""


def change_dynamic(
    frame: np.ndarray, vmin: int, vmax: int, normalize: bool
) -> np.ndarray:
    frame[frame < vmin] = vmin
    frame[frame > vmax] = vmax
    if normalize:
        value_min = np.min(frame)
        value_max = np.max(frame)
        frame = (frame - value_min) / (value_max - value_min)
    return frame


def assemble_HSV_image(
    image_entry: str,
    amplitude_min: int,
    amplitude_max: int,
    coherence_min: int,
    coherence_max: int,
    output: str,
):
    image_ds = gdal.Open(image_entry)

    if image_ds.RasterCount < 3:
        raise NumberOfBandsError("Entry image do not have at least 3 bands")
    else:
        amplitude = image_ds.GetRasterBand(1).ReadAsArray()
        phase = image_ds.GetRasterBand(2).ReadAsArray()
        coherence = image_ds.GetRasterBand(3).ReadAsArray()

        new_amplitude = change_dynamic(
            amplitude, vmin=amplitude_min, vmax=amplitude_max, normalize=True
        )
        new_phase = change_dynamic(phase, vmin=0, vmax=2 * np.pi, normalize=True)
        new_coherence = change_dynamic(
            coherence, vmin=coherence_min, vmax=coherence_max, normalize=True
        )
        shape = amplitude.shape
        image_hsv = np.zeros(shape + (3,))
        image_hsv[:, :, 2] = new_amplitude
        image_hsv[:, :, 0] = new_phase
        image_hsv[:, :, 1] = new_coherence
        image_rgb = hsv_to_rgb(image_hsv)
        temp = np.array(255 * image_rgb, dtype=np.uint8)

        driver = gdal.GetDriverByName("GTiff")
        outdata = driver.Create(
            output, image_ds.RasterXSize, image_ds.RasterYSize, 3, gdal.GDT_UInt16
        )
        outdata.SetGeoTransform(
            image_ds.GetGeoTransform()  ##sets same geotransform as input
        )
        outdata.SetProjection(image_ds.GetProjection())  ##sets same projection as input
        outdata.GetRasterBand(1).WriteArray(temp[:, :, 0])  ##writing data
        outdata.GetRasterBand(2).WriteArray(temp[:, :, 1])
        outdata.GetRasterBand(3).WriteArray(temp[:, :, 2])
        outdata.FlushCache()
        outdata = None


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "interferometry_image",
        help="interferometry image containing in the first 3 bands amplitude, phase and coherence",
    )
    parser.add_argument("image_out", help="output path")
    parser.add_argument(
        "--min_amplitude",
        nargs="?",
        default=7,
        type=int,
        help="Minimum value for amplitude",
    )
    parser.add_argument(
        "--max_amplitude",
        nargs="?",
        default=100,
        type=int,
        help="Maximum value for amplitude",
    )
    parser.add_argument(
        "--min_coherence",
        nargs="?",
        default=0,
        type=int,
        help="Minimum value for coherence",
    )
    parser.add_argument(
        "--max_coherence",
        nargs="?",
        default=1,
        type=int,
        help="Maximum value for coherence",
    )

    args = parser.parse_args()

    assemble_HSV_image(
        args.interferometry_image,
        args.min_amplitude,
        args.max_amplitude,
        args.min_coherence,
        args.max_coherence,
        args.image_out,
    )
