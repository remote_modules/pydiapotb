# Python environment 

Some packages are required to run DiapOTB's processing chains with :
* GDAL
* h5py
* jsonschema
* numpy
* matplotlib
* PIL
* pathlib

A conda environment can be made to launch DiapOTB. An example is available into *share/* directory.

# Installation

DiapOTB (including python processing) depends on OTB (OTB 8.1+). Python chains are bindings to some OTB and DiapOTB applications. Thus, you will need to install OTB and DiapOTB binaries on your platform, first. You can find an installation guide in the previous Readme.

Then, a version of GDAL which is compatible with your OTB version is also required.

`
NB: If you are using OTB binary distribution (including DiapOTB), you can simplify your installation by droping the simplified version of gdal-config into the bin directory of your OTB installation. This script retrieves the current version of GDAL used with OTB and makes sure to have the same version. If you have compiled OTB from sources, you can choose a version of GDAL to bind OTB and for your python interperter.
`

To simplify, installation and then execution, a setup.py is available. You can directly install DiapOTB python chains with :

```
% Load OTB/DiapOTB environment
% with otbenv.profile, if you are using a binary distribution
% with environment variables, otherwise

% (Optional) Create a dedicated environment with a conda virtuel environment (or a python virtual environment)
conda create -n diapotb_env python==3.8.4
conda activate diapotb_env

% Install DiapOTB chains
cd <install_DiapOTB_directory>/python_src
python setup.py install

# Execute the selected processing (for instance, diapOTB.py)
diapOTB.py <config_file.json>
```
