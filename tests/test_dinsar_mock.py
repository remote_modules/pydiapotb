import pytest
from diapotb.lib.DiapOTBProcessingFactory import DiapOTBProcessingFactory
from diapotb.lib.core.DiapOTBEnums import ChainNames, ChainModes, ScriptNames, Satellite
from diapotb.lib.DInSAR import DInSarParamOthers, DInSarParamS1IW, DInSarInputKeysOthers, \
    DInSarInputKeysS1IW, DInSarInputKeysTSX

@pytest.fixture(scope="function")
def prepare_param(mode):
    """Add keys to param dictionary
    """
    param = {}

    # Common parameters
    param[str(DInSarParamOthers.MLAZI)] = 3
    param[str(DInSarParamOthers.MLRAN)] = 3
    param[str(DInSarParamOthers.MLAZI_GRID)] = 3
    param[str(DInSarParamOthers.MLRAN_GRID)] = 3
    param[str(DInSarParamOthers.GRID_STEP_RAN)] = 150
    param[str(DInSarParamOthers.GRID_STEP_AZI)] = 150
    param[str(DInSarParamOthers.GRID_THRESHOLD)] = 0.3
    param[str(DInSarParamOthers.GRID_GAP)] = 1000
    param[str(DInSarParamOthers.INTERF_GAIN)] = 0.1
    param[str(DInSarParamOthers.ADVANTAGE)] = "projection"

    # Specific parameters for S1IW
    if mode == ChainModes.S1_IW:
        param[str(DInSarParamS1IW.BURSTIDS)] = [0, 1]
        param[str(DInSarParamS1IW.ESD_ITER)] = 2

    yield param
    del param

@pytest.fixture(scope="function")
def prepare_inputs(mode, prepare_param):
    """Build a fake input dict for Dinsar
    """
    inputs = {}

    # Common inputs
    inputs[str(DInSarInputKeysOthers.ML_REFERENCE)] = "ml_reference.tiff"
    inputs[str(DInSarInputKeysOthers.DEMPROJ_REFERENCE)] = "dem_proj_reference.tiff"
    inputs[str(DInSarInputKeysOthers.CARTESIAN_ESTIMATION_REFERENCE)] = "cartmean_reference.tiff"
    inputs[str(DInSarInputKeysOthers.DOP0_SECONDARY)] = 0.12
    inputs[str(DInSarInputKeysOthers.ML_SECONDARY)] = "ml_secondary.tiff"
    inputs[str(DInSarInputKeysOthers.DEMPROJ_SECONDARY)] = "dem_proj_secondary.tiff"

    # Specific inputs for S1IW
    if mode == ChainModes.S1_IW:
        # First add specific keys
        inputs[str(DInSarInputKeysS1IW.BURSTS_REFERENCE)] = "burst_reference.tiff"
        inputs[str(DInSarInputKeysS1IW.DERAMP_REFERENCE)] = "deramp_reference.tiff"
        inputs[str(DInSarInputKeysS1IW.BURSTS_SECONDARY)] = "burst__secondary.tiff"
        inputs[str(DInSarInputKeysS1IW.DERAMP_SECONDARY)] = "deramp_secondary.tiff"

        # Then transform all valuesin inputs to list
        for key, val in inputs.items():
            inputs[key] = []
            for burst_id in prepare_param[str(DInSarParamS1IW.BURSTIDS)]:
                if key != str(DInSarInputKeysOthers.DOP0_SECONDARY):
                    inputs[key].append(val.split(".tiff")[0] + str(burst_id) + ".tiff")
                else:
                    inputs[key].append(0.12)
    # Specific intputs for TSX
    elif mode == ChainModes.TSX:
        inputs[str(DInSarInputKeysTSX.DERAMP_REFERENCE)] = "deramp_reference.tiff"
        inputs[str(DInSarInputKeysTSX.DERAMP_SECONDARY)] = "deramp_secondary.tiff"
        inputs[str(DInSarInputKeysTSX.DERAMP_RESAMPLE_SECONDARY)] = "resamp_deramp_secondary.tiff"

    yield inputs
    del inputs

@pytest.fixture(scope="function")
def create_dinsar_chain(mode, mocker, prepare_param):
    """Build dinsar chain thanks to our Factory
    """
    mocker.patch(
            'diapotb.lib.core.DiapOTBProcessing.os.path.exists',
            return_value=True)


    chain_factory = DiapOTBProcessingFactory(mode=mode)

    reference_name = "ref_img.tiff"
    reference_dir = "."
    secondary_name = "sec_img.tiff"
    secondary_dir = "."
    param = prepare_param
    output_dir = "."

    dinsar_chain = chain_factory.create_processing(str(ChainNames.DINSAR),
                                                   secondary_image=secondary_name,
                                                   secondary_dir=secondary_dir,
                                                   reference_image=reference_name,
                                                   reference_dir=reference_dir,
                                                   param=param,
                                                   output_dir=output_dir)

    # Return chain
    yield dinsar_chain

    del dinsar_chain
    del chain_factory

#### Base class to mock application execution ######
class MockApplication():
    """Base class to mock OTB applications :
    Do not execute applications, just check if all inputs/outputs/param were provided
    """
    def mock_application(self):
        print("Mock Application Execution : Do nothing instead of otbApplication.Application.ExecuteAndWriteOutput")
        return True

    def mock_main_filtering(self, reference_path, secondary_path, output_dir, tmp_dir=""):
        print("Mock main_filtering : return reference_path")
        return reference_path

    def mock_get_output_image(self, parameter_name):
        print("Mock get_output_image : return current name")
        return parameter_name

#### Tests ####
class TestDInSAR(MockApplication):
    """ Test PreProcessing chain (only chains not applications)
    """

    @pytest.fixture(scope="class", params=[ChainModes.OTHERS,
                                           ChainModes.S1_IW,
                                           ChainModes.TSX])
    def mode(self, request):
        return request.param


    def test_chain(self, create_dinsar_chain, prepare_inputs, mocker, mode):

        # Mock functions
        # Common patch
        mocker.patch(
            'otbApplication.Application.ExecuteAndWriteOutput',
            self.mock_application
        )

        # For TSX
        if mode == ChainModes.TSX:
            mocker.patch(
                'diapotb.lib.DInSAR.main_filtering',
                self.mock_main_filtering
            )

        # S1_IW (ESD step)
        if mode == ChainModes.S1_IW:
            mocker.patch(
                'otbApplication.Application.Execute',
                self.mock_application
            )

            mocker.patch(
                'diapotb.lib.PostProcessing.OTBApplicationWrapper.get_output_image',
                self.mock_get_output_image
            )

        # Get chain
        dinsar_chain = create_dinsar_chain

        dinsar_chain.append_inputs(prepare_inputs)

        # Execute
        dinsar_chain.execute(dem="dem.hgt")

        # Get outputs
        dinsar_chain.get_outputs()

        assert len(dinsar_chain.get_outputs()) > 0
