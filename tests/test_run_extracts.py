import pytest
import shutil
import os
from enum import Enum


import json
from diapotb.lib.DiapOTBProcessingFactory import DiapOTBProcessingFactory
from diapotb.ConfigFile import ConfigFile
from diapotb.lib.core.DiapOTBEnums import ChainNames, ChainModes, ScriptNames, Satellite
from diapotb.lib.DInSAR import DInSarParamOthers
from diapotb.lib.PreProcessing import PreProcessingParamOthers
from diapotb.lib.PostProcessing import PostProcessingParamOthers
from diapotb.lib.Ground import GroundParamOthers

class CaseNames(Enum):
    S4 = "S4Reunion"
    CSK = "CSKReunion"

    def __str__(self):
        return self.value


@pytest.fixture(scope="function")
def prepare_json_file(test, tmpdir_factory):
    """Create from a template, a ConfigFile
    """
    # Create tmp dir (with tmpdir_factory pytest fixture) and tmp_file
    my_tmpdir = tmpdir_factory.mktemp("run_" + test)

    # Get inputs from current directory and get data
    current_dir = os.path.dirname(os.path.realpath(__file__))

    data_dir = os.path.join(current_dir,
                            "../data/Input/")


    # Change data config global path to input images
    # Select input following test
    if test == str(CaseNames.S4):
        # Get required template
        json_template = os.path.join(current_dir, "config_S4Reunion_extract_template.json")

        # Load file
        with open(json_template, 'r') as f:
            data_config = json.load(f)

        data_config["Global"]["in"]["Master_Image_Path"] = os.path.abspath(os.path.join(data_dir,
                                                                                        "S1_Reunion/s1a_s4_20160818_extract.tiff"))
        data_config["Global"]["in"]["Slave_Image_Path"] = os.path.abspath(os.path.join(data_dir,
                                                                                       "S1_Reunion/s1b_s4_20160929_extract.tiff"))
        data_config["Global"]["in"]["DEM_Path"] = os.path.abspath(os.path.join(data_dir,
                                                                               "S1_Reunion/S22E055_extract.tiff"))
    elif test == str(CaseNames.CSK):
        # Get required template
        json_template = os.path.join(current_dir, "config_CSKReunion_extract_template.json")

        # Load file
        with open(json_template, 'r') as f:
            data_config = json.load(f)

        data_config["Global"]["in"]["Master_Image_Path"] = os.path.abspath(os.path.join(data_dir,
                                                                                        "CSK_Reunion/CSKS2_SCS_B_HI_15_VV_RA_SF_20170223023902_20170223023910_extract.tiff"))
        data_config["Global"]["in"]["Slave_Image_Path"] = os.path.abspath(os.path.join(data_dir,
                                                                                       "CSK_Reunion/CSKS1_SCS_B_HI_15_VV_RA_SF_20170522023843_20170522023850_extract.tiff"))
        data_config["Global"]["in"]["DEM_Path"] = os.path.abspath(os.path.join(data_dir,
                                                                               "CSK_Reunion/S22E055_extract.tiff"))
    else:
        raise Exception("Unknown test case")

    # Change output to tmp
    data_config["Global"]["out"]["output_dir"] = os.path.abspath(my_tmpdir)

    json_file = os.path.join(my_tmpdir, "json_file" + test + ".json")
    with open(json_file, 'w') as outfile:
        json.dump(data_config, outfile, indent=4)

    print(my_tmpdir)

    yield json_file

    # Remove tmp_dir
    #shutil.rmtree(str(my_tmp_dir))



#### Tests ####
class TestRun():
    """ Test to execute extract inputs
    (a whole processing for extract inputs)
    """

    # if S4 is before CSK and ortho is enabled, SARCartesianMean for CSK can send a seg fault
    # A fix is to call in SARDEMProjection filter DEMHandler->ClearDEMs() to clear DEM of the singleton
    @pytest.fixture(scope="class", params=[str(CaseNames.S4),
                                           str(CaseNames.CSK)
                                           ])
    def test(self, request):
        return request.param


    def test_run(self, prepare_json_file):

        # Check and load configuration file
        config_handler = ConfigFile(prepare_json_file, str(ScriptNames.SIMPLE_S1SM))
        config_handler.load_configfile()

        # Prepare output and logger
        output_dir = config_handler.get_output_dir()

        # Retrieve parameters from configuration file
        param_dict = config_handler.create_param_dict_from_config_file(DInSarParamOthers,
                                                                       str(ChainNames.DINSAR))

        param_dict_pre = config_handler.create_param_dict_from_config_file(PreProcessingParamOthers,
                                                                           str(ChainNames.PRE_PROCESSING))

        # Append param dict with doppler_file
        param_dict_pre[str(PreProcessingParamOthers.DOPFILE)] = config_handler.get_doppler_file()

        param_dict_post = config_handler.create_param_dict_from_config_file(PostProcessingParamOthers,
                                                                            str(ChainNames.POST_PROCESSING))

        param_dict_ground = config_handler.create_param_dict_from_config_file(GroundParamOthers,
                                                                              str(ChainNames.GROUND))

        # Get and check main inputs : reference/secondary + dem and eof_path (if present)
        reference_path = config_handler.get_reference_image()
        secondary_path = config_handler.get_secondary_image()
        reference_dir = os.path.dirname(reference_path)
        secondary_dir = os.path.dirname(secondary_path)
        reference_name = os.path.basename(reference_path)
        secondary_name = os.path.basename(secondary_path)

        dem = config_handler.get_dem()

        # Create and execute each sub chain
        chain_factory = DiapOTBProcessingFactory(mode=ChainModes.OTHERS)

        # Pre processing on reference and secondary
        pre_processing_chain_reference = chain_factory.create_processing(str(ChainNames.PRE_PROCESSING),
                                                                     image=reference_name,
                                                                     image_dir=reference_dir,
                                                                     param=param_dict_pre,
                                                                     output_dir=output_dir)

        pre_processing_chain_reference.execute()

        pre_processing_chain_secondary = chain_factory.create_processing(str(ChainNames.PRE_PROCESSING),
                                                                     image=secondary_name,
                                                                     image_dir=secondary_dir,
                                                                     param=param_dict_pre,
                                                                     output_dir=output_dir)


        pre_processing_chain_secondary.execute()

        # Ground chain on reference and secondary
        ground_chain_reference = chain_factory.create_processing(str(ChainNames.GROUND),
                                                                 image=reference_name,
                                                                 image_dir=reference_dir,
                                                                 param=param_dict_ground,
                                                                 output_dir=output_dir)

        ground_chain_reference.append_inputs(pre_processing_chain_reference.get_outputs())

        ground_chain_reference.execute(dem=dem)

        # Change cartesian estimation to False
        param_dict_ground[str(GroundParamOthers.CARTESIAN_ESTIMATION)] = False
        ground_chain_secondary = chain_factory.create_processing(str(ChainNames.GROUND),
                                                                 image=secondary_name,
                                                                 image_dir=secondary_dir,
                                                                 param=param_dict_ground,
                                                                 output_dir=output_dir)

        ground_chain_secondary.append_inputs(pre_processing_chain_secondary.get_outputs())

        ground_chain_secondary.execute(dem=dem)

        # DINSAR
        dinsar_chain = chain_factory.create_processing(str(ChainNames.DINSAR),
                                                       secondary_image=secondary_name,
                                                       secondary_dir=secondary_dir,
                                                       reference_image=reference_name,
                                                       reference_dir=reference_dir,
                                                       param=param_dict,
                                                       output_dir=output_dir)

        dinsar_chain.append_inputs_reference(pre_processing_chain_reference.get_outputs())
        dinsar_chain.append_inputs_secondary(pre_processing_chain_secondary.get_outputs())
        dinsar_chain.append_inputs_reference(ground_chain_reference.get_outputs())
        dinsar_chain.append_inputs_secondary(ground_chain_secondary.get_outputs())

        dinsar_chain.execute(dem=dem)

        # Post-processing
        post_processing_chain = chain_factory.create_processing(str(ChainNames.POST_PROCESSING),
                                                                secondary_image=secondary_name,
                                                                secondary_dir=secondary_dir,
                                                                reference_image=reference_name,
                                                                reference_dir=reference_dir,
                                                                param=param_dict_post,
                                                                output_dir=output_dir)

        post_processing_chain.append_inputs_reference(ground_chain_reference.get_outputs())
        post_processing_chain.append_inputs(dinsar_chain.get_outputs())

        post_processing_chain.execute(dem_path=os.path.dirname(dem))

        assert True
