import pytest
from diapotb.lib.DiapOTBProcessingFactory import DiapOTBProcessingFactory
from diapotb.lib.core.DiapOTBEnums import ChainNames, ChainModes, ScriptNames, Satellite
from diapotb.lib.PreProcessing import PreProcessingParamOthers, PreProcessingParamS1IW
from diapotb.lib.core.DiapOTBExceptions import DiapOTBException

@pytest.fixture(scope="function")
def prepare_param(mode):
    """Add keys to param dictionary
    """
    param = {}

    # Common parameters
    param[str(PreProcessingParamOthers.MLAZI)] = 3
    param[str(PreProcessingParamOthers.MLRAN)] = 3
    param[str(PreProcessingParamOthers.MLGAIN)] = 0.1
    param[str(PreProcessingParamOthers.DOPFILE)] = "dopfile.txt"

    # Specific parameters for S1IW
    if mode == ChainModes.S1_IW:
        param[str(PreProcessingParamS1IW.BURSTIDS)] = [0, 1]
        param[str(PreProcessingParamS1IW.BURSTEXTRACT)] = [0, 1]

    yield param
    del param

@pytest.fixture(scope="function")
def create_pre_processing_chain(mode, mocker, prepare_param):
    """Build pre_processing chain thanks to our Factory
    """
    mocker.patch(
            'diapotb.lib.core.DiapOTBProcessing.os.path.exists',
            return_value=True)


    chain_factory = DiapOTBProcessingFactory(mode=mode)

    image_name = "./image.tiff"
    image_dir = "."
    param = prepare_param
    output_dir = "."


    pre_processing_chain = chain_factory.create_processing(str(ChainNames.PRE_PROCESSING),
                                                           image=image_name,
                                                           image_dir=image_dir,
                                                           param=param,
                                                           output_dir=output_dir)

    # Return chain
    yield pre_processing_chain

    del pre_processing_chain
    del chain_factory

#### Base class to mock application execution ######
class MockApplication():
    """Base class to mock OTB applications :
    Do not execute applications, just check if all inputs/outputs/param were provided
    """
    def mock_application(self):
        print("Mock Application Execution : Do nothing instead of otbApplication.Application.ExecuteAndWriteOutput")
        return True


#### Tests ####
class TestPreProc(MockApplication):
    """ Test PreProcessing chain (only chains not applications)
    """

    @pytest.fixture(scope="class", params=[ChainModes.OTHERS,
                                           ChainModes.S1_IW,
                                           ChainModes.TSX])
    def mode(self, request):
        return request.param


    def test_chain(self, create_pre_processing_chain, mode, mocker):

        # Mock functions
        # Common patch
        mocker.patch(
            'otbApplication.Application.ExecuteAndWriteOutput',
            self.mock_application
        )

        # Get chain
        pre_processing_chain = create_pre_processing_chain

        # Execute
        pre_processing_chain.execute()

        # If mode = TSX, main_resampling can be called if kwl_ref and kwl_sec are available
        # Check if an exception is raised in that case (geom does not exist)
        if mode == ChainModes.TSX:
            with pytest.raises(DiapOTBException):
                pre_processing_chain.execute(kwl_ref={}, kwl_sec={})

        # Get outputs
        pre_processing_chain.get_outputs()

        assert len(pre_processing_chain.get_outputs()) > 0
