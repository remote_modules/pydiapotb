import pytest
from diapotb.lib.DiapOTBProcessingFactory import DiapOTBProcessingFactory
from diapotb.lib.core.DiapOTBEnums import ChainNames, ChainModes, ScriptNames, Satellite
from diapotb.lib.PostProcessing import PostProcessingParamOthers, PostProcessingInputKeysOthers

@pytest.fixture(scope="function")
def prepare_param(mode):
    """Add keys to param dictionary
    """
    param = {}

    # Common parameters
    param[str(PostProcessingParamOthers.INTERF_GAIN)] = 0.1
    # TODO : Enable ORTHO (mock SetParameterString)
    param[str(PostProcessingParamOthers.ACTIVATE_ORTHO)] = "false"

    # Specific parameters for Others
    if mode == ChainModes.OTHERS:
        param[str(PostProcessingParamOthers.GRID_STEP_RAN)]= 150
        param[str(PostProcessingParamOthers.GRID_STEP_AZI)] = 150

    yield param
    del param

@pytest.fixture(scope="function")
def prepare_inputs(mode):
    """Build a fake input dict for Post-Processing
    """
    inputs = {}

    # Common input
    inputs[str(PostProcessingInputKeysOthers.INTERFERO)] = "interferogram.tiff"

    # Specific intputs for Others (+ TSX)
    if mode == ChainModes.OTHERS:
        inputs[str(PostProcessingInputKeysOthers.CARTESIAN_ESTIMATION_REFERENCE)] = "cartmean_reference.tiff"
        inputs[str(PostProcessingInputKeysOthers.COREGISTRATED_SECONDARY)] = "coregistrated.tiff"
        inputs[str(PostProcessingInputKeysOthers.GRIDS)] = "deformation_grid.tiff"

    yield inputs
    del inputs

@pytest.fixture(scope="function")
def create_post_precessing_chain(mode, mocker, prepare_param):
    """Build post-processing chain thanks to our Factory
    """
    mocker.patch(
            'diapotb.lib.core.DiapOTBProcessing.os.path.exists',
            return_value=True)


    chain_factory = DiapOTBProcessingFactory(mode=mode)

    reference_name = "ref_img.tiff"
    reference_dir = "."
    secondary_name = "sec_img.tiff"
    secondary_dir = "."
    param = prepare_param
    output_dir = "."

    post_processing_chain = chain_factory.create_processing(str(ChainNames.POST_PROCESSING),
                                                            secondary_image=secondary_name,
                                                            secondary_dir=secondary_dir,
                                                            reference_image=reference_name,
                                                            reference_dir=reference_dir,
                                                            param=param,
                                                            output_dir=output_dir)

    # Return chain
    yield post_processing_chain

    del post_processing_chain
    del chain_factory

#### Base class to mock application execution ######
class MockApplication():
    """Base class to mock OTB applications :
    Do not execute applications, just check if all inputs/outputs/param were provided
    """
    def mock_application(self):
        print("Mock Application Execution : Do nothing instead of otbApplication.Application.ExecuteAndWriteOutput")
        return True

    def mock_get_output_image(self, parameter_name):
        print("Mock get_output_image : return current name")
        return parameter_name



#### Tests ####
class TestPostProcessing(MockApplication):
    """ Test PostProcessing chain (only chains not applications)
    """

    @pytest.fixture(scope="class", params=[ChainModes.OTHERS,
                                           ChainModes.S1_IW])
    def mode(self, request):
        return request.param


    def test_chain(self, create_post_precessing_chain, prepare_inputs, mocker):

        # Mock functions
        # Common patch
        mocker.patch(
            'otbApplication.Application.ExecuteAndWriteOutput',
            self.mock_application
        )

        mocker.patch(
            'otbApplication.Application.Execute',
            self.mock_application
        )

        mocker.patch(
            'diapotb.lib.PostProcessing.OTBApplicationWrapper.get_output_image',
            self.mock_get_output_image
        )

        # Get chain
        post_precessing_chain = create_post_precessing_chain

        post_precessing_chain.append_inputs(prepare_inputs)

        # Execute
        post_precessing_chain.execute(dem_path="dem.hgt")

        # Get outputs
        post_precessing_chain.get_outputs()

        assert len(post_precessing_chain.get_outputs()) > 0
