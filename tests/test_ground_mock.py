
import pytest
from diapotb.lib.DiapOTBProcessingFactory import DiapOTBProcessingFactory
from diapotb.lib.core.DiapOTBEnums import ChainNames, ChainModes, ScriptNames, Satellite
from diapotb.lib.Ground import GroundParamOthers, GroundParamS1IW, GroundInputKeysOthers, \
    GroundInputKeysS1IW, GroundInputKeysTSX
from diapotb.lib.core.Utils import exception_tolerance

@pytest.fixture(scope="function")
def prepare_param(mode):
    """Add keys to param dictionary
    """
    param = {}

    # Common parameters
    param[str(GroundParamOthers.XYZ)] = True
    param[str(GroundParamOthers.NO_DATA)] = -32768
    param[str(GroundParamOthers.CARTESIAN_ESTIMATION)] = True

    # Specific parameters for S1IW
    if mode == ChainModes.S1_IW:
        param[str(GroundParamS1IW.BURSTIDS)] = [0, 1]

    yield param
    del param

@pytest.fixture(scope="function")
def prepare_inputs(mode, prepare_param):
    """Build a fake input dict for ground chain
    """
    inputs = {}

    # Specific inputs for S1IW
    if mode == ChainModes.S1_IW:
        # First add specific keys
        inputs[str(GroundInputKeysS1IW.DERAMP)] = "deramp.tiff"

        # Then transform all values in inputs to list
        for key, val in inputs.items():
            inputs[key] = []
            for burst_id in prepare_param[str(GroundParamS1IW.BURSTIDS)]:
                inputs[key].append(val.split(".tiff")[0] + str(burst_id) + ".tiff")
    # Specific input for TSX
    elif mode == ChainModes.TSX:
        inputs[str(GroundInputKeysTSX.RESAMPLE_DERAMP)] = "resamp_deramp.tiff"


    yield inputs
    del inputs

@pytest.fixture(scope="function")
def create_ground_chain(mode, mocker, prepare_param):
    """Build ground chain thanks to our Factory
    """
    mocker.patch(
            'diapotb.lib.core.DiapOTBProcessing.os.path.exists',
            return_value=True)


    chain_factory = DiapOTBProcessingFactory(mode=mode)

    reference_name = "ref_img.tiff"
    reference_dir = "."
    param = prepare_param
    output_dir = "."

    ground_chain = chain_factory.create_processing(str(ChainNames.GROUND),
                                                   image=reference_name,
                                                   image_dir=reference_dir,
                                                   param=param,
                                                   output_dir=output_dir)

    # Return chain
    yield ground_chain

    del ground_chain
    del chain_factory

#### Base class to mock application execution ######
class MockApplication():
    """Base class to mock OTB applications :
    Do not execute applications, just check if all inputs/outputs/param were provided
    """
    def mock_application(self):
        print("Mock Application Execution : Do nothing instead of otbApplication.Application.ExecuteAndWriteOutput")
        return True

    def mock_execute(self, **kwargs):
        print("Mock execute : Raise an exception")
        raise Exception("Test an exception")



#### Tests ####
class TestGround(MockApplication):
    """ Test PreProcessing chain (only chains not applications)
    """

    @pytest.fixture(scope="class", params=[ChainModes.OTHERS,
                                           ChainModes.S1_IW,
                                           ChainModes.TSX])
    def mode(self, request):
        return request.param


    def test_chain(self, create_ground_chain, prepare_inputs, mocker):

        # Mock functions
        # Common patch
        mocker.patch(
            'otbApplication.Application.ExecuteAndWriteOutput',
            self.mock_application
        )

        # Get chain
        ground_chain = create_ground_chain

        ground_chain.append_inputs(prepare_inputs)

        # Execute
        ground_chain.execute(dem="dem.hgt")

        # Get outputs
        ground_chain.get_outputs()

        assert len(ground_chain.get_outputs()) > 0


    def test_chain_with_exception(self, create_ground_chain, prepare_inputs, mocker):

        # Mock functions
        mocker.patch(
            'diapotb.lib.Ground.Ground.execute',
            self.mock_execute
        )

        # Get chain
        ground_chain = create_ground_chain

        ground_chain.append_inputs(prepare_inputs)

        # Set decorator to allow exception
        deco_ground_execute = exception_tolerance(ground_chain.execute)
        # Execute
        is_execption = deco_ground_execute(dem="dem.hgt")

        # Check return code : exception => 1
        assert is_execption == 1
