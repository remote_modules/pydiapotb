import pytest
import shutil
import os
import json
from enum import Enum

from diapotb.lib.core.DiapOTBEnums import Sensor, ScriptNames, ChainNames
from diapotb.ConfigFile import ConfigFile, MainTag, MiddleTag
from diapotb.lib.DInSAR import DInSarParamOthers, DInSarParamS1IW
from diapotb.lib.PreProcessing import PreProcessingParamOthers, PreProcessingParamS1IW
from diapotb.lib.PostProcessing import PostProcessingParamOthers, PostProcessingParamS1IW
from diapotb.lib.Ground import GroundParamOthers, GroundParamS1IW


class MainChains(Enum):
    DIAPOTB = "diapOTB"
    DIAPOTB_S1IW = "diapOTB_S1IW"
    SARMULTISLC = "SAR_MultiSlc"
    SARMULTISLC_S1IW = "SAR_MultiSlc_IW"

    def __str__(self):
        return self.value


@pytest.fixture(scope="function")
def build_configfile(chain_name):
    """Build a ConfigFile from examples.json
    """
    # Load examples according to the selected chain => Init a dictionnary with default parameters
    # Check and load configuration file
    if chain_name == MainChains.DIAPOTB:
        config_handler = ConfigFile.create_configfile_from_ex(str(ScriptNames.SIMPLE_S1SM))
        config_handler.load_configfile()

    elif chain_name == MainChains.DIAPOTB_S1IW:
        config_handler = ConfigFile.create_configfile_from_ex(str(ScriptNames.SIMPLE_S1IW))
        config_handler.load_configfile()

    elif chain_name == MainChains.SARMULTISLC:
        config_handler = ConfigFile.create_configfile_from_ex(str(ScriptNames.MULTI_SLC_S1SM))
        config_handler.load_configfile()

    elif chain_name == MainChains.SARMULTISLC_S1IW:
        config_handler = ConfigFile.create_configfile_from_ex(str(ScriptNames.MULTI_SLC_S1IW))
        config_handler.load_configfile()

    yield config_handler


def build_configfile_from_input(input_json, chain_name):
    """Build a ConfigFile from examples.json
    """
    # Load examples according to the selected chain => Init a dictionnary with default parameters
    # Check and load configuration file
    if chain_name == MainChains.DIAPOTB:
        config_handler = ConfigFile(input_json, str(ScriptNames.SIMPLE_S1SM))

    elif chain_name == MainChains.DIAPOTB_S1IW:
        config_handler = ConfigFile(input_json, str(ScriptNames.SIMPLE_S1IW))

    elif chain_name == MainChains.SARMULTISLC:
        config_handler = ConfigFile(input_json, str(ScriptNames.MULTI_SLC_S1SM))

    elif chain_name == MainChains.SARMULTISLC_S1IW:
        config_handler = ConfigFile(input_json, str(ScriptNames.MULTI_SLC_S1IW))

    return config_handler

#### Tests ####
class TestConfigFile():
    """ Test to check ConfigFile class
    """
    # In MB
    @pytest.fixture(scope="class", params=[MainChains.DIAPOTB,
                                           MainChains.DIAPOTB_S1IW,
                                           MainChains.SARMULTISLC,
                                           MainChains.SARMULTISLC_S1IW])
    def chain_name(self, request):
        return request.param


    def test_config_file(self, build_configfile, chain_name):
        """Check if an resampling is required """

        # Get config_ahndler and in-memory json
        config_handler = build_configfile
        data_config = config_handler.get_data_config()

        # Build each param dict from config_handler
        enum_dinsar = DInSarParamOthers
        if chain_name == MainChains.DIAPOTB_S1IW or chain_name == MainChains.SARMULTISLC_S1IW:
            enum_dinsar = DInSarParamS1IW

        param_dict_dinsar = config_handler.create_param_dict_from_config_file(enum_dinsar,
                                                                              str(ChainNames.DINSAR))

        enum_preproc = PreProcessingParamOthers
        if chain_name == MainChains.DIAPOTB_S1IW or chain_name == MainChains.SARMULTISLC_S1IW:
            enum_preproc = PreProcessingParamS1IW

        param_dict_pre = config_handler.create_param_dict_from_config_file(enum_preproc,
                                                                           str(ChainNames.PRE_PROCESSING))

        enum_postproc = PostProcessingParamOthers
        if chain_name == MainChains.DIAPOTB_S1IW or chain_name == MainChains.SARMULTISLC_S1IW:
            enum_postproc = PostProcessingParamS1IW

        param_dict_post = config_handler.create_param_dict_from_config_file(enum_postproc,
                                                                            str(ChainNames.POST_PROCESSING))

        enum_ground = GroundParamOthers
        if chain_name == MainChains.DIAPOTB_S1IW or chain_name == MainChains.SARMULTISLC_S1IW:
            enum_ground = GroundParamS1IW

        param_dict_ground = config_handler.create_param_dict_from_config_file(enum_ground,
                                                                              str(ChainNames.GROUND))

        # Check some values
        ml_key = "ML_ran"
        if chain_name == MainChains.DIAPOTB_S1IW or chain_name == MainChains.DIAPOTB:
            ml_key = "ML_range"

        assert data_config[str(ChainNames.PRE_PROCESSING)]["parameter"][ml_key] == \
            param_dict_pre[str(PreProcessingParamOthers.MLRAN)] == \
            param_dict_dinsar[str(DInSarParamOthers.MLRAN)]

        assert data_config[str(ChainNames.DINSAR)]["parameter"]["Interferogram_gain"] == \
            param_dict_dinsar[str(DInSarParamOthers.INTERF_GAIN)]

        assert data_config[str(ChainNames.POST_PROCESSING)]["parameter"]["Spacingxy"] == \
            param_dict_post[str(PostProcessingParamOthers.SPACING_XY)]

    @pytest.mark.xfail(reason="Expected exception")
    def test_config_exception(self, build_configfile, chain_name, tmpdir_factory):
        """Test a corrupted file as config file => exception"""

        # Create tmp dir (with tmpdir_factory pytest fixture) and tmp_file
        my_tmpdir = tmpdir_factory.mktemp("run_config_exception" + str(chain_name))

        # Get config handler and in-memory data
        config_handler = build_configfile
        data_config = config_handler.get_data_config()

        # Corrupt data in some way
        del data_config[str(ChainNames.PRE_PROCESSING)]["parameter"]["ML_gain"]

        json_file = os.path.join(my_tmpdir, "json_file.json")
        with open(json_file, 'w') as outfile:
            json.dump(data_config, outfile, indent=4)

        # rebuild a config Handler
        config_handler_new = build_configfile_from_input(json_file, chain_name)

        # check for the expected exception
        config_handler_new.load_configfile()

        shutil.rmtree(str(my_tmpdir))


class TestDefaultValue():
    """Test only default value from specific functions
    """
    @pytest.fixture(scope="class")
    def chain_name(self):
        return MainChains.SARMULTISLC_S1IW

    def test_default_value(self, build_configfile, chain_name, tmpdir_factory):
        """Retrieve default values
        """
        # Create tmp dir (with tmpdir_factory pytest fixture) and tmp_file
        my_tmpdir = tmpdir_factory.mktemp("run_config_exception" + str(chain_name))

        # Get config handler and in-memory data
        config_handler = build_configfile
        data_config = config_handler.get_data_config()

        # Delete values to get after default ones
        del data_config[str(MainTag.GLOBAL)][str(MiddleTag.PARAMETER)]["clean"]
        del data_config[str(MainTag.GLOBAL)][str(MiddleTag.PARAMETER)]["tmpdir_into_outputdir"]

        json_file = os.path.join(my_tmpdir, "json_file.json")
        with open(json_file, 'w') as outfile:
            json.dump(data_config, outfile, indent=4)

        # rebuild a config Handler
        config_handler_new = build_configfile_from_input(json_file, chain_name)

        # check for the expected exception
        config_handler_new.load_configfile()

        # Assert default value
        assert config_handler_new.get_clean() == False
        assert config_handler_new.get_tmp_status() == True

        shutil.rmtree(str(my_tmpdir))
