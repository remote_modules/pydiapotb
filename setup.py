#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess

from setuptools import setup, find_packages


# Import the library to make sure there is no side effect
import diapotb


def request_gdal_version():
    try:
        r = subprocess.run(["gdal-config", "--version"], stdout=subprocess.PIPE)
        version = r.stdout.decode("utf-8").strip("\n")
        print("GDAL %s detected on the system, using 'gdal=%s'" % (version, version))
        return version
    except Exception as ex:  # pylint: disable=broad-except
        return "3.2.2"


BASEDIR = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))

metadata = {}
with open(os.path.join(BASEDIR, "diapotb", "__meta__.py"), "r", encoding="utf-8") as f:
    exec(f.read(), metadata)

with open(os.path.join(BASEDIR, "README.md"), "r") as f:
    readme = f.read()

setup(
    name=metadata["__title__"],
    version=metadata["__version__"],
    description=metadata["__description__"],
    long_description=readme,
    long_description_content_type="text/markdown",
    author=metadata["__author__"],
    author_email=metadata["__author_email__"],
    url=metadata["__url__"],
    license=metadata["__license__"],
    keywords="Interferometry, SAR",
    # Include all packages (except a few ones like tests)
    packages=find_packages(exclude=("*.tests", "*.tests.*", "tests.*", "tests")),
    package_data={"": ["LICENSE", "NOTICE"]},
    include_package_data=True,  # Take MANIFEST.in into account
    python_requires=">=3.3, <4",
    install_requires=[
        "h5py<=3.1.1",
        "jsonschema",
        "numpy<=1.21.1",
        "gdal==" + request_gdal_version(),
        # Any way to require OTB ?
    ],
    extras_require={
        "dev": ["pylint", "pytest", "pytest-mock"],
        "docs": [
            "sphinx",
            "sphinx_rtd_theme",
            "sphinx_markdown_builder",
            "myst-parser",
        ],
    },
    project_urls={
        "Bug Tracker": "https://gitlab.orfeo-toolbox.org/remote_modules/diapotb/-/issues",
        "Documentation": "https://gitlab.orfeo-toolbox.org/remote_modules/diapotb/-/wikis/home",
        "Source Code": "https://gitlab.orfeo-toolbox.org/remote_modules/diapotb",
    },
    scripts=[
        "diapotb/diapOTB.py",
        "diapotb/diapOTB_S1IW.py",
        "diapotb/SAR_MultiSlc.py",
        "diapotb/SAR_MultiSlc_IW.py",
    ],
)
