<div align="justify">

# Processing Chains

## Description

DiapOTB remote module provides several processing chains that build a final interferogram from two SLC (Single Look Complex) images and a digital elevation model. The main processing can be displayed as follow :

![image](./Art/chains_description/diapOTB_mainProcessing.png)


This main processing is obtained by launching most of DiapOTB applications. The next schema sums up the link between all executions of DiapOTB/OTB applications :

![image](./Art/chains_description/diapOTB_flowchart.png)


At the end, a final interferogram is created as a VectorImage  :


* Band 1 : Amplitude


* Band 2 : Phase


* Band 3 : Coherence


* Band 4 (if present) : IsData boolean to indicate if output pixels are consistent

Example : Output interfergram for S1 SM product (Reunion island)


![image](./Art/results_DiapOTB/Results_Reunion.png)


Other optional outputs can be created such as a filtering interferogram.


NB: All outputs contain metadata inside the geom file (not the image itself). Thus, outputs are not georeferenced outside OTB. However, it is possible to add inside tiff images, the GCPs thanks to addGCP.py script. This script allows to add GCPs from an input geom file into a tiff image. Some sensors like Cosmo, do not owe GCPs natively. You can create this data with SARMetadataCorrection application before launching addGPC.py.
Please be aware that the GCPs come from inputs (referecence or secondary images) and intermediate geometries (such as ML) might not fit with GCPs.


## Available chains and execution

Four kinds of workflows are available according input products and modes :


* [diapOTB.py](./ProcessingChains/diapOTB_Chain) : Single interferometry chain for S1 Stripmap mode, TSX/PAZ/TDX and Cosmo


* [diapOTB_S1IW.py](./ProcessingChains/diapOTB_S1IW_Chain) : Single interferometry chain for S1 IW mode


* [SAR_MultiSlc.py](./ProcessingChains/Sar_MultiSlc_Chain) : Multitemporal interferometry chain for S1 Stripmap mode, TSX/PAZ/TDX and Cosmo


* [SAR_MultiSlc_IW.py](./ProcessingChains/Sar_MultiSlc_IW_Chain) : Multitemporal interferometry chain for S1 IW mode



All these chains can be executed by following the next [tutorial](ProcessingChains/tuto_chains).


A [Python API](PythonAPI/pythonAPI) was designed to organize python scripts and to help new chain development. In case of existing workflows do not fullfill your needs, you can easily create your own workflow by calling DiapOTB applications.



## Limitations

Two limitations can be noticed :
* Memory consumption : Some applications such as SARFineDeformationGrid or SARContenateBursts, may have a high memory consumption. It depends on input data volumetry and sensor/mode. For exemple, the S1 sensor for StripMap mode requires a high level of RAM (between 16GB and 32GB). It is necessary to adapt your processing with your material resources. You can, for instance, extract ROIs inside your inputs before launching DiapOTB chains.
* Instabilities dues to input GCPs for S1 sensor : GCPs are part of metadata for S1 sensor. You can find its with xml files contained into annotation directory of your input .SAFE. It was notified some incoherencies with GCPs following the IPF (Instrument Processing Facility) version.
