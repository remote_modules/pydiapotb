# Python Design

The purpose of this page is to describe the internal implementation of the python chains and give some information about architecture decisions which have been made.

DiapOTB provides four kinds of workflows following sensors and single or multitemporal interferometry chain. Each workflow is a dedicated python program : diapOTB.py, diapOTB_S1IW.py, SAR_MultiSlc.py and SAR_MultiSlc_IW.py.

Programs have a sole configuration file (json format) as input. This file contains everything our chains needed to be executed. The `ConfigFile` class handles APIs to get all information and to provide inputs for processing though an adapter. Also, programs are split in subchains which represent a specific processing on a single image or a couple of images.

The organization of the programs and classes is illustrated below in the class diagram :
![image](../Art/PythonAPI/uml/Python_API_main_chain.png)

All subchains are contained in `diapotb.lib` package. Each subchains shares `DiapOTBProcessing` as base class and can be instanced by the same factory.

![image](../Art/PythonAPI/uml/Python_API_chain.png)

The generic class `DiapOTBProcessing` owns a `ExecutorBuilder`. This builder creates following the current subchain and the given mode, a dedicated `Executor`. The instances of Executor represent real implementations which executes a flow of DiapOTB/OTB applications though a wrapper. A few internal processing (resampling or filtering for TSX/PAZ/TDX products) can also be called in the main function : `execute`.

![image](../Art/PythonAPI/uml/Python_API_core.png)

A more detailed view of the class implementation is available in the following diagram:

![image](../Art/PythonAPI/uml/Python_API_executors.png)
