# Python API (python_src/)

## Description

The python_src was organized with two packages : `diapotb` and `utils`.

The first one contains the main processing chains such as DInSAR, Ground or Pre_Processing and everything requires to run these chains.

`utils` package gathers executable scripts.


Our four main scripts (SAR_MultiSlc.py, SAR_MultiSlc_IW.py, diapOTB.py and diapOTB_S1IW.py) use `diapotb.lib` package to launch subchains. All applications are executed though `OTBApplicationWrapper` contained in `diapotb.lib.core.ApplicationWrapper` module. The main connexions can be displayed as follow:

![image](../Art/PythonAPI/python_src.png)

More details about architecture can be found [here](./pythonDesign.md)

## Packages


* [diapotb](diapotb)
  * [diapotb.lib](diapotb.lib)
  * [diapotb.lib.core](diapotb.lib.core)
  * [diapotb.lib.internal_processing](diapotb.lib.internal_processing )
  * [diapotb.lib.internal_processing.core](diapotb.lib.internal_processing.core)

* [utils package](utils)
