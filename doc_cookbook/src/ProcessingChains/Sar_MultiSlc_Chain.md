<div align="justify">

# SAR_MultiSlc.py

## Description

This workflow works on S1 StripMap, TSX/PAZ/TDX and Cosmo data. It spreads the main processing with multitemporal SAR images.

An input path indicates where to find all SAR images and a selection can be made with dates. One reference image is fixed by users and other images are processed as secondary image. This chain deals with DEM differently by creating automatically the corresponding DEM from a folder filled with SRTM files and a shapefile that contains the SRTM tile grid with the tiles location (lat,lon). The SRTM files must be hgt files and shapefiles are provided with DiapOTB. The SRTM tile grid shapefile is useful to select automatically the SRTM tiles needed in order to compute the interferograms on the area corresponding to the SAR images. Only SRTM files can be used here with the given shapefile.

![image](../Art/chains_description/diapOTB_SARMultiSlc.png)


For each couple (reference/secondary), the processing is divided into four parts descrided below :

* Pre-Processing Chain


* Ground Chain


* DIn-SAR Chain


* Post-Processing Chain : Orthorectification for final interferogram, filtering, multilook processing ...

## Launch this chain

This Python script takes a configuration file (.json) .

The configuration file has always the same organization :

![image](../Art/conf_chains/MultiSlc_Chain.png)

Five sections compose the configuration file :


* Global :


    * **SRTM_Shapefile :** Path to the SRTM shapefile


    * **SRTM_Path :** Path to the SRTM folder


    * **Geoid :** Path to Geoid file


    * **Master_Image :** Filename of the master image. For TSX/PAZ/TDX products, this field requires a relative path to reference image from **Input_path** as root.


    * **Start_Date :** Start date, format('20151222')


    * **End_Date :** End date, format('20171119')


    * **Exclude :** Time to exclude ('-9999' by default, no exlusion)


    * **Input_Path :** Path to input folder


    * **Output_Path :** Path to output folder


    * **clean :** If activated, returns a light version: Interferogram.tif + Coregistrated.tif + Concatenated (deramped, and multilooked) bursts.tif (activated by default)


    * **optram :** Available RAM (mb), by default the value is 4000


* Pre_Processing :


    * **doppler_file :** Output file to store Doppler0 result


    * **ML_gain :** Gain applied on MultiLooked images


    * **ML_ran :** MultLook factor on range dimension


    * **ML_azi :** MultiLook factor on azimut dimension


* Metadata_Correction :


    * **fine_metadata_file :** Output file to corrected metadata


    * **activate :** Boolean to activate this chain (By default false)


    * **GridStep_range :** Step for the correlation grid on range dimension


    * **GridStep_azimut :** Step for the correlation grid on azimut dimension


* DIn_SAR :


    * **GridStep_range :** Step for the deformation grid on range dimension


    * **GridStep_azimut :** Step for the deformation grid on azimut dimensio


    * **GridStep_Threshold :** Threshold for the correlation grid (applied on correlation rate)


    * **Grid_Gap :** Maximum gap between DEM grid values and the mean value. If the difference betwwen a value of the DEM grid ans the mena is superior to this gap then the value is set to the mean. (Avoid incoherent shift)


    * **Interferogram_gain  :** Gain applied on amplitude band of output inteferegram.


	* **Interferogram_mlran  :** Ml factor on range for output inteferegram (only for interferogram).


	* **Interferogram_mlazi  :** Ml factor on azimut for output inteferegram (only for interferogram).


    * **Activate_Interferogram :** Activate or deactivate interferogram output (Interferogram.tif). Activated by default.



    * **roi :** Define the lat lng ROI coordinates. Format : 'ulx uly lrx lry'


* Post_Processing :

    * **Activate_Ortho :** Activate or deactivate Orthorectified interferogram output (Orth_Interferogram.tif)


    * **Spacingxy :** Set the spatial resolution for OrthRectification in degrees. Default value is 0.0001


	* **Activate_Filtering :** Activate or deactivate GoldStein filtering for interferogram output (filtered_interferogram.tif)


    * **Filtered_Interferogram_mlran :** Ml factor on range for filtered inteferegram


	* **Filtered_Interferogram_mlazi :** Ml factor on azimut for filtered inteferegram


	* **Filtered_parameter_alpha :** alpha parameter for Goldstein filtering



The processing chain needs metadata to launch the applications. Thus, input images must be into the native directory for each kind of products (i.e SAFE directory for Sentinel-1 products). For your information, `Metadata_Correction` chain is never used (activate always sets to false) and will be removed, soon.
