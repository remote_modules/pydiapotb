<div align="justify">

# Use DiapOTB Chains

To launch processings chains, only one argument is required as inputs : a configuration file (json format).


A interactive script was developped into *utils* package to generate a configuration file following a user's Q&A.


Thus to run a chain, you can :
* Generate a configuration file. A python script helps you to create a new json file by loading examples (into <install_directory>/python_src/ex_config) and asking for general fields such as input_path or reference image ... 

*python <install_directory>/python_src/utils/generateConfigFile.py*

* Modify (or keep) the values into your json file (see the description of the selected processing chain)

* Execute the selected chain : 

*python <install_directory>/python_src/<selected_chain>.py <your_json_file>*


`
NB : Others scripts are availble into <install_directory>/python_src/utils. You can use for example, the getEOFFromESA script to retrieve EOF files with right proxy settings.
`
