<div align="justify">

# SAR_MultiSlc_IW.py

## Description

This workflow works on S1 IW data. It spreads the main processing with multitemporal SAR images.

An input path indicates where to find all SAR images and a selection can be made with dates. One reference image is fixed by users and other images are processed as secondary image. This chain deals with DEM differently by creating the corresponding DEM from a folder filled with SRTM files and a shapefile. The SRTM files must be hgt files and shapefiles are provided with DiapOTB. The SRTM tile grid shapefile is useful to select automatically the SRTM tiles needed in order to compute the interferograms on the area corresponding to the SAR images. Only SRTM files can be used here with the given shapefile.

![image](../Art/chains_description/diapOTB_SARMultiSlc.png)


For each couple (reference/secondary), the processing is divided into four parts descrided below :

* Pre-Processing Chain


* Ground Chain


* DIn-SAR Chain


* Post-Processing Chain : Burst concatenation, Orthorectification for final interferogram, filtering, multilook processing ...

## Launch this chain

This Python script spreads the general processing chain (SAR_MultiSlc.py).

The input configuration file (.json) has the following organization :

![image](../Art/conf_chains/MultiSlc_IW_Chain.png)

Five sections compose the configuration file :


* Global :


    * **SRTM_Shapefile :** Path to the SRTM shapefile


    * **SRTM_Path :** Path to the SRTM folder


    * **Geoid :** Path to Geoid file


    * **Master_Image :** Filename of the master image


    * **Start_Date :** Start date, format('20151222')


    * **End_Date :** End date, format('20171119')


    * **Exclude :** Time to exclude ('-9999' by default, no exclusion)


    * **Input_Path :** Path to input folder


    * **Output_Path :** Path to output folder


    * **clean :** If activated, returns a light version: Interferogram.tif + Coregistrated.tif + multilook.tif (activated by default)


    * **burst_index :** Burst index, format('0-4') ('0-8' by default)


    * **optram :** Available RAM (mb), by default the value is 4000


* Pre_Processing :


    * **doppler_file :** Output file to store Doppler0 result


    * **ML_range :** MultLook factor on range dimension


    * **ML_azimut :** MultLook factor on azimut dimension


    * **ML_gain :** Gain applied on MultiLooked images


* Ground :


* DIn_SAR :


    * **GridStep_range :** Step for the deformation grid on range dimension


    * **GridStep_azimut :** Step for the deformation grid on azimut dimensio


    * **GridStep_Threshold :** Threshold for the correlation grid (applied on correlation rate)


    * **Grid_Gap :** Maximum gap between DEM grid values and the mean value. If the difference betwwen a value of the DEM grid ans the mena is superior to this gap then the value is set to the mean. (Avoid incoherent shift)


    * **Interferogram_gain  :** Gain applied on amplitude band of output inteferegram.


    * **Activate_Interferogram :** Activate or deactivate interferogram output (Interferogram.rif). Activated by default.


    * **roi :** Define the lat lng ROI coordinates. Format : 'ulx uly lrx lry'


    * **ESD_iter :** Number of iterations for ESD loop. An automatic mode is possible by passing "auto" as value for this parameter.



* Post_Processing :

    * **Activate_Ortho :** Activate or deactivate Orthorectified interferogram output (Orth_Interferogram.tif)


    * **Spacingxy :** Set the spatial resolution for OrthRectification in degrees. Default value is 0.0001


	* **Activate_Filtering :** Activate or deactivate GoldStein filtering for interferogram output (filtered_interferogram.tif)


    * **Filtered_Interferogram_mlran :** Ml factor on range for filtered inteferegram


	* **Filtered_Interferogram_mlazi :** Ml factor on azimut for filtered inteferegram


	* **Filtered_parameter_alpha :** alpha parameter for Goldstein filtering



The processing chain needs metadata to launch the applications. Thus, input images must be into the native directory for each kind of products (i.e SAFE directory for Sentinel-1 products).
