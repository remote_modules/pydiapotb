<div align="justify">

# diapOTB.py

## Description

This workflow works on S1 StripMap TDX/PAZ/TDX and Cosmo data. It represents the main processing for this data and needs as inputs  : two SLC images and an only DEM file. The inputs should be filled into the configuration file descripted in the next chapter.

This chain is divided into four parts :


* Pre-Processing Chain


* Metadata Correction Chain


* DIn-SAR Chain


* Post-Processing Chain

## Launch this chain

This Python script takes a configuration file (.json) as input.
The configuration file has always the same organization :



![image](../Art/conf_chains/diapOTB_1_chain.png)

Five sections compose the configuration file :


* Global :


    * **Master_Image_Path :** Path to the Master SAR Image.


    * **Slave_Image_Path :** Path to the Master SAR Image


    * **DEM_Path :** Path to the DEM


    * **output_dir :** Output directory (all images or files created by DiapOTB will be stored inside this directory)


* Pre_Processing :


    * **doppler_file :** Output file to store Doppler0 result


    * **ML_range :** MultiLook factor on range dimension


    * **ML_azimut :** MultiLook factor on azimut dimension


    * **ML_gain :** Gain applied on MultiLooked images


* Metadata_Correction :


    * **fine_metadata_file :** Output file to corrected metadata


    * **activate :** Boolean to activate this chain (By default false)


    * **GridStep_range :** Step for the correlation grid on range dimension


    * **GridStep_azimut :** Step for the correlation grid on azimut dimension


* DIn_SAR :


    * **GridStep_range :** Step for the deformation grid on range dimension


    * **GridStep_azimut :** Step for the deformation grid on azimut dimensio


    * **GridStep_Threshold :** Threshold for the correlation grid (applied on correlation rate)


    * **Grid_Gap :** Maximum gap between DEM grid values and the mean value. If the difference betwwen a value of the DEM grid ans the mena is superior to this gap then the value is set to the mean. (Avoid incoherent shift)


    * **Interferogram_gain  :** Gain applied on amplitude band of output inteferegram.


	* **Interferogram_mlran  :** ML factor on range for output inteferegram (only for interferogram).


	* **Interferogram_mlazi  :** ML factor on azimut for output inteferegram (only for interferogram).


* Post_Processing :

    * **Activate_Ortho :** Activate or deactivate Orthorectified interferogram output (Orth_Interferogram.tif)


    * **Spacingxy :** Set the spatial resolution for OrthRectification in degrees. Default value is 0.0001


	* **Activate_Filtering :** Activate or deactivate GoldStein filtering for interferogram output (filtered_interferogram.tif)


    * **Filtered_Interferogram_mlran :** ML factor on range for filtered inteferegram


	* **Filtered_Interferogram_mlazi :** ML factor on azimut for filtered inteferegram


	* **Filtered_parameter_alpha :** alpha parameter for Goldstein filtering



The processing chain needs metadata to launch the applications. Thus the path to the input images must be into the native directory for each kind of products (i.e SAFE directory for Sentinel-1 products). The DEM has to be a unique file but can be built with several tiles by using some gdal commands, for example.
For your information, `Metadata_Correction` chain is never used (activate always sets to false) and will be removed, soon.
