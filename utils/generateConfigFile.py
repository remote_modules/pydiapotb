#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    generateConfigFile.py
    =====================

    Python script to generate configuration file (json format) with default parameters and user's paths

"""

import os
import sys
import re
import json
import readline, glob

import diapotb.lib.core.Utils as utils
from diapotb.lib.core.DiapOTBEnums import Sensor, ScriptNames
from diapotb.ConfigFile import ConfigFile, ConfigInputPattern


# Util fct for autocompletion
def complete(text, state):
    """
    Function for autocompletion
    """
    return (glob.glob(text + "*") + [None])[state]


# Print with colors
def prRed(skk):
    print("\033[91m {}\033[00m".format(skk))


def prGreen(skk):
    print("\033[92m {}\033[00m".format(skk))


def prYellow(skk):
    print("\033[93m {}\033[00m".format(skk))


def prLightPurple(skk):
    print("\033[94m {}\033[00m".format(skk))


def prPurple(skk):
    print("\033[95m {}\033[00m".format(skk))


def prCyan(skk):
    print("\033[96m {}\033[00m".format(skk))


def prLightGray(skk):
    print("\033[97m {}\033[00m".format(skk))


def prBlack(skk):
    print("\033[98m {}\033[00m".format(skk))


def check_file_dir(in_path, is_file=True):
    """Use utils fct to check if a file or dir exists"""
    if is_file:
        if not utils.check_if_exist(in_path):
            prRed(in_path + " does not exist")
            quit()
    else:
        if not utils.check_if_dir(in_path):
            prRed(in_path + " does not exist")
            quit()


# Input with colors
def genericInput(skk, color):
    """
    Override input function for colors and exceptions
    """
    try:
        return input(color.format(skk))
    except KeyboardInterrupt:
        prRed("Generation Aborted, Ctrl-C detected")
        sys.exit(1)
    except Exception as exp:
        prRed("Generation Aborted with the following error : {err}".format(err=exp))
        sys.exit(1)


def inRed(skk):
    return genericInput(skk, "\033[91m {}\033[00m")


def inGreen(skk):
    return genericInput(skk, "\033[92m {}\033[00m")


def inYellow(skk):
    return genericInput(skk, "\033[93m {}\033[00m")


def inLightPurple(skk):
    return genericInput(skk, "\033[94m {}\033[00m")


def inPurple(skk):
    return genericInput(skk, "\033[95m {}\033[00m")


def inCyan(skk):
    return genericInput(skk, "\033[96m {}\033[00m")


def inLightGray(skk):
    return genericInput(skk, "\033[97m {}\033[00m")


def inBlack(skk):
    return genericInput(skk, "\033[98m {}\033[00m")


# Questions to user for SAR_MultiSlc* chains
def askForMultiSlc(config_handler):
    """
    Q&A for SAR_MultiSlc* chains
    Modify the config_handler following user's awnsers
    """

    # Select sensor if SAR_MultiSlc
    sensor = str(Sensor.S1IW)
    if response == "SAR_MultiSlc":
        sensor = inLightPurple(
            "Please, select the wanted sensor "
            + str(Sensor.S1SM)
            + " (for Sentinel-1 StripMap mode) or "
            + str(Sensor.CSK)
            + " (for Cosmo-Skymed Spotligth and StriMap mode) "
            + str(Sensor.TSX)
            + " (for TSX/PAZ/TDX products) : "
        )

        if sensor not in [str(Sensor.S1SM), str(Sensor.CSK), str(Sensor.TSX)]:
            prRed("Unknown sensor")
            quit()

    # SRTM_Shapefile
    srtm_shapefile = os.path.realpath(
        inPurple("Please, enter your path to srtm shp : ")
    )
    check_file_dir(srtm_shapefile)

    # SRTM_Path
    srtm_path = os.path.realpath(
        inPurple("Please, enter your path to srtm hgt files : ")
    )
    check_file_dir(srtm_path)

    # Input/Output Paths
    input_path = os.path.realpath(
        inPurple("Please, enter your path to input images : ")
    )
    check_file_dir(input_path, is_file=False)

    output_path = os.path.realpath(
        inPurple("Where would you like to store the output results : ")
    )
    check_file_dir(output_path, is_file=False)

    # reference image (must be into Input_Path)
    reference_image = inPurple("Which image is your reference : ")
    check_file_dir(reference_image)
    if sensor != str(Sensor.TSX):
        reference_image = os.path.basename(reference_image)
    else:
        # TSX mode : relative path to image (from input_dir)
        common_path = os.path.commonpath(
            [os.path.abspath(input_path), os.path.abspath(reference_image)]
        )
        reference_image = os.path.relpath(reference_image, common_path)

    if not utils.get_img_from_dir(reference_image, input_path):
        prRed(reference_image + " not found into given input path " + input_path)
        prRed("Please check your input path")
        quit()
    else:
        correct = utils.check_image_pattern(reference_image, mode=sensor)
        if not correct:
            prRed(
                "Reference image "
                + reference_image
                + " does not respect naming conventions for the "
                "selected sensor"
            )
            quit()

    # Geoid file
    res_geoid = inPurple("Would you like to add a geoid file (yes/no) : ")
    geoid = None

    if res_geoid == "yes":
        geoid = os.path.realpath(
            inLightPurple("Please, enter your path to your geoid file : ")
        )
        check_file_dir(geoid)
    else:
        geoid = os.getenv("OTB_GEOID_FILE")
        if not geoid:
            prRed("Undefined geoid (empty OTB_GEOID_FILE environnement variable)")
            inPurple(
                "Please indicate a geoid file by setting path or with OTB_GEOID_FILE environnement variable"
            )
            quit()

    for retry in range(1, 6):
        # Start/End date for image selection (5 retries for this selection)
        res_date = inLightPurple(
            "Would you like to specify a start and end date for image selection (yes/no) : "
        )

        # Dummy dates to select by default all images into Input_Path
        start_date = "19000101"
        end_date = "29000101"
        pattern = "".join(["\d{8}"])
        if res_date == "yes":
            start_date = inPurple(
                "Please, indicate a start date with YYYYMMDD format : "
            )
            if not re.match(pattern, start_date):
                prRed(
                    "start_date "
                    + start_date
                    + " does not respect the expected format YYYYMMDD"
                )
                quit()

            end_date = inPurple("Please, indicate a end date with YYYYMMDD format : ")
            if not re.match(pattern, end_date):
                prRed(
                    "end_date "
                    + end_date
                    + " does not respect the expected format YYYYMMDD"
                )
                quit()

        # Indicate to user, all selected images with given dates, polarisation and input_path
        ext = "tiff"
        pol = ""
        iw = ""

        if sensor == str(Sensor.CSK):
            ext = "h5"
            pol = reference_image.split("_")[5]
        elif sensor == str(Sensor.TSX):
            ext = "cos"
            pol = os.path.basename(reference_image).split("_")[1]
        else:
            pol = reference_image.split("-")[3]

        if sensor == str(Sensor.S1IW):
            iw = reference_image.split("-")[1]
            ext = ""

        exclude = "-9999"
        tiff_list, throw_warning = utils.get_all_tiff(
            pol=pol, ext=ext, iw=iw, search_dir=input_path
        )
        tiff_dates = utils.get_tiff_with_dates(
            int(start_date), int(end_date), exclude, tiff_list, ext
        )

        # Avoid duplicates
        tiff_dates = utils.avoid_duplicates(tiff_dates)
        ref_abs_path = os.path.abspath(reference_image)
        if sensor == str(Sensor.TSX):
            ref_abs_path = os.path.join(os.path.abspath(input_path), reference_image)

        if os.path.abspath(ref_abs_path) in tiff_dates:
            tiff_dates.remove(os.path.abspath(ref_abs_path))

        if reference_image in tiff_dates:
            tiff_dates.remove(reference_image)

        prYellow("For your information, the selected images for processings will be : ")
        prYellow("As reference : " + reference_image)
        prYellow("As secondaries : " + str(tiff_dates))

        # Ask to continue if selection OK
        res_continue = inLightPurple(
            "Do you agree to continue with this selection (yes/no/exit) : "
        )

        if res_continue == "exit":
            prRed("You choose to exit, you can relaunch this script with new inputs")
            quit()

        if res_continue != "yes":
            if retry < 5:
                prRed(
                    "Previous selection does not fullfill your expectations, please select with different dates"
                )
            else:
                prRed(
                    "Previous selection does not fullfill your expectations with too many retries.\n You can relaunch this script with new inputs"
                )
                quit()
        else:
            break

    # EOF file
    eof_path = None
    if sensor not in [str(Sensor.CSK), str(Sensor.TSX)]:
        res_eof = inLightPurple("Would you like to indicate fine orbits (yes/no) : ")

        if res_eof == "yes":
            eof_path = os.path.realpath(
                inPurple("Please, enter your path to .EOF files : ")
            )
            check_file_dir(eof_path, is_file=False)

    # Fill with user's response our generic fields for SAR_MultiSlc* chains
    dict_user_qa = {
        str(ConfigInputPattern.SRTM_SHAPEFILE): srtm_shapefile,
        str(ConfigInputPattern.SRTM_PATH): srtm_path,
        str(ConfigInputPattern.INPUT_PATH): input_path,
        str(ConfigInputPattern.MASTER_IMAGE): reference_image,
        str(ConfigInputPattern.START_DATE): start_date,
        str(ConfigInputPattern.END_DATE): end_date,
    }

    if geoid:
        dict_user_qa[str(ConfigInputPattern.GEOID)] = geoid

    if eof_path:
        dict_user_qa[str(ConfigInputPattern.EOF)] = eof_path

    dict_user_qa[str(ConfigInputPattern.OUTPUT_PATH)] = output_path

    config_handler.override_data_config(**dict_user_qa)


# Questions to user for diapOTB* chains
def askForDiapOTB(config_handler):
    """
    Q&A for diapOTB* chains
    Modify the confog_handler following user's awnsers
    """

    # Select sensor if diapOTB
    sensor = str(Sensor.S1IW)
    if response == "diapOTB":
        sensor = inLightPurple(
            "Please, select the wanted sensor "
            + str(Sensor.S1SM)
            + " (for Sentinel-1 StripMap mode) or "
            + str(Sensor.CSK)
            + " (for Cosmo-Skymed Spotligth and StriMap mode) or "
            + str(Sensor.TSX)
            + " (for TSX/PAZ/TDX products) : "
        )

        if sensor not in [str(Sensor.S1SM), str(Sensor.CSK), str(Sensor.TSX)]:
            prRed("Unknown sensor")
            quit()

    # reference image (path to image)
    reference_image = os.path.realpath(inPurple("Which image is your reference : "))
    check_file_dir(reference_image)
    reference_image_base = os.path.basename(reference_image)

    correct = utils.check_image_pattern(reference_image_base, mode=sensor)
    if not correct:
        prRed(
            "Reference image "
            + reference_image_base
            + " does not respect naming conventions for the "
            "selected sensor"
        )
        quit()

    # reference image (path to image)
    secondary_image = os.path.realpath(inPurple("Which image is secondary : "))
    check_file_dir(secondary_image)
    secondary_image_base = os.path.basename(secondary_image)

    correct = utils.check_image_pattern(secondary_image_base, mode=sensor)
    if not correct:
        prRed(
            "Reference image "
            + secondary_image_base
            + " does not respect naming conventions for the "
            "selected sensor"
        )
        quit()

    # DEM Path
    dem_path = os.path.realpath(inPurple("Please, enter your path to your DEM : "))
    check_file_dir(dem_path)

    # Output Path
    output_path = os.path.realpath(
        inPurple("Where would you like to store the output results : ")
    )
    check_file_dir(os.path.dirname(output_path), is_file=False)

    # EOF file
    eof_path = None
    if sensor not in [str(Sensor.CSK), str(Sensor.TSX)]:
        res_eof = inLightPurple("Would you like to indicate fine orbits (yes/no) : ")

        if res_eof == "yes":
            eof_path = os.path.realpath(
                inPurple("Please, enter your path to .EOF files : ")
            )
            check_file_dir(eof_path, is_file=False)

    # Fill with user's response our generic fields for diapOTB* chains
    dict_user_qa = {
        str(ConfigInputPattern.MASTER): reference_image,
        str(ConfigInputPattern.SLAVE): secondary_image,
        str(ConfigInputPattern.DEM): dem_path,
        str(ConfigInputPattern.OUTPUTDIR): output_path,
    }

    if eof_path:
        dict_user_qa[str(ConfigInputPattern.EOF)] = eof_path

    config_handler.override_data_config(**dict_user_qa)


###################
###### Main #######
###################
if __name__ == "__main__":

    ######### Introduction prints #########
    prCyan("Welcome to DiapOTB remote module !")
    prCyan(
        "You can generate configuration files for the four available processing chains : diapOTB,"
        "diapOTB_S1IW, SAR_MultiSlc and SAR_MultiSlc_IW"
    )

    ######### Load the example for prepare the configuration file according to user's choice #########
    # First choice for user : the selected chain
    response = inLightGray(
        "Please, choose your processing chain (diapOTB, diapOTB_S1IW, SAR_MultiSlc and "
        "SAR_MultiSlc_IW) : "
    )

    if response not in ["diapOTB", "diapOTB_S1IW", "SAR_MultiSlc", "SAR_MultiSlc_IW"]:
        prRed("Wrong chain, please choose between available chains")
        quit()

    # Load examples according to the selected chain => Init a dictionnary with default parameters
    # Check and load configuration file
    if response == "diapOTB":
        config_handler = ConfigFile.create_configfile_from_ex(
            str(ScriptNames.SIMPLE_S1SM)
        )
        config_handler.load_configfile()

    elif response == "diapOTB_S1IW":
        config_handler = ConfigFile.create_configfile_from_ex(
            str(ScriptNames.SIMPLE_S1IW)
        )
        config_handler.load_configfile()

    elif response == "SAR_MultiSlc":
        config_handler = ConfigFile.create_configfile_from_ex(
            str(ScriptNames.MULTI_SLC_S1SM)
        )
        config_handler.load_configfile()

    elif response == "SAR_MultiSlc_IW":
        config_handler = ConfigFile.create_configfile_from_ex(
            str(ScriptNames.MULTI_SLC_S1IW)
        )
        config_handler.load_configfile()

    ########## Prepare for questions : with autocompletion #########
    readline.set_completer_delims(" \t\n;")
    readline.parse_and_bind("tab: complete")
    readline.set_completer(complete)

    ########## Ask to user for generic fields #########
    # SAR_MultiSlc* chains
    if response == "SAR_MultiSlc" or response == "SAR_MultiSlc_IW":
        try:
            askForMultiSlc(config_handler)
        except Exception as exp:
            prRed("Generation Aborted with the following error : {err}".format(err=exp))
            sys.exit(1)

    if response == "diapOTB" or response == "diapOTB_S1IW":
        try:
            askForDiapOTB(config_handler)
        except Exception as exp:
            prRed("Generation Aborted with the following error : {err}".format(err=exp))
            sys.exit(1)

    # Dump data_config with the new fields and the default parameters
    data_config = config_handler.get_data_config()

    res_json = os.path.realpath(
        inLightGray("Where do you want store your configuration file : ")
    )

    # if directory
    if os.path.isdir(res_json):
        res_json_name = inLightGray(
            "Please, enter a name for your configuration "
            "file (with .json extension) : "
        )

        if os.path.exists(os.path.join(res_json, res_json_name)):
            res_json_overwrite = inLightGray(
                "Would you like to overwrite the file " + res_json_name + " (yes/no) : "
            )

            if res_json_overwrite == "yes":
                with open(os.path.join(res_json, res_json_name), "w") as f:
                    json.dump(data_config, f, indent=2, sort_keys=False)
            else:
                prRed("Generation Aborted")
                quit()

        else:
            with open(os.path.join(res_json, res_json_name), "w") as f:
                json.dump(data_config, f, indent=2, sort_keys=False)

    # If file (or wrong path)
    else:
        if os.path.isdir(os.path.dirname(res_json)):

            if os.path.exists(res_json):
                res_json_overwrite = inLightGray(
                    "Would you like to overwrite the file " + res_json + " (yes/no) : "
                )

                if res_json_overwrite == "yes":
                    with open(res_json, "w") as f:
                        json.dump(data_config, f, indent=2, sort_keys=False)
                else:
                    prRed("Generation Aborted")
                    quit()
            else:
                with open(res_json, "w") as f:
                    json.dump(data_config, f, indent=2, sort_keys=False)

        else:
            prRed("Wrong path for the configuration file, Generation Aborted")

    ######### Conclusion prints #########
    prGreen("The configuration file was generated !")
    prCyan(
        "You can modify the parameters in it and launch the processing chain with this file as only argument"
    )
    prCyan(
        "You can find further information on https://gitlab.orfeo-toolbox.org/remote_modules/diapotb/-/wikis/"
    )
