#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    getEOFFromESA.py
    ================

    Python script to retrieve from ESA website, the EOF files

"""

import re
import os
import requests
import shutil
import argparse
import datetime
import xml.etree.ElementTree as ET


def searchForSafe(input_dir):
    """
    Search for Safe repository into input_dir

    :input_dir: (str) input directory
    :returns: (iterable) list of SAFE repository
    """
    list_SAFE = []
    for root, dirs, files in os.walk(input_dir):
        for i in (i for i in dirs):
            if i.endswith(".SAFE"):
                list_SAFE.append(i)

    return list_SAFE


def getPeriod(date, delta):
    """
    Construct the period from the date input

    :date: (str) date from the .SAFE file
    :delta: (str) number of days to get a period such as [date-delta TO date+delta]
    :returns: (str) period
    """
    date_tmp = date.split("T")[0]
    date_tmp = datetime.datetime.strptime(date_tmp, "%Y%m%d")
    # We know that the product covers a period of 3 days approximately. So with delta=2 days, we have a period big enough to have a result.
    date_str_start = date_tmp + datetime.timedelta(days=-delta)
    date_str_end = date_tmp + datetime.timedelta(days=delta)
    date_str_start = date_str_start.strftime("%Y-%m-%d")
    date_str_end = date_str_end.strftime("%Y-%m-%d")
    period = (
        "[" + date_str_start + "T00:00:00.000Z TO " + date_str_end + "T23:59:59.999Z]"
    )
    print(period)
    return period


def getRequest(beginPosition, endPosition, platform_name, filename, product_type):
    """
    Construct the request to get the product corresponding to the .SAFE file

    :beginPosition: (str) Date interval for the beginPosition
    :endPosition: (str) Date interval for the endPosition
    :platform_name: (str) Sentinel-1
    :filename: (str) S1A_* or S1B_*
    :product_type: (str) AUX_POEORB or AUX_RESORB
    :returns: (str) request
    """

    request = (
        "( beginPosition:"
        + beginPosition
        + " AND endPosition:"
        + endPosition
        + " ) AND ( (platformname:"
        + platform_name
        + " AND filename:"
        + filename
        + " AND producttype:"
        + product_type
        + "))"
    )

    return request


# Get contents of orbit files on disk
def download_EOF_File(list_product, output_dir, login, pswd):
    """
    Download in the output directory every .EOF file declared in the list_product

    :list_product: (list) list of dictonary {url_product, EOF_filename} where we have the url link to download the product and its filename
    :output_dir: (str) output directory (on disk) to put .EOF files
    :returns: ()
    """
    for product in list_product:
        # We try to download the product only if it is not already in the output directory
        path_output_file = os.path.join(output_dir, product["filename"])
        if not os.path.exists(path_output_file):
            try:
                # Get the content of eof_file
                response = requests.get(
                    product["url_product"],
                    auth=(login, pswd),
                    verify=False,
                    stream=True,
                )

                if response.status_code != 200 and response.status_code != 201:
                    print(response.status_code)
                    print("problem with the url {} ".format(product["url_product"]))
                    continue

                # Copy the content on disk
                with open(path_output_file, "wb") as out_file:
                    shutil.copyfileobj(response.raw, out_file)
                del response
            except Exception as e:
                print("exception for url {} ".format(e))


###################
###### Main #######
###################
if __name__ == "__main__":

    ###### Get the main argument : indir and outdir ######
    # Check arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("indir", help="input directory to search SAFE repository")
    parser.add_argument("outdir", help="output directory to store EOF files")
    parser.add_argument(
        "--type",
        "-t",
        type=str,
        choices=["POEORB", "RESORB"],
        default="POEORB",
        help=(
            "The type of data request. If set should be either POEORB or RESORB; "
            "if not set test first POEORB and backoff to RESORB else"
        ),
    )
    parser.add_argument(
        "--login", "-login", type=str, help="Login of the account to access Copernicus"
    )
    parser.add_argument(
        "--pswd", "-pswd", type=str, help="Password of the account to access Copernicus"
    )
    args = parser.parse_args()

    # Check input and output directory (if exist)
    if not os.path.exists(args.indir):
        print(args.indir + " does not exists")
        quit()

    if not os.path.exists(args.outdir):
        print(args.outdir + " does not exists")
        quit()

    # Require a check of user for connexion parameters (proxy, ...)
    response = input(
        "Check all your parameters for internet connexion (proxy, ssl ...), before continuing. \
    \nReady to continue (yes/no) :"
    )

    if response != "yes":
        print("Set all your parameters for internet connexion to relaunch this script")
        quit()

    list_InSAFE = searchForSafe(args.indir)

    if len(list_InSAFE) == 0:
        print("None SAFE repository was found into {}".format(args.indir))
        quit()

    # URL Link to the new platform
    url = "https://scihub.copernicus.eu/gnss/search"
    # By default, the product type is POEORB
    product_type = "AUX_POEORB"
    if args.type == "RESORB":
        product_type = "AUX_RESORB"
    platform_name = "Sentinel-1"
    # this list will save each url link to download the product and its filename for every file .SAFE from the input directory
    list_product = []
    for safe_file in list_InSAFE:
        print("Safe File : " + safe_file)
        # patterns for SAFE names
        pattern_date = r"\d{8}T\d{6}"
        pattern_sensor = "".join(["S1._"])

        try:
            dates = re.findall(pattern_date, safe_file)
            sensor = re.findall(pattern_sensor, safe_file)
        except Exception as e:
            print(
                "Safe name does not match with usual pattern and causes an exception : \
            {}".format(
                    e
                )
            )
            # Next safe
            continue
        print("Dates")
        print(dates)
        if len(dates) == 2 and len(sensor) == 1:
            filename = sensor[0] + "*"
            beginPosition = getPeriod(dates[0], 2)
            endPosition = getPeriod(dates[-1], 2)

            request_copernicus = getRequest(
                beginPosition, endPosition, platform_name, filename, product_type
            )
            payload = {"q": request_copernicus}
            r = requests.get(
                url, verify=False, auth=(args.login, args.pswd), params=payload
            )

            root = ET.fromstring(r.text)
            namespace = {
                "ns0": "http://www.w3.org/2005/Atom",
                "ns1": "http://a9.com/-/spec/opensearch/1.1/",
            }

            for child in root.findall("ns0:entry", namespace):
                # url link to download the product
                url_product = child.find("ns0:link", namespace).get("href")
                # the filename of the .EOF file
                filename = child.find("ns0:str", namespace).text
                date_str_filename_start = filename.split("_")[-2].lstrip("V")
                date_str_filename_end = filename.split("_")[-1].rstrip(".EOF")
                # We may have multiple results from the request, so we get the product where the period covered
                # by the .SAFE file is included in the period covered by the product file
                if (
                    date_str_filename_start < dates[0]
                    and date_str_filename_end > dates[1]
                ):
                    list_product.append(
                        {"url_product": url_product, "filename": filename}
                    )
            print(list_product)

    # Download the EOF file inside the output directory
    download_EOF_File(list_product, args.outdir, args.login, args.pswd)
